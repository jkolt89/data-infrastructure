# -*- coding: utf-8 -*-

import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.graph_objs as go
import plotly.express as px
import pymssql
from dash.dependencies import Input, Output

server = r'192.168.14.119'
user = r'spoperator'
    
password = '50lidp0w3r'
db = 'SolidPowerDev'
#cell = "'190604_a', '190819_a', '2018_0016_PC_H1'"
#cell = "'2015_0018_PC_A'"
cell = "'NPL0002001280010_FORM_70C'"


# run the following cmdln: gunicorn CycleBasedStatistics:app.server -b :8060

##spgetCellLog = "exec getCellLog"
##query = "spCycleStatistics " + cell
##query = "select * from channelstatistic where cellname IN (" + cell + " )"

#query = "exec getCellStatisticData_ARBIN "  + cell
#print(query)
#celllog_query = "exec dbo.getSelectFromCell"

conn = pymssql.connect(host=server, user=user, password=password, database=db)

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

#gcsd_df = pd.read_sql(query, conn)
#celllog_df = pd.read_sql(celllog_query, conn)

## df = df.sort_values("Data_Point")

##available_indicators = gcsddf['cycle_index'].unique()
##cell_names_df = celllog_df.sort_values(by=["Start_DateTime"])
##cell_names_df = celllog_df["CellName"].unique()

#fig = px.line(gcsd_df, x="cycle_index", y=["Total Discharge Capacity"]) #, color="City", barmode="group")
##fig = px.line(gcsd_df, x="cycle_index", y=["Efficiency"]) #, color="City", barmode="group")

conn.close()

########## APP LAYOUT ########################################
tab1labelwidth = 5
tab1entrywidth = 11

app.layout = html.Div([
    dbc.Form([
        html.H2('CellName Selector')
        html.Br(),
        html.Br(),
    
        dbc.FormGroup([
            dbc.Label('Enter CellName:', width=tab1labelwidth),
            dbc.Col(
                dbc.Input(id='submit1', type='text', placeholder="CellName"),
            )
        ], row=True),
    ])
])

## CALLBACK #####################################

#app.callback(
#    Output(component_id='my-output', component_property='children'),
#    #dash.dependencies.Input('submit', 'n_clicks'),
#    dash.dependencies.State('submit1', 'value'),
#    Input(component_id='my-input', component_property='value')
#    )

#def update_output_div(input_value):
#    return 'Output: {}'.format(input_value)

## END CALLBACK #################################

if __name__ == '__main__':
    app.run_server(debug=True)
    


