# -*- coding: utf-8 -*-


import dash
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import flask
import sys
import sqlalchemy
import datetime
from sqlalchemy import create_engine
from dash.exceptions import PreventUpdate


sys.path.append('/home/solidpower/pipython/Utilities')
sys.path.append('/home/solidpower/pipython/NodeRed')
sys.path.append('/home/solidpower')
sys.path.append('/home/solidpower/dash_app')

external_stylesheets = [dbc.themes.BOOTSTRAP]
# external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
# external_stylesheets = ['https://codepen.io/chriddyp/pen/dZVMbK.css']
server = flask.Flask(__name__)  # define flask app.server
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# run command: gunicorn 2dbcwebapp1:app.server -b 192.168.14.34:8060

GOOGLE = ''
DBC_GITHUB = "https://github.com/facultyai/dash-bootstrap-components"

navigation_bar = dbc.NavbarSimple(
    children=[
        dbc.NavItem(dbc.NavLink("", href=DBC_GITHUB)),
        dbc.DropdownMenu(
            nav=True,
            in_navbar=True,
            label="Menu",
            children=[
                dbc.DropdownMenuItem("Entry 1", href="https://google.com"),
                dbc.DropdownMenuItem("Entry 2", href="/test"),
                dbc.DropdownMenuItem(divider=True),
                dbc.DropdownMenuItem("A heading", header=True),
                dbc.DropdownMenuItem(
                    "Entry 3", href="/external-relative", external_link=True
                ),
                dbc.DropdownMenuItem("Entry 4 - does nothing"),
            ],
        ),
    ],
    brand="Solid Power Webapp",
    brand_href=GOOGLE,
    sticky="top",
)


# ------ Tab 7 (seven) - Material Drying Data Entry Form ---------------------------------------------------------------
tab7labelwidth = 9
tab7entrywidth = 11
tab7 = dbc.Card([
    dbc.CardBody([
        html.H2('Material Drying Data Entry Form'),
        html.Br(),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('User ID', width=4),
                        dbc.Col(
                            dbc.Input(id='tab7-User', type='text', placeholder="User ID Number"),
                        ),
                    ]), width=3,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Ingredient Lot Number:', width=6),
                        dbc.Col(
                            dbc.Input(id='tab7-ILN', type='text', placeholder="Ingredient Lot Number"),
                        ),
                    ]), width=6,
                ),
            ],
            form=True,
        ),

        dbc.FormGroup([
            dbc.Label('Product Part Number', width=tab7labelwidth),  # @ProductPN
            dbc.Col(
                dbc.Input(id='tab7-PPN', type='text', placeholder="Product Part Number"),
                width=tab7entrywidth,
            ), ], ), #row=True),

        #       dbc.FormGroup([
        #          dbc.Label('Item - not sure what this is', width=tab7labelwidth),  # @ProductPN
        #         dbc.Col(
        #            dbc.Input(id='tab7-Item', type='text', placeholder="Item ?"),
        #           width=tab7entrywidth)],
        #      row=True),

        # TODO: dont need supplier name or lot number
        dbc.FormGroup([
            dbc.Label('Supplier Name', width=tab7labelwidth),  # @ProductPN
            dbc.Col(
                dbc.Input(id='tab7-SupplierName', type='text', placeholder="Supplier Name"),
            )], ), #row=True),

        dbc.FormGroup([
            dbc.Label('Supplier Lot Number', width=tab7labelwidth),  # @ProductPN
            dbc.Col(
                dbc.Input(id='tab7-SupplierLN', type='text', placeholder="Supplier Lot Number"),
            )], ), #row=True),

        dbc.FormGroup([
            dbc.Label('K Solid Moisture (ppm)', width=tab7labelwidth),  # @ProductPN
            dbc.Col(
                dbc.Input(id='tab7-KSolid', type='text', placeholder="K Solid ?"),
            )], ), #row=True),

        html.Br(),

        # Input box for notes will go here someday... # @Notes

        html.Br(),
        dbc.Button('Submit', id='submit-Tab7', n_clicks=0, color='primary'),
        html.Br(),
        html.Br(),
        html.H6(id='tab7_container_button', children='Please only click submit once'),
    ])
])

app.layout = html.Div([
    navigation_bar,
    dbc.Container([
        dbc.Tabs([
            dbc.Tab(label='..... Test1 Data Entry .....', children=[tab7]),
        ])
    ])
])


# -------------------- Tab7 Callback ---------------------- #

@app.callback(
    dash.dependencies.Output('tab7_container_button', 'children'),
    [dash.dependencies.Input('submit-Tab7', 'n_clicks')],
    [dash.dependencies.State('tab7-ILN', 'value'), 
     dash.dependencies.State('tab7-PPN', 'value'),
     dash.dependencies.State('tab7-SupplierName', 'value'),
     dash.dependencies.State('tab7-SupplierLN', 'value'), 
     dash.dependencies.State('tab7-KSolid', 'value'),
     dash.dependencies.State('tab7-User', 'value')])
def update_output(n_clicks, ILN7, PLN7, SupplierName7, SupplierLN7, KSolid7, User7):
    if ILN7 and PLN7 and SupplierName7 and SupplierLN7 and KSolid7 and User7 is not None:

        print(ILN7, PLN7,  SupplierName7, SupplierLN7, KSolid7, User7)

        engine = create_engine("mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
        print('tab 7 callback')
        connection = engine.raw_connection()
        print('connection succeeded')
        try:
            connection = engine.raw_connection()
            print('connection succeeded')
            cursor = connection.cursor()
            print('cursor succeeded')
            querystring = 'exec dbo.insertDrying @UserOpID = \'{}\', @IngredientLN = \'{}\', @ProductPN = \'{}\', ' \
                          '@Supplier = \'{}\', @SupplierLotNr = \'{}\', @KSolidMoisture_ppm = \'{}\''.format(
                            User7, ILN7, PLN7, SupplierName7, SupplierLN7, KSolid7)
            print(querystring)
            cursor.execute(querystring)
            print('querystring is: ')
            print(querystring)
            #ExistingOGLots = cursor.fetchall()  # note: no comma
            print('results are: ')
            #print(type(ExistingOGLots))
            cursor.close()
            connection.commit()
        except Exception as e:
            print('there was an exception that occurred. Drying')
            print(e)
            connection.close()
            return '---error7 : {}'.format(e)
        finally:
            connection.close()

        engine.dispose()

        return u'Drying data successfully recorded for \'{}\''.format(ILN7) #replace if theres anything to be returned
    else:
        raise PreventUpdate
    
if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0', port=8060)
