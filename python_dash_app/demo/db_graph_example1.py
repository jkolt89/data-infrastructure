# -*- coding: utf-8 -*-

import dash
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import flask
import sys
import sqlalchemy
import datetime
from sqlalchemy import create_engine
from dash.exceptions import PreventUpdate
import pymssql
import pandas as pd
import plotly.graph_objs as go
from dash.dependencies import Input, Output

# run command: gunicorn db_graph_exampe1:app.server -b 192.168.14.34:8061
 
sys.path.append('/home/solidpower/pipython/Utilities')
sys.path.append('/home/solidpower/pipython/NodeRed')
sys.path.append('/home/solidpower')
sys.path.append('/home/solidpower/dash_app')

external_stylesheets = [dbc.themes.BOOTSTRAP]
server = flask.Flask(__name__)  # define flask app.server

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

server = r'192.168.14.119'
user = r'spoperator'
    
password = '50lidp0w3r'
db = 'SolidPowerDev'
cellname = "'180907_a'"

conn = pymssql.connect(host=server, user=user, password=password, database=db)

GOOGLE = ''
DBC_GITHUB = "https://github.com/facultyai/dash-bootstrap-components"

query = "exec getCellStatisticData_ARBIN " + cellname

df = pd.read_sql(query, conn)

conn.close()

app.layout = html.Div(children=[
    html.H1(children='Eddie App Request'),
    #style={"fontSize": "48px", "color": "red"},
    html.Div(children='''
        Proof of Concept
    '''),
    dcc.Textarea(
        id='textarea-example',
        value='CellName\nGoes Here',
        style={'width': '100%', 'height': 100},
    ),
    html.Div(id='textarea-example-output', style={'whiteSpace': 'pre-line'}),
    dcc.Graph(
        figure = {
            "data" : [
                {
                    "x" : df["cycle_index"],
                    "y" : df["Efficiency"],
                    "type" : "lines",
                }
            ],
            "layout": {"title": "Efficiency"},
        }
    ),
    dcc.Graph(
        figure = {
            "data" : [
                {
                    "x" : df["cycle_index"],
                    "y" : df["SpecificMaxChargeCapacity"],
                    "type" : "lines",
                }
            ],
            "layout": {"title": "Specific Max Charge Capacity"},
        }
    ),
    dcc.Graph(
        figure = {
            "data" : [
                {
                    "x" : df["cycle_index"],
                    "y" : df["Minimun Resistance"],
                    "type" : "lines",
                }
            ],
            "layout": {"title": "Minimun Resistance"},
        }
    )    
])

## CALLBACK ###################################################


###############################################################


if __name__ == '__main__':
    app.run_server(debug=True)