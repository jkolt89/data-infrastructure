
import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.graph_objs as go
from dash.dependencies import Input, Output
from sklearn import datasets
from sklearn.cluster import KMeans
# These are additional to the fisher iris example
import flask
import sys
import sqlalchemy
import datetime
from sqlalchemy import create_engine
from dash.exceptions import PreventUpdate
import time
import dash_table
from dash.exceptions import PreventUpdate

# TODO add a div to display which cell data is currently loaded
#   would be helpful to use the variable div from here:
#   https://community.plotly.com/t/loading-pandas-dataframe-into-data-table-through-a-callback/19354/16

path = r"/home/solidpower/dash_app/dev/cell_app_data/celldata.csv"   # SERVER
#path = r"C:\Users\Hannah.Zimmerman\PycharmProjects\TestEnv\celldata.csv"   # LOCAL


driver = "FreeTDS"  # SERVER
#driver = "SQL+Server+Native+Client+11.0"  # LOCAL


celldata = pd.read_csv(r"{}".format(path))
celldata.columns = celldata.columns.str.replace(' ', '')

external_stylesheets = [dbc.themes.BOOTSTRAP]
server = flask.Flask(__name__)  # define flask app.server
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)



#------------------------------------Entry Field-------------#

entrybox = dbc.Card(
    [
        dbc.FormGroup(
            [
                dbc.Label("Cell ID Number"),
                dbc.Input('Cell ID Number', placeholder='Cell ID #')
            ]
        ),
        dbc.Button('Load Data', id='CIDsubmit', n_clicks=0, color='primary'),
        html.H6(id='CIDsubmit_container_button', children='Please Only Click Once'),
    ],
    body=True,
)

#---------------------------- Graph and dropdowns for axes ---#

controls = dbc.Card(
    [
        dbc.FormGroup(
            [
                dbc.Label("X variable"),
                dcc.Dropdown(
                    id="x-variable",
                    options=[
                        {"label": col, "value": col} for col in celldata.columns
                    ],
                    value="cycle_index",
                ),
            ]
        ),
        dbc.FormGroup(
            [
                dbc.Label("Y variable"),
                dcc.Dropdown(
                    id="y-variable",
                    options=[
                        {"label": col, "value": col} for col in celldata.columns
                    ],
                    value="cycle_index",
                ),
            ]
        ),
    ],
    body=True,
)

#-------------------------------------------------------------------------------- App.layout ----------#

app.layout = dbc.Container(
    [
        html.H1("Cell data prototype application"),
        html.Hr(),
        html.Br(),

        dbc.Row(
            [
                dbc.Col(entrybox, md=3)
            ],
            align="left",
        ),
        html.A(html.Button('Refresh Data'), href='/'),

        dbc.Row(
            [
                dbc.Col(controls, md=4),
                dbc.Col(dcc.Graph(id="cluster-graph"), md=8),
            ],
            align="center",
        ),
    ],
    fluid=True,
)


#------------------------------------------------------------------------------------------   Callbacks ---------------#

@app.callback(
    Output("cluster-graph", "figure"),
    [
        Input("x-variable", "value"),
        Input("y-variable", "value"),
    ],
    [dash.dependencies.State('Cell ID Number', 'value')]
)
def make_graph(x, y, CIDNumber):

    if CIDNumber is None: raise PreventUpdate

    # df = celldata[x, y]
    df = celldata.loc[:, [x, y]]
    #print(df)
    data = [
        go.Scatter(
            x = df.loc[:, x],
            y = df.loc[:, y],
            marker={"size": 8},
            name="Graph",
        )
    ]


    layout = {"xaxis": {"title": x}, "yaxis": {"title": y}}

    return go.Figure(data=data, layout=layout)


# make sure that x and y values can't be the same variable
def filter_options(v):
    """Disable option v"""
    return [
        {"label": col, "value": col, "disabled": col == v}
        for col in celldata.columns
    ]


# functionality is the same for both dropdowns, so we reuse filter_options
app.callback(Output("x-variable", "options"), [Input("y-variable", "value")])(
    filter_options
)
app.callback(Output("y-variable", "options"), [Input("x-variable", "value")])(
    filter_options
)

#-------------------------------------------------------------------------------------------Load Data------------------#
@app.callback(
    dash.dependencies.Output('CIDsubmit_container_button', 'children'),
    [dash.dependencies.Input('CIDsubmit', 'n_clicks')],
    [dash.dependencies.State('Cell ID Number', 'value')])
def update_output(n_clicks, CIDNumber):
    if CIDNumber is not None:

        print(n_clicks, CIDNumber)

        engine = create_engine("mssql+pyodbc://spoperator:50lidp0w3r@192.168.14.119:1433/SolidPowerDev?driver={}".format(driver))
        print('cell id data retrieval callback')
        connection = engine.raw_connection()
        print('connection succeeded')
        try:
            connection = engine.raw_connection()
            print('connection succeeded')
            cursor = connection.cursor()
            print('cursor succeeded')
            querystring = 'exec dbo.getCellStatisticData_ARBIN @cellname = \'{}\''.format(CIDNumber)

            print(querystring)

            df = pd.read_sql_query(querystring, connection)

            # theres whitespace in the headers - this removes it
            df.columns = df.columns.str.replace(' ', '')

            df.to_csv(r'{}'.format(path), index=False)

            cursor.close()
            connection.commit()
        except Exception as e:
            print('An exception occurred - Cell Data App')
            print(e)
            connection.close()
            return '--- Data Retrieval Error: : {}'.format(e)
        finally:
            connection.close()

        engine.dispose()

        return u'Data was successfully retrieved for : {}'.format(CIDNumber)
    else:
        raise PreventUpdate



if __name__ == "__main__":
    app.run_server(debug=True, port=8888, host='0.0.0.0')
