# -*- coding: utf-8 -*-


import dash
import dash_html_components as html
import dash_core_components as dcc
import flask
import sys
import sqlalchemy
from sqlalchemy import create_engine
from dash.exceptions import PreventUpdate


sys.path.append('/home/solidpower/pipython/Utilities')
sys.path.append('/home/solidpower/pipython/NodeRed')
sys.path.append('/home/solidpower')
sys.path.append('/home/solidpower/dash_app')

external_stylesheets = ['https://codepen.io/chriddyp/pen/dZVMbK.css']
server = flask.Flask(__name__)  # define flask app.server
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# run the following cmdln: gunicorn basic:app.server -b :8000


# 	Written by Hannah - last update 1-13-21

app.layout = html.Div([
    dcc.Tabs([

        ##################     Tab 1
        dcc.Tab(label='Generate a New Lot Number', children=[
            html.Div([
                html.H2('Lot Number Generator'),
                html.H6('Talk to Hannah if you encounter any issues'),
                html.Br(),
                html.H6('Enter the following information to generate your lot number:'),
                html.Br(),
                html.H6('Lot Number Generation Location:'),
                dcc.Dropdown(
                    id='LNGL_Dropdown_Chosen',
                    options=[
                        {'label': '10 - Raw Material', 'value': '10'},
                        {'label': '11  Bear Q-6 (Colt)', 'value': '11'},
                        {'label': '12 - Raw Material to be Dried', 'value': '12'},
                        {'label': '13 - Open', 'value': '13'},
                        {'label': '14 - Cathode Slurry', 'value': '14'},
                        {'label': '15 - Separator Slurry', 'value': '15'},
                        {'label': '16 - Anode Slurry', 'value': '16'},
                        {'label': '17 - Bear S1-1', 'value': '17'},
                        {'label': '18 - Bear S1-2', 'value': '18'},
                        {'label': '19 - Bear S1-3', 'value': '19'},
                        {'label': '20 - Duke Open', 'value': '20'},
                        {'label': '21 - Duke Open', 'value': '21'},
                        {'label': '22 - Duke Open', 'value': '22'},
                        {'label': '23 - Duke S1-1', 'value': '23'},
                        {'label': '24 - Duke S1-2', 'value': '24'},
                        {'label': '25 - Duke S1-3', 'value': '25'},
                        {'label': '26 - Duke Open', 'value': '26'},
                        {'label': '27 - Duke HD', 'value': '27'},
                        {'label': '28 - Duke HDDM', 'value': '28'},
                        {'label': '29 - NMC Coating/Surface Mod', 'value': '29'},
                        {'label': '30 - Guse', 'value': '30'},
                        {'label': '31 - Guse HDDM', 'value': '31'},
                        {'label': '32 - Guse DMQX', 'value': '32'},
                        {'label': '33 - Guse C-3', 'value': '33'},
                        {'label': '34 - Guse Trey', 'value': '34'},
                        {'label': '35 - Guse HQ', 'value': '35'},
                        {'label': '36 - Guse Colt', 'value': '36'},
                        {'label': '37 - Guse Ace', 'value': '37'},
                        {'label': '38 - Guse Deuce', 'value': '38'},
                        {'label': '39 - Guse Open', 'value': '39'},
                        {'label': '40 - Ibis Open', 'value': '40'},
                        {'label': '41 - Ibis Open', 'value': '41'},
                        {'label': '42 - Ibis Open', 'value': '42'},
                        {'label': '43 - Ibis Open', 'value': '43'},
                        {'label': '44 - Ibis Open', 'value': '44'},
                        {'label': '45 - Ibis Open', 'value': '45'},
                        {'label': '46 - Ibis Open', 'value': '46'},
                        {'label': '47 - Ibis Open', 'value': '47'},
                        {'label': '48 - Ibis Open', 'value': '48'},
                        {'label': '49 - Ibis Open', 'value': '19'},
                        {'label': '50 - Cathode Coating T/B', 'value': '50'},
                        {'label': '51 - Cathode Coating T Only', 'value': '51'},
                        {'label': '52 - Cathode Coating B Only', 'value': '52'},
                        {'label': '53 - Separator 0.22 Al', 'value': '53'},
                        {'label': '54 - Separator 0.15 Al', 'value': '54'},
                        {'label': '55 - Separator PET', 'value': '55'},
                        {'label': '56 - Mock Electrolyte', 'value': '56'},
                        {'label': '57 - Open coating', 'value': '57'},
                        {'label': '58 - Open coating', 'value': '58'},
                        {'label': '59 - Cut and Tape Cathode', 'value': '59'},
                        {'label': '60 - Length Cut Guse Cathode', 'value': '60'},
                        {'label': '61 - Cathode', 'value': '61'},
                        {'label': '62 - Anode', 'value': '62'},
                        {'label': '63 - Guse Separator Slurry', 'value': '63'},
                        {'label': '64 - Guse Cathode Separator', 'value': '64'},
                        {'label': '65 - Guse Cathode Slurry', 'value': '65'},
                        {'label': '66 - Guse  Coated Cathode', 'value': '66'},
                        {'label': '67 - Guse Cathode', 'value': '67'},
                        {'label': '68 - Guse Cathode Calendar', 'value': '68'},
                        {'label': '69 - Guse Laminated Cathode', 'value': '69'},
                        {'label': '70 - Bear Separator Slurry', 'value': '70'},
                        {'label': '71 - Bear Cathode Separator', 'value': '71'},
                        {'label': '72 - Cut Stack', 'value': '72'},
                        {'label': '73 - Bear Coated Cathode', 'value': '73'},
                        {'label': '74 - Bear Cathode', 'value': '74'},
                        {'label': '75 - Bear Anode', 'value': '75'},
                        {'label': '76 - Bear Andoe Separator', 'value': '76'},
                        {'label': '77 - Slit Separator', 'value': '77'},
                        {'label': '78 - Laminated Anode', 'value': '78'},
                        {'label': '79 - Cut Anode', 'value': '79'},
                        {'label': '80 - 2Ah Final Assembly', 'value': '80'},
                        {'label': '81 - 2Ah Rig', 'value': '81'},
                        {'label': '82 - 2Ah Cell', 'value': '82'},
                        {'label': '85 - 20Ah Final Assembly', 'value': '85'},
                        {'label': '86 - 20Ah Rig', 'value': '86'},
                        {'label': '87 - 20Ah Cell', 'value': '87'},
                        {'label': 'Combined Bear', 'value': 'CB'},
                        {'label': 'Combined Duke', 'value': 'CD'},
                        {'label': 'Combined Guse', 'value': 'CG'}
                    ],
                    placeholder="Lot Number Generation Locaiton",
                ),

                html.Br(),
                html.H6('Employee ID Number:'),
                html.Div(
                    dcc.Input(id='input-on-submit3', type='text', placeholder="Employee ID Number", required=True,
                              debounce=True)),
                html.Br(),
                html.H6('Material Generation Number:'),
                html.Div(dcc.Input(id='input-on-submit4', type='text', placeholder='Material Generation Number',
                                   required=True, debounce=True)),
                html.Br(),
                html.Button('Submit', id='submit-val', n_clicks=0),
                html.Br(),
                html.H6(id='container-button-basic', children='Please only click submit once'),
                html.Br(),
                html.Br(),
                html.Br()
            ])
        ]),

        ##################     Tab 2
        dcc.Tab(label='Split Number Generator', children=[  # getLotNumberWithSplitLetter
            html.H2('Prototype Split Number Generator'),
            html.H6('Talk to Hannah if you encounter any issues'),
            html.Br(),
            #            html.H6('Original Lot Number:'),  # @LotNumber
            #            html.Div(
            #                dcc.Input(id='tab2-lotNum', type='text', placeholder="Lot Number", required=True,
            #                          debounce=True)),
            #            html.Br(),
            html.H6('Split Number:'),  # @SplitLetter
            html.Div(
                dcc.Input(id='tab2-splitNum', type='text', placeholder="Enter your split number", required=True,
                          debounce=True)),
            html.Br(),
            html.H6('Original Lot Number:'),  # @LotNumber
            html.Div(
                dcc.Input(id='tab2-lotNum', type='text', placeholder="Enter your lot number", required=True,
                          debounce=True)),
            html.Br(),
            html.H6('Employee ID Number:'),  # @EnteredBy
            html.Div(
                dcc.Input(id='tab2-employeeNum', type='text', placeholder="Employee ID Number", required=True,
                          debounce=True)),
            html.Br(),
            html.Button('Submit', id='submit-val2', n_clicks=0),
            html.Br(),
            html.H6(id='tab2_container_button', children='Please only click submit once'),
            html.Br()
        ]),
        ##################     Tab 3
        dcc.Tab(label='Part Number Generator', children=[
            html.H2('Part Number Generator'),
            html.Br(),

            html.Br(),
            html.H6('Select Material:'),
            dcc.Dropdown(
                id='Material_Selection',
                options=[
                    {'label': 'Anode Stack', 'value': 'AA'},
                    {'label': 'Acetonitrile', 'value': 'AC'},
                    {'label': 'Anode', 'value': 'AD'},
                    {'label': 'Aluminum Foil', 'value': 'AL'},
                    {'label': 'AL Foil Carbon Coated', 'value': 'AM'},
                    {'label': 'Argon', 'value': 'AR'},
                    {'label': 'Anode Slurry', 'value': 'AS'},
                    {'label': 'Acetone', 'value': 'AT'},
                    {'label': 'Bromide Agardite', 'value': 'BA'},
                    {'label': '2 Ah Cell', 'value': 'BB'},
                    {'label': '20 Ah Cell', 'value': 'BC'},
                    {'label': '200 mAh Cell ', 'value': 'BD'},
                    {'label': 'Bear Electrolyte', 'value': 'BE'},
                    {'label': 'Bear Cathode', 'value': 'BF'},
                    {'label': '2Ah Rig Assembly', 'value': 'BG'},
                    {'label': 'Bear Coated Cathode', 'value': 'BH'},
                    {'label': 'Bear Separator Slurry', 'value': 'BI'},
                    {'label': 'Bear Anode Separator', 'value': 'BJ'},
                    {'label': 'Bear Anode', 'value': 'BK'},
                    {'label': 'Bear', 'value': 'BR'},
                    {'label': 'Bear Cathode Separator', 'value': 'BS'},
                    {'label': 'Bear Weighting', 'value': 'BW'},
                    {'label': 'Chloride Agardite', 'value': 'CA'},
                    {'label': 'Cathode Stack', 'value': 'CB'},
                    {'label': 'Cathode Coating', 'value': 'CC'},
                    {'label': 'Cathode', 'value': 'CD'},
                    {'label': 'Guse', 'value': 'CE'},
                    {'label': 'Guse Cathode Cut', 'value': 'CF'},
                    {'label': 'Conductive Graphite', 'value': 'CG'},
                    {'label': 'Cut and Tape Cathode', 'value': 'CH'},
                    {'label': 'Guse Length Cut Cathode', 'value': 'CI'},
                    {'label': 'Cathode Calendar', 'value': 'CL'},
                    {'label': 'Guse Laminated Cathode', 'value': 'CM'},
                    {'label': 'Coated NMC', 'value': 'CN'},
                    {'label': 'Carbon', 'value': 'CO'},
                    {'label': 'Cathode Separator', 'value': 'CR'},
                    {'label': 'Cathode Slurry', 'value': 'CS'},
                    {'label': 'Cathode Stack', 'value': 'CT'},
                    {'label': 'DI Argyrodite', 'value': 'DA'},
                    {'label': 'Duke Electrolyte', 'value': 'DE'},
                    {'label': 'Diaper', 'value': 'DP'},
                    {'label': 'EtOH', 'value': 'EH'},
                    {'label': 'Electrolyte', 'value': 'EL'},
                    {'label': 'Ethyl Propionate', 'value': 'EP'},
                    {'label': 'Electrolyte Slurry', 'value': 'ES'},
                    {'label': 'Fixture Base', 'value': 'FA'},
                    {'label': 'Silicone Cutout', 'value': 'FB'},
                    {'label': 'Flat Washer 1/4', 'value': 'FC'},
                    {'label': 'Fixture Cutout', 'value': 'FD'},
                    {'label': 'Beleville Washer', 'value': 'FE'},
                    {'label': 'Hex Cap 1/4', 'value': 'FF'},
                    {'label': 'Flange Nut 1/4', 'value': 'FG'},
                    {'label': 'Ni Tab', 'value': 'FH'},
                    {'label': 'AL Tab', 'value': 'FI'},
                    {'label': 'Pouch Film', 'value': 'FJ'},
                    {'label': 'Tab Protector Tape', 'value': 'FK'},
                    {'label': 'Plastic Sheet', 'value': 'FL'},
                    {'label': 'Solid Power Sticker', 'value': 'FM'},
                    {'label': 'Rig Label', 'value': 'FN'},
                    {'label': 'Anodizing Tape', 'value': 'FO'},
                    {'label': 'Guse Anode', 'value': 'GA'},
                    {'label': 'Guse Cathode', 'value': 'GC'},
                    {'label': 'Guse Cathode Slurry', 'value': 'GG'},
                    {'label': 'Guse Coated Cathode', 'value': 'GH'},
                    {'label': 'Guse Separator Slurry', 'value': 'GI'},
                    {'label': 'Guse Separator', 'value': 'GS'},
                    {'label': 'HP Etoh', 'value': 'HP'},
                    {'label': 'IBIB', 'value': 'IB'},
                    {'label': 'Ibis', 'value': 'IE'},
                    {'label': 'Isopropyl Alcohol', 'value': 'IP'},
                    {'label': 'Lithium Bromide', 'value': 'LB'},
                    {'label': 'Lithium Chloride', 'value': 'LC'},
                    {'label': 'Lithium Foil', 'value': 'LF'},
                    {'label': 'Lithium Chips', 'value': 'LI'},
                    {'label': 'Lithium Iodine', 'value': 'LL'},
                    {'label': 'LPS', 'value': 'LP'},
                    {'label': 'Lithium Sulfide', 'value': 'LS'},
                    {'label': 'Masking Tape', 'value': 'MA'},
                    {'label': 'Molecular Sieves', 'value': 'MS'},
                    {'label': 'NB EtOH', 'value': 'NB'},
                    {'label': 'NMC', 'value': 'NC'},
                    {'label': 'Heat Treated NMC', 'value': 'NT'},
                    {'label': 'Octane', 'value': 'OC'},
                    {'label': 'PST-EB', 'value': 'PE'},
                    {'label': 'PVDF', 'value': 'PF'},
                    {'label': 'Pouch', 'value': 'PH'},
                    {'label': 'PAPI', 'value': 'PL'},
                    {'label': 'Phosphorous Pentasulfide', 'value': 'PP'},
                    {'label': 'Cell Rig', 'value': 'RC'},
                    {'label': 'Stiffener', 'value': 'SN'},
                    {'label': 'SLMP', 'value': 'SP'},
                    {'label': 'Separator Coated', 'value': 'SR'},
                    {'label': 'Separator Slurry', 'value': 'SS'},
                    {'label': 'Tab', 'value': 'TB'},
                    {'label': 'Carbon VGCF', 'value': 'VF'},
                    {'label': 'Waste Material', 'value': 'WM'},
                    {'label': 'DMQX Waste', 'value': 'WQ'},
                    {'label': 'Xylene', 'value': 'XY'}
                ],
                # value=[],
                placeholder="Select Material Generation",
            ),

            html.Br(),
            html.H6('Generation:'),
            dcc.Dropdown(
                id='Generation_Selection',
                options=[
                    {'label': 'Gen 1', 'value': '1'},
                    {'label': 'Gen 2', 'value': '2'},
                    {'label': 'Gen 3', 'value': '3'}
                ],
                # value=[],
                placeholder="Select Material Generation",
            ),

            html.Br(),
            html.H6('Material Form:'),
            dcc.Dropdown(
                id='MaterialForm_Selection',
                options=[
                    {'label': 'Liquid', 'value': '1'},
                    {'label': 'Solid', 'value': '2'},
                    {'label': 'Powder', 'value': '3'},
                    {'label': 'Mixture', 'value': '4'},
                    {'label': 'Gas', 'value': '5'},
                    {'label': 'Roll', 'value': '6'},
                    {'label': 'Coated', 'value': '7'},
                    {'label': 'Calendered', 'value': '8'},
                    {'label': 'Cell', 'value': '9'},
                    {'label': 'Battery', 'value': 'A'},
                    {'label': 'Stack', 'value': 'B'},
                    {'label': 'Rig Build', 'value': 'C'}
                ],
                # value=[],
                placeholder="Select a Material Form",
            ),
            html.Br(),
            html.H6('Process Location:'),
            dcc.Dropdown(
                id='ProcLoc_Selection',
                options=[
                    {'label': 'Raw Material', 'value': '1'},
                    {'label': 'Internal Synthesis', 'value': '2'},
                    {'label': 'Surface Modification', 'value': '3'},
                    {'label': 'Slurry Mix', 'value': '4'},
                    {'label': 'Cell Production', 'value': '5'},
                    {'label': 'Coating', 'value': '6'},
                    {'label': 'Pre-Crystallization', 'value': '7'}
                ],
                # value=[],
                placeholder="Select Process Location",
            ),
            html.Br(),
            html.H6('Material Description:'),
            dcc.Dropdown(
                id='MatDesc_Selection',
                options=[
                    {'label': 'No Description', 'value': '0000'},
                    {'label': 'D90 < 1 um', 'value': '0001'},
                    {'label': '0.1 < D50 < 0.8 um', 'value': '0002'},
                    {'label': '2.0 < D50 < 3.0 um', 'value': '0003'},
                    {'label': '3.0 < D50 < 4.0 um', 'value': '0004'},
                    {'label': '1.0 < D50 < 4.0 um', 'value': '0005'},
                    {'label': '2.0 < D50 < 6.0 um', 'value': '0006'},
                    {'label': 'For Cleaning Only', 'value': '0007'},
                    {'label': '0.38 Thickness', 'value': '0008'},
                    {'label': 'D90 < 10 um', 'value': '0011'},
                    {'label': 'Name of a cell shipped 9/10  Uday', 'value': '0013'},
                    {'label': 'Carbon KS-6', 'value': '0014'},
                    {'label': 'Carbon C65', 'value': '0015'},
                    {'label': '90mm X 0.035mm', 'value': '0018'},
                    {'label': 'Raw Material that requires Drying', 'value': '0019'},
                    {'label': '2Ah Cell', 'value': '0020'},
                    {'label': '20Ah Cell', 'value': '0021'},
                    {'label': '200 mAh Cell', 'value': '0022'},
                    {'label': '6% Bear', 'value': '0023'},
                    {'label': '14% Bear', 'value': '0024'},
                    {'label': 'H18 20um X 150mm', 'value': '0027'},
                    {'label': 'H18 20um X 250mm', 'value': '0028'},
                    {'label': '0.12mm', 'value': '0029'},
                    {'label': '50mm X 1.1m', 'value': '0030'},
                    {'label': '210mm X 0.85m', 'value': '0031'},
                    {'label': '150 X 12mm', 'value': '0032'},
                    {'label': '250 X 12mm', 'value': '0033'},
                    {'label': 'Slit Separator', 'value': '0034'},
                    {'label': 'Laminated Anode', 'value': '0035'},
                    {'label': 'Cut Anode', 'value': '0036'}
                ],
                # value=[],
                placeholder="Select Material Description",
            ),

            html.Div(id='MFS_Output'),

            html.Br(),
            html.Button('Submit', id='submit-Tab3',
                        n_clicks=0),
            html.Br(),
            html.Br(),
            html.H6(id='tab3_container_button', children=' '),
            html.Br()
        ]),

        ###################     Tab 4
        dcc.Tab(label='Record Combined Batches', children=[
            html.H2(
                'Combined Batch Recording Application - please use the lot number generator to create a combined lot number first'),
            html.Br(),
            html.H6('Combined Lot Number:'),  # @CombinedLotNr
            html.Div(
                dcc.Input(id='tab4-CombinedLotNum',
                          type='text',
                          placeholder="Combined Lot Number",
                          required=True,
                          debounce=True)
            ),
            html.Br(),
            html.H6('Weight (in grams):'),  # @StartWeight
            html.Div(
                dcc.Input(id='tab4-StartWeight',
                          type='text',
                          placeholder="Weight",
                          required=True,
                          debounce=True)
            ),
            html.Br(),
            html.H6(
                'Original Lot Number:       (this records one at a time. please re-run this application for each original lot nubmer you have)'),
            # @OriginalLotNr
            html.Div(
                dcc.Input(id='tab4-OriginalLotNum',
                          type='text',
                          placeholder="Original lot #",
                          required=True,
                          debounce=True)
            ),
            html.Br(),
            html.H6('Employee ID Number:'),  # @UserOpID
            html.Div(
                dcc.Input(id='tab4-EmployeeNum',
                          type='text',
                          placeholder="Employee ID Number",
                          required=True,
                          debounce=True)
            ),
            html.Br(),
            html.Button('Submit', id='submit-Tab4', n_clicks=0),
            html.Br(),
            html.H6(id='tab4_container_button', children='Please only click submit once'),
            html.Br()
        ]),

        ###################     Tab 5
        dcc.Tab(label='Conductivity Data Entry -- New!', children=[
            html.H2(
                'Conductivity Data Entry Form'),
            html.Br(),
            html.H6('User ID:'),  #@UserOpID
            html.Div(
                dcc.Input(id='tab5-User', type='text', placeholder="User ID Number", required=True, debounce=True)),
            html.Br(),
            html.H6('Lot Number:'),  # @ProductLN
            html.Div(
                dcc.Input(id='tab5-PLN', type='text', placeholder="Product Lot Number", required=True, debounce=True)),
            html.Br(),

            html.H6('Test Condition:'),  # @TestCondition
            html.Div(
                dcc.Input(id='tab5-TestCon', type='text', placeholder="Test Condition", required=True, debounce=True)),
            html.Br(),
            html.H6(
                'Test Temperature:'),  # @TestTemperature
            # @OriginalLotNr
            html.Div(
                dcc.Input(id='tab5-TestTemp', type='text', placeholder="Test Temperature", required=True,
                          debounce=True)),
            html.Br(),
            html.H6('Mass (grams):'),  # @MaterialMass_g
            html.Div(
                dcc.Input(id='tab5-Mass', type='text', placeholder="Material Mass", required=True, debounce=True)),
            html.Br(),
            html.H6('Plunger length number ?:'),  # @PlungerLengthNoPellet_mm
            html.Div(
                dcc.Input(id='tab5-PlungerLNo', type='text', placeholder="Plunger Length number", required=True,
                          debounce=True)),
            html.Br(),
            html.H6('Plunger Thickness (mm) ?:'),  # @PelletPlungerThickness_mm
            html.Div(
                dcc.Input(id='tab5-PlungerThickness', type='text', placeholder="Plunger Thickness", required=True,
                          debounce=True)),
            html.Br(),
            html.H6('Pellet Thickness (cm) ?:'),  # @PelletThickness_cm
            html.Div(
                dcc.Input(id='tab5-PelletThickness', type='text', placeholder="Pellet Thickness", required=True,
                          debounce=True)),
            html.Br(),
            html.H6('Material Density (gcc):'),  # @MaterialDensity_gcc
            html.Div(
                dcc.Input(id='tab5-MatDensity', type='text', placeholder="Material Density", required=True,
                          debounce=True)),
            html.Br(),
            html.H6('Axis Nyquist (ohms):'),  # @XAxisNyquist_ohms
            html.Div(
                dcc.Input(id='tab5-AxisNyquist', type='text', placeholder="Axis Nyquist", required=True,
                          debounce=True)),
            html.Br(),
            html.H6('Ionic Condictivity (SCM):'),  # @IonicConductivity_Scm
            html.Div(
                dcc.Input(id='tab5-IonCon', type='text', placeholder="Ionic Conductivity", required=True,
                          debounce=True)),
            html.Br(),
            html.H6('Electronic Condictivity (SCM):'),  # @ElectronicConductivity_Scm
            html.Div(
                dcc.Input(id='tab5-ECon', type='text', placeholder="Electronic Conductivity", required=True,
                          debounce=True)),
            html.Br(),
            html.Br(),
            html.Br(),

            # Input box for notes will go here someday... # @Notes

            html.Button('Submit', id='submit-Tab5', n_clicks=0),
            html.Br(),
            html.H6(id='tab5_container_button', children='Please only click submit once'),
            html.Br()
        ]),


        ###################     Tab 6
        dcc.Tab(label='Consumed Batch Data Entry -- New!', children=[
            html.H2(
                'Consumed Batch Data Entry Form'),
            html.Br(),
            html.H6('User ID:'),  # @UserOpID
            html.Div(
                dcc.Input(id='tab6-User', type='text', placeholder="User ID Number", required=True, debounce=True)),
            html.Br(),
            html.H6('Lot Number:'),  # @ProductLN
            html.Div(
                dcc.Input(id='tab6-PLN', type='text', placeholder="Product Lot Number", required=True, debounce=True)),
            html.Br(),
            html.H6('Start Date:'),  # @startdate
            html.Div(
                dcc.Input(id='tab6-StartDate', type='text', placeholder="Start Date", required=True, debounce=True)),
            html.Br(),
            html.H6('Finish Date:'),  # @finishdate
            # @OriginalLotNr
            html.Div(
                dcc.Input(id='tab6-FinishDate', type='text', placeholder="Finish Date", required=True, debounce=True)),
            html.Br(),
            html.H6('Quantity (grams):'),  # @Quantity_g
            html.Div(
                dcc.Input(id='tab6-Quantity', type='text', placeholder="Quantity (grams)", required=True, debounce=True)),
            html.Br(),
            html.H6('MRB:'),  # @MRB
            html.Div(
                dcc.Input(id='tab6-MRB', type='text', placeholder="MRB", required=True, debounce=True)),
            html.Br(),
            html.H6('Conductivity (mScm) ?:'),  # @Conductivity_mScm
            html.Div(
                dcc.Input(id='tab6-Conductivity', type='text', placeholder="Conductivity (mScm", required=True, debounce=True)),
            html.Br(),
            html.H6('Yield (grams):'),  # @Yield_g
            html.Div(
                dcc.Input(id='tab6-Yield', type='text', placeholder="Yield (grams)", required=True, debounce=True)),
            html.Br(),
            html.H6('Pass/Fail:'),  # @PassFail
            html.Div(
                dcc.Input(id='tab6-PassFail', type='text', placeholder="Pass/Fail", required=True, debounce=True)),
            html.Br(),
            html.H6('Electronic Conductivity (mScm):'),  # @ElectronicConductivity_mScm
            html.Div(
                dcc.Input(id='tab6-ECon', type='text', placeholder="Electronic Conductivity", required=True, debounce=True)),
            html.Br(),
            html.H6('Guse Generation ?:'),  # @GuseGen
            html.Div(
                dcc.Input(id='tab6-GuseGen', type='text', placeholder="Guse Generation", required=True, debounce=True)),
            html.Br(),
            html.Br(),
            html.Br(),

            # Input box for notes will go here someday... # @Notes

            html.Button('Submit', id='submit-Tab6', n_clicks=0),
            html.Br(),
            html.H6(id='tab6_container_button', children='Please only click submit once'),
            html.Br()
        ]),



        ###################     Tab 7
        dcc.Tab(label='Material Drying Data Entry -- New!', children=[
            html.H2(
                'Material Drying Data Entry Form'),
            html.Br(),
            html.H6('User ID:'),  # @UserOpID
            html.Div(
                dcc.Input(id='tab7-User', type='text', placeholder="User ID Number", required=True, debounce=True)),
            html.Br(),
            html.H6('Ingredient Lot Number:'),  # @IngredientLN
            html.Div(
                dcc.Input(id='tab7-ILN', type='text', placeholder="Ingredient Lot Number", required=True, debounce=True)),
            html.Br(),
            html.H6('Product Part Number:'),  # @ProductPN
            html.Div(
                dcc.Input(id='tab7-PPN', type='text', placeholder="Product Part Number", required=True, debounce=True)),
            html.Br(),
            html.H6('Item:'),  # @Item
            html.Div(
                dcc.Input(id='tab7-Item', type='text', placeholder="Item ?", required=True, debounce=True)),
            html.Br(),
            html.H6('Supplier Name:'),  # @Supplier
            html.Div(
                dcc.Input(id='tab7-SupplierName', type='text', placeholder="Supplier Name", required=True, debounce=True)),
            html.Br(),
            html.H6('Supplier Lot Number:'),  # @SupplierLotNr
            html.Div(
                dcc.Input(id='tab7-SupplierLN', type='text', placeholder="Supplier Lot Number", required=True,
                          debounce=True)),
            html.Br(),
            html.H6('K Solid Moisture (ppm)'),  # @KSolidMoisture_ppm
            html.Div(
                dcc.Input(id='tab7-KSolid', type='text', placeholder="K Solid ?", required=True, debounce=True)),
            html.Br(),
            html.Br(),
            html.Br(),

            # Input box for notes will go here someday... # @Notes



            html.Button('Submit', id='submit-Tab7', n_clicks=0),
            html.Br(),
            html.H6(id='tab7_container_button', children='Please only click submit once'),
            html.Br()
        ]),
    ])
])


# -------------------- Tab1 Callback ---------------------- #
@app.callback(
    dash.dependencies.Output('container-button-basic', 'children'),
    [dash.dependencies.Input('submit-val', 'n_clicks')],
    [dash.dependencies.State('LNGL_Dropdown_Chosen', 'value'),  # removed input 2 for split number TODO rename
     dash.dependencies.State('input-on-submit3', 'value'), dash.dependencies.State('input-on-submit4', 'value')])
def update_output(n_clicks, LNGL, userNum, Generation):
    if LNGL is not None and userNum is not None and Generation is not None:
        print(LNGL, userNum, Generation)

        engine = create_engine("mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
        print('tab1 - hello from the first callback - lot number generator')
        connection = engine.raw_connection()
        try:
            cursor = connection.cursor()
            querystring = 'exec getLotNumber @LotNrGenLocation = \'{}\', @EnteredBy = \'{}\', @GenNr = \'{}\''.format(
                LNGL, userNum, Generation)
            cursor.execute(querystring)
            print('querystring is: ')
            print(querystring)
            newLotNum, = list(cursor.fetchall())  # THIS COMMA MATTERS A LOT
            print('results are: ')
            print(newLotNum)
            cursor.close()
            connection.commit()
        except Exception as e:
            print('an exception occurred')
            print(e)
            connection.close()
            return '---error-tab1--- {}'.format(e)
        finally:
            print('closing connection to DB')
            connection.close()

        engine.dispose()
        # print(type(newLotNum))
        # newLotNum1 = newLotNum.pop(0)
        # print(type(newLotNum))
        # print(str(newLotNum))

        # this is an interesting thing:
        # The fetchall required a comma after the asignee variable name beacuse it was making a list that I couldnt format
        # solving that raised the issue of now it returned a class object. fortunately a string conversion worked, then a substring to finalize the conversion
        newLotNum = str(newLotNum)
        newLotNum = newLotNum[
                    3:16]  # Why, yes, this is hard coding the length of the barcode to fit in those constraints. sometimes it doesnt work - BE WARNED TODO
        return u'Your new lot number is: {}'.format(newLotNum)
    else:
        raise PreventUpdate


# return 'The input value was "{}" and the button has been clicked {} times. value2 was: {}, and value3 was {}'.format(LNGL, n_clicks, splitNum, userNum)


# -------------------- Tab2 Callback ---------------------- #
@app.callback(
    dash.dependencies.Output('tab2_container_button', 'children'),
    [dash.dependencies.Input('submit-val2', 'n_clicks')],
    [dash.dependencies.State('tab2-lotNum', 'value'), dash.dependencies.State('tab2-splitNum', 'value'),
     dash.dependencies.State('tab2-employeeNum', 'value')])
def update_output(n_clicks, OGLotNum, SplitLetter, userNum2):
    if OGLotNum is not None and SplitLetter is not None and userNum2 is not None:
        # print("hello")
        print(OGLotNum, SplitLetter, userNum2)
        #        if (len(SplitLetter) != 1) or (SplitLetter == ''):
        #            print("bad split number, try again")
        #            return u'Invalid split number. Should be 1 digit. Probably 0'
        #        if len(userNum2) != 2 or userNum2 == '':
        #            print("Invalid user number. Should be 2 Digits")
        #            return u'Invalid user number. Should be 2 digits'
        #        if len(OGLotNum) != 2 or OGLotNum == '':  # checking correct input -- this shoud be it's own funciton someday
        #            print("bad length")
        #            return u'Invalid lot number getneration location. Should be 2 digits. See Hannah or Matt for assitance finding the correct number'

        engine = create_engine("mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
        print('tab 2 - hello from the second callback - split number generator')
        # pd.options.mode.chained_assignment = None
        # querystring =  "exec getLotNumber {},{},{}".format(LNGL,splitNum,userNum)
        # newLotNum = pd.read_sql_query(querystring, con=engine)
        connection = engine.raw_connection()
        # print('this is the try clause')
        try:
            cursor = connection.cursor()
            print('cursor succeeded')
            querystring = 'exec dbo.getLotNumberWithSplitLetter @LotNumber = \'{}\', @SplitLetter = \'{}\', @EnteredBy = \'{}\''.format(
                OGLotNum, SplitLetter, userNum2)
            print(querystring)
            cursor.execute(querystring)
            print('querystring is: ')
            print(querystring)
            newLotNum, = list(cursor.fetchall())  # THIS COMMA MATTERS A LOT
            print('results are: ')
            print(newLotNum)
            cursor.close()
            connection.commit()
        except Exception as e:
            print('there was an exception that occurred. This will need to be looked at more')
            print(e)
            connection.close()
            return '---error2---'
        finally:
            connection.close()

        engine.dispose()
        # print(type(newLotNum))
        # newLotNum1 = newLotNum.pop(0)
        # print(type(newLotNum))
        # print(str(newLotNum))

        # this is an interesting thing:
        # The fetchall required a comma after the asignee variable name beacuse it was making a list that I couldnt format
        # solving that raised the issue of now it returned a class object. fortunately a string conversion worked, then a substring to finalize the conversion
        newLotNum = str(newLotNum)
        newLotNum = newLotNum[
                    3:16]  # Why, yes, this is hard coding the length of the barcode to fit in those constraints. sometimes it doesnt work - BE WARNED TODO
        return u'Your new lot number is: {}'.format(newLotNum)
    else:
        raise PreventUpdate


# -------------------- Tab3 Callback ---------------------- #
@app.callback(
    dash.dependencies.Output('tab3_container_button', 'children'),
    [dash.dependencies.Input('submit-Tab3', 'n_clicks')],
    [dash.dependencies.State('Material_Selection', 'value'),
     dash.dependencies.State('Generation_Selection', 'value'),
     dash.dependencies.State('MaterialForm_Selection', 'value'),
     dash.dependencies.State('ProcLoc_Selection', 'value'),
     dash.dependencies.State('MatDesc_Selection', 'value')])
def update_output(n_clicks, Material, Generation, MatForm, ProcLoc, MatDesc):
    if Material is not None and Generation is not None and MatForm is not None and ProcLoc is not None and MatDesc is not None:

        engine = create_engine("mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
        print('tab 3 - hello from the third callback - PNG')
        # connection = engine.raw_connection()
        # print('connection succeeded')
        try:
            connection = engine.raw_connection()
            print('connection succeeded')
            cursor = connection.cursor()
            print('cursor succeeded')
            # querystring = 'exec dbo.uspValidatePartNumber @MaterialShortName = \'{}\', @GenID = \'{}\', @MaterialFormID = \'{}\', @ProcessLocID = \'{}\' , @MDID = \'{}\''.format(Material, Generation, MatForm, ProcLoc, MatDesc)
            querystring = 'exec dbo.uspValidatePartNumber \'{}{}{}{}-{}\''.format(Material, Generation, MatForm,
                                                                                  ProcLoc, MatDesc)
            print(querystring)
            cursor.execute(querystring)
            print('querystring is: ')
            print(querystring)
            NewPartNumber = cursor.fetchall()  # note: no comma
            # NewPartNumber = 'hello'
            print('results are: ')
            print(type(NewPartNumber))
            cursor.close()
            connection.commit()
        except Exception as e:
            print('there was an exception that occurred - tab 3')
            print(e)
            connection.close()
            return '---Exception3 : {}'.format(e)
        finally:
            connection.close()

        engine.dispose()

        NewPartNumber = str(NewPartNumber)
        NewPartNumber = NewPartNumber.strip('u[](),\'\"\,\'')
        NewPartNumber = NewPartNumber.replace('"', '')
        return 'Your part number is:  "{}" '.format(NewPartNumber)
    # return 'You have selected "{}" and "{}" and "{}" and "{}" and "{}"  '.format(Material, Generation, MatForm, ProcLoc, MatDesc)

    else:
        raise PreventUpdate


# -------------------- Tab4 Callback ---------------------- #
@app.callback(
    dash.dependencies.Output('tab4_container_button', 'children'),
    [dash.dependencies.Input('submit-Tab4', 'n_clicks')],
    [dash.dependencies.State('tab4-CombinedLotNum', 'value'), dash.dependencies.State('tab4-OriginalLotNum', 'value'),
     dash.dependencies.State('tab4-EmployeeNum', 'value'), dash.dependencies.State('tab4-StartWeight', 'value')])
def update_output(n_clicks, CombinedLotNum, OGLotNumbers, userNum4, startWeight):
    if CombinedLotNum is not None and OGLotNumbers is not None and userNum4 is not None and startWeight is not None:
        # print("hello")
        print(CombinedLotNum, OGLotNumbers, userNum4, startWeight)

        engine = create_engine("mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
        print('tab 4 - hello from the fourth callback - Combined batch recording')
        # pd.options.mode.chained_assignment = None
        # querystring =  "exec getLotNumber {},{},{}".format(LNGL,splitNum,userNum)
        # newLotNum = pd.read_sql_query(querystring, con=engine)
        connection = engine.raw_connection()
        print('connection succeeded')
        try:
            connection = engine.raw_connection()
            print('connection succeeded')
            cursor = connection.cursor()
            print('cursor succeeded')
            querystring = 'exec dbo.insertCombinedBatches @CombinedLotNr = \'{}\', @OriginalLotNr = \'{}\', @UserOpID = \'{}\', @StartWeight = \'{}\''.format(
                CombinedLotNum, OGLotNumbers, userNum4, startWeight)
            print(querystring)
            cursor.execute(querystring)
            print('querystring is: ')
            print(querystring)
            ExistingOGLots = cursor.fetchall()  # note: no comma
            print('results are: ')
            print(type(ExistingOGLots))
            cursor.close()
            connection.commit()
        except Exception as e:
            print('there was an exception that occurred. This will need to be looked at more')
            print(e)
            connection.close()
            return '---error4 : {}'.format(e)
        finally:
            connection.close()

        engine.dispose()
        barcodes = ''
        for barcode in ExistingOGLots:
            l = str(barcode)
            i = l.strip('u[](),\'')
            barcodes = barcodes + i + '      '
        return u'This returned the following barcodes: {}'.format(barcodes)
    else:
        raise PreventUpdate


# -------------------- Tab5 Callback ---------------------- #
@app.callback(
    dash.dependencies.Output('tab5_container_button', 'children'),
    [dash.dependencies.Input('submit-Tab5', 'n_clicks')],
    [dash.dependencies.State('tab5-PLN', 'value'), dash.dependencies.State('tab5-TestCon', 'value'),
     dash.dependencies.State('tab5-TestTemp', 'value'), dash.dependencies.State('tab5-Mass', 'value'),
     dash.dependencies.State('tab5-PlungerLNo', 'value'), dash.dependencies.State('tab5-PlungerThickness', 'value'),
     dash.dependencies.State('tab5-PelletThickness', 'value'), dash.dependencies.State('tab5-MatDensity', 'value'),
     dash.dependencies.State('tab5-AxisNyquist', 'value'), dash.dependencies.State('tab5-IonCon', 'value'),
     dash.dependencies.State('tab5-ECon', 'value'), dash.dependencies.State('tab5-User', 'value')])
def update_output(n_clicks, PLN5, TestCon5, TestTemp5, Mass5, PluNo5, PluT5, PelT5, MatDensity5, AxisNyquest5, IonCon5, Econ5, User5):
    if User5 and PLN5 and TestCon5 and TestTemp5 and Mass5 and PluNo5 and PluT5 and PelT5 and MatDensity5 and AxisNyquest5 and IonCon5 and Econ5 is not None:
        #if PLN5 is not None and TestCon5 is not None and TestTemp5 is not None and Mass5 is not None and PluNo5 is not None and PluT5 is not None and PelT5 is not None and MatDensity5 is not None and AxisNyquest5 is not None and IonCon5 is not None and Econ5 is not None:
        # print("hello")
        print(PLN5, TestCon5, TestTemp5, Mass5, PluNo5, PluT5, PelT5, MatDensity5, AxisNyquest5, IonCon5, Econ5, User5)

        engine = create_engine("mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
        print('tab 5 callback')
        connection = engine.raw_connection()
        print('connection succeeded')
        try:
            connection = engine.raw_connection()
            print('connection succeeded')
            cursor = connection.cursor()
            print('cursor succeeded')
            querystring = 'exec dbo.insertProdConductivity @UserOpID = \'{}\', @ProductLN = \'{}\',@TestCondition = \'{}\', @TestTemperature = \'{}\', @MaterialMass_g = \'{}\', @PlungerLengthNoPellet_mm = \'{}\', @PelletPlungerThickness_mm = \'{}\', @MaterialDensity_gcc = \'{}\', @XAxisNyquist_ohms = \'{}\', @PelletThickness_cm = \'{}\', @IonicConductivity_Scm = \'{}\', @ElectronicConductivity_Scm = \'{}\''.format(
                User5, PLN5, TestCon5, TestTemp5, Mass5, PluNo5, PluT5, PelT5, MatDensity5, AxisNyquest5, IonCon5, Econ5)
            print(querystring)
            cursor.execute(querystring)
            print('querystring is: ')
            print(querystring)
            #ExistingOGLots = cursor.fetchall()  # note: no comma
            print('results are: ')
            #print(type(ExistingOGLots))
            cursor.close()
            connection.commit()
        except Exception as e:
            print('there was an exception that occurred. This will need to be looked at more')
            print(e)
            connection.close()
            return '---error5 : {}'.format(e)
        finally:
            connection.close()

        engine.dispose()

        return u'This is the returned value - text TBD'.format() #replace if theres anything to be returned
    else:
        raise PreventUpdate

# -------------------- Tab6 Callback ---------------------- #
@app.callback(
    dash.dependencies.Output('tab6_container_button', 'children'),
    [dash.dependencies.Input('submit-Tab6', 'n_clicks')],
    [dash.dependencies.State('tab6-PLN', 'value'), dash.dependencies.State('tab6-StartDate', 'value'),
     dash.dependencies.State('tab6-FinishDate', 'value'), dash.dependencies.State('tab6-Quantity', 'value'),
     dash.dependencies.State('tab6-MRB', 'value'), dash.dependencies.State('tab6-Conductivity', 'value'),
     dash.dependencies.State('tab6-Yield', 'value'), dash.dependencies.State('tab6-PassFail', 'value'),
     dash.dependencies.State('tab6-ECon', 'value'), dash.dependencies.State('tab6-GuseGen', 'value'),
     dash.dependencies.State('tab6-User', 'value')])
def update_output(n_clicks, PLN6, StartDate6, FinishDate6, Quantity6, MRB6, Conductivity6, Yield6, PassFail6, ECon6, GuseGen, User6):
    if PLN6 and StartDate6 and FinishDate6 and Quantity6 and MRB6 and Conductivity6 and Yield6 and PassFail6 and ECon6 and GuseGen and User6 is not None:

        print(PLN6, StartDate6, FinishDate6, Quantity6, MRB6, Conductivity6, Yield6, PassFail6, ECon6, GuseGen, User6)

        engine = create_engine("mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
        print('tab 5 callback')
        connection = engine.raw_connection()
        print('connection succeeded')
        try:
            connection = engine.raw_connection()
            print('connection succeeded')
            cursor = connection.cursor()
            print('cursor succeeded')
            querystring = 'exec dbo.insertConsumedBatch @UserOpID = \'{}\', @ProductLN = \'{}\',@startdate = \'{}\', @finishdate = \'{}\', @Quantity_g = \'{}\', @MRB = \'{}\', @Conductivity_mScm = \'{}\', @Yield = \'{}\', @PassFail = \'{}\', @ElectronicConductivity= \'{}\', @GuseGen = \'{}\''.format(
                User6, PLN6, StartDate6, FinishDate6, Quantity6, MRB6, Conductivity6, Yield6, PassFail6, ECon6, GuseGen)
            print(querystring)
            cursor.execute(querystring)
            print('querystring is: ')
            print(querystring)
            #ExistingOGLots = cursor.fetchall()  # note: no comma
            print('results are: ')
            #print(type(ExistingOGLots))
            cursor.close()
            connection.commit()
        except Exception as e:
            print('there was an exception that occurred. This will need to be looked at more')
            print(e)
            connection.close()
            return '---error6 : {}'.format(e)
        finally:
            connection.close()

        engine.dispose()

        return u'This is the returned value - text TBD'.format() #replace if theres anything to be returned
    else:
        raise PreventUpdate

# -------------------- Tab7 Callback ---------------------- #
@app.callback(
    dash.dependencies.Output('tab7_container_button', 'children'),
    [dash.dependencies.Input('submit-Tab7', 'n_clicks')],
    [dash.dependencies.State('tab7-ILN', 'value'), dash.dependencies.State('tab7-PPN', 'value'),
     dash.dependencies.State('tab7-Item', 'value'), dash.dependencies.State('tab7-SupplierName', 'value'),
     dash.dependencies.State('tab7-SupplierLN', 'value'), dash.dependencies.State('tab7-KSolid', 'value'),
     dash.dependencies.State('tab7-User', 'value')])
def update_output(n_clicks, ILN7, PLN7, Item7, SupplierName7, SupplierLN7, KSolid7, User7):
    if ILN7 and PLN7 and Item7 and SupplierName7 and SupplierLN7 and KSolid7 and User7 is not None:

        print(ILN7, PLN7, Item7, SupplierName7, SupplierLN7, KSolid7, User7)

        engine = create_engine("mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
        print('tab 5 callback')
        connection = engine.raw_connection()
        print('connection succeeded')
        try:
            connection = engine.raw_connection()
            print('connection succeeded')
            cursor = connection.cursor()
            print('cursor succeeded')
            querystring = 'exec dbo.insertDrying @UserOpID = \'{}\', @IngredientLN = \'{}\', @ProductPN = \'{}\', @Item = \'{}\', @Supplier = \'{}\', @SupplierLorNr = \'{}\', @KSolidMoisture_ppm = \'{}\''.format(
                User7, ILN7, PLN7, Item7, SupplierName7, SupplierLN7, KSolid7)
            print(querystring)
            cursor.execute(querystring)
            print('querystring is: ')
            print(querystring)
            #ExistingOGLots = cursor.fetchall()  # note: no comma
            print('results are: ')
            #print(type(ExistingOGLots))
            cursor.close()
            connection.commit()
        except Exception as e:
            print('there was an exception that occurred. This will need to be looked at more')
            print(e)
            connection.close()
            return '---error7 : {}'.format(e)
        finally:
            connection.close()

        engine.dispose()

        return u'This is the returned value - text TBD'.format() #replace if theres anything to be returned
    else:
        raise PreventUpdate



if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0', port=8050)