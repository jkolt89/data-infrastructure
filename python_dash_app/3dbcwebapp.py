# -*- coding: utf-8 -*-


import dash
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import flask
import sys
import sqlalchemy
import datetime
from sqlalchemy import create_engine
from dash.exceptions import PreventUpdate


sys.path.append('/home/solidpower')
sys.path.append('/home/solidpower/dash_app')

external_stylesheets = [dbc.themes.BOOTSTRAP]
# external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
# external_stylesheets = ['https://codepen.io/chriddyp/pen/dZVMbK.css']
server = flask.Flask(__name__)  # define flask app.server
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# run the following cmdln: gunicorn 3dbcwebapp:app.server -b :8000

# 	Written by Hannah - last update 2/10/2021


GOOGLE = ''
DBC_GITHUB = "https://github.com/facultyai/dash-bootstrap-components"

navigation_bar = dbc.NavbarSimple(
    children=[
        dbc.NavItem(dbc.NavLink("", href=DBC_GITHUB)),
        dbc.DropdownMenu(
        ),
    ],
    brand="Solid Power Webapp",
    brand_href=GOOGLE,
    sticky="top",
)




# ------ Tab 1 (one) - Lot Number Generator ------------------------------------------------------------------
tab1labelwidth = 5
tab1entrywidth = 11
tab1 = dbc.Card([
    dbc.CardBody([
        dbc.Form([
            html.H2('Lot Number Generator'),
            html.Br(),
            html.Br(),

            dbc.FormGroup([
                dbc.Label('User ID:', width=tab1labelwidth),
                dbc.Col(
                    dbc.Input(id='input-on-submit3', type='text', placeholder="Employee ID Number"),
                ),
            ], row=True),


            dbc.FormGroup([
                dbc.Label('Please select your lot number generation location:', width=tab1labelwidth),
                dbc.Col(
                    dcc.Dropdown(
                        id='LNGL_Dropdown_Chosen',
                        options=[
                            {'label': '10 - Raw Material', 'value': '10'},
                            {'label': '11 - Bear Q-6 (Colt)', 'value': '11'},
                            {'label': '12 - Open', 'value': '12'},
                            {'label': '13 - Open', 'value': '13'},
                            {'label': '14 - Cathode Slurry', 'value': '14'},
                            {'label': '15 - Separator Slurry', 'value': '15'},
                            {'label': '16 - Anode Slurry', 'value': '16'},
                            {'label': '17 - Bear S1-1', 'value': '17'},
                            {'label': '18 - Bear S1-2', 'value': '18'},
                            {'label': '19 - Bear S1-3', 'value': '19'},
                            {'label': '20 - Duke Open', 'value': '20'},
                            {'label': '21 - Duke Open', 'value': '21'},
                            {'label': '22 - Duke Open', 'value': '22'},
                            {'label': '23 - Duke S1-1', 'value': '23'},
                            {'label': '24 - Duke S1-2', 'value': '24'},
                            {'label': '25 - Duke S1-3', 'value': '25'},
                            {'label': '26 - Duke Open', 'value': '26'},
                            {'label': '27 - Duke HD', 'value': '27'},
                            {'label': '28 - Duke HDDM', 'value': '28'},
                            {'label': '29 - NMC Coating/Surface Mod', 'value': '29'},
                            {'label': '30 - Guse', 'value': '30'},
                            {'label': '31 - Guse HDDM', 'value': '31'},
                            {'label': '32 - Guse DMQX', 'value': '32'},
                            {'label': '33 - Guse C-3', 'value': '33'},
                            {'label': '34 - Guse Trey', 'value': '34'},
                            {'label': '35 - Guse HQ', 'value': '35'},
                            {'label': '36 - Guse Colt', 'value': '36'},
                            {'label': '37 - Guse Ace', 'value': '37'},
                            {'label': '38 - Guse Deuce', 'value': '38'},
                            {'label': '39 - Guse Open', 'value': '39'},
                            {'label': '40 - Ibis Open', 'value': '40'},
                            {'label': '41 - Ibis Open', 'value': '41'},
                            {'label': '42 - Ibis Open', 'value': '42'},
                            {'label': '43 - Ibis Open', 'value': '43'},
                            {'label': '44 - Ibis Open', 'value': '44'},
                            {'label': '45 - Ibis Open', 'value': '45'},
                            {'label': '46 - Ibis Open', 'value': '46'},
                            {'label': '47 - Ibis Open', 'value': '47'},
                            {'label': '48 - Ibis Open', 'value': '48'},
                            {'label': '49 - Ibis Open', 'value': '19'},
                            {'label': '50 - Cathode Coating T/B', 'value': '50'},
                            {'label': '51 - Cathode Coating T Only', 'value': '51'},
                            {'label': '52 - Cathode Coating B Only', 'value': '52'},
                            {'label': '53 - Separator 0.22 Al', 'value': '53'},
                            {'label': '54 - Separator 0.15 Al', 'value': '54'},
                            {'label': '55 - Separator PET', 'value': '55'},
                            {'label': '56 - Mock Electrolyte', 'value': '56'},
                            {'label': '57 - Open coating', 'value': '57'},
                            {'label': '58 - Open coating', 'value': '58'},
                            {'label': '59 - Cut and Tape Cathode', 'value': '59'},
                            {'label': '60 - Length Cut Cathode', 'value': '60'},
                            {'label': '61 - Cathode', 'value': '61'},
                            {'label': '62 - Anode', 'value': '62'},
                            {'label': '63 - Guse Separator Slurry', 'value': '63'},
                            {'label': '64 - Guse Cathode Separator', 'value': '64'},
                            {'label': '65 - Guse Cathode Slurry', 'value': '65'},
                            {'label': '66 - Guse  Coated Cathode', 'value': '66'},
                            {'label': '67 - Guse Cathode', 'value': '67'},
                            {'label': '68 - Guse Cathode Calendar', 'value': '68'},
                            {'label': '69 - Guse Laminated Cathode', 'value': '69'},
                            {'label': '70 - Bear Separator Slurry', 'value': '70'},
                            {'label': '71 - Bear Cathode Separator', 'value': '71'},
                            {'label': '72 - Cut Stack', 'value': '72'},
                            {'label': '73 - Bear Coated Cathode', 'value': '73'},
                            {'label': '74 - Bear Cathode', 'value': '74'},
                            {'label': '75 - Bear Anode', 'value': '75'},
                            {'label': '76 - Bear Andoe Separator', 'value': '76'},
                            {'label': '77 - Slit Separator', 'value': '77'},
                            {'label': '78 - Laminated Anode', 'value': '78'},
                            {'label': '79 - Cut Anode', 'value': '79'},
                            {'label': '80 - 2Ah Final Assembly', 'value': '80'},
                            {'label': '81 - 2Ah Rig', 'value': '81'},
                            {'label': '82 - 2Ah Cell', 'value': '82'},
                            {'label': '85 - 20Ah Final Assembly', 'value': '85'},
                            {'label': '86 - 20Ah Rig', 'value': '86'},
                            {'label': '87 - 20Ah Cell', 'value': '87'},
                            {'label': '88 - Cut Anode', 'value': '88'},
                            {'label': 'Combined Bear', 'value': 'CB'},
                            {'label': 'Combined Duke', 'value': 'CD'},
                            {'label': 'Combined Guse', 'value': 'CG'}
                        ],
                        placeholder="Lot Number Generation Location",
                    ),
                ),
            ], row=True),

            dbc.FormGroup([
                dbc.Label('Please select your material generation:', width=tab1labelwidth),
                dbc.Col(
                    dbc.Input(id='input-on-submit4', type='text', placeholder="Material Generation Number"),
                ),
            ], row=True),

            html.Br(),
            dbc.Button('Submit', id='submit-Tab1', n_clicks=0, color='primary'),
            html.Br(),
            html.Br(),
            html.H6(id='tab1_container_button', children='Please only click submit once'),
        ]),   # ( ([ ([ opposing from div, children, and form respectively
    ])
])







# ------ Tab 2 (two) - Conductivity Data Entry Form ------------------------------------------------------------------
tab2labelwidth = 3
tab2entrywidth = 5
tab2 = dbc.Card([
    dbc.CardBody([
        dbc.Form([
            html.H2('Split Number Generator'),
            html.Br(),
            html.Br(),

            dbc.FormGroup([
                dbc.Label('User ID:', width=tab2labelwidth),
                dbc.Col(
                    dbc.Input(id='tab2-employeeNum', type='text', placeholder="Employee ID Number"),
                ),
            ], row=True),

            dbc.FormGroup([
                dbc.Label('Please enter your Split Letter:', width=tab2labelwidth),
                dbc.Col(
                    dcc.Dropdown(
                        id='tab2-splitNum',
                        options=[
                            {'label': 'A', 'value': 'A'},
                            {'label': 'B', 'value': 'B'},
                            {'label': 'C', 'value': 'C'},
                            {'label': 'D', 'value': 'D'},
                            {'label': 'E', 'value': 'E'},
                            {'label': 'F', 'value': 'F'},
                            {'label': 'G', 'value': 'G'},
                            {'label': 'H', 'value': 'H'},
                            {'label': 'I', 'value': 'I'},
                            {'label': 'J', 'value': 'J'},
                            {'label': 'K', 'value': 'K'},
                            {'label': 'L', 'value': 'L'},
                            {'label': 'M', 'value': 'M'},
                            {'label': 'N', 'value': 'N'},
                            {'label': 'O', 'value': 'O'},
                            {'label': 'P', 'value': 'P'},
                            {'label': 'Q', 'value': 'Q'},
                            {'label': 'R', 'value': 'R'},
                            {'label': 'S', 'value': 'S'},
                            {'label': 'T', 'value': 'T'},
                            {'label': 'U', 'value': 'U'},
                            {'label': 'V', 'value': 'V'},
                            {'label': 'W', 'value': 'W'},
                            {'label': 'X', 'value': 'X'},
                            {'label': 'Y', 'value': 'Y'},
                            {'label': 'Z', 'value': 'Z'}
                        ],
                        placeholder="Select Split Letter",
                    ),
                ),
            ], row=True),

            dbc.FormGroup([
                dbc.Label('Please enter your original lot number:', width=tab2labelwidth),
                dbc.Col(
                    dbc.Input(id='tab2-lotNum', type='text', placeholder="Original lot number"),
                ),
            ], row=True),

            html.Br(),
            dbc.Button('Submit', id='submit-Tab2', n_clicks=0, color='primary'),
            html.Br(),
            html.Br(),
            html.H6(id='tab2_container_button', children='Please only click submit once'),
        ]),   # ( ([ ([ opposing from div, children, and form respectively
    ])
])






##################     Tab 3

# ------ Tab 3 (three) - Part Number Generator ------------------------------------------------------------------
tab3labelwidth = 3
tab3entrywidth = 11
tab3 = dbc.Card([
    dbc.CardBody([
        dbc.Form([
            html.H2('Part Number Generator'),
            html.Br(),

            dbc.FormGroup([
                dbc.Label('Material Generation:', width=tab3labelwidth),
                dbc.Col(
                    dcc.Dropdown(
                        id='Generation_Selection',
                        options=[
                            {'label': 'Gen 1', 'value': '1'},
                            {'label': 'Gen 2', 'value': '2'},
                            {'label': 'Gen 3', 'value': '3'}
                        ],
                        placeholder="Select Material Generation",
                    ),
                ),
            ], row=True),


            dbc.FormGroup([
                dbc.Label('Material:', width=tab3labelwidth),
                dbc.Col(
                    dcc.Dropdown(
                        id='Material_Selection',
                        options=[
                            {'label': 'Anode Stack', 'value': 'AA'},
                            {'label': 'Acetonitrile', 'value': 'AC'},
                            {'label': 'Anode', 'value': 'AD'},
                            {'label': 'Assembly Final', 'value': 'AF'},
                            {'label': 'Aluminum Foil', 'value': 'AL'},
                            {'label': 'AL Foil Carbon Coated', 'value': 'AM'},
                            {'label': 'Argon', 'value': 'AR'},
                            {'label': 'Anode Slurry', 'value': 'AS'},
                            {'label': 'Acetone', 'value': 'AT'},
                            {'label': 'Bromide Agardite', 'value': 'BA'},
                            {'label': '2 Ah Cell', 'value': 'BB'},
                            {'label': '20 Ah Cell', 'value': 'BC'},
                            {'label': '200 mAh Cell ', 'value': 'BD'},
                            {'label': 'Bear Electrolyte', 'value': 'BE'},
                            {'label': 'Bear Cathode', 'value': 'BF'},
                            {'label': '2Ah Rig Assembly', 'value': 'BG'},
                            {'label': 'Bear Coated Cathode', 'value': 'BH'},
                            {'label': 'Bear Separator Slurry', 'value': 'BI'},
                            {'label': 'Bear Anode Separator', 'value': 'BJ'},
                            {'label': 'Bear Anode', 'value': 'BK'},
                            {'label': 'Bear', 'value': 'BR'},
                            {'label': 'Bear Cathode Separator', 'value': 'BS'},
                            {'label': 'Bear Weighting', 'value': 'BW'},
                            {'label': 'Chloride Agardite', 'value': 'CA'},
                            {'label': 'Cell Stack', 'value': 'CB'},
                            {'label': 'Cathode Coating', 'value': 'CC'},
                            {'label': 'Cathode', 'value': 'CD'},
                            {'label': 'Guse', 'value': 'CE'},
                            {'label': 'Guse Cathode Cut', 'value': 'CF'},
                            {'label': 'Conductive Graphite', 'value': 'CG'},
                            {'label': 'Cut and Tape Cathode', 'value': 'CH'},
                            {'label': 'Guse Length Cut Cathode', 'value': 'CI'},
                            {'label': 'Cathode Calendar', 'value': 'CL'},
                            {'label': 'Guse Laminated Cathode', 'value': 'CM'},
                            {'label': 'Coated NMC', 'value': 'CN'},
                            {'label': 'Carbon', 'value': 'CO'},
                            {'label': 'Cathode Separator', 'value': 'CR'},
                            {'label': 'Cathode Slurry', 'value': 'CS'},
                            {'label': 'Cathode Stack', 'value': 'CT'},
                            {'label': 'DI Argyrodite', 'value': 'DA'},
                            {'label': 'Duke Electrolyte', 'value': 'DE'},
                            {'label': 'Diaper', 'value': 'DP'},
                            {'label': 'EtOH', 'value': 'EH'},
                            {'label': 'Electrolyte', 'value': 'EL'},
                            {'label': 'Ethyl Propionate', 'value': 'EP'},
                            {'label': 'Electrolyte Slurry', 'value': 'ES'},
                            {'label': 'Fixture Base', 'value': 'FA'},
                            {'label': 'Silicone Cutout', 'value': 'FB'},
                            {'label': 'Flat Washer 1/4', 'value': 'FC'},
                            {'label': 'Fixture Cutout', 'value': 'FD'},
                            {'label': 'Beleville Washer', 'value': 'FE'},
                            {'label': 'Hex Cap 1/4', 'value': 'FF'},
                            {'label': 'Flange Nut 1/4', 'value': 'FG'},
                            {'label': 'Ni Tab', 'value': 'FH'},
                            {'label': 'AL Tab', 'value': 'FI'},
                            {'label': 'Pouch Film', 'value': 'FJ'},
                            {'label': 'Tab Protector Tape', 'value': 'FK'},
                            {'label': 'Plastic Sheet', 'value': 'FL'},
                            {'label': 'Solid Power Sticker', 'value': 'FM'},
                            {'label': 'Rig Label', 'value': 'FN'},
                            {'label': 'Anodizing Tape', 'value': 'FO'},
                            {'label': 'Guse Anode', 'value': 'GA'},
                            {'label': 'Guse Cathode', 'value': 'GC'},
                            {'label': 'Guse Cathode Slurry', 'value': 'GG'},
                            {'label': 'Guse Coated Cathode', 'value': 'GH'},
                            {'label': 'Guse Separator Slurry', 'value': 'GI'},
                            {'label': 'Guse Separator', 'value': 'GS'},
                            {'label': 'HP Etoh', 'value': 'HP'},
                            {'label': 'IBIB', 'value': 'IB'},
                            {'label': 'Ibis', 'value': 'IE'},
                            {'label': 'Isopropyl Alcohol', 'value': 'IP'},
                            {'label': 'Lithium Bromide', 'value': 'LB'},
                            {'label': 'Lithium Chloride', 'value': 'LC'},
                            {'label': 'Lithium Foil', 'value': 'LF'},
                            {'label': 'Lithium Chips', 'value': 'LI'},
                            {'label': 'Lithium Iodine', 'value': 'LL'},
                            {'label': 'LPS', 'value': 'LP'},
                            {'label': 'Lithium Sulfide', 'value': 'LS'},
                            {'label': 'Masking Tape', 'value': 'MA'},
                            {'label': 'Molecular Sieves', 'value': 'MS'},
                            {'label': 'NB EtOH', 'value': 'NB'},
                            {'label': 'NMC', 'value': 'NC'},
                            {'label': 'Heat Treated NMC', 'value': 'NT'},
                            {'label': 'Octane', 'value': 'OC'},
                            {'label': 'PST-EB', 'value': 'PE'},
                            {'label': 'PVDF', 'value': 'PF'},
                            {'label': 'Pouch', 'value': 'PH'},
                            {'label': 'PAPI', 'value': 'PL'},
                            {'label': 'Phosphorous Pentasulfide', 'value': 'PP'},
                            {'label': 'Cell Rig', 'value': 'RC'},
                            {'label': 'Silicon Final Assembly', 'value': 'SF'},
                            {'label': 'Stiffener', 'value': 'SN'},
                            {'label': 'SLMP', 'value': 'SP'},
                            {'label': 'Separator Coated', 'value': 'SR'},
                            {'label': 'Separator Slurry', 'value': 'SS'},
                            {'label': 'Tab', 'value': 'TB'},
                            {'label': 'Carbon VGCF', 'value': 'VF'},
                            {'label': 'Waste Material', 'value': 'WM'},
                            {'label': 'DMQX Waste', 'value': 'WQ'},
                            {'label': 'Xylene', 'value': 'XY'}
                        ],
                        placeholder="Select Material",
                    ),
                ),
            ], row=True),

            dbc.FormGroup([
                dbc.Label('Material Form:', width=tab3labelwidth),
                dbc.Col(
                    dcc.Dropdown(
                        id='MaterialForm_Selection',
                        options=[
                            {'label': '1 - Liquid', 'value': '1'},
                            {'label': '2 - Solid', 'value': '2'},
                            {'label': '3 - Powder', 'value': '3'},
                            {'label': '4 - Mixture', 'value': '4'},
                            {'label': '5 - Gas', 'value': '5'},
                            {'label': '6 - Roll', 'value': '6'},
                            {'label': '7 - Coated', 'value': '7'},
                            {'label': '8 - Calendered', 'value': '8'},
                            {'label': '9 - Cell', 'value': '9'},
                            {'label': 'A - Battery', 'value': 'A'},
                            {'label': 'B - Stack', 'value': 'B'},
                            {'label': 'C - Rig Build', 'value': 'C'}
                        ],
                        # value=[],
                        placeholder="Select Material Form",
                    ),
                ),
            ], row=True),

            dbc.FormGroup([
                dbc.Label('Process Location:', width=tab3labelwidth),
                dbc.Col(
                    dcc.Dropdown(
                        id='ProcLoc_Selection',
                        options=[
                            {'label': '1 - Raw Material', 'value': '1'},
                            {'label': '2 - Internal Synthesis', 'value': '2'},
                            {'label': '3 - Surface Modification', 'value': '3'},
                            {'label': '4 - Slurry Mix', 'value': '4'},
                            {'label': '5 - Cell Production', 'value': '5'},
                            {'label': '6 - Coating', 'value': '6'},
                            {'label': '7 - Pre-Crystallization', 'value': '7'}
                        ],
                        # value=[],
                        placeholder="Select Process Location",
                    ),
                )], row=True),


            dbc.FormGroup([
                dbc.Label('Material Description:', width=tab3labelwidth),
                dbc.Col(
                    dcc.Dropdown(
                        id='MatDesc_Selection',
                        options=[
                            {'label': 'No Description', 'value': '0000'},
                            {'label': 'D90 < 1 um', 'value': '0001'},
                            {'label': '0.1 < D50 < 0.8 um', 'value': '0002'},
                            {'label': '2.0 < D50 < 3.0 um', 'value': '0003'},
                            {'label': '3.0 < D50 < 4.0 um', 'value': '0004'},
                            {'label': '1.0 < D50 < 4.0 um', 'value': '0005'},
                            {'label': '2.0 < D50 < 6.0 um', 'value': '0006'},
                            {'label': 'For Cleaning Only', 'value': '0007'},
                            {'label': '0.38 Thickness', 'value': '0008'},
                            {'label': 'D90 < 10 um', 'value': '0011'},
                            {'label': 'Name of a cell shipped 9/10  Uday', 'value': '0013'},
                            {'label': 'Carbon KS-6', 'value': '0014'},
                            {'label': 'Carbon C65', 'value': '0015'},
                            {'label': '90mm X 0.035 microns', 'value': '0018'},
                            {'label': 'Raw Material that requires Drying', 'value': '0019'},
                            {'label': '2Ah Cell', 'value': '0020'},
                            {'label': '20Ah Cell', 'value': '0021'},
                            {'label': '200 mAh Cell', 'value': '0022'},
                            {'label': '6% Bear', 'value': '0023'},
                            {'label': '14% Bear', 'value': '0024'},
                            {'label': 'H18 20um X 150mm', 'value': '0027'},
                            {'label': 'H18 20um X 250mm', 'value': '0028'},
                            {'label': '0.12mm', 'value': '0029'},
                            {'label': '50mm X 1.1m', 'value': '0030'},
                            {'label': '210mm X 0.85m', 'value': '0031'},
                            {'label': '150 X 12mm', 'value': '0032'},
                            {'label': '250 X 12mm', 'value': '0033'},
                            {'label': 'Slit Separator', 'value': '0034'},
                            {'label': 'Laminated Anode', 'value': '0035'},
                            {'label': 'Cut Anode', 'value': '0036'},
                            {'label': '25 mm', 'value': '0037'},
                            {'label': '90 mm', 'value': '0038'}
                        ],
                        # value=[],
                        placeholder="Select Material Description",
                    ),
                )], row=True),

            html.Br(),
            dbc.Button('Submit', id='submit-Tab3', n_clicks=0, color='primary'),
            html.Br(),
            html.Br(),
            html.H6(id='tab3_container_button', children='Please only click submit once'),
        ]),
    ])
])









# ------ Tab 4 (four) - Combined Batch Record Data Entry Form ------------------------------------------------------------------
tab4labelwidth = 3
tab4entrywidth = 11

tab4 = dbc.Card([
    dbc.CardBody([
        dbc.Form([
            html.H2('Combined Batch Record Form'),
            html.H6("please use the lot number generator to create a combined lot number first"),
            html.Br(),

            dbc.FormGroup([
                dbc.Label('User ID:', width=tab4labelwidth),
                dbc.Col(
                    dbc.Input(id='tab4-EmployeeNum', type='text', placeholder="User ID #"),
                ),
            ], row=True),

            dbc.FormGroup([
                dbc.Label('Combined Lot Number:', width=tab4labelwidth),
                dbc.Col(
                    dbc.Input(id='tab4-CombinedLotNum', type='text', placeholder="Combined Lot Number"),
                ),
            ], row=True),

            dbc.FormGroup([
                dbc.Label('Weight (g):', width=tab4labelwidth),
                dbc.Col(
                    dbc.Input(id='tab4-StartWeight', type='text', placeholder="Weight (grams)"),
                ),
            ], row=True),

            dbc.FormGroup([
                dbc.Label('Original Lot Number:', width=tab4labelwidth),
                dbc.Col(
                    dbc.Input(id='tab4-OriginalLotNum', type='text', placeholder="Original lot #"),
                ),
            ], row=True),
            html.Br(),
            html.H6('Please re-run this application for each original lot number in your combined batch. We will fix this in a future version of the app.'),

            html.Br(),
            dbc.Button('Submit', id='submit-Tab4', n_clicks=0, color='primary'),
            html.Br(),
            html.Br(),
            html.H6(id='tab4_container_button', children='Please only click submit once'),
        ]),   # ( ([ ([ opposing from div, children, and form respectively
    ])
])










# ------ Tab 5 (five) - Conductivity Data Entry Form ------------------------------------------------------------------
tab5labelwidth = 8
tab5entrywidth = 11
tab5 = dbc.Card([
    dbc.CardBody([
        dbc.Form([
            html.H2('Conductivity Data Entry Form'),
            html.Br(),

            dbc.Row(
                [
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('User ID', width=4),
                            dbc.Col(
                                dbc.Input(id='tab5-User', type='text', placeholder="User ID Number"),
                            ),
                        ]), width=3,
                    ),
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('Lot Number:', width=6),
                            dbc.Col(
                                dbc.Input(id='tab5-PLN', type='text', placeholder="Product Lot Number"),
                            ),
                        ]), width=6,
                    ),
                ],
                form=True,
            ),

            dbc.Row(
                [
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('Material Mass?:', width=6),
                            dbc.Col(
                                dbc.Input(id='tab5-mass', type='text', placeholder="Material Mass?"),
                            ),
                        ]), width=5,
                    ),

                ],
                form=True,
            ),

            dbc.Row(
                [
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('Ionic Intercept:', width=6),
                            dbc.Col(
                                dbc.Input(id='tab5-ionicInt', type='text', placeholder="Ionic Intercept"),
                            ),
                        ]), width=5,
                    ),
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('Electronic Intercept:', width=6),
                            dbc.Col(
                                dbc.Input(id='tab5-electricInt', type='text', placeholder="Electronic Intercept"),
                            ),
                        ]), width=6,
                    ),
                ],
                form=True,
            ),

            dbc.Row(
                [
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('Length Total?:', width=6),
                            dbc.Col(
                                dbc.Input(id='tab5-lenTotal', type='text', placeholder="Length total????"),
                            ),
                        ]), width=5,
                    ),
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('Length Tare:', width=6),
                            dbc.Col(
                                dbc.Input(id='tab5-lenTare', type='text', placeholder="Length Tare????"),
                            ),
                        ]), width=6,
                    ),
                ],
                form=True,
            ),
            dbc.Row([
                dbc.Col([
                    dbc.FormGroup([
                        dbc.Button('Calculate', id='submit-Tab5-calcs', n_clicks=0, color='success'),
                        html.H6(id='tab5_calcs_container_button', children='Calculate conductivity and density'),
                    ])
                ])
            ]),

            html.Br(),

            dbc.Row(
                [
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('Ionic Conductivity:', width=tab5labelwidth),
                            dbc.Col(
                                dbc.Input(id='tab5-IonCon', type='text', placeholder="Ionic Conductivity"),
                            ),
                        ]), width=4,
                    ),
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('Electronic Conductivity:', width=tab5labelwidth),
                            dbc.Col(
                                dbc.Input(id='tab5-ECon', type='text', placeholder="Electronic Conductivity"),
                            ),
                        ]), width=4,
                    ),
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('Material Density (gcc):', width=tab5labelwidth),
                            dbc.Col(
                                dbc.Input(id='tab5-MatDensity', type='text', placeholder="Material Density"),
                            ),
                        ]), width=4,
                    ),
                ],
                form=True,
            ),


            dbc.FormGroup([
                dbc.Label('Test Condition:', width=tab5labelwidth),
                dbc.Col(
                    dbc.Input(id='tab5-TestCon', type='text', placeholder="Test Condition"),
                ),
            ], ),

            dbc.FormGroup([
                dbc.Label('Test Temperature:', width=tab5labelwidth),
                dbc.Col(
                    dbc.Input(id='tab5-TestTemp', type='text', placeholder="Test Temperature"),
                ),
            ], ),

            dbc.FormGroup([
                dbc.Label('Mass (grams):', width=tab5labelwidth),
                dbc.Col(
                    dbc.Input(id='tab5-Mass', type='text', placeholder="Material Mass"),
                ),
            ], ),

            dbc.FormGroup([
                dbc.Label('Plunger Length Number:', width=tab5labelwidth),
                dbc.Col(
                    dbc.Input(id='tab5-PlungerLNo', type='text', placeholder="Plunger Length number"),
                ),
            ], ),

            dbc.FormGroup([
                dbc.Label('Plunger Thickness (mm):', width=tab5labelwidth),
                dbc.Col(
                    dbc.Input(id='tab5-PlungerThickness', type='text', placeholder="Plunger Thickness"),
                ),
            ], ),

            dbc.FormGroup([
                dbc.Label('Pellet Thickness (cm):', width=tab5labelwidth),
                dbc.Col(
                    dbc.Input(id='tab5-PelletThickness', type='text', placeholder="Pellet Thickness"),
                ),
            ], ),



            dbc.FormGroup([
                dbc.Label('Axis Nyquist (ohms):', width=tab5labelwidth),
                dbc.Col(
                    dbc.Input(id='tab5-AxisNyquist', type='text', placeholder="Axis Nyquist"),
                ),
            ], ),



            # TODO:
            # Input box for notes will go here someday... # @Notes

            html.Br(),
            dbc.Button('Submit', id='submit-Tab5', n_clicks=0, color='primary'),
            html.Br(),
            html.Br(),
            html.H6(id='tab5_container_button', children='Please only click submit once'),
        ]),   # ( ([ ([ opposing from div, children, and form respectively
    ])
])









# ------ Tab 6 (six) - Consumed Batch Entry Form -----------------------------------------------------------------------
tab6labelwidth = 3
tab6entrywidth = 11
tab6 = dbc.Card([
    dbc.CardBody([
        dbc.Form([
            html.H2('Consumed Batch Data Entry Form'),
            html.Br(),


            dbc.Row(
                [
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('User ID:', width=4),
                            dbc.Col(
                                dbc.Input(id='tab6-User', type='text', placeholder="User ID Number"),
                            ),
                        ]), width=3,
                    ),
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('MRB:', width=4),
                            dbc.Col(
                                dbc.Input(id='tab6-MRB', type='text', placeholder="MRB Number"),
                            ),
                        ]), width=3,
                    ),
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('Lot Number:', width=6),
                            dbc.Col(
                                dbc.Input(id='tab6-PLN', type='text', placeholder="Product Lot Number"),
                            ),
                        ]), width=6,
                    ),
                ],
                form=True,
            ),

            dbc.Row(
                [
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('Start Month:', width=6),
                            dbc.Col(
                                dcc.Dropdown(
                                    id='tab6-StartMonth',
                                    options=[
                                        {'label': 'January', 'value': '1'},
                                        {'label': 'February', 'value': '2'},
                                        {'label': 'March', 'value': '3'},
                                        {'label': 'April', 'value': '4'},
                                        {'label': 'May', 'value': '5'},
                                        {'label': 'June', 'value': '6'},
                                        {'label': 'July', 'value': '7'},
                                        {'label': 'August', 'value': '8'},
                                        {'label': 'September', 'value': '9'},
                                        {'label': 'October', 'value': '10'},
                                        {'label': 'November', 'value': '11'},
                                        {'label': 'December', 'value': '12'}
                                    ],
                                    placeholder="Select Start Month",
                                ),
                            ),
                        ]), width=4,
                    ),

                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('Start Day:', width=6),
                            dbc.Col(
                                dbc.Input(id='tab6-StartDay', type='text', placeholder="Start Day"),
                            ),
                        ]), width=4,
                    ),

                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('Start Year:', width=6),
                            dbc.Col(
                                dcc.Dropdown(
                                    id='tab6-StartYear',
                                    options=[
                                        {'label': '2020', 'value': '2020'},
                                        {'label': '2021', 'value': '2021'}

                                    ],
                                ),
                            )
                        ]),width=4,
                    ),
                ],
                form=True,
            ),

            dbc.Row(
                [
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('End Month:', width=6),
                            dbc.Col(
                                dcc.Dropdown(
                                    id='tab6-EndMonth',
                                    options=[
                                        {'label': 'January', 'value': '1'},
                                        {'label': 'February', 'value': '2'},
                                        {'label': 'March', 'value': '3'},
                                        {'label': 'April', 'value': '4'},
                                        {'label': 'May', 'value': '5'},
                                        {'label': 'June', 'value': '6'},
                                        {'label': 'July', 'value': '7'},
                                        {'label': 'August', 'value': '8'},
                                        {'label': 'September', 'value': '9'},
                                        {'label': 'October', 'value': '10'},
                                        {'label': 'November', 'value': '11'},
                                        {'label': 'December', 'value': '12'}
                                    ],
                                    placeholder="Select End Month",
                                ),
                            ),
                        ]), width=4,
                    ),

                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('End Day:', width=6),
                            dbc.Col(
                                dbc.Input(id='tab6-EndDay', type='text', placeholder="End Day"),
                            ),
                        ]), width=4,
                    ),

                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('End Year:', width=6),
                            dbc.Col(
                                dcc.Dropdown(
                                    id='tab6-EndYear',
                                    options=[
                                        {'label': '2020', 'value': '2020'},
                                        {'label': '2021', 'value': '2021'}

                                    ],
                                ),
                            )
                        ]), width=4,
                    ),
                ],
                form=True,
            ),


            dbc.Row(
                [
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('Quantity (g):', width=6),
                            dbc.Col(
                                dbc.Input(id='tab6-Quantity', type='text', placeholder="Quantity (grams)"),
                            ),
                        ]), width=5,
                    ),
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('Yield (g):', width=6),
                            dbc.Col(
                                dbc.Input(id='tab6-Yield', type='text', placeholder="Yield (grams)"),
                            ),
                        ]), width=6,
                    ),
                ],
                form=True,
            ),

            dbc.Row(
                [
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('Ionic Conductivity (mScm):', width=6),
                            dbc.Col(
                                dbc.Input(id='tab6-Conductivity', type='text', placeholder="Conductivity mScm"),
                            ),
                        ]), width=5,
                    ),
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('Electronic Conductivity (mScm):', width=6),
                            dbc.Col(
                                dbc.Input(id='tab6-ECon', type='text', placeholder="Electronic Conductivity"),
                            ),
                        ]), width=6,
                    ),
                ],
                form=True,
            ),


            dbc.Row(
                [
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('PSd10:', width=4),        # @PSd10 50 90
                            dbc.Col(
                                dbc.Input(id='tab6-psd10', type='text', placeholder="PSd10"),
                            ),
                        ]), width=4,
                    ),
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('PSd50:', width=4),
                            dbc.Col(
                                dbc.Input(id='tab6-psd50', type='text', placeholder="PSd50"),
                            ),
                        ]), width=4,
                    ),
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('PSd90:', width=4),
                            dbc.Col(
                                dbc.Input(id='tab6-psd90', type='text', placeholder="PSd90"),
                            ),
                        ]), width=4,
                    ),
                ],
                form=True,
            ),

            dbc.Row(
                [
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('RGB Red:', width=4),  # @RGBRed Green Blue
                            dbc.Col(
                                dbc.Input(id='tab6-rgbred', type='text', placeholder="RGB Red"),
                            ),
                        ]), width=4,
                    ),
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('RGB Green:', width=4),
                            dbc.Col(
                                dbc.Input(id='tab6-rgbgreen', type='text', placeholder="RGB Green"),
                            ),
                        ]), width=4,
                    ),
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('RGB Blue:', width=4),
                            dbc.Col(
                                dbc.Input(id='tab6-rgbblue', type='text', placeholder="RGB Blue"),
                            ),
                        ]), width=4,
                    ),
                ],
                form=True,
            ),


            dbc.Row(
                [
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('SSAm2g:', width=4),
                            dbc.Col(
                                dbc.Input(id='tab6-ssam2g', type='text', placeholder="SSAm2g"),
                            ),
                        ]), width=4,
                    ),
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('Material Generation:', width=6),
                            dbc.Col(
                                dcc.Dropdown(
                                    id='tab6-GuseGen',
                                    options=[
                                        {'label': 'Gen 1', 'value': '1'},
                                        {'label': 'Gen 2', 'value': '2'},
                                        {'label': 'Gen 3', 'value': '3'}
                                    ],
                                    placeholder="Select Material Generation",
                                )),
                        ]), width=4,
                    ),
                    dbc.Col(
                        dbc.FormGroup([
                            dbc.Label('Pass/Fail:', width=4),
                            dbc.Col(
                                dcc.Dropdown(
                                    id='tab6-PassFail',
                                    options=[
                                        {'label': 'Pass', 'value': 'Pass'},
                                        {'label': 'Fail', 'value': 'Fail'}
                                    ],
                                    placeholder="Select Pass or Fail",
                                ),),
                        ]), width=4,
                    ),
                ],
                form=True,
            ),

            # Input box for notes will go here someday... # @Notes

            html.Br(),
            dbc.Button('Submit', id='submit-Tab6', n_clicks=0, color='primary'),
            html.Br(),
            html.Br(),
            html.H6(id='tab6_container_button', children='Please only click submit once'),
        ]),   # ( ([ ([ opposing from div, children, and form respectively
    ])
])








# ------ Tab 7 (seven) - Material Drying Data Entry Form ---------------------------------------------------------------
tab7labelwidth = 9
tab7entrywidth = 11
tab7 = dbc.Card([
    dbc.CardBody([
        html.H2('Material Drying Data Entry Form'),
        html.Br(),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('User ID', width=4),
                        dbc.Col(
                            dbc.Input(id='tab7-User', type='text', placeholder="User ID Number"),
                        ),
                    ]), width=3,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Ingredient Lot Number', width=6),
                        dbc.Col(
                            dbc.Input(id='tab7-ILN', type='text', placeholder="Ingredient Lot Number"),
                        ),
                    ]), width=6,
                ),
            ],
            form=True,
        ),

        dbc.FormGroup([
            dbc.Label('Product Part Number', width=tab7labelwidth),  # @ProductPN
            dbc.Col(
                dbc.Input(id='tab7-PPN', type='text', placeholder="Product Part Number"),
                width=tab7entrywidth,
            ), ], ), #row=True),

        #       dbc.FormGroup([
        #          dbc.Label('Item - not sure what this is', width=tab7labelwidth),  # @ProductPN
        #         dbc.Col(
        #            dbc.Input(id='tab7-Item', type='text', placeholder="Item ?"),
        #           width=tab7entrywidth)],
        #      row=True),

        # TODO: dont need supplier name or lot number
        dbc.FormGroup([
            dbc.Label('Supplier Name', width=tab7labelwidth),  # @ProductPN
            dbc.Col(
                dbc.Input(id='tab7-SupplierName', type='text', placeholder="Supplier Name"),
            )], ), #row=True),

        dbc.FormGroup([
            dbc.Label('Supplier Lot Number', width=tab7labelwidth),  # @ProductPN
            dbc.Col(
                dbc.Input(id='tab7-SupplierLN', type='text', placeholder="Supplier Lot Number"),
            )], ), #row=True),

        dbc.FormGroup([
            dbc.Label('K Solid Moisture (ppm)', width=tab7labelwidth),  # @ProductPN
            dbc.Col(
                dbc.Input(id='tab7-KSolid', type='text', placeholder="K Solid ?"),
            )], ), #row=True),

        html.Br(),

        # Input box for notes will go here someday... # @Notes

        html.Br(),
        dbc.Button('Submit', id='submit-Tab7', n_clicks=0, color='primary'),
        html.Br(),
        html.Br(),
        html.H6(id='tab7_container_button', children='Please only click submit once'),
    ])
])



#--------------------tab 8 (eight) --- Slurry Entry-------------#
tab8labelwidth = 3
tab8entrywidth = 9
tab8 = dbc.Card([
    dbc.CardBody([
        html.H2('Slurry Data Entry - Synthesis and Quality Metrics'),
        html.Br(),
        html.Br(),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('User ID', width=4),
                        dbc.Col(
                            dbc.Input(id='tab8-User', type='text', placeholder="User ID Number"),
                        ),
                    ]), width=3,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Slurry Lot Number', width=6),
                        dbc.Col(
                            dbc.Input(id='tab8-SlurryLN', type='text', placeholder="Product Lot Number"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Slurry Part Number', width=6),
                        #SlurryLN is a combined lot number but we will NOT require people to run the combined batch stored procedure first
                        dbc.Col(
                            dbc.Input(id='tab8-SlurryPN', type='text', placeholder="Slurry Part Number"),
                        ),
                    ]), width=4,
                ),
            ],
            form=True,
        ),

        html.Br(),
        html.H5('Please enter how much of each synthesis lot went into this slurry:'),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Lot No 1', width=4),
                        dbc.Col(
                            dbc.Input(id='tab8-SynOGLN1', type='text', placeholder="Synthesis LN #1"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Part No 1', width=4),
                        dbc.Col(
                            dbc.Input(id='tab8-SynOGPN1', type='text', placeholder="Synthesis PN #1"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Mass 1', width=4),
                        dbc.Col(
                            dbc.Input(id='tab8-mass1', type='text', placeholder="Mass 1"),
                        ),
                    ]), width=4,
                ),
            ],
            form=True,
        ),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Lot No 2', width=4),
                        dbc.Col(
                            dbc.Input(id='tab8-SynOGLN2', type='text', placeholder="Synthesis LN #2"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Part No 2', width=4),
                        dbc.Col(
                            dbc.Input(id='tab8-SynOGPN2', type='text', placeholder="Synthesis PN #2"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Mass 2', width=4),
                        dbc.Col(
                            dbc.Input(id='tab8-mass2', type='text', placeholder="Mass 2"),
                        ),
                    ]), width=4,
                ),
            ],
            form=True,
        ),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Lot No 3', width=4),
                        dbc.Col(
                            dbc.Input(id='tab8-SynOGLN3', type='text', placeholder="Synthesis LN #3"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Part No 3', width=4),
                        dbc.Col(
                            dbc.Input(id='tab8-SynOGPN3', type='text', placeholder="Synthesis PN #3"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Mass 3', width=4),
                        dbc.Col(
                            dbc.Input(id='tab8-mass3', type='text', placeholder="Mass 3"),
                        ),
                    ]), width=4,
                ),
            ],
            form=True,
        ),


        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Lot No 4', width=4),
                        dbc.Col(
                            dbc.Input(id='tab8-SynOGLN4', type='text', placeholder="Synthesis LN #4"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Part No 4', width=4),
                        dbc.Col(
                            dbc.Input(id='tab8-SynOGPN4', type='text', placeholder="Synthesis PN #4"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Mass 4', width=4),
                        dbc.Col(
                            dbc.Input(id='tab8-mass4', type='text', placeholder="Mass 4"),
                        ),
                    ]), width=4,
                ),
            ],
            form=True,
        ),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Lot No 5', width=4),
                        dbc.Col(
                            dbc.Input(id='tab8-SynOGLN5', type='text', placeholder="Synthesis LN #5"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Part No 5', width=4),
                        dbc.Col(
                            dbc.Input(id='tab8-SynOGPN5', type='text', placeholder="Synthesis PN #5"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Mass 5', width=4),
                        dbc.Col(
                            dbc.Input(id='tab8-mass5', type='text', placeholder="Mass 5"),
                        ),
                    ]), width=4,
                ),
            ],
            form=True,
        ),

        html.Br(),
        html.H5('Slurry Quality Metrics:'),


        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Viscosity', width=4),
                        dbc.Col(
                            dbc.Input(id='tab8-viscosity', type='text', placeholder="Viscosity"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Particle Size', width=4),
                        dbc.Col(
                            dbc.Input(id='tab8-particlesize', type='text', placeholder="Particle Size"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('% Solids', width=4),
                        dbc.Col(
                            dbc.Input(id='tab8-pctsolids', type='text', placeholder="% Solid"),
                        ),
                    ]), width=4,
                ),
            ],
            form=True,
        ),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Power Law', width=4),
                        dbc.Col(
                            dbc.Input(id='tab8-powerlawnumber', type='text', placeholder="Power Law Number"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Notes', width=8),
                        dbc.Col(
                            dbc.Input(id='tab8-notes', type='text', placeholder="Notes"),
                        ),
                    ]), width=8,
                ),
            ],
            form=True,
        ),

        html.Br(),
        html.Br(),
        dbc.Button('Submit', id='submit-Tab8', n_clicks=0, color='primary'),
        html.Br(),
        html.Br(),
        html.H6(id='tab8_container_button', children='Please only click submit once'),

    ])
])

#--------------------tab 9 (Nine) --- Slurry Addititves Data Entry-------------#
tab9labelwidth = 3
tab9entrywidth = 9
tab9 = dbc.Card([
    dbc.CardBody([

        html.H2('Slurry Data Entry - Additives'),
        html.Br(),
        html.Br(),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('User ID', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-User', type='text', placeholder="User ID Number"),
                        ),
                    ]), width=3,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Slurry Lot Number', width=6),
                        dbc.Col(
                            dbc.Input(id='tab9-SlurryLN', type='text', placeholder="Product Lot Number"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Slurry Part Number', width=6),
                        #SlurryLN is a combined lot number but we will NOT require people to run the combined batch stored procedure first
                        dbc.Col(
                            dbc.Input(id='tab9-SlurryPN', type='text', placeholder="Slurry Part Number"),
                        ),
                    ]), width=4,
                ),
            ],
            form=True,
        ),

        html.Br(),
        html.H5('Polymers'),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Lot #', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-PolyLN1', type='text', placeholder="Polymer #1 Lot Number"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Part #', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-PolyPN1', type='text', placeholder="Polymer #1 Lot Number"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Mass 1', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-polymass1', type='text', placeholder="Polymer #1 Mass"),
                        ),
                    ]), width=4,
                ),
            ],
            form=True,
        ),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Lot #', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-PolyLN2', type='text', placeholder="Polymer #2 Lot Number"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Part #', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-PolyPN2', type='text', placeholder="Polymer #2 Lot Number"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Mass 2', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-polymass2', type='text', placeholder="Polymer #2 Mass"),
                        ),
                    ]), width=4,
                ),
            ],
            form=True,
        ),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Lot #', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-PolyLN3', type='text', placeholder="Polymer #3 Lot Number"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Part #', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-PolyPN3', type='text', placeholder="Polymer #3 Lot Number"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Mass 3', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-polymass3', type='text', placeholder="Polymer #3 Mass"),
                        ),
                    ]), width=4,
                ),
            ],
            form=True,
        ),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Lot #', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-PolyLN4', type='text', placeholder="Polymer #4 Lot Number"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Part #', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-PolyPN4', type='text', placeholder="Polymer #4 Lot Number"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Mass 4', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-polymass4', type='text', placeholder="Polymer #4 Mass"),
                        ),
                    ]), width=4,
                ),
            ],
            form=True,
        ),

        html.Br(),
        html.H5('Solvents'),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Lot #', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-solventLN1', type='text', placeholder="Solvent #1 Lot Number"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Part #', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-solventPN1', type='text', placeholder="Solvent #1 Lot Number"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Mass 1', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-solventmass1', type='text', placeholder="Solvent #1 Mass"),
                        ),
                    ]), width=4,
                ),
            ],
            form=True,
        ),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Lot #', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-solventLN2', type='text', placeholder="Solvent #2 Lot Number"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Part #', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-solventPN2', type='text', placeholder="Solvent #2 Lot Number"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Mass 1', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-solventmass2', type='text', placeholder="Solvent #2 Mass"),
                        ),
                    ]), width=4,
                ),
            ],
            form=True,
        ),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Lot #', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-solventLN3', type='text', placeholder="Solvent #3 Lot Number"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Part #', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-solventPN3', type='text', placeholder="Solvent #3 Lot Number"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Mass 1', width=4),
                        dbc.Col(
                            dbc.Input(id='tab9-solventmass3', type='text', placeholder="Solvent #3 Mass"),
                        ),
                    ]), width=4,
                ),
            ],
            form=True,
        ),


        html.Br(),
        html.Br(),
        dbc.Button('Submit', id='submit-Tab9', n_clicks=0, color='primary'),
        html.Br(),
        html.Br(),
        html.H6(id='tab9_container_button', children='Please only click submit once'),


    ])

])


#-------------------- tab 10 (ten) --- Bulk Material Log -------------#
tab10labelwidth = 3
tab10entrywidth = 9
tab10 = dbc.Card([
    dbc.CardBody([

        html.H2('Bulk Material Log Data Entry Form'),
        html.Br(),
        html.Br(),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('User ID:', width=4),
                        dbc.Col(
                            dbc.Input(id='tab10-User', type='text', placeholder="User ID Number"),
                        ),
                    ]), width=3,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Ingredient Name:', width=6),
                        dbc.Col(
                            dbc.Input(id='tab10-IngredientName', type='text', placeholder="Ingredient Name"),
                        ),
                    ]), width=6,
                ),

            ],
            form=True,
        ),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Ingredient Lot Number:', width=6),
                        dbc.Col(
                            dbc.Input(id='tab10-IngredientLN', type='text', placeholder="Ingredient Lot Number"),
                        ),
                    ]), width=6,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Ingredient Part Number:', width=6),
                        dbc.Col(
                            dbc.Input(id='tab10-IngredientPN', type='text', placeholder="Ingredient Part Number"),
                        ),
                    ]), width=6,
                ),
            ],
            form=True,
        ),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Supplier Lot Number:', width=6),
                        dbc.Col(
                            dbc.Input(id='tab10-SupplierLN', type='text', placeholder="Supplier Lot Number"),
                        ),
                    ]), width=6,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Supplier Name:', width=6),
                        dbc.Col(
                            dbc.Input(id='tab10-Supplier', type='text', placeholder="Supplier Name"),
                        ),
                    ]), width=6,
                )
            ],
            form=True,
        ),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Date Received - Month:', width=8),
                        dbc.Col(
                            dcc.Dropdown(
                                id='tab10-ReceivedMonth',
                                options=[
                                    {'label': 'January', 'value': '1'},
                                    {'label': 'February', 'value': '2'},
                                    {'label': 'March', 'value': '3'},
                                    {'label': 'April', 'value': '4'},
                                    {'label': 'May', 'value': '5'},
                                    {'label': 'June', 'value': '6'},
                                    {'label': 'July', 'value': '7'},
                                    {'label': 'August', 'value': '8'},
                                    {'label': 'September', 'value': '9'},
                                    {'label': 'October', 'value': '10'},
                                    {'label': 'November', 'value': '11'},
                                    {'label': 'December', 'value': '12'}
                                ],
                                placeholder="Select Month",
                            ),
                        ),
                    ]), width=4,
                ),

                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Day:', width=6),
                        dbc.Col(
                            dbc.Input(id='tab10-ReceivedDay', type='text', placeholder="Day"),
                        ),
                    ]), width=4,
                ),

                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Year:', width=6),
                        dbc.Col(
                            dbc.Input(id='tab10-ReceivedYear', type='text', placeholder="Year"),
                        ),
                    ]),width=4,
                ),
            ],
            form=True,
        ),


        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Container Number:', width=6),
                        dbc.Col(
                            dbc.Input(id='tab10-ContainerNr', type='text', placeholder="Container #"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Total Weight:', width=6),
                        dbc.Col(
                            dbc.Input(id='tab10-TotalWeight', type='text', placeholder="Total Weight"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Measurement Type:', width=6),
                        dbc.Col(
                            dbc.Input(id='tab10-MeasurementType', type='text', placeholder="Measurement Type"),
                        ),
                    ]), width=4,
                ),
            ],
            form=True,
        ),

        dbc.Row(
            [
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Deviation Number:', width=6),
                        dbc.Col(
                            dbc.Input(id='tab10-DeviationNr', type='text', placeholder="Deviation Number"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('MRB Number:', width=6),
                        dbc.Col(
                            dbc.Input(id='tab10-MRBNr', type='text', placeholder="MRB Number"),
                        ),
                    ]), width=4,
                ),
                dbc.Col(
                    dbc.FormGroup([
                        dbc.Label('Notes:', width=6),
                        dbc.Col(
                            dbc.Input(id='tab10-Notes', type='text', placeholder="Notes"),
                        ),
                    ]), width=4,
                ),
            ],
            form=True,
        ),


        html.Br(),
        html.Br(),
        dbc.Button('Submit', id='submit-Tab10', n_clicks=0, color='primary'),
        html.Br(),
        html.Br(),
        html.H6(id='tab10_container_button', children='Please only click submit once'),


    ])

])


app.layout = html.Div([
    navigation_bar,
    dbc.Container([
        dbc.Tabs([
            dbc.Tab(label='.     Lot Number Generator     .', children=[tab1]),
            dbc.Tab(label='.     Split Number Generator     .', children=[tab2]),
            dbc.Tab(label='.     Part Number Generator     .', children=[tab3]),
            dbc.Tab(label='.     Record Combined Batches     .', children=[tab4]),
            dbc.Tab(label='............  Conductivity Data Entry ...........', children=[tab5]),
            dbc.Tab(label='........... Consumed Batch Data Entry ...........', children=[tab6]),
            dbc.Tab(label='........... Material Drying Data Entry ...........', children=[tab7]),
            dbc.Tab(label='---- Slurry Data Entry - Synthesis and Quality Metrics ---', children=[tab8]),
            dbc.Tab(label='---- Slurry Data Entry - Record Additives ---', children=[tab9]),
            dbc.Tab(label='---- Bulk Material Log ---', children=[tab10])
        ])
    ])
])








# -------------------- Tab1 Callback -----------------------------------------------------------------------------######
@app.callback(
    dash.dependencies.Output('tab1_container_button', 'children'),
    [dash.dependencies.Input('submit-Tab1', 'n_clicks')],
    [dash.dependencies.State('LNGL_Dropdown_Chosen', 'value'),  # removed input 2 for split number TODO rename
     dash.dependencies.State('input-on-submit3', 'value'), dash.dependencies.State('input-on-submit4', 'value')])
def update_output(n_clicks, LNGL, userNum, Generation):
    if LNGL is not None and userNum is not None and Generation is not None:
        print(LNGL, userNum, Generation)

        engine = create_engine("mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
        print('tab1 - hello from the first callback - lot number generator')
        connection = engine.raw_connection()
        try:
            cursor = connection.cursor()
            querystring = 'exec getLotNumber @LotNrGenLocation = \'{}\', @EnteredBy = \'{}\', @GenNr = \'{}\''.format(
                LNGL, userNum, Generation)
            cursor.execute(querystring)
            print('querystring is: ')
            print(querystring)
            newLotNum, = list(cursor.fetchall())  # THIS COMMA MATTERS A LOT
            print('results are: ')
            print(newLotNum)
            cursor.close()
            connection.commit()
        except Exception as e:
            print('an exception occurred - LNG - tab 1')
            print(e)
            connection.close()
            return '---error-tab1--- {}'.format(e)
        finally:
            print('closing connection to DB')
            connection.close()

        engine.dispose()
        # print(type(newLotNum))
        # newLotNum1 = newLotNum.pop(0)
        # print(type(newLotNum))
        # print(str(newLotNum))

        # this is an interesting thing:
        # The fetchall required a comma after the asignee variable name beacuse it was making a list that I couldnt format
        # solving that raised the issue of now it returned a class object. fortunately a string conversion worked, then a substring to finalize the conversion
        newLotNum = str(newLotNum)
        newLotNum = newLotNum[
                    3:16]  # Why, yes, this is hard coding the length of the barcode to fit in those constraints. sometimes it doesnt work - BE WARNED TODO
        return u'Your new lot number is: {}'.format(newLotNum)
    else:
        raise PreventUpdate


# return 'The input value was "{}" and the button has been clicked {} times. value2 was: {}, and value3 was {}'.format(LNGL, n_clicks, splitNum, userNum)


# -------------------- Tab2 Callback ---------------------- #
@app.callback(
    dash.dependencies.Output('tab2_container_button', 'children'),
    [dash.dependencies.Input('submit-Tab2', 'n_clicks')],
    [dash.dependencies.State('tab2-lotNum', 'value'), dash.dependencies.State('tab2-splitNum', 'value'),
     dash.dependencies.State('tab2-employeeNum', 'value')])
def update_output(n_clicks, OGLotNum, SplitLetter, userNum2):
    if OGLotNum is not None and SplitLetter is not None and userNum2 is not None:
        # print("hello")
        print(OGLotNum, SplitLetter, userNum2)

        engine = create_engine("mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
        print('tab 2 - hello from the second callback - split number generator')
        # pd.options.mode.chained_assignment = None
        # querystring =  "exec getLotNumber {},{},{}".format(LNGL,splitNum,userNum)
        # newLotNum = pd.read_sql_query(querystring, con=engine)
        connection = engine.raw_connection()
        # print('this is the try clause')
        try:
            cursor = connection.cursor()
            print('cursor succeeded')
            querystring = 'exec dbo.getLotNumberWithSplitLetter @LotNumber = \'{}\', @SplitLetter = \'{}\', @EnteredBy = \'{}\''.format(
                OGLotNum, SplitLetter, userNum2)
            print(querystring)
            cursor.execute(querystring)
            print('querystring is: ')
            print(querystring)
            newLotNum, = list(cursor.fetchall())  # THIS COMMA MATTERS A LOT
            print('results are: ')
            print(newLotNum)
            cursor.close()
            connection.commit()
        except Exception as e:
            print('there was an exception that occurred. Split Letter')
            print(e)
            connection.close()
            return '---error2---'
        finally:
            connection.close()

        engine.dispose()
        # print(type(newLotNum))
        # newLotNum1 = newLotNum.pop(0)
        # print(type(newLotNum))
        # print(str(newLotNum))

        # this is an interesting thing:
        # The fetchall required a comma after the asignee variable name beacuse it was making a list that I couldnt format
        # solving that raised the issue of now it returned a class object. fortunately a string conversion worked, then a substring to finalize the conversion
        newLotNum = str(newLotNum)
        newLotNum = newLotNum[
                    3:16]  # Why, yes, this is hard coding the length of the barcode to fit in those constraints. sometimes it doesnt work - BE WARNED TODO
        return u'Your new lot number is: {}'.format(newLotNum)
    else:
        raise PreventUpdate


# -------------------- Tab3 Callback ---------------------- #
@app.callback(
    dash.dependencies.Output('tab3_container_button', 'children'),
    [dash.dependencies.Input('submit-Tab3', 'n_clicks')],
    [dash.dependencies.State('Material_Selection', 'value'),
     dash.dependencies.State('Generation_Selection', 'value'),
     dash.dependencies.State('MaterialForm_Selection', 'value'),
     dash.dependencies.State('ProcLoc_Selection', 'value'),
     dash.dependencies.State('MatDesc_Selection', 'value')])
def update_output(n_clicks, Material, Generation, MatForm, ProcLoc, MatDesc):
    if Material is not None and Generation is not None and MatForm is not None and ProcLoc is not None and MatDesc is not None:

        engine = create_engine("mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
        print('tab 3 - hello from the third callback - PNG')
        # connection = engine.raw_connection()
        # print('connection succeeded')
        try:
            connection = engine.raw_connection()
            print('connection succeeded')
            cursor = connection.cursor()
            print('cursor succeeded')

            # querystring = 'exec dbo.uspValidatePartNumber @MaterialShortName = \'{}\', @GenID = \'{}\',
            # @MaterialFormID = \'{}\', @ProcessLocID = \'{}\' , @MDID = \'{}\''.format(Material, Generation,
            # MatForm, ProcLoc, MatDesc)

            querystring = 'exec dbo.uspValidatePartNumber \'{}{}{}{}-{}\''.format(Material, Generation, MatForm,
                                                                                  ProcLoc, MatDesc)
            print(querystring)
            cursor.execute(querystring)
            print('querystring is: ')
            print(querystring)
            NewPartNumber = cursor.fetchall()  # note: no comma
            # NewPartNumber = 'hello'
            print('results are: ')
            print(type(NewPartNumber))
            cursor.close()
            connection.commit()
        except Exception as e:
            print('there was an exception that occurred - tab 3 - validate PN')
            print(e)
            connection.close()
            return '---Exception3 : {}'.format(e)
        finally:
            connection.close()

        engine.dispose()

        NewPartNumber = str(NewPartNumber)
        NewPartNumber = NewPartNumber.strip('u[](),\'\"\,\'')
        NewPartNumber = NewPartNumber.replace('"', '')
        return 'Your part number is:  "{}" '.format(NewPartNumber)

    else:
        raise PreventUpdate


# -------------------- Tab4 Callback ---------------------- #
@app.callback(
    dash.dependencies.Output('tab4_container_button', 'children'),
    [dash.dependencies.Input('submit-Tab4', 'n_clicks')],
    [dash.dependencies.State('tab4-CombinedLotNum', 'value'), dash.dependencies.State('tab4-OriginalLotNum', 'value'),
     dash.dependencies.State('tab4-EmployeeNum', 'value'), dash.dependencies.State('tab4-StartWeight', 'value')])
def update_output(n_clicks, CombinedLotNum, OGLotNumbers, userNum4, startWeight):
    if CombinedLotNum is not None and OGLotNumbers is not None and userNum4 is not None and startWeight is not None:
        # print("hello")
        print(CombinedLotNum, OGLotNumbers, userNum4, startWeight)

        engine = create_engine("mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
        print('tab 4 - hello from the fourth callback - Combined batch recording')
        # pd.options.mode.chained_assignment = None
        # querystring =  "exec getLotNumber {},{},{}".format(LNGL,splitNum,userNum)
        # newLotNum = pd.read_sql_query(querystring, con=engine)
        connection = engine.raw_connection()
        print('connection succeeded')
        try:
            connection = engine.raw_connection()
            print('connection succeeded')
            cursor = connection.cursor()
            print('cursor succeeded')
            querystring = 'exec dbo.insertCombinedBatches @CombinedLotNr = \'{}\', @OriginalLotNr = \'{}\', @UserOpID = \'{}\', @StartWeight = \'{}\''.format(
                CombinedLotNum, OGLotNumbers, userNum4, startWeight)
            print(querystring)
            cursor.execute(querystring)
            print('querystring is: ')
            print(querystring)
            ExistingOGLots = cursor.fetchall()  # note: no comma
            print('results are: ')
            print(type(ExistingOGLots))
            cursor.close()
            connection.commit()
        except Exception as e:
            print('there was an exception that occurred. Combined batches')
            print(e)
            connection.close()
            return '---error4 : {}'.format(e)
        finally:
            connection.close()

        engine.dispose()
        barcodes = ''
        for barcode in ExistingOGLots:
            l = str(barcode)
            i = l.strip('u[](),\'')
            barcodes = barcodes + i + '      '
        return u'This returned the following barcodes: {}'.format(barcodes)
    else:
        raise PreventUpdate


# ---------------------------- Tab5 - Calculation Callbacks for Conductivity App ------------------------------------- #
@app.callback(
    [dash.dependencies.Outuput('tab5-thickness-out', 'children'),
     dash.dependencies.Outuput('tab5-elecCon-out', 'children'),
     dash.dependencies.Outuput('tab5-ionCon-out', 'children'),
     dash.dependencies.Outuput('tab5-density-out', 'children')],
    [dash.dependencies.State('tab5-lenTotal', 'value'), dash.dependencies.State('tab5-lenTare', 'value'),
     dash.dependencies.State('tab5-ionicInt', 'value'), dash.dependencies.State('tab5-elecInt', 'value'),
     dash.dependencies.State('tab5-mass', 'value')]
              )
def update_output(lentotal, lentare, iint, eint, mass5calc):
    if lentotal and lentare and iint and eint and mass5calc is not None:

        thickness = (lentotal-lentare)/10




        return u'this is the thickness \'{}\''.format(thickness)
    else:
        raise PreventUpdate


# -------------------- Tab5 Callback - Conductivity ---------------------- #
@app.callback(
    dash.dependencies.Output('tab5_container_button', 'children'),
    [dash.dependencies.Input('submit-Tab5', 'n_clicks')],
    [dash.dependencies.State('tab5-PLN', 'value'), dash.dependencies.State('tab5-TestCon', 'value'),
     dash.dependencies.State('tab5-TestTemp', 'value'), dash.dependencies.State('tab5-Mass', 'value'),
     dash.dependencies.State('tab5-PlungerLNo', 'value'), dash.dependencies.State('tab5-PlungerThickness', 'value'),
     dash.dependencies.State('tab5-PelletThickness', 'value'), dash.dependencies.State('tab5-MatDensity', 'value'),
     dash.dependencies.State('tab5-AxisNyquist', 'value'), dash.dependencies.State('tab5-IonCon', 'value'),
     dash.dependencies.State('tab5-ECon', 'value'), dash.dependencies.State('tab5-User', 'value')])
def update_output(n_clicks, PLN5, TestCon5, TestTemp5, Mass5, PluNo5, PluT5, PelT5, MatDensity5, AxisNyquest5, IonCon5, Econ5, User5):
    if User5 and PLN5 and TestCon5 and TestTemp5 and Mass5 and PluNo5 and PluT5 and PelT5 and MatDensity5 and \
            AxisNyquest5 and IonCon5 and Econ5 is not None:
        print(PLN5, TestCon5, TestTemp5, Mass5, PluNo5, PluT5, PelT5, MatDensity5, AxisNyquest5, IonCon5, Econ5, User5)

        engine = create_engine("mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
        print('tab 5 callback')
        connection = engine.raw_connection()
        print('connection succeeded')
        try:
            connection = engine.raw_connection()
            print('connection succeeded')
            cursor = connection.cursor()
            print('cursor succeeded')
            querystring = 'exec dbo.insertProdConductivity @UserOpID = \'{}\', @ProductLN = \'{}\',' \
                          '@TestCondition = \'{}\', @TestTemperature = \'{}\', @MaterialMass_g = \'{}\', ' \
                          '@PlungerLengthNoPellet_mm = \'{}\', @PelletPlungerThickness_mm = \'{}\', ' \
                          '@MaterialDensity_gcc = \'{}\', @XAxisNyquist_ohms = \'{}\', @PelletThickness_cm = \'{}\', ' \
                          '@IonicConductivity_Scm = \'{}\', @ElectronicConductivity_Scm = \'{}\''.format(
                User5, PLN5, TestCon5, TestTemp5, Mass5, PluNo5, PluT5, PelT5, MatDensity5, AxisNyquest5,
                IonCon5, Econ5)

            print(querystring)
            cursor.execute(querystring)
            print('querystring is: ')
            print(querystring)
            #ExistingOGLots = cursor.fetchall()  # note: no comma
            print('results are: ')
            #print(type(ExistingOGLots))
            cursor.close()
            connection.commit()
        except Exception as e:
            print('there was an exception that occurred. Conductivity')
            print(e)
            connection.close()
            return '---error5 : {}'.format(e)
        finally:
            connection.close()

        engine.dispose()

        return u'Conductivity data recorded successfully for \'{}\''.format(PLN5)
    else:
        raise PreventUpdate

# -------------------- Tab6 Callback ---------------------- #
@app.callback(
    dash.dependencies.Output('tab6_container_button', 'children'),
    [dash.dependencies.Input('submit-Tab6', 'n_clicks')],
    [dash.dependencies.State('tab6-PLN', 'value'), dash.dependencies.State('tab6-StartDay', 'value'),
     dash.dependencies.State('tab6-StartMonth', 'value'), dash.dependencies.State('tab6-StartYear', 'value'),
     dash.dependencies.State('tab6-EndMonth', 'value'), dash.dependencies.State('tab6-EndYear', 'value'),
     dash.dependencies.State('tab6-EndDay', 'value'), dash.dependencies.State('tab6-Quantity', 'value'),
     dash.dependencies.State('tab6-MRB', 'value'), dash.dependencies.State('tab6-Conductivity', 'value'),
     dash.dependencies.State('tab6-Yield', 'value'), dash.dependencies.State('tab6-PassFail', 'value'),
     dash.dependencies.State('tab6-ECon', 'value'), dash.dependencies.State('tab6-GuseGen', 'value'),
     dash.dependencies.State('tab6-User', 'value')])
def update_output(n_clicks, PLN6, StartDay6, StartMonth6, StartYear6, FinishMonth6, FinishYear6, FinishDay6, Quantity6,
                  MRB6, Conductivity6, Yield6, PassFail6, ECon6, GuseGen, User6):
    if PLN6 and StartDay6 and StartMonth6 and StartYear6 and FinishMonth6 and FinishYear6 and FinishDay6 and Quantity6 \
            and MRB6 and Conductivity6 and Yield6 and PassFail6 and ECon6 and GuseGen and User6 is not None:

        print(PLN6, StartDay6, StartMonth6, StartYear6, FinishMonth6, FinishYear6, FinishDay6, Quantity6, MRB6,
              Conductivity6, Yield6, PassFail6, ECon6, GuseGen, User6)

        StartDate6 = datetime.datetime(int(StartYear6), int(StartMonth6), int(StartDay6))
        FinishDate6 = datetime.datetime(int(FinishYear6), int(FinishMonth6), int(FinishDay6))

        engine = create_engine("mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
        print('tab 6 callback')
        connection = engine.raw_connection()
        print('connection succeeded')
        try:
            connection = engine.raw_connection()
            print('connection succeeded')
            cursor = connection.cursor()
            print('cursor succeeded')
            querystring = 'exec dbo.insertConsumedBatch @UserOpID = \'{}\', @ProductLN = \'{}\',@startdate = \'{}\', ' \
                          '@finishdate = \'{}\', @Quantity_g = \'{}\', @MRB = \'{}\', @Conductivity_mScm = \'{}\', ' \
                          '@Yield_g = \'{}\', @PassFail = \'{}\', @ElectronicConductivity_mScm = \'{}\', ' \
                          '@GuseGen = \'{}\''.format(
                User6, PLN6, StartDate6, FinishDate6, Quantity6, MRB6, Conductivity6, Yield6, PassFail6, ECon6, GuseGen)

            print(querystring)
            cursor.execute(querystring)
            print('querystring is: ')
            print(querystring)
            #ExistingOGLots = cursor.fetchall()  # note: no comma
            print('results are: ')
            #print(type(ExistingOGLots))
            cursor.close()
            connection.commit()
        except Exception as e:
            print('there was an exception that occurred. Consumed Batch Record')
            print(e)
            connection.close()
            return '---error6 : {}'.format(e)
        finally:
            connection.close()

        engine.dispose()

        return u'Consumed batch data successfully recorded for \'{}\''.format(PLN6)
    else:
        raise PreventUpdate

# -------------------- Tab7 Callback ---------------------- #
@app.callback(
    dash.dependencies.Output('tab7_container_button', 'children'),
    [dash.dependencies.Input('submit-Tab7', 'n_clicks')],
    [dash.dependencies.State('tab7-ILN', 'value'), dash.dependencies.State('tab7-PPN', 'value'),
     dash.dependencies.State('tab7-SupplierName', 'value'),
     dash.dependencies.State('tab7-SupplierLN', 'value'), dash.dependencies.State('tab7-KSolid', 'value'),
     dash.dependencies.State('tab7-User', 'value')])
def update_output(n_clicks, ILN7, PLN7, SupplierName7, SupplierLN7, KSolid7, User7):
    if ILN7 and PLN7 and SupplierName7 and SupplierLN7 and KSolid7 and User7 is not None:

        print(ILN7, PLN7,  SupplierName7, SupplierLN7, KSolid7, User7)

        engine = create_engine("mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
        print('tab 7 callback')
        connection = engine.raw_connection()
        print('connection succeeded')
        try:
            connection = engine.raw_connection()
            print('connection succeeded')
            cursor = connection.cursor()
            print('cursor succeeded')
            querystring = 'exec dbo.insertDrying @UserOpID = \'{}\', @IngredientLN = \'{}\', @ProductPN = \'{}\', ' \
                          '@Supplier = \'{}\', @SupplierLotNr = \'{}\', @KSolidMoisture_ppm = \'{}\''.format(
                User7, ILN7, PLN7, SupplierName7, SupplierLN7, KSolid7)
            print(querystring)
            cursor.execute(querystring)
            print('querystring is: ')
            print(querystring)
            #ExistingOGLots = cursor.fetchall()  # note: no comma
            print('results are: ')
            #print(type(ExistingOGLots))
            cursor.close()
            connection.commit()
        except Exception as e:
            print('there was an exception that occurred. Drying')
            print(e)
            connection.close()
            return '---error7 : {}'.format(e)
        finally:
            connection.close()

        engine.dispose()

        return u'Drying data successfully recorded for \'{}\''.format(ILN7) #replace if theres anything to be returned
    else:
        raise PreventUpdate

    # -------------------- Tab8 Callback ---------------------- #
@app.callback(
    dash.dependencies.Output('tab8_container_button', 'children'),
    [dash.dependencies.Input('submit-Tab8', 'n_clicks')],
    [dash.dependencies.State('tab8-SlurryLN', 'value'), dash.dependencies.State('tab8-SlurryPN', 'value'),
     dash.dependencies.State('tab8-SynOGLN1', 'value'),dash.dependencies.State('tab8-SynOGPN1', 'value'),
     dash.dependencies.State('tab8-SynOGLN2', 'value'), dash.dependencies.State('tab8-SynOGPN2', 'value'),
     dash.dependencies.State('tab8-SynOGLN3', 'value'), dash.dependencies.State('tab8-SynOGPN3', 'value'),
     dash.dependencies.State('tab8-SynOGLN4', 'value'), dash.dependencies.State('tab8-SynOGPN4', 'value'),
     dash.dependencies.State('tab8-SynOGLN5', 'value'), dash.dependencies.State('tab8-SynOGPN5', 'value'),
     dash.dependencies.State('tab8-mass1', 'value'), dash.dependencies.State('tab8-mass2', 'value'),
     dash.dependencies.State('tab8-mass3', 'value'), dash.dependencies.State('tab8-mass4', 'value'),
     dash.dependencies.State('tab8-mass5', 'value'), dash.dependencies.State('tab8-viscosity', 'value'),
     dash.dependencies.State('tab8-particlesize', 'value'), dash.dependencies.State('tab8-powerlawnumber', 'value'),
     dash.dependencies.State('tab8-pctsolids', 'value'), dash.dependencies.State('tab8-notes', 'value'),
     dash.dependencies.State('tab8-User', 'value')])
def update_output(n_clicks, sln8, spn8, synln8_1, synpn8_1, synln8_2, synpn8_2, synln8_3, synpn8_3,
                  synln8_4, synpn8_4, synln8_5, synpn8_5, mass8_1, mass8_2, mass8_3, mass8_4, mass8_5, visco8,
                  particlesz8, powerlawno8, pctsolids8, notes8, user8):

    if sln8 and spn8 and synln8_1 and synpn8_1 and synln8_2 and synpn8_2 and mass8_1 and mass8_2 and visco8 \
            and particlesz8 and powerlawno8 and pctsolids8 and user8 is not None:
        # ONLY REQUIRES 2 input original synthesis batches


        print(n_clicks, sln8, spn8, synln8_1, synpn8_1, synln8_2, synpn8_2, synln8_3, synpn8_3,
              synln8_4, synpn8_4, synln8_5, synpn8_5, mass8_1, mass8_2, mass8_3, mass8_4, mass8_5, visco8, particlesz8,
              powerlawno8, pctsolids8, notes8, user8)

        engine = create_engine(
            "mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
        print('tab 8 callback pt1 - slurry input comb. batches')
        connection = engine.raw_connection()
        print('connection succeeded')
        try:
            connection = engine.raw_connection()
            print('connection succeeded')
            cursor = connection.cursor()
            print('cursor succeeded')
            querystring = 'exec dbo.insertCombinedBatches @UserOpID = \'{}\', @OriginalLotNr = \'{}\', ' \
                          '@OriginalLotNr2 = \'{}\', @OriginalLotNr3 = \'{}\', @OriginalLotNr4 = \'{}\', ' \
                          '@OriginalLotNr5 = \'{}\', @StartWeight = \'{}\', @StartWeight2 = \'{}\', ' \
                          '@StartWeight3 = \'{}\', @StartWeight4 = \'{}\', @StartWeight5 = \'{}\', ' \
                          '@CombinedLotNr = \'{}\''.format(user8, synln8_1, synln8_2, synln8_3, synln8_4, synln8_5,
                                                           mass8_1, mass8_2, mass8_3, mass8_4, mass8_5, sln8)

            print(querystring)
            cursor.execute(querystring)
            print('querystring is: ')
            print(querystring)
            # ExistingOGLots = cursor.fetchall()  # note: no comma
            print('results are: ')
            cursor.close()
            connection.commit()
        except Exception as e:
            print('there was an exception that occurred. Slurry Creation - recording OG synth. LN/PN')
            print(e)
            connection.close()
            return '---error8 (slurry creation pt1 - insert combined batches SP) : {}'.format(e)
        finally:
            connection.close()

        engine.dispose()

        # it's probably not necessary to close and then reopen the cursor in the same callback TODO

        engine = create_engine(
            "mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
        print('tab 8 callback, part2 - inert quality metrics')
        connection = engine.raw_connection()
        print('connection succeeded')
        try:
            connection = engine.raw_connection()
            print('connection succeeded')
            cursor = connection.cursor()
            print('cursor succeeded')
            querystring = 'exec dbo.insertSlurryQualityMetrics @ProductLN = \'{}\', @ProductPN = \'{}\', ' \
                          '@Viscosity = \'{}\', @ParticleSize = \'{}\', @PowerLawNr = \'{}\', @PercentSolids = \'{}\', ' \
                          '@Notes = \'{}\', @UserOpID = \'{}\''.format(
                sln8, spn8, visco8, particlesz8, powerlawno8, pctsolids8, notes8, user8)

            print(querystring)
            cursor.execute(querystring)
            print('querystring is: ')
            print(querystring)
            # ExistingOGLots = cursor.fetchall()  # note: no comma
            print('results are: ')
            cursor.close()
            connection.commit()
        except Exception as e:
            print('there was an exception that occurred. Slurry Creation - recording quality metrics')
            print(e)
            connection.close()
            return '---error8 (insert slurry quality metrics) : {}'.format(e)
        finally:
            connection.close()

        engine.dispose()

        return u'Slurry data successfully recorded for \'{}\''.format(
            sln8)
    else:
        raise PreventUpdate


# -------------------- Tab9 Callback ---------------------- #  # LEFT OFF HERE TODO
@app.callback(
    dash.dependencies.Output('tab9_container_button', 'children'),
    [dash.dependencies.Input('submit-Tab9', 'n_clicks')],
    [dash.dependencies.State('tab9-SlurryLN', 'value'), dash.dependencies.State('tab9-SlurryPN', 'value'),
     dash.dependencies.State('tab9-PolyLN1', 'value'), dash.dependencies.State('tab9-PolyPN1', 'value'),
     dash.dependencies.State('tab9-PolyLN2', 'value'), dash.dependencies.State('tab9-PolyPN2', 'value'),
     dash.dependencies.State('tab9-PolyLN3', 'value'), dash.dependencies.State('tab9-PolyPN3', 'value'),
     dash.dependencies.State('tab9-PolyLN4', 'value'), dash.dependencies.State('tab9-PolyPN4', 'value'),
     dash.dependencies.State('tab9-solventLN1', 'value'), dash.dependencies.State('tab9-solventPN1', 'value'),
     dash.dependencies.State('tab9-solventLN2', 'value'), dash.dependencies.State('tab9-solventPN2', 'value'),
     dash.dependencies.State('tab9-solventLN3', 'value'), dash.dependencies.State('tab9-solventPN3', 'value'),
     dash.dependencies.State('tab9-polymass1', 'value'), dash.dependencies.State('tab9-polymass2', 'value'),
     dash.dependencies.State('tab9-polymass3', 'value'), dash.dependencies.State('tab9-polymass4', 'value'),
     dash.dependencies.State('tab9-solventmass1', 'value'), dash.dependencies.State('tab9-solventmass2', 'value'),
     dash.dependencies.State('tab9-solventmass3', 'value'), dash.dependencies.State('tab9-User', 'value')])
def update_output(n_clicks, sln9, spn9, polymerLN9_1, polymerPN9_1, polymerLN9_2, polymerPN9_2, polymerLN9_3,
                  polymerPN9_3, polymerLN9_4, polymerPN9_4, solvLN9_1, solvPN9_1, solvLN9_2, solvPN9_2, solvLN9_3,
                  solvPN9_3, polymass9_1, polymass9_2, polymass9_3, polymass9_4, solvmass9_1, solvmass9_2, solvmass9_3,
                  user9):
    if sln9 and spn9 and polymerLN9_1 and polymerPN9_1 and polymerLN9_2 and polymerPN9_2 and polymerLN9_3 and \
            polymerPN9_3 and polymerLN9_4 and polymerPN9_4 and solvLN9_1 and solvPN9_1 and solvLN9_2 and solvPN9_2 \
            and solvLN9_3 and solvPN9_3 and polymass9_1 and polymass9_2 and polymass9_3 and polymass9_4 and \
            solvmass9_1 and solvmass9_2 and solvmass9_3 and user9 is not None:
        # TODO WE MAY NOT NEED ALL 4 POLYMERS OR ALL 3 SOLVENTS!!!!!!!!!!!!!!!!

        print(n_clicks, sln9, spn9, polymerLN9_1, polymerPN9_1, polymerLN9_2, polymerPN9_2, polymerLN9_3,
              polymerPN9_3, polymerLN9_4, polymerPN9_4, solvLN9_1, solvPN9_1, solvLN9_2, solvPN9_2, solvLN9_3,
              solvPN9_3, polymass9_1, polymass9_2, polymass9_3, polymass9_4, solvmass9_1, solvmass9_2, solvmass9_3,
              user9)

        engine = create_engine(
            "mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
        print('tab 8 callback')
        connection = engine.raw_connection()
        print('connection succeeded')
        try:
            connection = engine.raw_connection()
            print('connection succeeded')
            cursor = connection.cursor()
            print('cursor succeeded')                  # @UserOpID1 IS NOT CORRECT
            querystring = 'exec dbo.usp_WeighingMultiInput @UserOpID = \'{}\', @IngredientLN1 = \'{}\', ' \
                          '@IngredientPN1 = \'{}\', @IngredientActual1 = \'{}\', @IngredientLN2 = \'{}\', ' \
                          '@IngredientPN2 = \'{}\', @IngredientActual2 = \'{}\', @IngredientLN3 = \'{}\', ' \
                          '@IngredientPN3 = \'{}\', @IngredientActual3 = \'{}\', @IngredientLN4 = \'{}\', ' \
                          '@IngredientPN4 = \'{}\', @IngredientActual4 = \'{}\', @IngredientLN5 = \'{}\', ' \
                          '@IngredientPN5 = \'{}\', @IngredientActual5 = \'{}\', @IngredientLN6 = \'{}\', ' \
                          '@IngredientPN6 = \'{}\', @IngredientActual6 = \'{}\', @IngredientLN7 = \'{}\', ' \
                          '@IngredientPN7 = \'{}\', @IngredientActual7 = \'{}\', @ProductLN = \'{}\''.format(
                user9, polymerLN9_1, polymerPN9_1, polymass9_1, polymerLN9_2, polymerPN9_2, polymass9_2,
                polymerLN9_3, polymerPN9_3, polymass9_3, polymerLN9_4, polymerPN9_4, polymass9_4, solvLN9_1,
                solvPN9_1, solvmass9_1, solvLN9_2, solvPN9_2, solvmass9_2, solvLN9_3, solvPN9_3,
                solvmass9_3, sln9)

            print(querystring)
            cursor.execute(querystring)
            print('querystring is: ')
            print(querystring)
            # ExistingOGLots = cursor.fetchall()  # note: no comma
            print('results are: ')
            cursor.close()
            connection.commit()
        except Exception as e:
            print('there was an exception. Slurry additives')
            print(e)
            connection.close()
            return '---error9 (insert solv. and polymer mass - slurry additives) : {}'.format(e)
        finally:
            connection.close()

        engine.dispose()
        return u'Slurry additive data successfully recorded for ....oof actually this needs work' # TODO \'{}\''.format()



# -------------------- Tab10 (ten) Callback ---------------------- #

@app.callback(
    dash.dependencies.Output('tab10_container_button', 'children'),
    [dash.dependencies.Input('submit-Tab10', 'n_clicks')],
    [dash.dependencies.State('tab10-IngredientName', 'value'), dash.dependencies.State('tab10-IngredientLN', 'value'),
     dash.dependencies.State('tab10-IngredientPN', 'value'), dash.dependencies.State('tab10-Supplier', 'value'),
     dash.dependencies.State('tab10-SupplierLN', 'value'),
     dash.dependencies.State('tab10-ContainerNr', 'value'), dash.dependencies.State('tab10-TotalWeight', 'value'),
     dash.dependencies.State('tab10-MeasurementType', 'value'), dash.dependencies.State('tab10-DeviationNr', 'value'),
     dash.dependencies.State('tab10-MRBNr', 'value'), dash.dependencies.State('tab10-Notes', 'value'),
     dash.dependencies.State('tab10-User', 'value'), dash.dependencies.State('tab10-ReceivedYear', 'value'),
     dash.dependencies.State('tab10-ReceivedDay', 'value'), dash.dependencies.State('tab10-ReceivedMonth', 'value')])
def update_output(n_clicks, ingredientname10, ingredientLN10, ingredientPN10, suppliername10, supplierLN10, containernr10,
                  totalweight10, measurementtype10, deviationnr10, mrbnr10, notes10, user10, year, day, month):
    if ingredientname10 and ingredientLN10 and ingredientPN10 and suppliername10 and supplierLN10 and containernr10 \
            and totalweight10 and measurementtype10 and deviationnr10 and mrbnr10 and notes10 and user10 and year \
            and day and month is not None:
        # TODO check for inputs that are not required

        print(n_clicks, ingredientname10, ingredientLN10, ingredientPN10, suppliername10, supplierLN10, containernr10,
              totalweight10, measurementtype10, deviationnr10, mrbnr10, notes10, user10, year, day, month)

        datereceived = datetime.datetime(int(year), int(month), int(day))

        engine = create_engine(
            "mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
        print('tab 10 callback - bulk material log')
        connection = engine.raw_connection()
        print('connection succeeded')
        try:
            connection = engine.raw_connection()
            print('connection succeeded')
            cursor = connection.cursor()
            print('cursor succeeded')
            querystring = 'exec dbo.insertBulkMaterialLog @IngredientName = \'{}\', @IngredientLN = \'{}\', ' \
                          '@IngredientPN = \'{}\', @Supplier = \'{}\', @SupplierLN = \'{}\', ' \
                          '@ContainerNr = \'{}\', @TotalWeight = \'{}\', @MeasurementType = \'{}\', ' \
                          '@DeviationNr = \'{}\', @MRBNr = \'{}\', @Notes = \'{}\', @UserOpID = \'{}\',' \
                          '@DateReceived = \'{}\''.format(ingredientname10, ingredientLN10, ingredientPN10,
                                                          suppliername10, supplierLN10, containernr10, totalweight10, measurementtype10,
                                                          deviationnr10, mrbnr10, notes10, user10, datereceived)

            print(querystring)
            cursor.execute(querystring)
            print('querystring is: ')
            print(querystring)
            # ExistingOGLots = cursor.fetchall()  # note: no comma
            print('results are: ')
            cursor.close()
            connection.commit()
        except Exception as e:
            print('An exception occurred - Bulk Material Log')
            print(e)
            connection.close()
            return '---error10 (Bulk Material Log) : {}'.format(e)
        finally:
            connection.close()

        engine.dispose()
        return u'Bulk material log data successfully recorded for \'{}\''.format(mrbnr10)
    else:
        raise PreventUpdate

if __name__ == '__main__':
    app.run_server(debug=True)#, host='0.0.0.0', port=8050)