from pylogix import PLC
from pylogix.lgx_response import Response
from typing import List, Set, Dict, Tuple, Optional
from datetime import datetime
import csv
from enum import Enum
import os

CELLCOUNT = 20
LAYERCOUNT = 30

class LayerTypes(Enum):
    Anode = 1
    Cathode = 2

class cell:
    def __init__(self, index):
        self.cell_str = f'Cells_To_Upload[{index}]'
        self.CellSerialNumberTag = f'{self.cell_str}.CellSerialNumber'
        self.ValidTag = f'{self.cell_str}.Valid'
        self.CellSerialNumber = ''
        self.AnodeLayers: List[AnodeLayer] = []
        self.CathodeLayers: List[CathodeLayer] = []
        for layer in range(0,LAYERCOUNT,1):
            self.AnodeLayers.append(AnodeLayer(self.cell_str, layer))
            self.CathodeLayers.append(CathodeLayer(self.cell_str, layer))
        self.Valid = False
        self.index = index

    def get_tags(self) -> [str]:
        result = [self.CellSerialNumberTag, self.ValidTag]
        for anode_layer in self.AnodeLayers:
            result += anode_layer.get_tags()
        for cathode_layer in self.CathodeLayers:
            result += cathode_layer.get_tags()
        return result

    def set_from_tags(self, tags: {str, Response}):
        self.CellSerialNumber = tags[self.CellSerialNumberTag].Value
        self.Valid = tags[self.ValidTag].Value
        for anode_layer in self.AnodeLayers:
            anode_layer.set_from_tags(tags)
        for cathode_layer in self.CathodeLayers:
            cathode_layer.set_from_tags(tags)

class AnodeLayer:
    def __init__(self, cell_str: str, layer_index: int):
        self.LayerTag = f'{cell_str}.AnodeLayers[{layer_index}]'
        self.Common = Common(self.LayerTag)

    def get_tags(self) -> [str]:
        return self.Common.get_tags()

    def set_from_tags(self, tags: {str, Response}):
        self.Common.set_from_tags(tags)

class CathodeLayer:
    def __init__(self, cell_str: str, layer_index: int):
        self.LayerTag = f'{cell_str}.CathodeLayers[{layer_index}]'
        self.Common = Common(self.LayerTag)

    def get_tags(self) -> [str]:
        return self.Common.get_tags()

    def set_from_tags(self, tags: {str, Response}):
        self.Common.set_from_tags(tags)

class Common:
    def __init__(self, layer_str: str):
        self.Lot_Number = ''
        self.Part_Number = ''
        self.Mag_SN = ''
        self.Exists = False
        self.CommonTag = f'{layer_str}.Common'
        self.Lot_NumberTag = f'{self.CommonTag}.Lot_Number'
        self.Part_NumberTag = f'{self.CommonTag}.Part_Number'
        self.Mag_SNTag = f'{self.CommonTag}.Mag_SN'
        self.ExistsTag = f'{self.CommonTag}.Exists'

    def get_tags(self) -> [str]:
        return [self.Lot_NumberTag, self.Part_NumberTag, self.Mag_SNTag, self.ExistsTag]

    def set_from_tags(self, tags: {str, Response}):
        self.Lot_Number = tags[self.Lot_NumberTag].Value
        self.Part_Number = tags[self.Part_NumberTag].Value
        self.Mag_SN = tags[self.Mag_SNTag].Value
        self.Exists = tags[self.ExistsTag].Value

def read_cells() -> [cell]:
    with PLC() as comm:
        comm.IPAddress = '192.168.14.137'
        cells: List[cell] = []
        for cell_index in range(0,CELLCOUNT,1):
            new_cell = cell(cell_index)
            cells.append(new_cell)
            tags = new_cell.get_tags()
            ret = comm.Read(tags)
            tag_dic = {}
            for ret_tag in ret:
                if ret_tag.TagName in tag_dic.keys():
                    print('duplicate tag in dictionary: ' + ret_tag)
                tag_dic[ret_tag.TagName] = ret_tag
            new_cell.set_from_tags(tag_dic)
        return cells

def print_db_rows(directory: str, cells: List[cell]):
    file = os.path.join(directory, "Log time " + datetime.now().strftime("%H%M%S") + '.csv')
    with open(file, 'w', newline='') as f:
        csvwriter = csv.writer(f)
        csvwriter.writerow(['Cell SN', 'Layer LN', 'Layer PN', 'Mag SN', 'Layer number', 'Layer Type'])
        for current_cell in cells:
            if current_cell.Valid:
                for index, anode_layer in enumerate(current_cell.AnodeLayers):
                    if anode_layer.Common.Exists:
                        row = [current_cell.CellSerialNumber,
                               anode_layer.Common.Part_Number,
                               anode_layer.Common.Lot_Number,
                               anode_layer.Common.Mag_SN,
                               index,
                               LayerTypes.Anode]
                        csvwriter.writerow(row)
                for index, cathode_layer in enumerate(current_cell.CathodeLayers):
                    if cathode_layer.Common.Exists:
                        row = [current_cell.CellSerialNumber,
                               cathode_layer.Common.Part_Number,
                               cathode_layer.Common.Lot_Number,
                               cathode_layer.Common.Mag_SN,
                               index,
                               LayerTypes.Cathode]
                        csvwriter.writerow(row)

def read_cells_test() -> [cell]:
    with PLC() as comm:
        comm.IPAddress = '192.168.14.137'
        cells: List[cell] = []
        for cell_index in range(0,CELLCOUNT,1):
            new_cell = cell(cell_index)
            cells.append(new_cell)
            tags = new_cell.get_tags()
            tag_dic = {}
            for tag in tags:
                ret = comm.Read(tag)
                tag_dic[ret.TagName] = ret
            new_cell.set_from_tags(tag_dic)
        return cells

def main():
    cells = read_cells_test()
    direc = os.path.abspath(os.path.curdir + r'\logs')
    if not os.path.exists(direc):
        os.mkdir(direc)
    print_db_rows(direc, cells)

if __name__ == "__main__":
    main()





