import pymssql
import pylogix
from pylogix import PLC
import pandas as pd
import sqlalchemy
from sqlalchemy import create_engine
import pyodbc
from datetime import datetime
#192.168.14.131
#192.168.14.137
#192.168.14.146

username = "spauto"
pw = "5p4ut05p4ut0122"
#db = 'SP_ManuLine'
db = "SP_Automation"
port = "1433"
server = "192.168.14.122"

#1
# 07/18/2021
# PICK AND PLACE
# Cells_To_Upload
# With DB insert and LOOPs through buffarray 
# add valid tags exist==    True checking

#ipaddress = '192.168.14.137'
ipaddress = '192.168.14.131'

cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+db+';UID='+username+';PWD='+ pw)

cursor = cnxn.cursor()

df = []
buffarray = str(0)

debug = 0  # 1 to print to screen, 0 for no printing

#now = datetime.now()
#print("Start Time =", now.strftime("%H:%M:%S"))

#print("buffarray = " + buffarray)
#print("---------------------------------------------------------------")

def getBufferMonitorData(buffarray):
    
    with PLC() as comm:
        comm.IPAddress = ipaddress
        
        ### Valid ####################################################################################
        ret = comm.Read("Cells_To_Upload[" + buffarray + "].Valid")
        valid = ret.Value
        
        if debug == 1:
            #print("--Valid-------------------------------------------------------------\n")
            #print(ret.TagName, ret.Value, ret.Status)
            print("Valid:  " + str(valid))
          
        #if valid == True:
        if 1 == 1:
            
            #### InputLN  ####################################################################
            ret = comm.Read("PNLN_DB_Buffer_Tracker.Buffer[" + buffarray + "].InputLN")
            InputLN = ret.Value
            
            if debug == 1:
                #print(ret.TagName, ret.Value, ret.Status)
                print("InputLN: " + str(InputLN))

            ##### InputPN ################################################################
            ret = comm.Read("PNLN_DB_Buffer_Tracker.Buffer[" + buffarray + "].InputPN")
            InputPN = ret.Value

            if debug ==1:
                #print("--InputPN-------------------------------------------------------------\n")
                print("InputPN:  " + str(InputPN))
                #print(ret.TagName, ret.Value, ret.Status)

            ret = comm.Read("PNLN_DB_Buffer_Tracker.Buffer[" + buffarray + "].ProductPN")
            ProductPN = ret.Value

            if debug == 1:
                print("ProductPN:  " + str(ProductPN))
                #print("--CommonPartNumber = ProductPN-------------------------------------------------------------\n")
                #print(ret.TagName, ret.Value, ret.Status)

            ###  ProductLN
            ret = comm.Read("PNLN_DB_Buffer_Tracker.Buffer[" + buffarray + "].ProductLN")
            ProductLN = ret.Value

            if debug == 1:
                #print("--ProductLN-------------------------------------------------------------\n")
                #print(ret.TagName, ret.Value, ret.Status)
                print("ProductLN:  " + str(ProductLN))
                print("-------------------------------------------------------------------------------------")

            df.append([InputLN, InputPN, ProductLN, ProductPN])

# IF HISTORY IS WANTED TO BE KEPT THEN UNCOMMENT NEXT 2 LINES
truncatetbl = "truncate table dbo.BufferMonitor"
cursor.execute(truncatetbl)

now = datetime.now()
print("Start Time =", now.strftime("%H:%M:%S"))
print("Getting PLC Data")

for buffarray in range(0,20):
    getBufferMonitorData(str(buffarray))
    
    if debug ==1:
        print("buffarray:  " + str(buffarray)) # + "  buffarray2:  " + str(buffarray2))
    
df = pd.DataFrame(df, columns=['InputLN', 'InputPN', 'ProductLN', 'ProductPN'])

now = datetime.now()
print("PLC Time =", now.strftime("%H:%M:%S"))

for index, row in df.iterrows():
    #print("INSERT")
    cursor.execute("exec dbo.insertBufferMonitor ?,?,?,?", str(row.InputLN), str(row.InputPN), str(row.ProductLN), str(row.ProductPN))
    #cursor.execute("INSERT INTO dbo.BufferMonitor([InputLN], [InputPN], [ProductPN], [ProductLN]) VALUES(?,?,?,?)"
                   #, str(row.InputLN), str(row.InputPN), str(row.ProductPN), str(row.ProductLN))
    
    #print("INSERT INTO dbo.BufferMonitor([InputLN], [InputPN], [ProductLN], [ProductPN]) VALUES(?,?,?,?)"
                   #, str(row.InputLN), str(row.InputPN), str(row.ProductLN), str(row.ProductPN))

cnxn.commit()
cnxn.close()
#print("FINISHED")
now = datetime.now()
print("DB Insert Time =", now.strftime("%H:%M:%S"))