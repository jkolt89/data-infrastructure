import pymssql
import pylogix
from pylogix import PLC
import pandas as pd
import sqlalchemy
from sqlalchemy import create_engine
import pyodbc
from datetime import datetime
#192.168.14.131
#192.168.14.137
#192.168.14.146

#username = "superuser"
#pw = "90rl()RL"
username = "spauto"
pw = "5p4ut05p4ut0122"
#db = 'SP_ManuLine'
db = "SP_Automation"
port = "1433"
server = "192.168.14.122"

#1
# 07/18/2021
# Buffer Monitor
# With DB insert and LOOPs through BOTH buffarray and buffray2 
# add exist and valid tags checking

ipaddress = '192.168.14.137'

cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+db+';UID='+username+';PWD='+ pw)

cursor = cnxn.cursor()

df = []
buffarray = str(0)
buffarray2 = str(0)
upperLimit = 5
debug = 1  # 1 to print to screen, 0 for no printing

#now = datetime.now()
#print("Start Time =", now.strftime("%H:%M:%S"))

#print("buffarray = " + buffarray)
#print("---------------------------------------------------------------")

def getCellsToUpload(buffarray, buffarray2):
    
    with PLC() as comm:
        comm.IPAddress = ipaddress
        
        ### Valid ####################################################################################
        ret = comm.Read("Cells_To_Upload[" + buffarray + "].Valid")
        valid = ret.Value
        
        if debug == 1:
            #print("--Valid-------------------------------------------------------------\n")
            #print(ret.TagName, ret.Value, ret.Status)
            print("Valid:  " + str(valid))
          
        if valid == True:
            
            #print("--Exists-------------------------------------------------------------\n")
            ret = comm.Read("Cells_To_Upload[" + buffarray + "].AnodeLayers[" + buffarray2 + "].Common.Exists")
            exist = ret.Value

            if exist == True:
                if debug == 1:
                    #print(ret.TagName, ret.Value, ret.Status)
                    print("\n***************************  ANODE  ***************************\n")
                    print("exist:  " + str(exist))

                #print("--CellName-------------------------------------------------------------\n")
                ret = comm.Read("Cells_To_Upload[" + buffarray + "].CellSerialNumber")
                CellName = ret.Value
                if debug == 1:
                    #print(ret.TagName, ret.Value, ret.Status)
                    print("Cellname: " + str(CellName))

                #### Common #####################################################################################

                ##### CommonLotNumber = ProductLN ################################################################
                ret = comm.Read("Cells_To_Upload[" + buffarray + "].AnodeLayers[" + buffarray2 + "].Common.Lot_Number")
                ProductLN = ret.Value

                if debug ==1:
                    #print("--CommonLotNumber-------------------------------------------------------------\n")
                    print("ProductLN:  " + str(ProductLN))
                    #print(ret.TagName, ret.Value, ret.Status)

                ret = comm.Read("Cells_To_Upload[" + buffarray + "].AnodeLayers[" + buffarray2 + "].Common.Part_Number")
                ProductPN = ret.Value

                if debug == 1:
                    print("ProductPN:  " + str(ProductPN))
                    #print("--CommonPartNumber = ProductPN-------------------------------------------------------------\n")
                    #print(ret.TagName, ret.Value, ret.Status)

                ret = comm.Read("Cells_To_Upload[" + buffarray + "].AnodeLayers[" + buffarray2 + "].Common.Mag_SN")
                MagSN = ret.Value

                if debug == 1:
                    #print("--MAG_SN-------------------------------------------------------------\n")
                    #print(ret.TagName, ret.Value, ret.Status)
                    print("MagSN:  " + str(MagSN))

                ret = comm.Read("Cells_To_Upload[" + buffarray + "].AnodeLayers")
                anodeLayers = ret.Value

                if debug == 1:
                    #print("--MAG_SN-------------------------------------------------------------\n")
                    #print(ret.TagName, ret.Value, ret.Status)
                    print("anodeLayers:  " + str(anodeLayers))
                    
            ############################################################################################################
            ## cathode layers
            #########################################################################################################
            
            #######  Exists  ################################################################################################## 
            
            ret = comm.Read("Cells_To_Upload[" + buffarray + "].CathodeLayers[" + buffarray2 + "].Common.Exists")
            exist = ret.Value

            if exist == True:
                if debug == 1:
                    #print(ret.TagName, ret.Value, ret.Status)
                    print("\n***************************  CATHODE  ***************************\n")
                    print("exist:  " + str(exist))
                    
                #######  Cathode Layers  #####################################################################################
                #ret = comm.Read("Cells_To_Upload[" + buffarray + "].CathodeLayers")
                ret = comm.Read("Cells_To_Upload[0].CathodeLayers")
                cathodeLayers = ret.Value

                #######  Product Lot Number  #####################################################################################
                #ret = comm.Read("Cells_To_Upload[" + buffarray + "].CathodeLayers[" + buffarray2 + "].Common.Lot_Number")
                ret = comm.Read("Cells_To_Upload[0].CathodeLayers[0].Common.Lot_Number")
                ProductLN = ret.Value

                if debug ==1:
                    print("ProductLN:  " + str(ProductLN))
                    #print(ret.TagName, ret.Value, ret.Status)

                #######  Product Part Number  #####################################################################################
                ret = comm.Read("Cells_To_Upload[" + buffarray + "].CathodeLayers[" + buffarray2 + "].Common.Part_Number")
                ProductPN = ret.Value

                if debug == 1:
                    print("ProductPN:  " + str(ProductPN))
                    #print(ret.TagName, ret.Value, ret.Status)

                #######  Mag_SN  #####################################################################################
                ret = comm.Read("Cells_To_Upload[" + buffarray + "].CathodeLayers[" + buffarray2 + "].Common.Mag_SN")
                MagSN = ret.Value

                if debug == 1:
                    #print(ret.TagName, ret.Value, ret.Status)
                    print("cathodeLayers:  " + str(MagSN))
                    print("-------------------------------------------------------------------------------------")

            LayerNr = buffarray2
            df.append([CellName, ProductLN, ProductPN, MagSN, LayerNr, anodeLayers, cathodeLayers])
            #df.append([CellName, ProductLN, ProductPN, MagSN, LayerNr])

# IF HISTORY IS NOT WANTED TO BE KEPT THEN UNCOMMENT NEXT 2 LINES
#truncatetbl = "truncate table dbo.[PickNPlace]"
#cursor.execute(truncatetbl)

now = datetime.now()
print("Start Time =", now.strftime("%H:%M:%S"))
print("Getting PLC Data")

for buffarray in range(0,upperLimit):

    for buffarray2 in range(0,upperLimit):
        getCellsToUpload(str(buffarray), str(buffarray2))
        
        if debug ==1:
            print("buffarray:  " + str(buffarray) + "  buffarray2:  " + str(buffarray2))
        
    df = pd.DataFrame(df, columns=['CellName', 'ProductLN', 'ProductPN', 'MagSN', 'LayerNr', 'anodeLayers', 'cathodeLayers'])

now = datetime.now()
print("PLC Time =", now.strftime("%H:%M:%S"))

for index, row in df.iterrows():
    #print("INSERT")
    #cursor.execute("exec dbo.insertPickNPlace ?,?,?,?,?", str(row.CellName), str(row.ProductLN), str(row.ProductPN), str(row.MagSN), str(row.LayerNr))
    
    ###########################################################################################################################################################
    ## BY STORED PROCEDURE
    
    cursor.execute("exec dbo.insertPickNPlace @CellName='" + str(row.CellName) + "', @ProductLN='" + str(row.ProductLN) + "', @ProductPN='" + str(row.ProductPN) + "', @Mag_SN='" + str(row.MagSN) + "', @LayerNr= '" + str(row.LayerNr) + "'")
    
    #print("exec dbo.insertPickNPlace @CellName='" + str(row.CellName) + "', @ProductLN='" + str(row.ProductLN) + "', @ProductPN='" + str(row.ProductPN) + "', @Mag_SN='" + str(row.MagSN) + "', @LayerNr= '" + str(row.LayerNr) + "'")
    
    ###########################################################################################################################################################
    ## BY HARDCODED INSERT
    
    #cursor.execute("INSERT INTO dbo.PickNPlace([CellName], [ProductLN], [ProductPN], [Mag_SN], LayerNr, [AnodeLayers], [CathodeLayers] ) VALUES(?,?,?,?,?,?,?)"
                   #, str(row.CellName), str(row.ProductLN), str(row.ProductPN), str(row.MagSN), str(row.LayerNr), str(row.anodeLayers), str(row.cathodeLayers))
    
    #print("INSERT INTO dbo.PickNPlace([CellName], [ProductLN], [ProductPN], [Mag_SN], LayerNr, [AnodeLayers], [CathodeLayers]) VALUES(?,?,?,?,?,?)"
                   #, str(row.CellName), str(row.ProductLN), str(row.ProductPN), str(row.MagSN), str(row.LayerNr), str(row.anodeLayers), str(row.cathodeLayers))

    
cnxn.commit()
cnxn.close()

now = datetime.now()
print("DB Insert Time =", now.strftime("%H:%M:%S"))
