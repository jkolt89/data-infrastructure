import pymssql
import pylogix
from pylogix import PLC
import pandas as pd
import sqlalchemy
from sqlalchemy import create_engine
import pyodbc

#192.168.14.131
#192.168.14.137
#192.168.14.146

#username = "superuser"
#pw = "90rl()RL"
username = "spauto"
pw = "5p4ut05p4ut0122"
#db = 'SP_ManuLine'
db = "SP_Automation"
port = "1433"
server = "192.168.14.122"

# 07/18/2021
#PICK AND PLACE
# Cells_To_Upload
# With DB insert and LOOPs through BOTH buffarray and buffray2 

ipaddress = '192.168.14.137'

cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+db+';UID='+username+';PWD='+ pw)

cursor = cnxn.cursor()

df = []
buffarray = str(0)
buffarray2 = str(0)
debug = 0  # 1 to print to screen, 0 for no printing

#print("buffarray = " + buffarray)
#print("---------------------------------------------------------------")

def getCellsToUpload(buffarray, buffarray2):
    with PLC() as comm:
        comm.IPAddress = ipaddress

        ### Valid ####################################################################################
        ret = comm.Read("Cells_To_Upload[" + buffarray + "].Valid")
        valid = ret.Value
        
        if debug == 1:
            #print("--Valid-------------------------------------------------------------\n")
            #print(ret.TagName, ret.Value, ret.Status)
            print("Valid:  " + str(valid))
            
        #print("--CellSerialNumber-------------------------------------------------------------\n")
        ret = comm.Read("Cells_To_Upload[" + buffarray + "].CellSerialNumber")
        cellSerialNumber = ret.Value
        if debug == 1:
            #print(ret.TagName, ret.Value, ret.Status)
            print("cellSerialNumber: " + str(cellSerialNumber))
            
        #### Common #####################################################################################
        
        #print("--Common-------------------------------------------------------------\n")
        #ret = comm.Read("Cells_To_Upload[" + buffarray + "].AnodeLayers[" + buffarray2 + "].Common")
        #print(ret.TagName, ret.Value[0:10], ret.Status)

        ##### CommonLotNumber = ProductLN ################################################################
        
        ProductLN = ret.Value
        
        if debug ==1:
            #print("--CommonLotNumber-------------------------------------------------------------\n")
            print("ProductLN:  " + str(ProductLN))
            #print(ret.TagName, ret.Value, ret.Status)

        
        
        ret = comm.Read("Cells_To_Upload[" + buffarray + "].AnodeLayers[" + buffarray2 + "].Common.Part_Number")
        ProductPN = ret.Value
        
        if debug == 1:
            print("ProductPN:  " + str(ProductPN))
            #print("--CommonPartNumber = ProductPN-------------------------------------------------------------\n")
            #print(ret.TagName, ret.Value, ret.Status)
        
        ret = comm.Read("Cells_To_Upload[" + buffarray + "].AnodeLayers[" + buffarray2 + "].Common.Mag_SN")
        MagSN = ret.Value
        
        if debug == 1:
            #print("--MAG_SN-------------------------------------------------------------\n")
            #print(ret.TagName, ret.Value, ret.Status)
            print("MagSN:  " + str(MagSN))
        
        #print("--Exists-------------------------------------------------------------\n")
        ret = comm.Read("Cells_To_Upload[" + buffarray + "].AnodeLayers[" + buffarray2 + "].Common.Exists")
        exist = ret.Value
        
        if debug == 1:
            #print(ret.TagName, ret.Value, ret.Status)
            print("exist:  " + str(exist))
            print("-------------------------------------------------------------------------------------")
        
        df.append([cellSerialNumber, ProductLN, ProductPN, MagSN, valid, exist])

# IF HISTORY IS NOT WANTED TO BE KEPT THEN UNCOMMENT NEXT 2 LINES
truncatetbl = "truncate table dbo.PickNPlace"
cursor.execute(truncatetbl)
        
for buffarray in range(0,20):
    #buffarray2 = buffarray
    for buffarray2 in range(0,20):
        getCellsToUpload(str(buffarray), str(buffarray2))
        o
        if debug ==1:
            print("buffarray:  " + str(buffarray) + "  buffarray2:  " + str(buffarray2))
        
df = pd.DataFrame(df, columns=['cellSerialNumber', 'ProductLN', 'ProductPN', 'MagSN', 'Valid', 'exist'])

for index, row in df.iterrows():
    #print("INSERT")
    cursor.execute("INSERT INTO dbo.PickNPlace([CellSerialNumber], [ProductLN], [ProductPN], [Mag_SN], [Valid], [Exist]) VALUES(?,?,?,?,?,?)"
                   , row.cellSerialNumber, str(row.ProductLN), row.ProductPN, row.MagSN, str(row.Valid), str(row.exist))
    #print("INSERT INTO dbo.PickNPlace([CellSerialNumber], [ProductLN], [ProductPN], [Mag_SN], [Valid], [Exist]) VALUES(?,?,?,?,?,?)"
                   #, row.cellSerialNumber, row.ProductLN, row.ProductPN, row.MagSN, row.Valid, row.exist)

cnxn.commit()
cnxn.close()

print("FINISHED")