import pymssql
import pylogix
from pylogix import PLC
import pandas as pd
import sqlalchemy
from sqlalchemy import create_engine
import pyodbc

#192.168.14.131
#192.168.14.137
#192.168.14.146

username = "spauto"
pw = "5p4ut05p4ut0122"
db = "SP_Automation"
port = "1433"
server = "192.168.14.122"

# READS MAGAZINER PLC MAGAZINE CONTENTS DATA INTO DATAFRAME AND INSERTS DATAFRAME INTO DB!!!
#####################################################################################################################

cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+db+';UID='+username+';PWD='+ pw)
#cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER='+server+';DATABASE='+db+';UID='+username+';PWD='+ pw)
#cnxn = pyodbc.connect(server=server, user=username, password=pw, database=db)
cursor = cnxn.cursor()

ipaddress = '192.168.14.131'
buffarray = str(0)
#df = pd.DataFrame()
df = []
debug = 0 # 1 to print to screen, 0 for no printing

def getMagazinerData(buffarray):
    with PLC() as comm:
        comm.IPAddress = ipaddress

        ret = comm.Read("Magazine_Contents[" + buffarray + "].Serial_Number")
        serialnr = str(ret.Value)
        ret = comm.Read("Magazine_Contents[" + buffarray + "].Layer_Count")
        layercount = str(ret.Value)
        ret = comm.Read("Magazine_Contents[" + buffarray + "].Electrode_Part_Number")
        electrodePN = str(ret.Value)
        ret = comm.Read("Magazine_Contents[" + buffarray + "].Electrode_Lot_Number")
        electrodeLN = str(ret.Value)
        
        #regular insert
        #print(serialnr, layercount, electrodePN, electrodeLN)
        #dbinsert = "INSERT INTO Magaziner([SerialNumber], [LayerCount], [ElectrodePN], [ElectrodeLN]) VALUES (" + serialnr + "', '" + layercount + "', '" + electrodePN + "', '" + electrodeLN + "')"
        #print(dbinsert)
        
        df.append([serialnr, layercount, electrodePN, electrodeLN])
        # TO DO: pandas insert


# IF HISTORY IS NOT WANTED TO BE KEPT THEN UNCOMMENT NEXT 2 LINES
truncatetbl = "truncate table MagazinerContent"
cursor.execute(truncatetbl)

for buffarray in range(0,100):
    #print(buffarray)
    getMagazinerData(str(buffarray))

#print(df)

df = pd.DataFrame(df, columns=['SerialNumber', 'LayerCount', 'ElectrodePN', 'ElectrodeLN'])
#print(df)

for index, row in df.iterrows():
    #print("INSERT")
    #cursor.execute("INSERT INTO Magaziner([SerialNumber], [LayerCount], [ElectrodePN], [ElectrodeLN]) VALUES(?,?,?,?)", row.SerialNumber, row.LayerCount, row.ElectrodePN, row.ElectrodeLN)
    #cursor.execute("INSERT INTO dbo.MagazinerContent([SerialNumber], [LayerCount], [ElectrodePN], [ElectrodeLN]) VALUES(?,?,?,?)"
                   #, row.SerialNumber, row.LayerCount, row.ElectrodePN, row.ElectrodeLN)
    cursor.execute("exec dbo.insertMagazinerContent ?,?,?,?", str(row.SerialNumber), str(row.LayerCount), str(row.ElectrodePN), str(row.ElectrodeLN))

cnxn.commit()
cnxn.close()

if debug == 1:
    print("FINISH")
