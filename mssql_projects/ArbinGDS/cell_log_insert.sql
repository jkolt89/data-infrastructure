alter procedure Cell_Log_Insert
	@cellname varchar(500)
AS
BEGIN
-- exec Cell_Log_Insert @cellname='deleteme'
-- select * from Cell_log
-- delete from cell_log where clid=2235
INSERT INTO [dbo].[cell_log]
           (
		   [CellName]
           --,[PurposeDescription]
           --,[Cathode]
           --,[temperature]
           --,[MaxVoltage]
           --,[die]
           --,[CurrentCollector]
           --,[ActiveCathode]
           --,[ActiveCathodeDateCoated]
           --,[electrolyte]
           --,[TotalCathodeMass_g]
           --,[DieDiameter_cm]
           --,[CathodeParticleCoating]
           --,[WeightCoatingPercent]
           --,[NominalPercentActiveMaterial]
           --,[RatioActiveMaterial]
           --,[ActiveMaterialTheoreticalCapacity_mAhg]
           --,[CellTheoreticalCapacity_mAhg]
           --,[Loading_mg_cm2]
           --,[Capacity_mAh]
           --,[Anode]
           --,[AnodeMass_g]
           --,[Separator]
           --,[SeparatorMass_g]
           --,[MaximumStackPressure_ton]
           --,[CathodePressure_ton]
           --,[CageTorque_lbs]
           --,[Operator]
           --,[Location]
           --,[NrCyclesCompleted]
           --,[FirstCycleCEPercent]
           --,[FirstCycleCharge]
           --,[FirstCycleDischarge]
           --,[InitialResistance_ohm]
           --,[MaxVoltageWithOutlier]
           --,[MaxVoltageNoOutlier]
           --,[MaxDischargeCapacity]
           --,[ExcelTime]
           --,[PythonTime]
           --,[LastDateTime]
           --,[Channel]
           --,[ArbinNotes]
           --,[StoppageReason]
           --,[StoppageCycleNr]
           --,[Notes]
		   )
     VALUES
           (
		   @cellname
           --,<PurposeDescription, varchar(1000),>
           --,<Cathode, varchar(1000),>
           --,<temperature, varchar(500),>
           --,<MaxVoltage, varchar(50),>
           --,<die, varchar(500),>
           --,<CurrentCollector, varchar(500),>
           --,<ActiveCathode, varchar(500),>
           --,<ActiveCathodeDateCoated, varchar(500),>
           --,<electrolyte, varchar(500),>
           --,<TotalCathodeMass_g, varchar(500),>
           --,<DieDiameter_cm, varchar(500),>
           --,<CathodeParticleCoating, varchar(500),>
           --,<WeightCoatingPercent, varchar(500),>
           --,<NominalPercentActiveMaterial, varchar(500),>
           --,<RatioActiveMaterial, varchar(500),>
           --,<ActiveMaterialTheoreticalCapacity_mAhg, varchar(500),>
           --,<CellTheoreticalCapacity_mAhg, varchar(500),>
           --,<Loading_mg_cm2, varchar(500),>
           --,<Capacity_mAh, varchar(500),>
           --,<Anode, varchar(500),>
           --,<AnodeMass_g, varchar(500),>
           --,<Separator, varchar(500),>
           --,<SeparatorMass_g, varchar(500),>
           --,<MaximumStackPressure_ton, varchar(500),>
           --,<CathodePressure_ton, varchar(500),>
           --,<CageTorque_lbs, varchar(500),>
           --,<Operator, varchar(500),>
           --,<Location, varchar(500),>
           --,<NrCyclesCompleted, varchar(500),>
           --,<FirstCycleCEPercent, varchar(500),>
           --,<FirstCycleCharge, varchar(500),>
           --,<FirstCycleDischarge, varchar(500),>
           --,<InitialResistance_ohm, varchar(500),>
           --,<MaxVoltageWithOutlier, varchar(500),>
           --,<MaxVoltageNoOutlier, varchar(500),>
           --,<MaxDischargeCapacity, varchar(500),>
           --,<ExcelTime, varchar(500),>
           --,<PythonTime, varchar(500),>
           --,<LastDateTime, varchar(500),>
           --,<Channel, varchar(500),>
           --,<ArbinNotes, varchar(500),>
           --,<StoppageReason, varchar(500),>
           --,<StoppageCycleNr, varchar(500),>
           --,<Notes, varchar(500),>
		   )
END