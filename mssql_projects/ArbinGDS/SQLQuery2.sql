update [dbo].[ArbinActiveCells_Stage] set FileDate = cast(substring( filetext, 1, charindex(' ', filetext)-1) as date) 
where FileText like '%.res' and FileDate is null
go

declare @maxfiledate date
select @maxfiledate = max(filedate) from ArbinActiveCells_STAGE where filetext like '%.res' and FileName is null
print @maxfiledate

-- delete from ArbinActiveCells_Stage where filedate <> @maxfiledate or FileDate is null or FileText like '%arbinsys.res'
-- delete from [ArbinActiveCells_Stage] where filetext not like '%.res' or filedate <> @maxfiledate or FileText like '%arbinsys.res'
delete from [ArbinActiveCells_Stage] where filetext not like '%.res' or filedate <> cast( getdate() as date) or FileText like '%arbinsys.res'
go

update [dbo].[ArbinActiveCells_Stage] set FileName = reverse(substring( reverse(filetext), 1, charindex(' ', reverse(filetext))-1)) 
where FileName is null
go

update ArbinActiveCells_STAGE set Filename = replace(filename, '.res', '')
--where filename like '%.res' and filename is null
go

update ArbinActiveCells_Stage set Arbin = 'ARB 4'
where Arbin is null
go
--select * from ArbinActiveCells_STAGE where FileText like '%.res' and FileDate is null