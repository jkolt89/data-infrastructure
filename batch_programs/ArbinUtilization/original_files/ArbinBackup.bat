echo off
echo #############################################
echo #                                           #
echo #             ARBIN Data Backup             #
echo #                                           #
echo #############################################
REM delete x: drive if there. Open a new connection to I2CDSM and labe x:. xcopy files
net use p: /delete /yes
REM net use p: "\\I2CDSM\Solid Power Technical\Cell Data\Raw Cell Data-Arbin3"
net use p: "\\I2CDSM\Solid Power Technical\JohnK"
xcopy "C:\ArbinSoftware\MITS_PRO\Data\*.*" "p:\*.*" /c /e /d /y
net use p: /delete /yes 