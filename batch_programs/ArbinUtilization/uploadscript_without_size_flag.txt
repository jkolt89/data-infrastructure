option echo off 
option batch on 
option confirm off

#open/auth connection 
open sftp://solidpower:LyftBatt5280@192.168.15.25

#ex for how to put a single file onto server 
#put C:\Users\Arbin\Desktop\test.txt /home/solidpower/

#synchronize local data dir to server data dir
# the | excludes files, see this:  https://winscp.net/forum/viewtopic.php?t=14174
# resumesupport=off disabless the default .filepart transfer which really bogs down the Ubuntu server by essentially giving it thousands of files to copy

# example if you need to block a file for some reason
#synchronize remote -resumesupport=off -filemask="*.res | BF_190122*;BF_c2f_181214c*;BF_c2f_190328*;HASP_Li-LPSBI-LPSBI*"  C:\ArbinSoftware\MITS_PRO\Data /home/solidpower/raid/data/raw_files

#the mirror switch means even if the files on the remote have a newer timestamp, which sometimes happens with arbin .res files (which are 
#really microsoft access files which rarely even get their timestamp updated but occassionally end up for example 1 second newer on the
#server (the remote)).

#the criteria switch tells the program to compare the file size to determine if the files are different (as opposed to using time)
#this switch may be unneccesary when using the -mirror switch

#docs at winscp.net/eng/docs/scriptcommand_synchronize

#you can add the -preview switch if you want to run the script but not have it actually do anything.
#in the script that launches this code just add:  cmd /k   at the end so the window will stay open and you can view the results.

synchronize remote -mirror -resumesupport=off -filemask="*.res"  C:\ArbinSoftware\MITS_PRO\Data /home/solidpower/raid/data/raw_files
exit