# -*- coding: utf-8 -*-
"""
Created on Wed Jun 24 14:02:07 2020

@author: johnk
"""

import tkinter as tk
from tkinter import ttk
import sqlalchemy as sal
from sqlalchemy import create_engine
import pyodbc
from numpy import random as nr
from pandastable import Table
import pandas as pd

username = 'johnk'
pw = '89cr*(CR'
db = 'SP_ManuLine'
port = 1433
server = '192.168.14.119'

cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+db+';UID='+username+';PWD='+ pw)
cursor = cnxn.cursor()

cnxn.autocommit = True

def show_entry_fields():
    print("LotNrGenLocation: %s\nSplit Letter: %s\nUser ID: %s" % (e1.get(), e2.get(), e3.get()))
    
def submit_db():
    LotNrGenLocation = e1.get()
    splitletter = e2.get()
    userid = e3.get()
    print("LotNrGenLocation: " + LotNrGenLocation + "    splitletter: " + splitletter + "     user: " + userid)
    query = "exec getLotNumber @LotNrGenLocation = '" + LotNrGenLocation + "', @SplitLetter = '" + splitletter + "', @EnteredBy = '" + userid + "'"
    print(query)
    cursor.execute(query)
    #print(out)
    
def getLotNum():
    cursor.execute("SELECT top 1 * from LotNumberList order by LNID desc;") 
    lotnum = cursor.fetchone() 
    #while lotnum: 
        #print(lotnum[1])
        #lotnum = cursor.fetchone()
    return lotnum

def quitWindow():
    master.destroy()
    cnxn.close()
    
master = tk.Tk()

master.winfo_toplevel().title("Lot Number Generator")
master.winfo_toplevel().geometry("500x200")

tk.Label(master, text="Lot Nr Gen Location").grid(row=0)
tk.Label(master, text="Split Letter").grid(row=1)
tk.Label(master, text="User ID").grid(row=2)
tk.Label(master, text="Generation").grid(row=3)

e1 = tk.Entry(master, width=50)
e2 = tk.Entry(master, width=50)
e3 = tk.Entry(master, width=50)
e4 = tk.Entry(master, width=50)

e1.grid(row=0, column=1)
e2.grid(row=1, column=1)
e3.grid(row=2, column=1)
e4.grid(row=3, column=1)

tk.Button(master, text='Submit', command=submit_db, bg='cyan', padx=4, pady=4).grid(row=5, column=0, sticky=tk.W, pady=4, padx=4)
tk.Button(master, text='Show', command=show_entry_fields, padx=4, pady=4).grid(row=5, column=1, sticky=tk.W, pady=4, padx=4)
tk.Button(master, text='Quit', command=quitWindow, padx=4, pady=4).grid(row=5, column=2, sticky=tk.W, pady=4, padx=4)

tk.mainloop()