# -*- coding: utf-8 -*-
"""
Created on Tue Jul  7 16:41:10 2020

@author: johnk
"""

import tkinter as tk
from tkinter import ttk
import pyodbc
from numpy import random as nr
import pandas as pd

username = 'johnk'
pw = '89cr*(CR'
db = 'SP_ManuLine'
port = 1433
server = '192.168.14.119'

cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+db+';UID='+username+';PWD='+ pw)
cursor = cnxn.cursor()

cnxn.autocommit = True

# pandas connection and read from weighing table
query = "select LotNumber, UserOpName, lnlgDesc, EntryDate from [dbo].[vuCurrentLotNumbers] where UserOpName not like '%Hannah%' and EntryDate is not null"
df = pd.read_sql_query(query, cnxn)
df = df.head(100).to_numpy()

def show():

    tempList = df

    for i, (LotNr, UserOpName, Desc, EntryDate) in enumerate(tempList, start=1):
            listBox.insert("", "end", values=(i, LotNr, UserOpName, Desc, EntryDate))

LotNrs = tk.Tk() 

label = tk.Label(LotNrs, text="Current Lot Numbers", font=("Arial",30)).grid(row=0, columnspan=3)
# create Treeview with 3 columns
cols = ('Order', 'Lot Nr', 'UserOpName', 'Desc', 'EntryDate')

listBox = ttk.Treeview(LotNrs, columns=cols, show='headings')
# set column headings
for col in cols:
    listBox.heading(col, text=col)   
    
listBox.grid(row=1, column=0, columnspan=2)

showLots = tk.Button(LotNrs, text="Show Lots", width=15, command=show).grid(row=4, column=1)
#closeButton = tk.Button(scores, text="Close", width=15, command=exit).grid(row=4, column=1)

LotNrs.mainloop()