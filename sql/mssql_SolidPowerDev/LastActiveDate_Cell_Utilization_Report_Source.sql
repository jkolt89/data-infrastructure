--use SolidPowerDev
-- TODO: CHECK LAST ACTIVE DATES BETWEEN CELLOG AND CELL TABLE AFTER UPDATE
------- MAIN QUERY -----------------------------------------
--UPDATES LAST ACTIVE DATE IN CELL TABLE TO TODAY'S DATE IF THERE IS NEW ARBIN DATA CAPTURED IN CHANNEL NORMAL TABLE
-- rollback tran jk
use SolidPowerDev
go
--#################################
--BEGIN TRAN jk
--go
SELECT DISTINCT c.CellName, getdate() LAD
into DELME_LastActDate
FROM dbo.cell c
INNER JOIN dbo.ChannelNormalTable cn
on c.CellName = cn.CellName
go
UPDATE dbo.cell SET LastActiveDate = LAD
from DELME_LastActDate d inner join cell c on c.cellname = d.CellName
go
drop table DELME_LastActDate
go
-- commit tran jk
--#####################################


--UPDATE dbo.cell SET LastActiveDate = t.LAD
--from (
---- select distinct c.CellName, c.EntryDate, c.LastActiveDate LAD, getdate() as LastActiveDate
--SELECT DISTINCT c.CellName, getdate() LAD
--FROM dbo.cell c
--INNER JOIN dbo.ChannelNormalTable cn
--on c.CellName = cn.CellName
----where LastActiveDate is null
--) t
--INNER JOIN dbo.cell c
--ON c.CellName = t.CellName
--commit tran jk

--begin tran jk 
update SolidPowerDev.dbo.Cell set LastActiveDate = cl.LastDateTime
from SolidPowerDev.dbo.vuCellLog cl 
inner join SolidPowerDev.dbo.Cell c
on cl.Cellname = c.CellName
where cast(cl.LastDateTime as date) = cast(getdate() as date)


---------ACTIVE CELL COUNT QUERY GROUPED BY INDIVIDUAL ARBINS
select isnull(KeyID, 'TOTAL') KeyID, max(NrChannels) TotalNrChannels, count(KeyID) KeyIDCount, 
cast(cast(count(i.KeyID) as float) / max(i.NrChannels) * 100 as decimal(10,2)) ArbinUtilizationPercent
from dbo.cell c
right outer join dbo.vucelllog cl
	on cl.CellName = c.cellname
inner join Instrument i
	on c.Serial_Number = i.serialnumber
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
	--and dbo.isBBData(c.Cellname) = 'Arbin'
--group by  c.cellname, cl.cellname, channel_number, channel_index, LastDateTime, cl.channel, i.SerialNumber, i.KeyID, i.Description
--group by rollup (keyid)
group by grouping sets (keyid)
UNION ALL
select 'TOTAL' KeyID, 572 as TotalNrChannels, count(keyID) KeyIDCount, cast((cast(count(keyid) as float) / 572 ) * 100 as decimal(8,2)) ArbinUtilizationPercent
from dbo.cell c
right outer join dbo.vucelllog cl
	on cl.CellName = c.cellname
inner join Instrument i
	on c.Serial_Number = i.serialnumber
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
order by 1

-------------------------------------------------------------
--UTILIZAITON PERCENT QUERY
SELECT [Src], ch.[cellname], [ChannelNumber], [ChannelIndex], [ActiveCellChannelCount], [LastDateTime], ch.[NrChannels], ch.[KeyID], [Description], [SerialNumber],
	UPPER(Operator) Operator, UtilizationTotal, UtilizationPercent
  FROM [dbo].[vuChannelsInUse] ch
  full outer join (
    select KeyID, count(KeyID) UtilizationTotal, max(NrChannels) NrChannels, round((cast(count(keyid) as float) / max(nrchannels) *100), 2) UtilizationPercent
  from [vuChannelsInUse]
  group by KeyID
) a
on a.keyid = ch.keyid

---------- END OF MAIN QUERIES -------------------------------

SELECT [Src], ch.[cellname], [ChannelNumber], [ChannelIndex], [ActiveCellChannelCount], [LastDateTime], ch.[NrChannels], isnull(ch.[KeyID], 'BB') KeyID, 
	isnull([Description], 'Becker Board') [Description], [SerialNumber],
	UPPER(isnull(Operator, 'collin')) Operator, UtilizationTotal, UtilizationPercent
  FROM [dbo].[vuChannelsInUse] ch
  full outer join (
    select isnull(KeyID, 'BB') KeyID, count(KeyID) UtilizationTotal, max(NrChannels) NrChannels, round((cast(count(keyid) as float) / max(nrchannels) *100), 2) UtilizationPercent
  from [vuChannelsInUse]
  group by KeyID -- isnull([KeyID], 'BB') 
) a
on a.keyid = ch.keyid

-- TEST
-- CELLS THAT DID NOT HAVE NEW FILES TODAY YET HAVE A CURRENT DATE IN THE CELLLOG OF TODAY
select c.cellname CellCellName, c.LastActiveDate, LastFiledate, cl.CellName vuCellLogCellName, cl.LastDateTime, datediff(day, c.LastActiveDate, cl.LastDateTime) LADDiff
	, Datediff(day, c.LastFiledate, cl.LastDateTime) LFDDiff
	, '' AS blank, * 
from cell c right outer join vucelllog cl on c.CellName = cl.cellname
where cast(cl.LastDateTime as date) = cast(getdate() as date)
and dbo.isBBdata(cl.cellname) = 'Arbin'
and c.cellname is not null
order by Datediff(day, c.LastFiledate, cl.LastDateTime), c.cellname --datediff(day, c.LastActiveDate, cl.LastDateTime) , c.cellname

--CELLS IN CELLLOG NOT IN CELL FOR TODAY
select c.cellname, c.LastActiveDate, cl.CellName, cl.LastDateTime, datediff(day, c.LastActiveDate, cl.LastDateTime) LADDiff
, Datediff(day, c.LastFiledate, cl.LastDateTime) LFDDiff
	, '' AS blank, *
from cell c right outer join vucelllog cl on c.CellName = cl.cellname
where cast(cl.LastDateTime as date) = cast(getdate() as date)
and dbo.isBBdata(cl.cellname) = 'Arbin'
--and c.cellname is not null
order by Datediff(day, c.LastFiledate, cl.LastDateTime), c.cellname --datediff(day, c.LastActiveDate, cl.LastDateTime) , c.cellname



SELECT DISTINCT c.CellName, c.Channel_Number, c.Channel_Index, EntryDate, LastActiveDate,  datediff(day, c.EntryDate, LastActiveDate)
FROM SolidPowerDev.dbo.cell c
INNER JOIN ( select distinct cellname from SolidPowerDev.dbo.ChannelNormalTable) cn
on c.CellName = cn.CellName
order by Channel_number, Channel_Index

select EntryDate, LastActiveDate, datediff(day, EntryDate, LastActiveDate) NrDaysOnline, * from cell where cast(LastActiveDate as date) = cast(getdate() as date)

-- CHECK DIFFERENCES BETWEEN CELL AND VUCELLLOG
select dbo.isBBData(cl.cellname) Src, cl.Cellname clCellname, cl.LastDateTime CLLastDateTime, c.CellName cCellName, 
	c.EntryDate, c.LastActiveDate CellLastActDate, datediff( day, c.LastActiveDate, cl.LastDateTime) DateDffLastActDt
	--, * 
from cell c right outer join vuCellLog cl on c.CellName = cl.CellName 
where cast(LastDateTime as date) = cast(getdate() as date)
order by 1, c.EntryDate desc

---------- BEGIN OF DEVELOPMENT QUERIES - NOT TO BE USED -------------------------------

select * from cell where cast(LastActiveDate as date) = cast(getdate() as date)
select * from cell where LastActiveDate is not null order by LastActiveDate desc
select cellname, EntryDate, LastActiveDate from cell order by 3 desc
select * from vuCellLog where cast(LastDateTime as date) = cast(getdate() as date)
select * from SP_DataWarehouse..celllog where cast([Last DateTime] as date) = cast(getdate() as date) and dbo.isBBData([cell #])='Arbin'

select * from vuCellLog cl
inner join cell c on c.CellName = cl.CellName
where cast(LastDateTime as date) = cast(getdate() as date)
order by LastDateTime desc


select distinct c.CellName, c.EntryDate, c.LastActiveDate LAD, getdate() as LastActiveDate
from cell c
inner join ChannelNormalTable cn
on c.CellName = cn.CellName

select cl.* from Cell c
inner join vuCellLog cl
on c.cellname = cl.cellname

select entrydate, lastactivedate from cell

--commit tran jk
--update cell set LastActiveDate = cl.lastdatetime
--from vuCellLog cl
--inner join cell c
--on cl.cellname = c.CellName

select * from ChannelNormalTable
select * from Global_Table
SELECT * from Cell -- where CellName = 'A2003250005_CCCV_cycling_45oC_Part2'
select * from vucelllog order by 1 desc

select * from cell c
inner join ChannelNormalTable cn
on c.CellName = cn.CellName

-- A2003250005_CCCV_cycling_45oC_Part2 has null LastActiveDate
select distinct c.cellname, c.EntryDate, c.LastActiveDate
from cell c
inner join ChannelNormalTable cn
on c.CellName = cn.CellName

--update cell set LastActiveDate = null where cellname = 'A2003250005_CCCV_cycling_45oC_Part2'

select distinct c.CellName, c.EntryDate, c.LastActiveDate LAD, getdate() as LastActiveDate, datediff(day, c.EntryDate, getdate())
from cell c
inner join ChannelNormalTable cn
on c.CellName = cn.CellName
/*
--SELECT distinct cellname from ChannelNormalTable
--select count(*) from ChannelNormalTable
--where c.cellname in ('NPL00020070041_C5Ch_2CDch_cycling_70oC','NPL00020070042_C5Ch_2CDch_cycling_70oC')
--where LastActiveDate is null

--begin tran jk
--update cell set LastActiveDate = t.LAD
--from (
---- select distinct c.CellName, c.EntryDate, c.LastActiveDate LAD, getdate() as LastActiveDate
--select distinct c.CellName, getdate() LAD
--from cell c
--inner join ChannelNormalTable cn
--on c.CellName = cn.CellName
--where LastActiveDate is null ) t
--inner join cell c
--on c.CellName = t.CellName
--commit tran jk

--UPDATE cell
--SET    LastActiveDate = t.LAD
--FROM   (
--       -- select distinct c.CellName, c.EntryDate, c.LastActiveDate LAD, getdate() as LastActiveDate
--       SELECT DISTINCT c.CellName,
--                       Getdate() LAD
--        FROM   cell c
--               INNER JOIN ChannelNormalTable cn
--                       ON c.CellName = cn.CellName
--       --where LastActiveDate is null
--       ) t
--       INNER JOIN cell c
--               ON c.CellName = t.CellName  
*/
SELECT DISTINCT c.CellName, getdate() LAD
FROM SolidPowerDev.dbo.cell c
INNER JOIN SolidPowerDev.dbo.ChannelStatisticTable cn
on c.CellName = cn.CellName

select distinct cellname from ChannelNormalTable
select distinct cellname from ChannelStatisticTable
select distinct cellname from Global_Table
select dbo.isbbdata(cellname) Src, * from vuCellLog where cast(LastDateTime as date) = cast(getdate() as date) order by 1 

SELECT DISTINCT c.CellName, c.Channel_Number, c.Channel_Index, EntryDate, LastActiveDate,  datediff(day, c.EntryDate, LastActiveDate)
FROM SolidPowerDev.dbo.cell c
INNER JOIN ( select distinct cellname, LastDateTime from SolidPowerDev.dbo.vuCellLog) cn
on c.CellName = cn.CellName
where cast(cn.LastDateTime as date) = cast(getdate() as date)
order by Channel_number, Channel_Index

SELECT DISTINCT dbo.isbbdata(c.cellname) Src, c.CellName, c.Channel_Number, c.Channel_Index, EntryDate, LastDateTime, LastActiveDate,  datediff(day, c.EntryDate, LastActiveDate)
FROM SolidPowerDev.dbo.cell c
left outer JOIN ( select distinct cellname, LastDateTime from SolidPowerDev.dbo.vuCellLog) cn
on c.CellName = cn.CellName
where cast(cn.LastDateTime as date) = cast(getdate() as date)
and c.cellname is not null
order by 1, Channel_number, Channel_Index

select distinct dbo.isbbdata(cl.cellname) Src, c.cellname as ccellname, cl.cellname as clcellname
from cell c right outer join vuCellLog cl on c.cellname = cl.CellName
where cast(cl.LastDateTime as date) = cast(getdate() as date) 
order by 1

select * from vucelllog where cast(LastDateTime as date) = cast(getdate() as date)  and dbo.isBBData(cellname) = 'BB'
select dbo.isbbdata(cellname), * from vucelllog where cast(LastDateTime as date) = cast(getdate() as date) order by 1 
select dbo.isbbdata(cellname), * from vucelllog

--Dates start from 1st January 1900 i.e. 1/1/1900 has a date serial number of 1.

--40909.4166666667
select cast('44033.55652' as date)

select datediff( day, '01/01/1900', '2012/01/01')+2
select datediff( day, '01/01/1900', '2012/01/01')+2
select datediff( day, '01/01/1900', LastDateTime)+1.5 from vuCellLog
	
select dateadd(day, datediff( day, '01/01/1900', LastDateTime)+1.5), ExcelTime)
	from vucelllog

select datediff( day, '01/01/1900', LastDateTime)+1.5, ExcelTime, dateadd( day, datediff( day, '01/01/1900', LastDateTime)+1.5, ExcelTime ) 
	, ExcelTime + '01/01/1900'
	from vuCellLog

select --substring( ExcelTime, 0, patindex(ExcelTime,  '.'))
	ExcelTime
	, patindex( '%.%', cast(ExcelTime as varchar(20)))
	, dateadd( day, ExcelTime, '01/01/1900') 
	, dateadd( day, ExcelTime, patindex( '%.%', cast(ExcelTime as varchar(20)))) 
	from vuCellLog

select cl.cellname, LastActiveDate, c.cellname, LastDateTime, * from cell c
right outer join vuCellLog cl on c.CellName = cl.CellName
where cast(cl.LastDateTime as date) = cast(getdate() as date)
--and cast(c.LastActiveDate as date) = cast(getdate() as date)
and dbo.isBBdata(cl.cellname) = 'Arbin'
order by c.cellname

---------------------------------------------------------------------------------
select * from (
select cellname, LastActiveDate from cell
where cast(LastActiveDate as date) = cast(getdate() as date)
) a right outer join (
select cellname, LastDateTime from vuCellLog
where cast(LastDateTime as date) = cast(getdate() as date)
and dbo.isBBdata(cellname) = 'Arbin'
) b
on a.Cellname = b.CellName
order by b.cellname

select c.cellname, c.LastActiveDate, cl.CellName, cl.LastDateTime, datediff(day, c.LastActiveDate, cl.LastDateTime) LADDiff
, Datediff(day, c.LastFiledate, cl.LastDateTime) LFDDiff
	, '' AS blank, *
from cell c right outer join vucelllog cl on c.CellName = cl.cellname
where cast(cl.LastDateTime as date) = cast(getdate() as date)
and dbo.isBBdata(cl.cellname) = 'Arbin'
--and c.cellname is not null
order by datediff(day, c.LastActiveDate, cl.LastDateTime) , c.cellname

select isnull(c.cellname, cl.cellname) Cellname, c.LastActiveDate, c.LastFileDate, c.LastArbinDate  , cl.CellName, cl.LastDateTime
, Datediff(day, c.LastFiledate, cl.LastDateTime) LFDDiff
	, '' AS blank, *
from cell c right outer join vucelllog cl on c.CellName = cl.cellname
where cast(cl.LastDateTime as date) = cast(getdate() as date)
and dbo.isBBdata(cl.cellname) = 'Arbin'
--and c.cellname is not null
order by datediff(day, c.LastActiveDate, cl.LastDateTime) desc, c.cellname

select c.cellname, LastFileDate, LastDateTime
from cell c inner join vucelllog cl on c.CellName = cl.CellName
where cast(cl.LastDateTime as date) = cast(getdate() as date)
order by LastFileDate desc

select cellname, LastActiveDate, LastFileDate
from cell 
where cast(LastActiveDate as date) = cast(getdate() as date)
order by LastActiveDate desc

select cellname, LastDateTime
from vucelllog
where cast(LastDateTime as date) = cast(getdate() as date)
and dbo.Isbbdata(cellname) = 'Arbin'
order by LastDateTime desc

-- 1) *** !!!07/24/2020 Today's arbin count WITH DISTINCT ARBIN INSTRUMENT IDENTIFIER!!!
select dbo.isBBdata(isnull(c.cellname, cl.cellname)) Src, isnull(c.cellname, cl.cellname) cellname, 
	--isnull(cast(channel_number as varchar(30)), 'No .res File Exists') ChannelNumber, 
	isnull(cast(channel_number as varchar(30)), cl.Channel) ChannelNumber, 
	isnull( cast(channel_index as varchar(40)), 'No Channel Index for BB cells') ChannelIndex,
	count(isnull(c.cellname, cl.cellname)) ActiveCellChannelCount, LastDateTime
	, i.KeyID, i.Description, i.SerialNumber
from dbo.cell c
right outer join dbo.vucelllog cl
	on cl.CellName = c.cellname
inner join Instrument i
	on c.Serial_Number = i.serialnumber
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
	--and dbo.isBBData(c.Cellname) = 'Arbin'
group by  c.cellname, cl.cellname, channel_number, channel_index, LastDateTime, cl.channel, i.SerialNumber, i.KeyID, i.Description
order by 1, Channel_Number desc, Channel_Index desc

