--REPORT QUERIES
--Number of cells in use (being tested) today BOTH BB AND ARBIN

--GOOGLE DATA STUDIO REPORT QUERY - UTILIZAITON PERCENT QUERY
	
--where operator = 'uday'


-- 07/24/2020 Today's arbin count WITH DISTINCT ARBIN INSTRUMENT IDENTIFIER!!!
select dbo.isBBdata(isnull(c.cellname, cl.cellname)) Src, isnull(c.cellname, cl.cellname) cellname, 
	--isnull(cast(channel_number as varchar(30)), 'No .res File Exists') ChannelNumber, 
	isnull(cast(channel_number as varchar(30)), cl.Channel) ChannelNumber, 
	isnull( cast(channel_index as varchar(40)), 'No Channel Index for BB cells') ChannelIndex,
	count(isnull(c.cellname, cl.cellname)) ActiveCellChannelCount, LastDateTime
	, i.KeyID, i.Description, i.SerialNumber
from dbo.cell c
right outer join dbo.vucelllog cl
	on cl.CellName = c.cellname
inner join Instrument i
	on c.Serial_Number = i.serialnumber
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
	--and dbo.isBBData(c.Cellname) = 'Arbin'
group by  c.cellname, cl.cellname, channel_number, channel_index, LastDateTime, cl.channel, i.SerialNumber, i.KeyID, i.Description
order by 1, Channel_Number desc, Channel_Index desc

---------------------------------------------------------------------------------------------------------------------
-- select * from SP_Datawarehouse.dbo.CellUtilHistory

--select * into SP_Datawarehouse.dbo.CellUtilHistory
insert into SP_Datawarehouse.dbo.CellUtilHistory([KeyID], [LastDateTime], [TotalNrChannels], [KeyIDCount], [ArbinUtilizationPercent])
--from (
select top 100 percent isnull(KeyID, 'TOTAL') KeyID, max(LastDateTime) LastDateTime, max(NrChannels) TotalNrChannels, count(KeyID) KeyIDCount, 
cast(cast(count(i.KeyID) as float) / max(i.NrChannels) * 100 as decimal(10,2)) ArbinUtilizationPercent
from SolidPowerDev.dbo.cell c
right outer join SolidPowerDev.dbo.vucelllog cl
	on cl.CellName = c.cellname
inner join SolidPowerDev..Instrument i
	on c.Serial_Number = i.serialnumber
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
	--and dbo.isBBData(c.Cellname) = 'Arbin'
--group by  c.cellname, cl.cellname, channel_number, channel_index, LastDateTime, cl.channel, i.SerialNumber, i.KeyID, i.Description
--group by rollup (keyid)
group by grouping sets (keyid)
UNION ALL
select top 100 percent 'TOTAL' KeyID, max(LastDateTime) LastDateTime, 572 as TotalNrChannels, count(keyID) KeyIDCount, cast((cast(count(keyid) as float) / 572 ) * 100 as decimal(8,2)) ArbinUtilizationPercent
from SolidPowerDev.dbo.cell c
right outer join SolidPowerDev.dbo.vucelllog cl
	on cl.CellName = c.cellname
inner join SolidPowerDev..Instrument i
	on c.Serial_Number = i.serialnumber
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
order by 1
--) a

-------------------------------------------------------------

--#########################################################################################################################
-- *** 07/24/2020 count active cells for current day by channel_number and channel_index. get cells right outer join for both those in cell table and not in
select dbo.isBBdata(isnull(c.cellname, cl.cellname)) Src, isnull(c.cellname, cl.cellname) cellname, 
	--isnull(cast(channel_number as varchar(30)), 'No .res File Exists') ChannelNumber, 
	isnull(cast(channel_number as varchar(30)), cl.Channel) ChannelNumber, 
	isnull( cast(channel_index as varchar(40)), 'No Channel Index for BB cells') ChannelIndex,
	count(isnull(c.cellname, cl.cellname)) ActiveCellChannelCount, LastDateTime
from SolidPowerDev.dbo.cell c
right outer join SolidPowerDev.dbo.vucelllog cl
on cl.CellName = c.cellname
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
	--and dbo.isBBData(c.Cellname) = 'Arbin'
group by  c.cellname, cl.cellname, channel_number, channel_index, LastDateTime, cl.channel
order by 1, Channel_Number desc, Channel_Index desc


-- 07/10/2020 !!!NEW QUERY FOR ALEX count active cells for current day by channel_number and channel_index
select dbo.isBBdata(c.cellname) Src, c.cellname, channel_number, channel_index, count(c.cellname) ActiveCellChannelCount, LastDateTime
from SolidPowerDev.dbo.cell c
inner join SolidPowerDev.dbo.vucelllog cl
on cl.CellName = c.cellname
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
	--and dbo.isBBData(c.Cellname) = 'Arbin'
group by  c.cellname, channel_number, channel_index, LastDateTime
order by 1, Channel_Number, Channel_Index


---------ACTIVE CELL COUNT QUERY GROUPED BY INDIVIDUAL ARBINS
select isnull(KeyID, 'TOTAL') KeyID, count(KeyID) KeyIDCount
from dbo.cell c
right outer join dbo.vucelllog cl
	on cl.CellName = c.cellname
inner join Instrument i
	on c.Serial_Number = i.serialnumber
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
group by rollup(keyid)
order by 1
------------------------------------------------------------------------
-- same as above with Nr Days Cell is online and the date differences between my cell table and the celllog
select dbo.isBBdata(c.cellname) Src, c.cellname, channel_number, channel_index, count(c.cellname) ActiveCellChannelCount, EntryDate, LastDateTime, LastActiveDate
	, datediff(day, c.EntryDate, getdate()) DaysOnline
from SolidPowerDev.dbo.cell c
inner join SolidPowerDev.dbo.vucelllog cl
on cl.CellName = c.cellname
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
	--and dbo.isBBData(c.Cellname) = 'Arbin'
group by  c.cellname, channel_number, channel_index, LastDateTime, c.EntryDate, LastActiveDate
order by 1, Channel_Number, Channel_Index

-------------------------------------------------------------------------

-- SAME AS ABOVE QUERY WITHOUT CELLNAME
select channel_number, channel_index, count(c.cellname) ActiveCellChannelCount, LastDateTime
from SolidPowerDev.dbo.cell c
inner join SolidPowerDev.dbo.vucelllog cl
on cl.CellName = c.cellname
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
group by  channel_number, channel_index, LastDateTime
order by Channel_Number, Channel_Index

------------------------------------------------------------------------

select SolidPowerDev.dbo.isBBData(v.cellname) Src, count(SolidPowerDev.dbo.isBBData(v.cellname)) ActiveCells, cast(v.LastDateTime as Date) LastDateActive
from SolidPowerDev.dbo.vuCellLog v
where cast(v.LastDateTime as date) is not null 
group by SolidPowerDev.dbo.isbbdata(v.cellname), cast(v.LastDateTime as Date)
order by cast(v.LastDateTIme as date) desc  


--	ALL CELLS THAT ARE RUNNING TODAY  (Arbin and BB)
select SolidPowerDev.dbo.isBBData(cellname), * from SolidPowerDev.dbo.vuCellLog 
where cast(cast([LastDateTime] as datetime) as date) = cast(getdate() as date)


--active arbin only channel cell count
select Channel_Number, count(CountChannel_Number)
from (
select c.CellName, c.Channel_Number, count(c.Channel_Number) CountChannel_Number
from SolidPowerDev.dbo.ChannelNormal cn
inner join SolidPowerDev.dbo.Cell c
	on cn.CellName = c.CellName
where cast(cast( Date_time as datetime) as date) = cast(getdate() as date)
group by c.CellName, c.Channel_Number
) a
group by Channel_Number

--Number of cells in use (being tested) today  ARBIN ONLY
select SolidPowerDev.dbo.isbbdata(v.cellname) Src, channel, count(v.cellname) CellChannelCount, cast(v.LastDateTime as Date) LastDate
--from SolidPowerDev.dbo.cell c
--inner join SolidPowerDev.dbo.vuCellLog v
from SolidPowerDev.dbo.vuCellLog v
--on c.cellname = v.cellname
where cast(v.LastDateTime as date) is not null -- datepart(dd, v.LastDateTime) = datepart(dd,getdate())
--and  datepart(m, v.LastDateTime) = datepart(m,getdate())
--and  datepart(yy, v.LastDateTime) = datepart(yy,getdate())
--where cast(v.LastDateTime as date) = cast(getdate() as date)
group by SolidPowerDev.dbo.isbbdata(v.cellname), v.Channel, cast(v.LastDateTime as Date)
order by cast(v.LastDateTIme as date) desc  --channel_number

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- SCRATCH
--WIP
select --top 100 
	isnull(SolidPowerDev.dbo.isBBData(c.cellname), SolidPowerDev.dbo.isBBData(c.cellname)) Src, cast(cast(Start_DateTime as datetime) as date), * 
from SolidPowerDev.dbo.cell c
left outer join
	BeckerBoard.dbo.BBCell b
	on c.Cellname = b.CellName
where cast(cast(c.Start_DateTime as datetime) as date) = cast(cast(getdate() as datetime) as date)
or cast(cast(b.EntryDate as datetime) as date) = cast(cast(getdate() as datetime) as date)

select * from 
--	ALL CELLS THAT ARE RUNNING TODAY  (Arbin and BB)
select dbo.isBBData(cellname), * from vuCellLog 
where datepart(dd, cast([LastDateTime] as datetime)) = datepart(dd,getdate())
and  datepart(m, cast([LastDateTime] as datetime) ) = datepart(m,getdate())
and  datepart(yy, cast([LastDateTime] as datetime) ) = datepart(yy,getdate())

-- NR OF CHANNELS USED THROUGHOUT HISTORY
SELECT distinct channel_number, count(channel_Number) from cell group by channel_number
select Cellname, LastDateTime, Channel from SolidPowerDev.dbo.vuCellLog where cast(LastDateTime as date) = cast(getdate() as date)

-- ALL CELLS BEING TESTED TODAY (ARBIN AND BB)
select * from SolidPowerDev.dbo.vuCellLog
where datepart(dd, LastDateTime) = datepart(dd,getdate())
and  datepart(m, LastDateTime) = datepart(m,getdate())
and  datepart(yy, LastDateTime) = datepart(yy,getdate())

--ARBIN CELLS BEING TESTED TODAY AND THE CHANNEL THEY ARE ON
select cellname, channel, count(channel) channelCount 
from SolidPowerDev.dbo.vuCellLog
where datepart(dd, LastDateTime) = datepart(dd,getdate())
and  datepart(m, LastDateTime) = datepart(m,getdate())
and  datepart(yy, LastDateTime) = datepart(yy,getdate())
and [SolidPowerDev].dbo.[isBBData](cellname) = 'Arbin'
group by channel, cellname
order by channel

select channel_number, channel_index
from cell
where datepart(dd, LastDateTime) = datepart(dd,getdate())
and  datepart(m, LastDateTime) = datepart(m,getdate())
and  datepart(yy, LastDateTime) = datepart(yy,getdate())
and [dbo].[isBBData](cellname) = 'Arbin'

select  dbo.isBBdata(cellname) Srce, channel, count(channel) channelCount from vuCellLog
where datepart(dd, LastDateTime) = datepart(dd,getdate())
and  datepart(m, LastDateTime) = datepart(m,getdate())
and  datepart(yy, LastDateTime) = datepart(yy,getdate())
and [dbo].[isBBData](cellname) = 'Arbin'
group by cellname, channel
order by channel

select * from 

select cellname, channel, count(channel) channelCount from vuCellLog
where datepart(dd, LastDateTime) = datepart(dd,getdate())
and  datepart(m, LastDateTime) = datepart(m,getdate())
and  datepart(yy, LastDateTime) = datepart(yy,getdate())
and [dbo].[isBBData](cellname) = 'Arbin'
group by channel, cellname
order by channel

select distinct channel_number, count(channel_number) from cell group by Channel_Number

select count(channel_number), channel_number from cell
where cellname in (
select cellname 
from vuCellLog
where datepart(dd, LastDateTime) = datepart(dd,getdate())
and  datepart(m, LastDateTime) = datepart(m,getdate())
and  datepart(yy, LastDateTime) = datepart(yy,getdate())
and [dbo].[isBBData](cellname) = 'Arbin'
group by cellname)
group by Channel_Number --,channel_index, 
order by Channel_Number--, channel_index

select cellname 
from vuCellLog
where datepart(dd, LastDateTime) = datepart(dd,getdate())
and  datepart(m, LastDateTime) = datepart(m,getdate())
and  datepart(yy, LastDateTime) = datepart(yy,getdate())
and [dbo].[isBBData](cellname) = 'Arbin'
group by cellname

select channel_index, channel_number from cell
where cellname in (
select cellname 
from vuCellLog
where datepart(dd, LastDateTime) = datepart(dd,getdate())
and  datepart(m, LastDateTime) = datepart(m,getdate())
and  datepart(yy, LastDateTime) = datepart(yy,getdate())
and [dbo].[isBBData](cellname) = 'Arbin'
group by cellname)
--group by Channel_Number --,channel_index, 
order by Channel_Number--, channel_index

select channel_index, channel_number from cell
where cellname in (
select cellname 
from vuCellLog
where datepart(dd, LastDateTime) = datepart(dd,getdate())
and  datepart(m, LastDateTime) = datepart(m,getdate())
and  datepart(yy, LastDateTime) = datepart(yy,getdate())
and [dbo].[isBBData](cellname) = 'Arbin'
group by cellname)
--group by Channel_Number --,channel_index, 
order by Channel_Number--, channel_index


select count(channel_index), channel_number from cell
where cellname in (
select cellname 
from vuCellLog
where datepart(dd, LastDateTime) = datepart(dd,getdate())
and  datepart(m, LastDateTime) = datepart(m,getdate())
and  datepart(yy, LastDateTime) = datepart(yy,getdate())
and [dbo].[isBBData](cellname) = 'Arbin'
group by cellname)
group by channel_index, Channel_Number --,
order by Channel_Number--, channel_index


select max(channel_Number) Channel_Number, channel_index, count(channel_index) ChannelIndexCount from cell
where cellname in (
select cellname 
from vuCellLog
where datepart(dd, LastDateTime) = datepart(dd,getdate())
and  datepart(m, LastDateTime) = datepart(m,getdate())
and  datepart(yy, LastDateTime) = datepart(yy,getdate())
and [dbo].[isBBData](cellname) = 'Arbin'
group by cellname)
group by channel_index, Channel_Number --,
order by Channel_Number, channel_index

select 'Arbin' Source, max(channel_Number) Channel_Number,  count(channel_number) ChannelNrCount from cell
where cellname in (
select cellname 
from vuCellLog
where datepart(dd, LastDateTime) = datepart(dd,getdate())
and  datepart(m, LastDateTime) = datepart(m,getdate())
and  datepart(yy, LastDateTime) = datepart(yy,getdate())
and [dbo].[isBBData](cellname) = 'Arbin'
group by cellname)
group by  Channel_Number --,
--order by Channel_Number
union all
select 'BB', max(channel_Number) Channel_Number,  count(channel_number) ChannelNrCount from cell
where cellname in (
select cellname 
from vuCellLog
where datepart(dd, LastDateTime) = datepart(dd,getdate())
and  datepart(m, LastDateTime) = datepart(m,getdate())
and  datepart(yy, LastDateTime) = datepart(yy,getdate())
and [dbo].[isBBData](cellname) = 'BB'
group by cellname)
group by  Channel_Number --,
order by Channel_Number


select channel_number, channel_index
from cell where cellname in (
select cellname 
from vuCellLog
where datepart(dd, LastDateTime) = datepart(dd,getdate())
and  datepart(m, LastDateTime) = datepart(m,getdate())
and  datepart(yy, LastDateTime) = datepart(yy,getdate())
and [dbo].[isBBData](cellname) = 'Arbin'
)
order by Channel_Number, Channel_Index

select max(lastdatetime) from vuCellLog

-- number of active channel_number, channel_index count
select channel_number, channel_index, count(channel_index) ActiveCellChannelCount, cast(LastDateTime as date)
from SolidPowerDev.dbo.cell c
inner join SolidPowerDev.dbo.vucelllog cl
on cl.CellName = c.cellname
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
group by  channel_number, channel_index, cast(LastDateTime as date)
order by Channel_Number, channel_index

---------------------------------------------------------------
--MISC QUERIES

--select * from cell order by entrydate desc
--select cast(EntryDate as varchar(11)) as EntryDate, count(cast(EntryDate as varchar(11)))
----, count(*) 
--from cell 
--group by cast(cast(EntryDate as varchar(11)) as datetime)
--order by entrydate desc

select cast(EntryDate as varchar(11)) EntryDate, count(channel_index)--datepart(dd, EntryDate) dy, 
channel_index, Channel_Number--, Channel_Type
from cell
where datepart(dd, EntryDate) = datepart(dd,getdate())
and  datepart(m, EntryDate) = datepart(m,getdate())
and  datepart(yy, EntryDate) = datepart(yy,getdate())
group by Channel_Number, cast(EntryDate as varchar(11))

select max(EntryDate), count(channel_number)--datepart(dd, EntryDate) dy, 
 Channel_Number, Channel_Index  --, Channel_Type
from channelnormal
where datepart(dd, EntryDate) = datepart(dd,getdate())
and  datepart(m, EntryDate) = datepart(m,getdate())
and  datepart(yy, EntryDate) = datepart(yy,getdate())
group by Channel_Number, channel_index

select *
from SP_DataWarehouse..celllog
where datepart(dd, cast([Last DateTime] as datetime)) = datepart(dd,getdate())
and  datepart(m, cast([Last DateTime] as datetime) ) = datepart(m,getdate())
and  datepart(yy, cast([Last DateTime] as datetime) ) = datepart(yy,getdate())

select channel, count(channel) ChannelCount, [LastDateTime] 
from SolidPowerDev.dbo.vucelllog 
where datepart(dd, [LastDateTime] ) = datepart(dd,getdate())
and  datepart(m, [LastDateTime] ) = datepart(m,getdate())
and  datepart(yy, [LastDateTime] ) = datepart(yy,getdate())
group by channel, [LastDateTime]

select *
from SolidPowerDev..vucelllog
where datepart(dd, cast([LastDateTime] as datetime)) = datepart(dd,getdate())
and  datepart(m, cast([LastDateTime] as datetime) ) = datepart(m,getdate())
and  datepart(yy, cast([LastDateTime] as datetime) ) = datepart(yy,getdate())


select dbo.isBBdata(cellname), channel, count(channel) ChannelCount
from SolidPowerDev..vucelllog
where datepart(dd, cast([LastDateTime] as datetime)) = datepart(dd,getdate())
and  datepart(m, cast([LastDateTime] as datetime) ) = datepart(m,getdate())
and  datepart(yy, cast([LastDateTime] as datetime) ) = datepart(yy,getdate())
and SolidPowerDev.dbo.isBBData(cellname) = 'Arbin'
group by dbo.isBBdata(cellname), channel
order by cast(channel as int)

select * from cell
where datepart(dd, EntryDate) = datepart(dd,getdate())
and  datepart(m, EntryDate) = datepart(m,getdate())
and  datepart(yy, EntryDate) = datepart(yy,getdate())

select cast(EntryDate as varchar(11)) EntryDate, channel_index, count(channel_number) CountChannelNr,--datepart(dd, EntryDate) dy, 
 Channel_Number--, Channel_Type
from cell
where datepart(dd, EntryDate) = datepart(dd,getdate())
and  datepart(m, EntryDate) = datepart(m,getdate())
and  datepart(yy, EntryDate) = datepart(yy,getdate())
group by  cast(EntryDate as varchar(11)), Channel_Number, channel_index

select LastDateTime, channel, count(channel)
from vuCellLog

group by LastDateTime, channel
order by LastDateTime desc

select LastDateTime, channel, count(channel)
from vuCellLog
group by LastDateTime, channel
order by LastDateTime desc

select top 100 [Arbin Notes], * from SP_DataWarehouse..celllog
select distinct [Cage torque (in-lbs)] from SP_DataWarehouse..celllog

select dbo.isbbdata(cellname), channel, * from vuCellLog 
select top 100 * from SolidPowerDev.dbo.Cell
select SolidPowerDev.dbo.isBBData(cellname), channel 
from SolidPowerDev.dbo.vuCellLog 
where cast(LastDateTime as date) = cast(getdate() as date)
group by SolidPowerDev.dbo.isBBData(cellname),channel

select c.Channel_Number, channel_index, count(c.CellName)
from SolidPowerDev.dbo.ChannelNormal cn
inner join SolidPowerDev.dbo.Cell c 
on cn.CellName = c.CellName
where cast(cast(cn.date_time as datetime) as date) = cast(getdate() as date)
group by c.Channel_Number, channel_index

select c.Channel_Number, channel_index, count(c.CellName)
from SolidPowerDev.dbo.ChannelNormal cn
inner join SolidPowerDev.dbo.Cell c 
on cn.CellName = c.CellName
where cast(cast(cn.date_time as datetime) as date) = cast(getdate() as date)
group by c.Channel_Number, channel_index

select c.CellName, c.Channel_Number, count(c.Channel_Number) CountChannel_Number
from SolidPowerDev.dbo.ChannelNormal cn
inner join SolidPowerDev.dbo.Cell c
	on cn.CellName = c.CellName
where cast(cast( Date_time as datetime) as date) = cast(getdate() as date)
group by c.CellName, c.Channel_Number

select SolidPowerDev.dbo.isbbdata(c.cellname) Src, channel_number
	--, count(c.cellname) CellChannelCount
	, count(CHannel_Number) CellChannelCount
	, cast(v.LastDateTime as Date) LastDate
from SolidPowerDev.dbo.vuCellLog v
left outer join SolidPowerDev.dbo.cell c
on c.cellname = v.cellname
where cast(v.LastDateTime as date) is not null 
group by SolidPowerDev.dbo.isbbdata(c.cellname), c.Channel_Number, cast(v.LastDateTime as Date)
order by cast(v.LastDateTIme as date) desc  --channel_number

select SolidPowerDev.dbo.isBBData(v.cellname) Src, count(SolidPowerDev.dbo.isBBData(v.cellname)) ActiveCells, cast(v.LastDateTime as Date) LastDateActive
from SolidPowerDev.dbo.vuCellLog v

where cast(v.LastDateTime as date) is not null 
group by SolidPowerDev.dbo.isbbdata(v.cellname), cast(v.LastDateTime as Date)
order by cast(v.LastDateTIme as date) desc  

-------------------------------------------

select channel, count(channel) ChannelCount from vuCellLog where cast(LastDateTime as date) = cast( GETDATE() as date) group by channel 
order by cast(count(channel) as int ) desc

select dbo.isBBData([CellName]) CellSource, channel, count(channel) ChannelCount 
from vuCellLog where cast(LastDateTime as date) = cast( GETDATE() as date) 
group by channel, dbo.isBBdata(CellName)
order by cast(count(channel) as int ) desc

select dbo.isBBData([CellName]) CellSource, channel, count(channel) ChannelCount, cast(LastDateTime as date)
from vuCellLog 
where cast(LastDateTime as date) = cast( GETDATE() as date) 
-- and isnumeric(channel) = 1
and dbo.isBBData(CellName) = 'Arbin'
group by dbo.isBBData([CellName]), channel, dbo.isBBdata(CellName), cast(LastDateTime as date)
-- order by dbo.isBBData([CellName]) , cast(count(channel) as int ) desc
order by  cast( channel as int)

select dbo.isBBData([CellName]) CellSource, channel, count(channel) ChannelCount, cast(LastDateTime as date)
from vuCellLog 
where cast(LastDateTime as date) = cast( GETDATE() as date) 
-- and isnumeric(channel) = 1
and dbo.isBBData(CellName) = 'BB'
group by dbo.isBBData([CellName]), channel, dbo.isBBdata(CellName), cast(LastDateTime as date)

select channel_number, channel_index, count(cellname)
from SolidPowerDev.dbo.cell c
inner join SP_DataWarehouse.dbo.celllog cl
on cl.[Cell #] = c.cellname
where cast(cl.[Last DateTime] as date) = cast(getdate() as date)
group by  channel_number, channel_index
order by count(cellname)

select channel_number, channel_index, count(c.cellname) ActiveCellChannelCount, LastDateTime
from SolidPowerDev.dbo.cell c
inner join SolidPowerDev.dbo.vucelllog cl
on cl.CellName = c.cellname
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
group by  channel_number, channel_index, LastDateTime
order by Channel_Number, Channel_Index

select cast(getdate() as date)
select [Last DateTime], cast([Last DateTime] as date) from SP_DataWarehouse..celllog

select channel_number, count(c.cellname) ActiveCellChannelCount, LastDateTime
from SolidPowerDev.dbo.cell c
inner join SolidPowerDev.dbo.vucelllog cl
on cl.CellName = c.cellname
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
group by  channel_number, channel_index, LastDateTime
order by Channel_Number, Channel_Index

select channel_number, channel_index, count(channel_index) ActiveCellChannelCount, cast(LastDateTime as date)
from SolidPowerDev.dbo.cell c
inner join SolidPowerDev.dbo.vucelllog cl
on cl.CellName = c.cellname
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
group by  channel_number, channel_index, cast(LastDateTime as date)
order by Channel_Number, channel_index

select c.cellname, channel_number, channel_index, count(c.cellname) ActiveCellChannelCount, LastDateTime
from SolidPowerDev.dbo.cell c
inner join SolidPowerDev.dbo.vucelllog cl
on cl.CellName = c.cellname
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
group by  c.cellname, channel_number, channel_index, LastDateTime
order by Channel_Number, Channel_Index


select c.cellname, channel_number, channel_index, count(c.cellname) ActiveCellChannelCount, LastDateTime
from SolidPowerDev.dbo.cell c
inner join SolidPowerDev.dbo.vucelllog cl
on cl.CellName = c.cellname
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
group by  c.cellname, channel_number, channel_index, LastDateTime
order by Channel_Number, Channel_Index

select c.cellname, channel_number, channel_index, count(c.cellname) ActiveCellChannelCount, LastDateTime
from SolidPowerDev.dbo.cell c
left outer join SolidPowerDev.dbo.vucelllog cl
on cl.CellName = c.cellname
where isnull(cast(cl.[LastDateTime] as date), cast(getdate() as date)) = cast(getdate() as date)
--where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
group by  c.cellname, channel_number, channel_index, LastDateTime
order by Channel_Number, Channel_Index


-- NEW Query USES CELL TABLE ONLY FOR LASTACTIVEDATE 
select c.cellname, channel_number, channel_index, count(c.cellname) ActiveCellChannelCount, LastActiveDate
from SolidPowerDev.dbo.cell c
--inner join SolidPowerDev.dbo.vucelllog cl on cl.CellName = c.cellname
where cast(c.LastActiveDate as date) = cast(getdate() as date)
group by  c.cellname, channel_number, channel_index, c.LastActiveDate
order by Channel_Number, Channel_Index

select distinct c.Cellname
from SolidPowerDev.dbo.cell c
inner join SolidPowerDev.dbo.vucelllog cl
on cl.CellName = c.cellname

select c.cellname, channel_number, channel_index, count(c.cellname) ActiveCellChannelCount, EntryDate, LastActiveDate, datediff(day, c.EntryDate, getdate()) NrOfDays
from SolidPowerDev.dbo.cell c
where cast(c.[LastActiveDate] as date) = cast(getdate() as date)
group by  c.cellname, channel_number, channel_index, LastActiveDate, c.EntryDate
order by Channel_Number, Channel_Index

select dbo.isBBdata(c.cellname) Src, c.cellname, channel_number, channel_index, count(c.cellname) ActiveCellChannelCount, EntryDate, LastDateTime, LastActiveDate
	, datediff(day, c.EntryDate, getdate()) DaysOnline
from SolidPowerDev.dbo.cell c
inner join SolidPowerDev.dbo.vucelllog cl
on cl.CellName = c.cellname
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
	--and dbo.isBBData(c.Cellname) = 'Arbin'
group by  c.cellname, channel_number, channel_index, LastDateTime, c.EntryDate, LastActiveDate
order by 1, Channel_Number, Channel_Index

select lastactivedate from cell order by 1 desc


select dbo.isBBdata(c.cellname) Src, c.cellname, channel_number, channel_index, count(c.cellname) ActiveCellChannelCount, LastDateTime
	, [PyStartDate]
from SolidPowerDev.dbo.cell c
inner join SolidPowerDev.dbo.vucelllog cl
on cl.CellName = c.cellname
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
	--and dbo.isBBData(c.Cellname) = 'Arbin'
group by  c.cellname, channel_number, channel_index, LastDateTime,  [PyStartDate]
order by 1, Channel_Number, Channel_Index


-- get cells right outer join for both those in cell table and not in
select dbo.isBBdata(c.cellname) Src, isnull(c.cellname, cl.cellname) cellname, isnull(cast(channel_number as varchar(30)), 'No .res File Exists') ChannelNumber, channel_index, 
	count(isnull(c.cellname, cl.cellname)) ActiveCellChannelCount, LastDateTime
from SolidPowerDev.dbo.cell c
right outer join SolidPowerDev.dbo.vucelllog cl
on cl.CellName = c.cellname
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
	--and dbo.isBBData(c.Cellname) = 'Arbin'
group by  c.cellname, cl.cellname, channel_number, channel_index, LastDateTime
order by 1, Channel_Number desc, Channel_Index desc

--UDATE LAST ACTIVEFILEDATE AND LASTARBINDATE IN CELL TABLE
--begin tran jk
--update cell set [LastArbinDate] = a.lada
--from ( select cellname, max(cast(cast(Arbindate as varchar(20)) + ' ' + cast(ArbinTime as varchar(20)) as smalldatetime)) lada
--from ChannelNormal
--group by cellname ) a
--inner join cell c on c.CellName = a.CellName
---- commit tran jk

--begin tran jk
--update cell set [LastFileDate] = a.lada
--from ( select cellname, max(entrydate) lada
--from ChannelNormal
--group by cellname ) a
--inner join cell c on c.CellName = a.CellName
---- commit tran jk


select dbo.isBBdata(c.cellname) Src, isnull(c.cellname, cl.cellname) cellname, isnull(cast(channel_number as varchar(30)), 'No .res File Exists') ChannelNumber, channel_index, 
	count(isnull(c.cellname, cl.cellname)) ActiveCellChannelCount, LastDateTime
from SolidPowerDev.dbo.cell c
right outer join SolidPowerDev.dbo.vucelllog cl
on cl.CellName = c.cellname
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
	--and dbo.isBBData(c.Cellname) = 'Arbin'
group by  c.cellname, cl.cellname, channel_number, channel_index, LastDateTime
order by 1, Channel_Number desc, Channel_Index desc


select dbo.isBBdata(isnull(c.cellname, cl.cellname)) Src, isnull(c.cellname, cl.cellname) cellname, 
	--isnull(cast(channel_number as varchar(30)), 'No .res File Exists') ChannelNumber, 
	isnull(cast(channel_number as varchar(30)), cl.Channel) ChannelNumber, 
	isnull( cast(channel_index as varchar(40)), 'No Channel Index for BB cells') ChannelIndex,
	count(isnull(c.cellname, cl.cellname)) ActiveCellChannelCount, LastDateTime
from SolidPowerDev.dbo.cell c
right outer join SolidPowerDev.dbo.vucelllog cl
on cl.CellName = c.cellname
where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
	--and dbo.isBBData(c.Cellname) = 'Arbin'
group by  c.cellname, cl.cellname, channel_number, channel_index, LastDateTime, cl.channel
order by 1, Channel_Number desc, Channel_Index desc




