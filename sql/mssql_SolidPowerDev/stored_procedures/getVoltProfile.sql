USE [SolidPowerDev]
GO

/****** Object:  StoredProcedure [dbo].[getVoltProfile]    Script Date: 8/4/2020 13:19:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

















CREATE PROCEDURE [dbo].[getVoltProfile]
	@Cellname varchar(200) = 'none',
	@CycleIndex int = '0'
AS

-- exec [getVoltProfile] '171111_graphite_a', 1
-- exec [getVoltProfile] '180206_d', 1
-- exec [getVoltProfile] '180824_b', 2

--sp_randomcell
BEGIN

	--if  @CellName = '191124_b'
	--	select * from BeckerBoard.dbo.VoltProfile
	if not exists (select 1 from SolidPowerDev.dbo.Cell where CellName = @Cellname )
		begin
		if @CycleIndex != 0
			exec BeckerBoard.dbo.getBBVoltProfile @cellname, @CycleIndex
		else
			exec BeckerBoard.dbo.getBBVoltProfile @cellname
		end
	else
		begin
		if @CycleIndex != 0
			exec SolidPowerDev.dbo.getVoltProfile_ARBIN @cellname, @CycleIndex
		else
			exec SolidPowerDev.dbo.getVoltProfile_ARBIN @cellname
		end
END
GO


