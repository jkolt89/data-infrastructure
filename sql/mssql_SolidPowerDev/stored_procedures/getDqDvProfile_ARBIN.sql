USE [SolidPowerDev]
GO

/****** Object:  StoredProcedure [dbo].[getDqDvProfile_ARBIN]    Script Date: 8/4/2020 13:17:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

















CREATE PROCEDURE [dbo].[getDqDvProfile_ARBIN]
	@CellName varchar(250) = 'NA',
	@Cycle_index int = 0
AS

-- EXAMPLES
-- exec SolidPowerDev.dbo.[getDqDvProfile] '180403_Ilya_Indigo'
-- exec SolidPowerDev.dbo.[getDqDvProfile] '180820_a', 1
-- exec SolidPowerDev.dbo.getDqDvProfile '171111_graphite_a', 2
--declare @cellname varchar(100), @cycle_index int
--select @cellname = '190820_b', @cycle_index=1
if @Cycle_index != 0
	BEGIN
		if @CellName = 'NA'
			select 'CellName needed.'
		else
		with dqdv_cte(cCNTID, pCNTID, Cycle_Index, [Delta Charge Capacity], [Delta Discharge Capacity], [Delta Voltage], cCellName, cTest_ID, 
						cData_Point, cVoltage, cCharge_Capacity, pCellName, pTest_ID, pData_Point, pVoltage, pCharge_Capacity)
		as (
		select 
			cn.CNTID cCNTID, 
			pr.CNTID pCNTID, 
			cn.Cycle_Index, 
			cn.Charge_Capacity - pr.Charge_Capacity [Delta Charge Capacity], 
			cn.Discharge_Capacity - pr.DisCharge_Capacity [Delta Discharge Capacity],
			cn.Voltage - pr.Voltage [Delta Voltage],
			cn.CellName cCellName, 
			cn.Test_ID cTest_ID, 
			cn.Data_Point cData_Point, 
			cn.Voltage cVoltage, 
			cn.Charge_Capacity cCharge_Capacity,
			pr.CellName pCellName, 
			pr.Test_ID pTest_ID, 
			pr.Data_Point pData_Point, 
			pr.Voltage pVoltage, 
			pr.Charge_Capacity pCharge_Capacity
		from ChannelNormal cn
		inner join ChannelNormal pr
		-- on pr.cntid = cn.cntid-1
		-- on pr.Data_Point = cn.Data_Point - 1
		on pr.Data_Point - 1 = cn.Data_Point
		where pr.Charge_Capacity != 0
			and cn.Voltage - pr.Voltage  != 0
			-- FOR TESTING PURPOSES ONLY. REMOVE NEXT LINE FOR PROD
			and cn.CellName = @cellname
			and cn.Cycle_Index = @cycle_index
			and pr.CellName = @cellname
			and pr.Cycle_Index = @cycle_index
		)
		select 
			--cCNTID, pCNTID, 
			-- cCellName CellName, 
			Cycle_Index, 
			cTest_ID Test_ID, 
			cData_Point Data_Point, 
			[Delta Charge Capacity]/[Delta Voltage] [dqdv Charge],
			[Delta Discharge Capacity]/[Delta Voltage] [dqdv Discharge],
			[Delta Charge Capacity], 
			[Delta Discharge Capacity], 
			[Delta Voltage], 
			cVoltage as Voltage
		from dqdv_cte
		order by cCNTID
	END
ELSE
	BEGIN
		with dqdv_cte(cCNTID, pCNTID, Cycle_Index, [Delta Charge Capacity], [Delta Discharge Capacity], [Delta Voltage], cCellName, cTest_ID, 
						cData_Point, cVoltage, cCharge_Capacity, pCellName, pTest_ID, pData_Point, pVoltage, pCharge_Capacity)
		as (
		select 
			cn.CNTID cCNTID, 
			pr.CNTID pCNTID, 
			cn.Cycle_Index, 
			cn.Charge_Capacity - pr.Charge_Capacity [Delta Charge Capacity], 
			cn.Discharge_Capacity - pr.Discharge_Capacity [Delta Discharge Capacity],
			cn.Voltage - pr.Voltage [Delta Voltage],
			cn.CellName cCellName, 
			cn.Test_ID cTest_ID, 
			cn.Data_Point cData_Point, 
			cn.Voltage cVoltage, 
			cn.Charge_Capacity cCharge_Capacity,
			pr.CellName pCellName, 
			pr.Test_ID pTest_ID, 
			pr.Data_Point pData_Point, 
			pr.Voltage pVoltage, 
			pr.Charge_Capacity pCharge_Capacity
		from ChannelNormal cn
		inner join ChannelNormal pr
		-- on pr.cntid = cn.cntid-1
		-- on pr.Data_Point = cn.Data_Point - 1
		on pr.Data_Point - 1 = cn.Data_Point
		where pr.Charge_Capacity != 0
			and cn.Voltage - pr.Voltage  != 0
			-- FOR TESTING PURPOSES ONLY. REMOVE NEXT LINE FOR PROD
			and cn.CellName = @cellname
			and pr.CellName = @cellname
			
		)
		select 
			--cCNTID, pCNTID, 
			-- cCellName CellName, 
			Cycle_Index, 
			cTest_ID Test_ID, 
			cData_Point Data_Point, 
			[Delta Charge Capacity]/[Delta Voltage] [dqdv Charge],
			[Delta Discharge Capacity]/[Delta Voltage] [dqdv Discharge],
			[Delta Charge Capacity], 
			[Delta Discharge Capacity], 
			[Delta Voltage], 
			cVoltage as Voltage
		from dqdv_cte
		order by cCNTID
	END
GO


