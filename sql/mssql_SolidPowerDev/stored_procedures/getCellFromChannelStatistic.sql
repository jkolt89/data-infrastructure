USE [SolidPowerDev]
GO

/****** Object:  StoredProcedure [dbo].[getCellFromChannelStatistic]    Script Date: 8/4/2020 13:11:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[getCellFromChannelStatistic]
	@cellname varchar(150) = 'ALL'
AS
-- getCellFromChannelStatistic 'DM_DC_190520_a_BL_30mm_C-3'
BEGIN
	SET NOCOUNT ON;
	if @cellname = 'All'
		select 
			CSTID, CellName, Test_ID, Data_Point, Vmax_On_Cycle, Charge_Time, Discharge_Time, EntryDate
		from SolidPowerDev.dbo.ChannelStatistic
	else
		select
			CSTID, CellName, Test_ID, Data_Point, Vmax_On_Cycle, Charge_Time, Discharge_Time, EntryDate
		from SolidPowerDev.dbo.ChannelStatistic where cellname = @cellname
END
GO


