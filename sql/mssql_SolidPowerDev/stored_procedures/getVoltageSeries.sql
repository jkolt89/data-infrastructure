USE [SolidPowerDev]
GO

/****** Object:  StoredProcedure [dbo].[getVoltageSeries]    Script Date: 8/4/2020 13:18:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

















CREATE PROCEDURE [dbo].[getVoltageSeries]
	@Cellname varchar(200) = 'none',
	@CycleIndex int = '0'
AS
-- queries both Arbin and BB cell data
-- exec [getVoltageSeries] '171111_graphite_a', 1
-- exec [getVoltageSeries] '180206_d', 1
-- exec [getVoltageSeries] '191018_c', 1
BEGIN
	SET NOCOUNT ON;
	declare @CellSource varchar(10)
	select @CellSource = SolidPowerDev.dbo.isBBData(@Cellname)
	-- print @CellSource
	if @cellname = '191124_b'
		begin
		select cycle as Cycle_index, --[Test Time], 
		cast(cast(EpochTS as varchar(10)) as bigint) - ( select top 1 cast(cast(EpochTS as varchar(10)) as bigint) from BeckerBoard.dbo.SPCell where cellname = @Cellname) [Test Time]
		, v as Voltage
		from BeckerBoard.dbo.SPCell
			where cellname = @Cellname
			and I is not null
			--and [Time] not like '% 2020 %'
			order by 2

		end
		else
		begin

		if @CellSource = 'Arbin'
			begin
			if @CycleIndex = 0
				select  Cycle_index, 
				--Test_Time as [Test Time], 
				cast(Test_Time as bigint) as [Test Time], 
				Voltage -- as Charge_V
				from SolidPowerDev.dbo.ChannelNormal
				where cellname = @Cellname
				order by Test_Time
			else
				select  Cycle_index, 
				--Test_Time as [Test Time], 
				cast(Test_Time as bigint) as [Test Time], 
				Voltage -- as Charge_V
				from SolidPowerDev.dbo.ChannelNormal
				where cellname = @Cellname
				and cycle_index = @CycleIndex
				order by Test_Time
			end
		else
			begin
			-- print 'BB'
			if @CycleIndex = 0
				select Cycle + 1 as Cycle_index,	
					--[time] as [Test Time],
					--cast([Time] as bigint) [Test Time],
					--cast(measureEndTime as bigint) [Test Time],
					--cast(cast([Time] as varchar(10)) as bigint) - ( select top 1 cast(cast([Time] as varchar(10)) as bigint) from BeckerBoard.dbo.SPCell where cellname = @Cellname) as [Test Time],
					--cast(cast([Time] as varchar(10)) as int) - ( select top 1 cast(cast([Time] as varchar(10)) as int) from BeckerBoard.dbo.SPCell where cellname = @Cellname) as [Test Time],
					--cast(I as DEC(18,6)) as Voltage -- as Charge_V
					--cast(I as varchar(10)) tmpcol,
					--case when [Time] not like '% 2020 %'
						--then --cast(cast( [Time] as varchar(40)) as bigint) - ( select top 1 cast(cast([Time] as varchar(40)) as bigint) from BeckerBoard.dbo.SPCell where cellname = @Cellname)-- as [Test Time]
					cast(cast( [EpochTS] as varchar(40)) as bigint) - ( select top 1 cast(cast(EpochTS as varchar(40)) as bigint) from BeckerBoard.dbo.SPCell where cellname = @Cellname)-- as [Test Time]
					--else
						--(datediff(s, '1970-01-01 00:00:00', [Time]) - 19800)  - ( select top 1 cast(cast([Time] as varchar(10)) as bigint) from BeckerBoard.dbo.SPCell where cellname = @Cellname)
						--( select top 1 datediff( s, '1970-01-01 00:00:00', EpochTS ) + 25200  ) - ( select top 1 datediff( s, '1970-01-01 00:00:00', EpochTS ) + 25200  from BeckerBoard.dbo.SPCell where cellname = @Cellname)
					--end 
					 as [Test Time],
					V as Voltage
				--Cast(cast( I as varchar(10)) as decimal(10,6))
				from BeckerBoard.dbo.SPCell
				where cellname = @Cellname
				and V > 0
				and I is not null
				order by 2
			else
			select Cycle + 1 as Cycle_index,	
				--[time] as [Test Time],
				--cast([Time] as bigint) [Test Time],
				--cast(measureEndTime as bigint) [Test Time],
				--cast(cast([Time] as varchar(10)) as bigint) - ( select top 1 cast(cast([Time] as varchar(10)) as bigint) from BeckerBoard.dbo.SPCell where cellname = @Cellname) as [Test Time],
				--cast(cast([Time] as varchar(10)) as int) - ( select top 1 cast(cast([Time] as varchar(10)) as int) from BeckerBoard.dbo.SPCell where cellname = @Cellname) as [Test Time],
				--cast(I as DEC(18,6)) as Voltage -- as Charge_V
				--cast(I as varchar(10)) tmpcol,
				--case when [Time] not like '% 2020 %'
					--then --cast(cast( [Time] as varchar(40)) as bigint) - ( select top 1 cast(cast([Time] as varchar(40)) as bigint) from BeckerBoard.dbo.SPCell where cellname = @Cellname)-- as [Test Time]
				cast(cast( [EpochTS] as varchar(40)) as bigint) - ( select top 1 cast(cast(EpochTS as varchar(40)) as bigint) from BeckerBoard.dbo.SPCell where cellname = @Cellname)-- as [Test Time]
				--else
					--(datediff(s, '1970-01-01 00:00:00', [Time]) - 19800)  - ( select top 1 cast(cast([Time] as varchar(10)) as bigint) from BeckerBoard.dbo.SPCell where cellname = @Cellname)
					--( select top 1 datediff( s, '1970-01-01 00:00:00', EpochTS ) + 25200  ) - ( select top 1 datediff( s, '1970-01-01 00:00:00', EpochTS ) + 25200  from BeckerBoard.dbo.SPCell where cellname = @Cellname)
				--end 
				 as [Test Time],
				V as Voltage
				--Cast(cast( I as varchar(10)) as decimal(10,6))
			from BeckerBoard.dbo.SPCell
			where cellname = @Cellname
			and cycle+1 = @CycleIndex
			and I is not null
			and V > 0
			order by 2
			end
		end
END
GO


