USE [SolidPowerDev]
GO

/****** Object:  StoredProcedure [dbo].[getResistanceProfile_ARBIN]    Script Date: 8/4/2020 13:17:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










CREATE PROCEDURE [dbo].[getResistanceProfile_ARBIN]
	@CellName varchar(150) = 'NA',
	@withzero varchar(20) = 'withzero'
-- [getResistanceProfile] '2019_0016d_pc_j', 'nozero';  --  '190913_b'; 
-- [getResistanceProfile] 'dm_dc_191124_c_33pct_prod_bear_c10'
-- exec [getResistanceProfile] 'DM_DC_200109_d_cc_191223-1_chrg_1st_c20_c5'
-- exec  [getResistanceProfile] 'DM_DC_191211_c_48pct_bear_no_s_C5'
-- exec  [getResistanceProfile] 'dm_dc_191218_b_cc_191216_li2s_c10'
AS
BEGIN
set nocount on

DECLARE @t1 TABLE (t1id int primary key identity(1,1), Test_ID int,  ci INT, data_point int, Resistance float, [current] float, voltage float)

DECLARE @subset TABLE (t1id int primary key, Test_ID int,  ci INT, data_point int, Resistance float, [current] float, voltage float)

DECLARE @output table (oid int primary key identity(1,1), t1id int, Test_ID int, ci INT, data_point int, Resistance float, rv float, mc float, 
	lav float, lac float, direction varchar(30))

declare @lav float, @lac float, @upperlim int, @lowerlim int, @count int

insert into @t1 (Test_ID, ci, data_point, Resistance, [current], voltage)
select Test_ID, Cycle_Index, data_point, internal_resistance, [current], voltage
	from SolidPowerDev.dbo.ChannelNormal
	where cellname = @CellName
	order by CNTID -- data_point
-- select * from @t1
--create clustered index tmp_IX on @t1(t1id)

insert into @output( t1id, Test_ID, ci, data_point, Resistance, mc,  lav, rv, lac, direction)
select top 1 t1id, Test_ID, ci, data_point, Resistance, [current] mc, voltage lav, voltage rv, [current] lac, 'start_test'
from @t1
where t1id = 1

insert into @output(t1id, Test_ID, ci, data_point, Resistance, rv, mc)
select distinct c1.t1id, c1.Test_ID, c1.ci, c1.data_point, c1.Resistance, c1.voltage, c1.[current]
	from @t1 c1
	inner join @t1 c2
	-- on c1.Test_ID=c2.Test_ID and c1.Data_Point -1=c2.Data_Point 
	on c1.t1id - 1= c2.t1id
	where -- c1.cellname = @CellName and 
	c1.Resistance != c2.Resistance
	order by c1.Test_ID, c1.ci, c1.Data_Point

select @lav = lav, @lac = lac from @output where oid = 1

update @output set lav = @lav, lac=@lac, direction = 'no current' where oid = 2

select @count = count(*) from @t1
-- select * from @output
while @count > 0
	BEGIN
	select top 1 @upperlim = data_point from @output where lav is null

	insert into @subset select distinct * from @t1 where data_point <= @upperlim
	delete from @t1 where data_point <=@upperlim
	--select * from @subset
	select top 1 @lac = s2.[current], @lav = s2.voltage
	from @subset s1 inner join @subset s2 on s1.t1id-1 = s2.t1id
	where s1.[current] != s2.[current]
	and s1.[current] = 0 and s2.[current] != 0
	order by s1.t1id desc

	update @output set lac=@lac, lav=@lav where data_point = @upperlim

	if not exists (select 1 from @output where lav is null and lac is null )
		begin
		break
		--delete from @t1
		--select @count = 0
		end
	else
		select @count = count(*) from @t1
		delete from @subset
	
	--print 'COUNT IS:  ' + STR(@count)
	END


if @withzero = 'withzero'
	select distinct
		ci as cycle_index, data_point, round(resistance, 8) resistance, rv as rest_voltage, round(mc, 8) as measurement_current, 
		lav as last_active_voltage, lac as last_active_current, 
		case
			-- need to verify below 'discharge no rest' calc
			when resistance = 0 and oid != 1 and lac < 0 then 'discharge no rest'
			-- need to verify below 'charge no rest' calc
			when resistance = 0 and oid != 1 and lac >= 0 then 'charge no rest'
			when lac > 0 and resistance != 0 then 'charge with rest'
			when lac < 0 and resistance != 0 then 'discharge with rest'
			when lac = 0 and resistance != 0 and oid != 1 then 'no current'
			else 'start test'
		end as direction
	from @output
	order by data_point
	--where direction != 'start test'
else
	select distinct
		ci as cycle_index, data_point, round(resistance, 8) resistance, rv as rest_voltage, round(mc, 8) as measurement_current, 
		lav as last_active_voltage, lac as last_active_current, 
		case 
			-- need to verify below 'discharge no rest' calc
			when resistance = 0 and oid != 1 and lac < 0 then 'discharge no rest'
			-- need to verify below 'charge no rest' calc
			when resistance = 0 and oid != 1 and lac >= 0 then 'charge no rest'
			when lac > 0 and resistance != 0 then 'charge with rest'
			when lac < 0 and resistance != 0 then 'discharge with rest'
			when lac = 0 and resistance != 0 and oid != 1 then 'no current'
			else 'start test'
		end as direction
	from @output
	where Resistance != 0
	--and direction != 'start test'
	order by data_point

END
GO


