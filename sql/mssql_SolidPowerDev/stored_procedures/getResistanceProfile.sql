USE [SolidPowerDev]
GO

/****** Object:  StoredProcedure [dbo].[getResistanceProfile]    Script Date: 8/4/2020 13:17:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[getResistanceProfile]
	@cellname varchar(100)  = 'NA'
AS
BEGIN
	SET NOCOUNT ON;
/*
	charge 2 failure analysis calc:
	Cumulative charge capacity (the total amount of charge a cell has passed during its life) vs. time
	Cumulative discharge capacity (the total amount of discharge a cell has passed during its life) vs. time
	Calculated resistance (voltage/current)
	exec [getCharge2FailureAnalysis] '180327_a'
	exec [getCharge2FailureAnalysis] '191115_b'
	sp_randomcell
*/

--if exists ( select SolidPowerDev.dbo.isBBdate(@cellname))
if exists (select 1 from SolidPowerDev.dbo.Cell where CellName = @Cellname )
	-- print 'Arbin'
	exec SolidPowerDev.dbo.getResistanceProfile_ARBIN @cellname
else
	-- print 'BB'
	exec BeckerBoard.dbo.getBBResistanceProfile @cellname
END
GO


