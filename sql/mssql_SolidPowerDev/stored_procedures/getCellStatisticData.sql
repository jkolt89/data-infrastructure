USE [SolidPowerDev]
GO

/****** Object:  StoredProcedure [dbo].[getCellStatisticData]    Script Date: 8/4/2020 13:12:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE procedure [dbo].[getCellStatisticData]
	@cellname varchar(100)
-- select top 10 cellname from BeckerBoard.dbo.SPCell group by cellName order by newid()	-- 191018_g
-- select top 10 cellname from SolidPowerDev.dbo.Cell group by cellName order by newid()	-- 2018_0016f_PC_K1
AS
-- declare @cellname varchar(100)
-- set @cellname = '191018_g'
-- set @cellname = '2018_0016f_PC_K1'
-- getCellStatisticData_ARBIN '2018_0016f_PC_K1'
-- getCellStatisticData '2018_0016f_PC_K1'
if exists ( select 1 from SolidPowerDev.dbo.Cell where cellname = @cellname)
	-- print 'Arbin'
	--exec SolidPowerDev.dbo.getCellStatisticData_ARBIN @cellname
	exec SolidPowerDev.dbo.[getCellStatisticData_ARBIN] @cellname
else
	-- print 'BB'
	-- exec BeckerBoard.dbo.getBBSPCell @cellname
	exec BeckerBoard.dbo.getBBCellCycleStats @cellname
GO


