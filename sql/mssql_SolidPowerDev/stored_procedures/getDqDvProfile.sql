USE [SolidPowerDev]
GO

/****** Object:  StoredProcedure [dbo].[getDqDvProfile]    Script Date: 8/4/2020 13:16:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
















CREATE PROCEDURE [dbo].[getDqDvProfile]
	@CellName varchar(250) = 'NA',
	@Cycle_index int = 0
AS

-- EXAMPLES
-- exec SolidPowerDev.dbo.[getDqDvProfile] '180403_Ilya_Indigo'
-- exec SolidPowerDev.dbo.[getDqDvProfile] '180820_a', 1
-- exec SolidPowerDev.dbo.getDqDvProfile '171111_graphite_a', 2 --Arbin
-- exec SolidPowerDev.dbo.[getDqDvProfile] '191025_e'		--BB
-- exec SolidPowerDev.dbo.[getDqDvProfile] '191124_a', 1	--BB
-- exec SolidPowerDev.dbo.[getDqDvProfile] '191124_a', 2	--BB
-- exec SolidPowerDev.dbo.[getDqDvProfile] '191107_f' ,1	--BB
--declare @cellname varchar(100), @cycle_index int
--select @cellname = '190820_b', @cycle_index=1
BEGIN

	if not exists (select 1 from SolidPowerDev.dbo.Cell where CellName = @Cellname )
		begin
		if @Cycle_index != 0
			exec BeckerBoard.dbo.getBBDqDvProfile @cellname, @Cycle_index
		else
			exec BeckerBoard.dbo.getBBDqDvProfile @cellname
		end
	else
		begin
		if @Cycle_index != 0
			exec SolidPowerDev.dbo.getDqDvProfile_ARBIN @cellname, @Cycle_index
		else
			exec SolidPowerDev.dbo.getDqDvProfile_ARBIN @cellname
		end
END
GO


