USE [SolidPowerDev]
GO

/****** Object:  StoredProcedure [dbo].[getCellStatisticData_ARBIN]    Script Date: 8/4/2020 13:12:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[getCellStatisticData_ARBIN]
	@CellName varchar(250) = 'NA'

AS
BEGIN

	SET NOCOUNT ON;
--declare @cellname varchar(250)
--set @cellname = '180907_a'
-- [getCellStatisticData_ARBIN_NEW] '180907_a'
-- [getCellStatisticData] '180907_a'
-- [zDEV_getCellStatisticData_ARBIN_NEW1] '160405_a'

declare @activepercent float, @mass float

if exists ( select 1 from SP_DataWarehouse.dbo.CellLog where [Cell #] = @cellname )
	begin
	select @mass = isnull([Total Cathode Mass (g)], 1), @activepercent = isnull([nominal percent active material] , 1.00) 
	from SP_Datawarehouse.dbo.CellLog where [Cell #] = @CellName;
	end
else
	begin
	select @mass = isnull(mass, 1) from SolidPowerDev.dbo.Cell where cellname = @cellname;
	print @mass
	set @activepercent = 1;
	end

if @mass = 0 select @mass = 1;
if @activepercent = 0 select @activepercent = 1;


select cycle_index, 
	max(charge_capacity) [Total Charge Capacity], 
	max(discharge_capacity) [Total Discharge Capacity]
	, max(discharge_capacity) / case when max(charge_capacity) is null then 1
								when max(charge_capacity) = 0.0 then 1
								else max(charge_capacity) end as Efficiency
	, max(charge_capacity) / case when max(discharge_capacity) is null then 1
								when max(discharge_capacity) = 0.0 then 1
								else max(discharge_capacity) end as InverseEfficiency
	
	, max(Internal_Resistance) as [Maximum Resistance]
	, max(Internal_Resistance) as [Charge Resistance Min]
	
	, MAX(charge_capacity) / @mass AS SpecificMaxChargeCapacity
	, MAX(charge_capacity) / @Mass AS [specific charge capacity]

	, MAX(discharge_capacity) / @mass AS SpecificMaxDischargeCapacity
	, MAX(discharge_capacity) / @Mass AS [specific discharge capacity]

	, (MAX(Discharge_capacity) / @Mass) * 1000 as [Discharge Capacity (mAh/g)]

	, MAX(charge_capacity) / @Mass / @activepercent AS [specific active charge capacity]
	, MAX(discharge_capacity) / @Mass / @activepercent AS [specific active discharge capacity]

	, max(Internal_Resistance) as [Maximum Resistance]
	, max(Internal_Resistance) as [Charge Resistance Min]

	, min(Internal_Resistance) as [Minimun Resistance]
	, min(Internal_Resistance) as [Charge Resistance Min Min]

	, min(voltage) as [Min Charge Voltage]
	, MAX(Voltage) AS [Max Charge Voltage]
from SolidPowerDev.dbo.ChannelNormal
where cellname = @cellname
and cycle_index <= (select isnull([# of cycles completed], 0) CycleNr from SP_DataWarehouse.dbo.celllog where [Cell #] = @cellname)
group by cycle_index
order by cycle_index

END
GO


