USE [SolidPowerDev]
GO

/****** Object:  StoredProcedure [dbo].[getVoltProfile_ARBIN]    Script Date: 8/4/2020 13:20:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[getVoltProfile_ARBIN]
	@Cellname varchar(200) = 'none',
	@CycleIndex int = '0'
AS
-- last update 02/06/2020 add 10000 multiplier to charge/discharge values per Nathan request

-- exec [getVoltProfile_ARBIN] '171111_graphite_a', 1
-- exec [getVoltProfile_ARBIN] '180206_d', 1  --arbin
-- exec [getVoltProfile_ARBIN] '160825_b', 10
-- select dbo.isBBdata('180824_b')
BEGIN
--declare @cellname varchar(200), @cycleindex int
--select @cellname = '180206_d', @cycleindex=0

declare @mass float, @ratio float
select @mass = cast(isnull([Total Cathode Mass (g)], 1.00) as float), @ratio = isnull([ratio active material], 1.00)
	from SP_DataWarehouse.dbo.CellLog where [Cell #] = @cellname

--print @ratio

declare @voltprof table (vpid int primary key identity (1,1), Test_Time bigint, Cycle_index int , Data_Point int,  [Specific Charge Cap mAh/g] float, 
		[Total Charge Cap mAh] float, [Charge V] float, [Active Charge Cap mAh/g] float, 
					[Specific Discharge Cap mAh/g] float, [Total Discharge Cap mAh] float, 
					[Active Discharge Cap mAh/g] float, [Discharge V] float, Voltage float)

	SET NOCOUNT ON;
	if @Cellname = 'none'
		select 'Please provide a CellName.'
	else
		begin
		if @Cycleindex != 0
		BEGIN
		-- cycle_name by Cycle_index
		insert into @voltprof(Test_Time, Cycle_index, Data_Point,  [Specific Charge Cap mAh/g], [Total Charge Cap mAh], [Charge V], [Active Charge Cap mAh/g], 
					[Specific Discharge Cap mAh/g], [Total Discharge Cap mAh], [Active Discharge Cap mAh/g], [Discharge V], Voltage)
		select
			cast(cn.Test_Time as bigint) Test_Time,
			cn.Cycle_index,
			cn.Data_Point,
			--cn.charge_capacity  /  cast(isnull(cl.[Total Cathode Mass (g)], 1.00) as float)  [Specific Charge Cap mAh/g],
			( cn.charge_capacity  /  @mass ) * 1000 [Specific Charge Cap mAh/g],
			cn.Charge_Capacity * 1000  as [Total Charge Cap mAh], --TotalChargeCapacity,
			cn.Voltage as [Charge V],
			(cn.Charge_Capacity /  @mass  / @ratio) * 1000 AS [Active Charge Cap mAh/g],   
			( cn.discharge_capacity /  @mass ) * 1000 AS [Specific Discharge Cap mAh/g], -- SpecificDischargeCapacity,
			( cn.disCharge_Capacity * 1000 )as [Total Discharge Cap mAh], -- TotalDischargeCapacity,
			(cn.disCharge_Capacity /  @mass   / @ratio ) * 1000 as [Active Discharge Cap mAh/g], -- ActiveDischargeCapacity,
			cn.Voltage as [Discharge V]
			, cn.Voltage as Voltage
		--into @voltprof
		from SolidPowerDev.dbo.ChannelNormal cn
		--inner join SolidPowerDev.dbo.Cell c on cn.cellname = c.cellname 
		inner join SP_DataWarehouse.dbo.Celllog cl on cn.cellname = cl.[Cell #]
		where cn.cellname = @Cellname
		and cl.[Cell #] =  @cellname
		and cn.charge_capacity  / @mass  > 0.00
		and @mass > 0.00
		and Cycle_index = @CycleIndex

			--select * from @voltprof
			select cast(a.Test_Time as bigint) [Test Time], a.Cycle_index, a.data_point, a.Voltage, b.[Specific Charge Cap mAh/g], c.[Total Charge Cap mAh], [Charge V], e.[Active Charge Cap mAh/g],
			f.[Specific Discharge Cap mAh/g], g.[Total Discharge Cap mAh], h.[Active Discharge Cap mAh/g], [Discharge V]
			from (
			select Test_Time, Cycle_index, data_point, [Charge V], [Discharge V], Voltage from @voltprof) a 
			left outer join (
			select max(Test_Time) as Test_Time, Cycle_index, max(data_point) data_point, [Specific Charge Cap mAh/g] from @voltprof group by Cycle_index, [Specific Charge Cap mAh/g] 
			) b on a.Cycle_index=b.Cycle_index AND a.Data_Point=b.data_point
			left outer join (
			select max(Test_Time) as Test_Time, Cycle_index, max(data_point) data_point, [Total Charge Cap mAh] from @voltprof group by Cycle_index, [Total Charge Cap mAh] 
			) c on a.Cycle_index=c.Cycle_index AND a.Data_Point=c.data_point
			--left outer join (
			--select Cycle_index, max(data_point) data_point, [Charge V] from @voltprof group by Cycle_index, [Charge V] 
			--) d  on a.Cycle_index=d.Cycle_index AND a.Data_Point=d.data_point
			left outer join (
			select max(Test_Time) as Test_Time, Cycle_index, max(data_point) data_point, [Active Charge Cap mAh/g] from @voltprof group by Cycle_index, [Active Charge Cap mAh/g] 
			) e  on a.Cycle_index=e.Cycle_index AND a.Data_Point=e.data_point
			left outer join (
			select max(Test_Time) as Test_Time, Cycle_index, max(data_point) data_point, [Specific Discharge Cap mAh/g] from @voltprof group by Cycle_index, [Specific Discharge Cap mAh/g]
			) f  on a.Cycle_index=f.Cycle_index AND a.Data_Point=f.data_point
			left outer join (
			select max(Test_Time) as Test_Time, Cycle_index, max(data_point) data_point, [Total Discharge Cap mAh] from @voltprof group by Cycle_index, [Total Discharge Cap mAh]
			) g  on a.Cycle_index=g.Cycle_index AND a.Data_Point=g.data_point
			left outer join (
			select max(Test_Time) as Test_Time, Cycle_index, max(data_point) data_point, [Active Discharge Cap mAh/g] from @voltprof group by Cycle_index, [Active Discharge Cap mAh/g]
			) h  on a.Cycle_index=h.Cycle_index AND a.Data_Point=h.data_point
			--left outer join (
			--select Cycle_index, max(data_point) data_point, [Discharge V] from @voltprof group by Cycle_index, [Discharge V]
			--) i  on a.Cycle_index=i.Cycle_index AND a.Data_Point=i.data_point
			order by a.data_point 
		END
/*
		else
--		-- cell name all Cycle_index
		BEGIN
			--select 'No Cycle_Index Supplied'
-------------------------------------------------------------------------------------------------------------
--------Volt Profile all cycles
-------------------------------------------------------------------------------------------------------------
		-- cycle_name by Cycle_index
			insert into @voltprof(Test_Time, Cycle_index, Data_Point,  [Specific Charge Cap mAh/g], [Total Charge Cap mAh], [Charge V], [Active Charge Cap mAh/g], 
					[Specific Discharge Cap mAh/g], [Total Discharge Cap mAh], [Active Discharge Cap mAh/g], [Discharge V], Voltage)
			select
				cast(cn.Test_Time as bigint) Test_Time,
				cn.Cycle_index,
				cn.Data_Point,
				cn.charge_capacity  /  @mass  [Specific Charge Cap mAh/g],
				cn.Charge_Capacity as [Total Charge Cap mAh], --TotalChargeCapacity,
				cn.Voltage as [Charge V],
				(cn.Charge_Capacity /  @mass  / @ratio ) AS [Active Charge Cap mAh/g],   
				cn.discharge_capacity /  @mass  AS [Specific Discharge Cap mAh/g], -- SpecificDischargeCapacity,
				cn.disCharge_Capacity as [Total Discharge Cap mAh], -- TotalDischargeCapacity,
				(cn.disCharge_Capacity /  @mass   / @ratio ) as [Active Discharge Cap mAh/g], -- ActiveDischargeCapacity,
				cn.Voltage as [Discharge V]
				, cn.Voltage as Voltage
		from SolidPowerDev.dbo.ChannelNormal cn
		--inner join SolidPowerDev.dbo.Cell c on cn.cellname = c.cellname 
		inner join SP_DataWarehouse.dbo.Celllog cl 
			on cn.cellname = cl.[Cell #]
		where cn.cellname = @Cellname
		and cl.[Cell #] =  @cellname
		and cn.charge_capacity  / @mass  > 0.00
		and @mass > 0.00
		--and Cycle_index = @CycleIndex

				--select * from @voltprof
				select a.Test_Time [Test Time], a.Cycle_index, a.data_point, b.[Specific Charge Cap mAh/g], c.[Total Charge Cap mAh], [Charge V], e.[Active Charge Cap mAh/g],
				f.[Specific Discharge Cap mAh/g], g.[Total Discharge Cap mAh], h.[Active Discharge Cap mAh/g], [Discharge V]
				from (
				select Test_Time, Cycle_index, data_point, [Charge V], [Discharge V] from @voltprof) a 
				left outer join (
				select max(Test_Time) as Test_Time, Cycle_index, max(data_point) data_point, [Specific Charge Cap mAh/g] 
					from @voltprof group by Cycle_index, [Specific Charge Cap mAh/g] 
				) b on a.Cycle_index=b.Cycle_index AND a.Data_Point=b.data_point
				left outer join (
				select max(Test_Time) as Test_Time, Cycle_index, max(data_point) data_point, [Total Charge Cap mAh] 
					from @voltprof group by Cycle_index, [Total Charge Cap mAh] 
				) c on a.Cycle_index=c.Cycle_index AND a.Data_Point=c.data_point
				--left outer join (
				--select Cycle_index, max(data_point) data_point, [Charge V] from @voltprof group by Cycle_index, [Charge V] 
				--) d  on a.Cycle_index=d.Cycle_index AND a.Data_Point=d.data_point
				left outer join (
				select max(Test_Time) as Test_Time, Cycle_index, max(data_point) data_point, [Active Charge Cap mAh/g]	
					from @voltprof group by Cycle_index, [Active Charge Cap mAh/g] 
				) e  on a.Cycle_index=e.Cycle_index AND a.Data_Point=e.data_point
				left outer join (
				select max(Test_Time) as Test_Time, Cycle_index, max(data_point) data_point, [Specific Discharge Cap mAh/g] 
					from @voltprof group by Cycle_index, [Specific Discharge Cap mAh/g]
				) f  on a.Cycle_index=f.Cycle_index AND a.Data_Point=f.data_point
				left outer join (
				select max(Test_Time) as Test_Time, Cycle_index, max(data_point) data_point, [Total Discharge Cap mAh] 
					from @voltprof group by Cycle_index, [Total Discharge Cap mAh]
				) g  on a.Cycle_index=g.Cycle_index AND a.Data_Point=g.data_point
				left outer join (
				select max(Test_Time) as Test_Time, Cycle_index, max(data_point) data_point, [Active Discharge Cap mAh/g] 
					from @voltprof group by Cycle_index, [Active Discharge Cap mAh/g]
				) h  on a.Cycle_index=h.Cycle_index AND a.Data_Point=h.data_point
				--left outer join (
				--select Cycle_index, max(data_point) data_point, [Discharge V] from @voltprof group by Cycle_index, [Discharge V]
				--) i  on a.Cycle_index=i.Cycle_index AND a.Data_Point=i.data_point
				order by a.data_point  
		END
	*/
	end

END
GO


