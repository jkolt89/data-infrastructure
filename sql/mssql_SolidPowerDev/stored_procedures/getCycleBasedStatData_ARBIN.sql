USE [SolidPowerDev]
GO

/****** Object:  StoredProcedure [dbo].[getCycleBasedStatData_ARBIN]    Script Date: 8/4/2020 13:16:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[getCycleBasedStatData_ARBIN]
	@cellname varchar(100) = 'NA'
AS
BEGIN
	
	SET NOCOUNT ON;
	if @cellname = 'NA'
		select 'Please Enter a CellName'
	else
	SELECT
	   top 100 PERCENT 
	   --IDENTITY(int, 1, 1) AS CBS_ID,
	   cn.CellName,
	   cn.Cycle_Index,
	   c.Mass,
	   -- SPECIFIC CHARGE AND DISCHARGE CAPACITY
	   MAX(cn.charge_capacity) / c.mass AS SpecificMaxChargeCapacity,
	   MAX(cn.Discharge_capacity) / c.mass AS SpecificMaxDischargeCapacity,
	   max(cn.Charge_Energy)/c.mass as SpecificMaxEnergyCapacity,
	   max(cn.Discharge_Energy)/c.mass as SpecificMinEnergyCapacity,
	   -- CHARGE CAPACITY
	   AVG(cn.charge_capacity) AvgChargeCapacity,
	   MAX(cn.charge_capacity) MaxChargeCapacity,
	   -- DISCHARGE CAPACITY
	   MAX(cn.discharge_capacity) MaxDischargeCapacity,
	   --EFFICIENCY
	   MAX(cn.Discharge_Capacity) / MAX(cn.Charge_Capacity) AS Efficiency,
	   MAX(cn.Charge_Capacity) / MAX(cn.Discharge_Capacity) AS InverseEfficiency,
	   -- VOLTAGE
	   MAX(cn.Voltage) AS MaxChargeVoltage,
	   MIN(cn.Voltage) AS MinChargeVoltage,
	   AVG(cn.Voltage) AvgChargeVoltage,
	   -- CHARGE / DISCHARGE ENERGY
	   max(cn.charge_energy) as MaxChargeEnergy,
	   max(cn.discharge_energy) as MaxDischargeEnergy
	   --charge rate= (mass * max charge c ap/mass) / median(current)	
	   --, c.mass * max(cn.Charge_Capacity)/@median AS ChargeRate
	   , c.mass * max(cn.Charge_Capacity)/max(median) AS ChargeRate
	   -- discharge rate = (mass * max charge cap/mass) / median(current)	
	   --, c.mass * max(cn.Discharge_Capacity)/@median AS DischargeRate
	   , c.mass * max(cn.Discharge_Capacity)/max(median) AS DischargeRate
	   --, max(median)

	--INTO SolidPowerDev.dbo.CycleBasedStatistic
	FROM
	   SolidPowerDev.dbo.ChannelNormal cn 
	   INNER JOIN
		  Cell c 
		  ON c.CellName = cn.CellName 
		INNER JOIN 
		( select distinct CellName, round(count(cntid)/2.0, 0) median from SolidPowerDev.dbo.ChannelNormal
			group by CellName ) s
		on s.Cellname = cn.CellName
		--RIGHT OUTER JOIN
		--SP_DataWareHouse.dbo.CycleBasedStatistic cbs
		--on c.Cellname = cbs.cellname
		--and cn.Test_ID = cbs.TestID
		--and cn.Data_Point = cbs.Data
	WHERE
	   c.mass > 0 
	   AND cn.Charge_Capacity > 0 
	   and c.cellname = @CellName
	GROUP BY
	   cn.CellName,
	   cn.Cycle_Index,
	   c.mass
	HAVING MAX(cn.Discharge_Capacity)>0 
		and MAX(cn.charge_Capacity)>0
	--ORDER BY
		--newid(),
		--cn.CellName,
		--cn.Cycle_Index
END
GO


