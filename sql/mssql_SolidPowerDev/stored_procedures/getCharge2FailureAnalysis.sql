USE [SolidPowerDev]
GO

/****** Object:  StoredProcedure [dbo].[getCharge2FailureAnalysis]    Script Date: 8/4/2020 13:13:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[getCharge2FailureAnalysis]
	@cellname varchar(100)  = 'NA'
AS
BEGIN
	SET NOCOUNT ON;
/*
	charge 2 failure analysis calc:
	Cumulative charge capacity (the total amount of charge a cell has passed during its life) vs. time
	Cumulative discharge capacity (the total amount of discharge a cell has passed during its life) vs. time
	Calculated resistance (voltage/current)
	exec [getCharge2FailureAnalysis_ARBIN] '180327_a'
*/

if exists ( select 1 from SolidPowerDev.dbo.Cell where cellname = @cellname)
	-- print 'Arbin'
	exec SolidPowerDev.dbo.getCharge2FailureAnalysis_ARBIN @cellname
else
	-- print 'BB'
	exec BeckerBoard.dbo.getBBCharge2FailureAnalysis @cellname
END
GO


