USE [SolidPowerDev]
GO

/****** Object:  View [dbo].[vuCellLog]    Script Date: 10/18/2019 9:53:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE view [dbo].[vuCellLog] as
SELECT [CLID]
      ,[Cell #] as [CellName]
	  ,case when [Arbin Notes] like '%Bear%' or [Cell #] like '%Bear%' or [Purpose/Description] like '%bear%' then 'BEAR'
	  when ([Arbin Notes] like '%haps%' or [Cell #] like '%haps%' ) and ([Arbin Notes] not in ('%Bear%') or [Cell #] not in ('%Bear%') or [Purpose/Description] not in ('%bear%')) then 'HAPS'
	  when ([Arbin Notes] like '%hasp%' or [Cell #] like '%hasp%' ) and ([Arbin Notes] not in ('%Bear%', '%haps%') or [Cell #] not in ('%Bear%', '%haps%')) then 'HASP'
	  when ([Arbin Notes] like '%li%' or [Cell #] like '%li%' ) 
		and ([Arbin Notes] not in ('%Bear%', '%haps%', '%hasp%') or [Cell #] not in ('%Bear%', '%haps%', '%hasp%')) then 'LI'
	  when ([Arbin Notes] like '%cop%' or [Cell #] like '%cop%' ) 
		and ([Arbin Notes] not in ('%Bear%', '%haps%', '%hasp%', '%li%') or [Cell #] not in ('%Bear%', '%haps%', '%hasp%', '%li%')) then 'QUADCOP'
	  when ([Arbin Notes] like '%haitao%' or [Cell #] like '%haitao%' ) 
		and ([Arbin Notes] not in ('%Bear%', '%haps%', '%hasp%', '%li%', '%cop%') or [Cell #] not in ('%Bear%', '%haps%', '%hasp%', '%li%', '%cop%')) then 'HAITAO'
	  when ([Arbin Notes] like '%pouch%' or [Cell #] like '%pouch%' ) 
		and ([Arbin Notes] not in ('%Bear%', '%haps%', '%hasp%', '%li%', '%cop%', '%haitao%') or [Cell #] not in ('%Bear%', '%haps%', '%hasp%', '%li%', '%cop%', '%haitao%')) then 'POUCH'
	  when ([Arbin Notes] like '%graphite%' or [Cell #] like '%graphite%' ) 
		and ([Arbin Notes] not in ('%Bear%', '%haps%', '%hasp%', '%li%', '%cop%', '%haitao%', '%pouch%') or [Cell #] not in ('%Bear%', '%haps%', '%hasp%', '%li%', '%cop%', '%haitao%', '%pouch%')) then 'GRAPHITE'
	  end as CellType
      ,[# of cycles completed] [NrCompletedCycles]
      ,[1st cycle CE (%)] [CycleCE1] 
      ,[1st cycle charge] [CycleCharge1]
      ,[1st cycle discharge] [CycleDischarge1]
      ,[Anode]
      ,[Anode Mass (g)] [AnodeMassg]
      ,case when [Arbin Notes] in ('na', 'nan') then NULL
	  else [Arbin Notes]
	  end [ArbinNotes]
      ,[Cage torque (in-lbs)] [CageTorqueLBS]
      ,[Capacity (mAh)] [CapacitymAH]
      ,[Cathode]
      ,[Cathode Pressure (tons)] [CathodePressure]
      ,[Channel]
      ,[Current Collector] [CurrentCollector]
      ,[Cycle # of stoppage] [NrCycleStoppage]
      ,[Die Diameter (1.3 or 1.6 cm)] [DieDiameter]
      ,[Excel Time] [ExcelTime]
      ,[Initial Resistance (ohms)] [InitResistance]
	  ,case when [Last DateTime] like '20%'
       then cast(SUBSTRING([Last DateTime], 0, 20) as smalldatetime)  
	   else NULL
	  end as [LastDateTime]
      ,[Loading (mg/cm^2)] [Loading]
      ,[Location]
      ,[Maximum Stack Pressure (tons)] [MaxStackPressure]
      ,[Notes]
      ,case when [Cell #] like 'BF_%' then 'BF'
	  else upper([Operator]) 
	  end as Operator
	  ,case when [Purpose/Description] like 'Notes from Arbin:%' or [Purpose/Description] like 'No notes entered%'
		then ltrim(rtrim(replace(replace([Purpose/Description], 'Notes from Arbin: ', ''), 'No notes entered', '')))
      when len(ltrim(rtrim(replace(replace([Purpose/Description], 'Notes from Arbin: ', ''), 'No notes entered', ''))))  = 0 
		then null 
	  else [Purpose/Description]
	  end [PurposeDesc]
      ,case when [Python Time] like '20%'
       then cast(SUBSTRING([Python Time] , 0, 20) as smalldatetime)  
	   else NULL
	  end as [StartDate]
      ,[Reason for stoppage] [StoppageReason]
      ,[Separator]
      ,[Separator Mass (g)] [SeperatorMass]
      ,[Total Cathode Mass (g)] [TtlCathodeMass]
      ,[active cathode] [ActiveCathode]
      ,[active cathode date coated] [ActiveCathodeDateCoated]
      ,[active material theoretical capacity (mAh/g)] [ActiveTheoreticalCap]
      ,[cathode particle coating] [CathParticleCoating]
      ,[cell theoretical capacity (mAh/g)] [CellTheoreticalCap]
      ,[die]
      ,[electrolyte]
      ,[max voltage] [MaxVoltage]
      ,[max voltage w/ outlier] [MaxVoltageOut]
      ,[max voltage w/o outlier] [MaxVoltageNoOut]
      ,[maximum discharge capacity] [MaxDisCap]
      ,[nominal percent active material] [NominalPercentActiveMaterial]
      ,[ratio active material] [RatioActiveMaterial]
      ,[temperature]
      ,[wt% coating] [WtPercentCoating]
--  FROM [SolidPowerDev].[dbo].[CellLog]
FROM [SP_DataWarehouse].[dbo].[CellLog]
GO


