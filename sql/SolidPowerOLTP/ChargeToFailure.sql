USE [SolidPowerDev]
GO

/****** Object:  StoredProcedure [dbo].[getCharge2FailureAnalysis]    Script Date: 10/18/2019 9:55:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[getCharge2FailureAnalysis]
	@cellname varchar(100) = '180327_a'
AS
BEGIN
	SET NOCOUNT ON;
/*
	charge 2 failure analysis calc:
	Cumulative charge capacity (the total amount of charge a cell has passed during its life) vs. time
	Cumulative discharge capacity (the total amount of discharge a cell has passed during its life) vs. time
	Calculated resistance (voltage/current)
*/

	select -- top 100 percent
	cellname, cycle_index, Data_Point, Charge_Capacity, Discharge_Capacity,
	voltage, [current], (voltage/[current]) as CalcRes,
	SUM(Charge_Capacity) OVER (ORDER BY Data_Point) AS CumulativeChargeCap,
	SUM(Discharge_Capacity) OVER (ORDER BY Data_Point) AS CumulativeDischargeCap
from
	SolidPowerDev.dbo.ChannelNormal
where
	Charge_Capacity != 0
and
	[current] > 0
	and CellName = @cellname  -- '180327_a' -- '140424_a'
order by
	 Data_Point
END
GO


