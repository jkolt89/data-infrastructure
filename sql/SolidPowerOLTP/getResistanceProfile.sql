USE [SolidPowerDev]
GO

/****** Object:  StoredProcedure [dbo].[getResistanceProfile]    Script Date: 10/18/2019 9:56:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[getResistanceProfile]
	@CellName varchar(150) = 'NA',
	@withzero varchar(20) = 'withzero'
-- getResistanceProfile '2019_0016d_pc_j', 'nozero';  --  '190913_b'; 
AS
BEGIN

SET NOCOUNT ON;

-- if @CellName = 'NA'
	-- select 'Please enter a CellName.'
-- else

DECLARE @t1 TABLE (t1id int identity(1,1), Test_ID int, ci INT, data_point int, Resistance float, [current] float, voltage float)
declare @output table (oid int identity(1,1), t1id int, Test_ID int, ci INT, data_point int, Resistance float, rv float, mc float, 
	lav float, lac float, direction varchar(30))
declare @data_slice_v table (dsvid int identity(1,1), v float)
declare @data_slice_c table (dscid int identity(1,1), c float)
declare @data_slice table (dsid int identity(1,1), v float, c float)

declare @count int, @last_point int, @iter int, @this_r float, @last_r float, @this_c float, @last_c float, @lav float, @lac float,
	@this_v float, @last_v float, @slice_count int, @new_count int, @current float, @voltage float, @break int

select @count = 1, @last_point = 0, @this_r = 0.0, @last_r=0.0, @this_c=0.000000, @last_c=0.00000, @lav=0.0, @lac=0.0, @this_v=0.0, @last_v=0.0,
	@current=0.0, @new_count=0, @break=0

insert into @t1
select Test_ID, Cycle_Index, data_point, internal_resistance, [current], voltage
	from SolidPowerDev.dbo.ChannelNormal
	where cellname = @CellName
	order by Test_ID, Cycle_index, Data_Point
--select * from @t1 where data_point = 1
select @iter = count(*) from @t1

--print('count begin' + str(@count) + '     iter:  ' + str(@iter))

insert into @output(Test_ID, t1id, ci, data_point, Resistance, mc, lav, rv, lac, direction )
select Test_ID, t1id, ci, data_point, resistance, [current], voltage, voltage, [current], 'start_test'
from @t1
where t1id=1
--order by Test_ID, ci

select @count = 2

while @count <= @iter
		begin
		-- if this_res != last_res append r, mc, lav, rv, ci
		select @this_r = resistance, @this_c = [current], @this_v=voltage from @t1 where t1id = @count
		select @last_r = resistance, @this_c = [current], @last_v=voltage from @t1 where t1id = @count - 1
		--print('count:  ' + str(@count))
		--print(@last_r)
		--print(@this_r)
		if @this_r != @last_r
			begin
			insert into @output(Test_ID, t1id, ci, data_point, Resistance, mc, rv )
			select Test_ID, t1id, ci, data_point, resistance, [current], voltage
			from @t1
			where t1id= @count
			--print(@last_c)
			if @last_c != 0.00
				begin
				--print(@last_v)
				--print(@this_v)
				select @break = 0
				select top 1 @break = oid from @output where lav is null and lac is null order by oid
				update @output set lav = @last_v, lac = @last_c where oid = @break

				if @last_c > 0
					
					update @output set direction = 'Charge no rest' where oid = @break
				else
					update @output set direction = 'Discharge no rest' where oid = @break

				-- update @output set lav = @last_v, lac = @last_c where lav is null and lac is null
				end
			else
				begin
				--delete from @data_slice_c
				--delete from @data_slice_v
				delete from @data_slice

				-- combine below into 1 table?
				--insert into @data_slice_v(v)
					--select [voltage] from @t1 where t1id between @last_point and @count

				--insert into @data_slice_c(c)
					--select [current] from @t1 where t1id between @last_point and @count
				-----------------------------------
				insert into @data_slice(v, c)
					select voltage, [current] from @t1 where t1id between @last_point and @count

				select @slice_count = count(*) from	@data_slice		-- @data_slice_c
				
				--select @last_point = @slice_count  + 1
				-- print('slice_count: ' + str(@slice_count) + '   last_point: ' + str(@last_point))

				select @new_count = 1

				while @new_count <= @slice_count
					begin
					select top 1 @current = c, @voltage=v from @data_slice order by dsid desc

					if @current > 0.00
						begin
						-- update last_active_voltage, last_active_current
						--print(@current)
						select @break = 0
						select top 1 @break = oid from @output where lav is null and lac is null order by oid
						update @output set lav = @voltage, lac = @current, direction = 'Charge with rest' where oid = @break
						break
						-- update @output set c=@current, v=@voltage where c is null and v is null
						end

					if @current < 0.00
						begin
						-- update last_active_voltage, last_active_current
						--print(@current)
						select @break = 0
						select top 1 @break = oid from @output where lav is null and lac is null order by oid
						update @output set lav = @voltage, lac = @current, direction = 'Discharge with rest' where oid = @break
						break
						-- update @output set c=@current, v=@voltage where c is null and v is null
						end
					
					
					end
				select @new_count += 1
				end
			select @last_point = @count
			end

		select @count += 1
		
		end

--select top 10 * from @t1
if @withzero = 'withzero'
	select Test_ID, ci as Cycle_Index, Resistance, rv as Rest_Voltage, mc as Measurement_Current, 
	lav as Last_Active_Voltage, lac as Last_Active_Current, Direction
	from @output
else
	select Test_ID, ci as Cycle_Index, Resistance, rv as Rest_Voltage, mc as Measurement_Current, 
	lav as Last_Active_Voltage, lac as Last_Active_Current, Direction
	from @output
	where Resistance != 0

END
GO


