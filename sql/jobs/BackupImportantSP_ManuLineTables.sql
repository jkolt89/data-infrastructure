--2284
declare @rowcnt int, @rowcnt_bak int

select @rowcnt = count(*) from [dbo].[ConsumedBatch]
select @rowcnt_bak = count(*) from [dbo].[ConsumedBatch_BAK]

if @rowcnt > @rowcnt_bak
	begin
	drop table ConsumedBatch_BAK

	select * into ConsumedBatch_BAK from ConsumedBatch
	end
--else
--	print 'NO'

select @rowcnt=0, @rowcnt_bak=0

select @rowcnt = count(*) from [dbo].[LotNumberList]
select @rowcnt_bak = count(*) from [dbo].[LotNumberList_BAK]

if @rowcnt > @rowcnt_baks
	begin
	drop table LotNumberList_BAK

	select * into LotNumberList_BAK from LotNumberList
	end
--else
--	print 'NO'

select @rowcnt=0, @rowcnt_bak=0

select @rowcnt = count(*) from [dbo].[Weighing]
select @rowcnt_bak = count(*) from [dbo].[Weighing_BAK]

if @rowcnt > @rowcnt_bak
	begin
	drop table Weighing_BAK

	select * into Weighing_BAK from Weighing
	end
--else
--	print 'NO'

-- select count(*) from ConsumedBatch_BAK
-- select count(*) from LotNumberList_BAK

