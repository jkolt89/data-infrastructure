use SP_DataWarehouse
go
--MAIN QUERIES FOR REPORTING

-- compares cell util activity between using the cell_log table and from counting files in directory of arbin computers
SELECT aac.KeyIDCount - cu.KeyIDCount CountDiff, 
isnull(isnull(aac.KeyIDCount - cu.KeyIDCount, aac.KeyIDCount), cu.KeyIDCount) CountDiffNulls,  
aac.ArbinUtilizationPercent - cu.ArbinUtilizationPercent UtilPercentDiff , * 
FROM (
select Arbin as KeyID, FileDate, count(CellName) KeyIDCount, i.NrChannels as TotalNrChannels, 
round((cast(count(CellName) as float)/ i.NrChannels ) * 100, 2) ArbinUtilizationPercent
from [ArbinActiveCells] ac
inner join SolidPowerDev.dbo.Instrument i
on ac.Arbin=i.KeyID
group by arbin, FileDate, i.NrChannels
--order by FileDate desc, arbin
union 
select 'TOTAL' as Arbin, FileDate, sum(cellcount) KeyIDCount, 572 as TotalNrChannels, 
	round((cast(sum(cellcount) as float) / 572 ) * 100, 2) ArbinUtilizationPercent
from (
select Arbin, FileDate, count(CellName) CellCount --, i.NrChannels NrChannels, round((cast(count(CellName) as float)/ i.NrChannels ) * 100, 2) UtilizationPercent
from [ArbinActiveCells] ac
inner join SolidPowerDev.dbo.Instrument i
on ac.Arbin=i.KeyID
group by ac.arbin, ac.FileDate, i.NrChannels ) a
group by a.FileDate
) aac
------------
full outer join CellUtilHistory cu
on aac.FileDate = cast( cu.LastDateTime as date) and aac.keyid = cu.keyid
where aac.FileDate >= '2020-09-28'
order by FileDate desc, cast(replace(replace(aac.KeyID, 'ARB ', ''), 'TOTAL', 999) as int) -- aac.KeyID


--END MAIN QUERIES

select * from ArbinActiveCells aac
inner join CellUtilHistory cu
on aac.FileDate = cast( cu.LastDateTime as date) and aac.arbin = cu.keyid

--delete from ArbinActiveCells where filedate = '2020-09-26'

--truncate table ArbinActiveCells_Stage
--select * into zDELME_ArbinActiveCells_Stage from ArbinActiveCells_Stage
select * from [dbo].[ArbinActiveCells_Stage] where filename is null
select * from [dbo].ArbinActiveCells
select Arbin, FileDate, count(CellName) CellCount from [ArbinActiveCells] group by arbin, FileDate order by FileDate desc, arbin
--delete from ArbinActiveCells_Stage where Arbin = 'arb 2'
--delete from ArbinActiveCells_Stage where arbin is null
--select distinct filedate from ArbinActiveCells
--delete from ArbinActiveCells_Stage where AACSID not in (
-- select AACSID
-- from [dbo].[ArbinActiveCells_Stage]
-- where filetext like '%.res' 
-- and cast(substring( filetext, 1, charindex(' ', filetext)-1) as date) = cast( getdate() as date)
-- and filetext not like '%ArbinSys.res%'
-- )

select *
from [dbo].[ArbinActiveCells_Stage]
where filetext like '%.res' 
and cast(substring( filetext, 1, charindex(' ', filetext)-1) as date) = cast( getdate() as date)
and filetext not like '%ArbinSys.res%'

 --rollback tran jk
 --delete from ArbinActiveCells_Stage where AACSID not in (
 --select AACSID
 --from [dbo].[ArbinActiveCells_Stage]
 --where filetext like '%.res' 
 --and cast(substring( filetext, 1, charindex(' ', filetext)-1) as date) = cast( getdate() as date)
 --and filetext not like '%ArbinSys.res%'
 --and FileName is not null
 --)

-- select * from SolidPowerDev..Instrument

select Arbin, max(FileDate) FileDate, count(FileName) CellCount , max(NrChannels) NrChannels
, round(cast(count(FileName) as float)/max(nrchannels),2)*100 Prcnt
from [ArbinActiveCells_Stage] a
inner join SolidPowerDev..Instrument i
on a.arbin=i.KeyID
group by arbin 
order by arbin

select * from ArbinActiveCells_Stage a
inner join SolidPowerDev..Instrument i
on a.arbin = i.keyid

select cast(sum(Cellcount) as float)/sum(NrChannels) from (
select Arbin, max(FileDate) FileDate, count(FileName) CellCount , max(NrChannels) NrChannels
, round(cast(count(FileName) as float)/max(nrchannels),2)*100 Prcnt
from [ArbinActiveCells_Stage] a
inner join SolidPowerDev..Instrument i
on a.arbin=i.KeyID
group by arbin ) a

insert into ArbinActiveCells( [FileText], [Arbin], [FileDate], CellName)
select  [FileText], [Arbin], [FileDate], [CellName] from dev..ArbinActiveCells

--update ArbinActiveCells_STAGE set Filename = replace(filename, '.res', '')
--where filename like '%.res'

SELECT * FROM (
select Arbin, FileDate, count(CellName) CellCount, i.NrChannels, round((cast(count(CellName) as float)/ i.NrChannels ) * 100, 2) UtilizationPercent
from [ArbinActiveCells] ac
inner join SolidPowerDev.dbo.Instrument i
on ac.Arbin=i.KeyID
group by arbin, FileDate, i.NrChannels
--order by FileDate desc, arbin
union 
select 'TOTAL' as Arbin, FileDate, sum(cellcount) CellCount, 572 as NrChannels, round((cast(sum(cellcount) as float) / 572 ) * 100, 2) UtilizationPercent
from (
select Arbin, FileDate, count(CellName) CellCount --, i.NrChannels NrChannels, round((cast(count(CellName) as float)/ i.NrChannels ) * 100, 2) UtilizationPercent
from [ArbinActiveCells] ac
inner join SolidPowerDev.dbo.Instrument i
on ac.Arbin=i.KeyID
group by ac.arbin, ac.FileDate, i.NrChannels ) a
group by a.FileDate
) z
order by FileDate desc, arbin

SELECT * FROM (
select Arbin, FileDate, count(CellName) CellCount, i.NrChannels, round((cast(count(CellName) as float)/ i.NrChannels ) * 100, 2) UtilizationPercent
, null as UtilizationPercentAllChannels
from [ArbinActiveCells] ac
inner join SolidPowerDev.dbo.Instrument i
on ac.Arbin=i.KeyID
group by arbin, FileDate, i.NrChannels
--order by FileDate desc, arbin
union 
select 'TOTAL' as Arbin, FileDate, sum(cellcount) CellCount, SUM( NrChannels) as NrChannelsInUse, 
round((cast(sum(cellcount) as float) / SUM( NrChannels)  ) * 100, 2) UtilizationPercent,
round((cast(sum(cellcount) as float) / 572 ) * 100, 2) UtilizationPercentAllChannels
from (
select Arbin, FileDate, count(CellName) CellCount, i.NrChannels NrChannels --, round((cast(count(CellName) as float)/ i.NrChannels ) * 100, 2) UtilizationPercent
from [ArbinActiveCells] ac
inner join SolidPowerDev.dbo.Instrument i
on ac.Arbin=i.KeyID
group by ac.arbin, ac.FileDate, i.NrChannels ) a
group by a.FileDate
) z
order by FileDate desc, arbin



SELECT [Src], ch.[cellname], [ChannelNumber], [ChannelIndex], [ActiveCellChannelCount], [LastDateTime], ch.[NrChannels], ch.[KeyID], [Description], [SerialNumber],
	UPPER(Operator) Operator, ch.NrCompletedCycles, UtilizationTotal, UtilizationPercent
  FROM [dbo].[vuChannelsInUse] ch
  full outer join (
    select KeyID, count(KeyID) UtilizationTotal, max(NrChannels) NrChannels, round((cast(count(keyid) as float) / max(nrchannels) *100), 2) UtilizationPercent
  from [vuChannelsInUse]
  group by KeyID
) a
on a.keyid = ch.keyid

--------------------------------------------------------------------------

USE [SolidPowerDev]
GO

/****** Object:  View [dbo].[vuChannelsInUse]    Script Date: 9/25/2020 17:09:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





--CREATE view [dbo].[vuChannelsInUse]
--as

--select isnull(KeyID, 'TOTAL') KeyID, max(NrChannels) TotalNrChannels, count(KeyID) KeyIDCount, 
--cast(cast(count(i.KeyID) as float) / max(i.NrChannels) * 100 as decimal(10,2)) ArbinUtilizationPercent
--from dbo.cell c
--right outer join dbo.vucelllog cl
--	on cl.CellName = c.cellname
--inner join Instrument i
--	on c.Serial_Number = i.serialnumber
--where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
--	--and dbo.isBBData(c.Cellname) = 'Arbin'
----group by  c.cellname, cl.cellname, channel_number, channel_index, LastDateTime, cl.channel, i.SerialNumber, i.KeyID, i.Description
----group by rollup (keyid)
--group by grouping sets (keyid)
--UNION ALL
--select 'TOTAL' KeyID, 572 as TotalNrChannels, count(keyID) KeyIDCount, cast((cast(count(keyid) as float) / 572 ) * 100 as decimal(8,2)) ArbinUtilizationPercent
--from dbo.cell c
--right outer join dbo.vucelllog cl
--	on cl.CellName = c.cellname
--inner join Instrument i
--	on c.Serial_Number = i.serialnumber
--where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
--order by 1

--select count(*) from vuCellLog where cast([LastDateTime] as date) = cast(getdate() as date)
select top 100 percent
	dbo.isBBdata(isnull(c.cellname, cl.cellname)) Src, 
	isnull(c.cellname, cl.cellname) cellname, 
	--isnull(cast(channel_number as varchar(30)), cl.Channel) ChannelNumber, 
	isnull( cast(channel_index as varchar(40)), 'No Channel Index for BB cells') ChannelIndex,
	--cl.NrCompletedCycles,
	substring( schedule_file_name, 0, CHARINDEX('\', schedule_file_name)) Operator,
	count(isnull(c.cellname, cl.cellname)) ActiveCellChannelCount, 
	FileDate as LastDateTime, 
	i.NrChannels as totalnumberchannels --, SumChannels
	, i.KeyID, 
	i.Description, 
	i.SerialNumber
from dbo.cell c
right join dbo.Instrument i
	on c.Serial_Number = i.serialnumber
-- right outer join dbo.vucelllog cl
inner join SP_DataWarehouse.dbo.ArbinActiveCells cl
	on cl.CellName = c.cellname
where cast(cl.FileDate as date) = cast(getdate() as date)
and dbo.isBBData(cl.cellname) = 'Arbin'
--where cast(cl.[LastDateTime] as date) = '2020/07/24'
--where year(LastDateTime) = 2020
--group by c.cellname, cl.cellname, channel_number, channel_index, FileDate, cl.channel, i.SerialNumber, i.KeyID, i.Description , NrChannels, schedule_file_name
group by c.cellname, cl.cellname, channel_number, channel_index, FileDate, i.SerialNumber, i.KeyID, i.Description , NrChannels, schedule_file_name
order by FileDate desc, cast(Channel_Number as int), Channel_Index
GO

select * from [dbo].[ArbinActiveCells_Stage]
select * from [dbo].[ArbinActiveCells]
select * from SolidPowerDev.dbo.Instrument

select Arbin, count(arbin) ArbinCount 
from [dbo].[ArbinActiveCells_Stage] 
group by Arbin

select Arbin, count(arbin) ArbinCount 
from [dbo].[ArbinActiveCells_Stage] s
right outer join SolidPowerDev.dbo.Instrument i
on i.keyid = s.arbin
group by Arbin

select * into [admin].[dbo].[ArbinActiveCells_BAK] from SP_DataWarehouse.dbo.ArbinActiveCells
select top 10 * from SolidPowerDev..cell order by EntryDate desc


--if exists ( select top 1 1 from ArbinActiveCells where filedate = cast(getdate() as date) )
--	delete from ArbinActiveCells where filedate = cast(getdate() as date)

select * from ArbinActiveCells where filedate = cast(getdate() as date)

select * from [dbo].[vuFileCellUtilHistory]

select * from [dbo].[vuCompareArbinUtil] order by FileDate desc, cast(replace(replace(KeyID, 'ARB ', ''), 'TOTAL', 999) as int) -- aac.KeyID

select * from vuCompareArbinUtil 
--where filedate = cast(getdate() as date)
order by filedate desc, cast(replace(replace(KeyID, 'ARB ', ''), 'TOTAL', 999) as int) 

select * from dev.dbo.testdstexec
select * from arbinactivecells
select distinct Filename from [dbo].[ArbinActiveCells_Stage]

select distinct * from arbinactivecells
select cellname from arbinactivecells where filedate = cast(getdate() as date)

select * from SolidPowerDev..vuCellLog


select *  from arbinactivecells where filedate = cast(getdate() as date) and arbin = 'arb 3'
select * from SP_ManuLine..MillSetup
--delete from SP_ManuLine..MillSetup where msid =1
