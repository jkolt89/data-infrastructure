/****** Script for SelectTopNRows command from SSMS  ******/
SELECT *
  FROM [SP_DataWarehouse].[dbo].[CompareArbinUtilHistory]
  --where keyid = 'ARB 5'
  order by keyid desc, entrydate desc

select max(pid), filedate, keyidcount, keyid
from [CompareArbinUtilHistory]
group by keyidcount, filedate, keyid
order by keyid desc, filedate desc


--##################################################################################

--CREATE view [dbo].[vuCompareArbinUtil]
--as

--MAIN QUERIES FOR REPORTING
-- select * from vuCompareArbinUtil order by FileDate desc, cast(replace(replace(KeyID, 'ARB ', ''), 'TOTAL', 999) as int) 
-- compares cell util activity between using the cell_log table and from counting files in directory of arbin computers
--select top 100 percent * from (
SELECT --top 100 percent
	aac.KeyIDCount - cl.KeyIDCount CountDiff, 
	isnull(isnull(aac.KeyIDCount - cl.KeyIDCount, aac.KeyIDCount), cl.KeyIDCount) CountDiffNulls,  
	aac.ArbinUtilizationPercent - cl.ArbinUtilizationPercent UtilPercentDiff
	--, aac.KeyID
	--, FileDate 
	-- , cl.LastDateTime
	, aac.*
	--, cl.KeyId as cuKeyID
	, LastDateTime as clLastDateTime
	, cl.KeyIDCount as clKeyIDCount
	, cl.ArbinUtilizationPercent as clArbinUtilizationPercent
	--,'' as split
	--, cl.*
FROM (
select Arbin as KeyID, FileDate, count(CellName) KeyIDCount, i.NrChannels as TotalNrChannels, 
round((cast(count(CellName) as float)/ i.NrChannels ) * 100, 2) ArbinUtilizationPercent, max(EntryDate) EntryDate
from [ArbinActiveCells] ac
inner join SolidPowerDev.dbo.Instrument i
on ac.Arbin=i.KeyID
group by arbin, FileDate, i.NrChannels
--order by FileDate desc, arbin
union 
select 'TOTAL' as Arbin, FileDate, sum(cellcount) KeyIDCount, 572 as TotalNrChannels, 
	round((cast(sum(cellcount) as float) / 572 ) * 100, 2) ArbinUtilizationPercent, max(EntryDate)
from (
select Arbin, FileDate, count(CellName) CellCount
 , max(EntryDate) EntryDate--, i.NrChannels NrChannels, round((cast(count(CellName) as float)/ i.NrChannels ) * 100, 2) UtilizationPercent
from [ArbinActiveCells] ac
inner join SolidPowerDev.dbo.Instrument i
on ac.Arbin=i.KeyID
group by ac.arbin, ac.FileDate, i.NrChannels ) a
group by a.FileDate
) aac
------------
full outer join CellUtilHistory cl
on aac.FileDate = cast( cl.LastDateTime as date) and aac.keyid = cl.keyid
where aac.FileDate >= '2020-09-28'
--order by aac.FileDate desc, cast(replace(replace(aac.KeyID, 'ARB ', ''), 'TOTAL', 999) as int) -- aac.KeyID
--) z
--order by FileDate desc, cast(replace(replace(KeyID, 'ARB ', ''), 'TOTAL', 999) as int) -- aac.KeyID

--END MAIN QUERIES
GO


