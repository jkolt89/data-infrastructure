USE [SolidPowerDev]
GO

/****** Object:  View [dbo].[vuChannelsInUse]    Script Date: 9/25/2020 17:34:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





--CREATE view [dbo].[vuChannelsInUse]
--as

--select isnull(KeyID, 'TOTAL') KeyID, max(NrChannels) TotalNrChannels, count(KeyID) KeyIDCount, 
--cast(cast(count(i.KeyID) as float) / max(i.NrChannels) * 100 as decimal(10,2)) ArbinUtilizationPercent
--from dbo.cell c
--right outer join dbo.vucelllog cl
--	on cl.CellName = c.cellname
--inner join Instrument i
--	on c.Serial_Number = i.serialnumber
--where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
--	--and dbo.isBBData(c.Cellname) = 'Arbin'
----group by  c.cellname, cl.cellname, channel_number, channel_index, LastDateTime, cl.channel, i.SerialNumber, i.KeyID, i.Description
----group by rollup (keyid)
--group by grouping sets (keyid)
--UNION ALL
--select 'TOTAL' KeyID, 572 as TotalNrChannels, count(keyID) KeyIDCount, cast((cast(count(keyid) as float) / 572 ) * 100 as decimal(8,2)) ArbinUtilizationPercent
--from dbo.cell c
--right outer join dbo.vucelllog cl
--	on cl.CellName = c.cellname
--inner join Instrument i
--	on c.Serial_Number = i.serialnumber
--where cast(cl.[LastDateTime] as date) = cast(getdate() as date)
--order by 1

--select count(*) from vuCellLog where cast([LastDateTime] as date) = cast(getdate() as date)
select top 100 percent
	dbo.isBBdata(isnull(c.cellname, cl.cellname)) Src, isnull(c.cellname, cl.cellname) cellname, 
	--isnull(cast(channel_number as varchar(30)), cl.Channel) ChannelNumber, 
	cast(channel_number as varchar(30)) ChannelNumber, 
	isnull( cast(channel_index as varchar(40)), 'No Channel Index for BB cells') ChannelIndex,
	--cl.NrCompletedCycles,
	substring( schedule_file_name, 0, CHARINDEX('\', schedule_file_name)) Operator,
	count(isnull(c.cellname, cl.cellname)) ActiveCellChannelCount, 
	cl.FileDate, NrChannels --, SumChannels
	, i.KeyID, i.Description, i.SerialNumber
from dbo.cell c
right join dbo.Instrument i
	on c.Serial_Number = i.serialnumber
-- right outer join dbo.vucelllog cl
--inner join dbo.vucelllog cl
inner join SP_DataWarehouse.dbo.ArbinActiveCells cl
	on cl.CellName = c.cellname
where cast(cl.FileDate as date) = cast(getdate() as date)
and dbo.isBBData(cl.cellname) = 'Arbin'
--where cast(cl.[LastDateTime] as date) = '2020/07/24'
--where year(LastDateTime) = 2020
--group by c.cellname, cl.cellname, cl.NrCompletedCycles, channel_number, channel_index, cl.FileDate, cl.channel, i.SerialNumber, i.KeyID, i.Description , NrChannels, schedule_file_name
group by c.cellname, cl.cellname, channel_number, channel_index, cl.FileDate, i.SerialNumber, i.KeyID, i.Description , NrChannels, schedule_file_name
order by cl.FileDate desc, cast(Channel_Number as int), Channel_Index
GO


