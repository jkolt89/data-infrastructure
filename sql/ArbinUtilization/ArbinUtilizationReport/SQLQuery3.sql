--create view vuFileCellUtilHistory
--as
select top 100 percent
	cast(KeyID as varchar(50)) KeyID, 
	cast(LastDateTime as smalldatetime ) LastDateTime,
	cast(TotalNrChannels as int) TotalNrChannels,
	cast(KeyIDCount as int) KeyIDCount,
	cast([ArbinUtilizationPercent] as float) as [ArbinUtilizationPercent]
from (
select Arbin as KeyID, FileDate as LastDateTime, i.NrChannels as TotalNrChannels, 
	count(CellName) KeyIDCount, round((cast(count(CellName) as float)/ i.NrChannels ) * 100, 2) ArbinUtilizationPercent
from [ArbinActiveCells] ac
inner join SolidPowerDev.dbo.Instrument i
on ac.Arbin=i.KeyID
group by arbin, FileDate, i.NrChannels
--order by FileDate desc, arbin
union 
select 'TOTAL' as Arbin, FileDate, 572 as TotalNrChannels, sum(cellcount) KeyIDCount, 
	round((cast(sum(cellcount) as float) / 572 ) * 100, 2) ArbinUtilizationPercent
from (
select Arbin, FileDate, count(CellName) CellCount --, i.NrChannels NrChannels, round((cast(count(CellName) as float)/ i.NrChannels ) * 100, 2) UtilizationPercent
from [ArbinActiveCells] ac
inner join SolidPowerDev.dbo.Instrument i
on ac.Arbin=i.KeyID
--where filedate >= '2020/09/29'
group by ac.arbin, ac.FileDate, i.NrChannels ) a
group by a.FileDate
	) b
	where b.LastDateTime >= '2020/09/28'
	order by b.LastDateTime desc, cast(replace(replace(b.KeyID, 'ARB ', ''), 'TOTAL', 999) as int)