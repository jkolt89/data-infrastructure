CREATE TABLE public."Location" (
    LocationID   serial primary key,
    LocName      character varying,
    LocDesc      character varying
);


CREATE TABLE public."Equipment" (
    equipmentid   serial primary key,
    ename         character varying,
    ipaddress     character varying,
    locationid    INTEGER ,
	FOREIGN KEY (locationid) REFERENCES "Location" ( locationid )
);

CREATE TABLE public."Instrument" (
    instrumentid   serial primary key,
    iname          character varying,
    idesc          character varying,
    equipmentid    INTEGER,
	FOREIGN KEY (equipmentid) REFERENCES "Equipment" ( equipmentid )
);

CREATE TABLE public.channel (
    channelid      serial primary key,
    chname         character varying,
    instrumentid   INTEGER ,
	FOREIGN KEY (instrumentid) REFERENCES "Instrument" ( instrumentid )
);

CREATE TABLE measurementtype (
    measurementtypeid   serial primary key,
    mtypename           character varying,
    mtypeunit           character varying
);

CREATE TABLE employee (
    employeeid    serial primary key,
    opfirstname   character varying,
    oplastname    character varying
);

CREATE TABLE widget (
    widgetid     serial primary key,
    widgetname   character varying,
    barcode      character varying,
    runstart     character varying,
    employeeid   INTEGER ,
	FOREIGN KEY (employeeid) REFERENCES "employee" ( employeeid )
);

CREATE TABLE measurement (
    measurementid       serial primary key,
    mkey                character varying,
    mvalue              character varying,
    channelid           INTEGER ,
    measurementtypeid   INTEGER ,
    widgetid     INTEGER ,
	FOREIGN KEY (channelid) REFERENCES  channel ( channelid ),
	FOREIGN KEY (measurementtypeid) REFERENCES  measurementtype ( measurementtypeid ),
	FOREIGN KEY (widgetid) REFERENCES  widget ( widgetid )
);
