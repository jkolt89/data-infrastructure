USE [BeckerBoard]
GO

/****** Object:  StoredProcedure [dbo].[getBBResistanceProfile]    Script Date: 8/4/2020 13:24:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
















CREATE PROCEDURE [dbo].[getBBResistanceProfile]
	@CellName varchar(150) = 'NA',
	@withzero varchar(20) = 'withzero'
-- [zDEV_getBBResistanceProfile5] '191118_e', 'nozero';  --  '190913_b'; 
-- exec BeckerBoard.dbo.[zDEV_getBBResistanceProfile5] '191018_e'
-- [zDEV_getBBResistanceProfile5] '191118_f', 'nozero'
-- exec BeckerBoard.dbo.[zDEV_getBBResistanceProfile5] '191018_e'
-- sp_randomcell
AS
BEGIN
set nocount on
-- last active voltage s/be ~ 2.5, 4.1, 4.2 and 4.3
DECLARE @t1 TABLE (t1id int primary key identity(1,1), SPCellID int, Test_ID bigint, [Time] smalldatetime, EpochTS bigint, 
				ci INT, data_point bigint, Resistance float, [current] float, voltage float)

DECLARE @subset TABLE (t1id int primary key, SPCellID int, Test_ID bigint,  [Time] smalldatetime, EpochTS bigint, ci INT, 
			data_point bigint, Resistance float, [current] float, voltage float)

DECLARE @output table (oid int primary key identity(1,1), SPCellID int, t1id int, Test_ID bigint, [Time] smalldatetime, EpochTS bigint, 
			ci INT, data_point bigint, Resistance float, rv float, mc float, lav float, lac float, direction varchar(30))

declare @lav float, @lac float, @upperlim int, @lowerlim int, @count int

-- get charge and discharge data values from SPCell for R, I, V
insert into @t1 ( SPCellID, Test_ID, [time], EpochTS, ci, data_point, Resistance, [current], voltage)
select max(SPCellID) SPCellID, max(cast([measureEndTime] as bigint)) as Test_ID, [time], max(EpochTS) EpochTS, cycle + 1 as Cycle_Index
	--, cast([measureEndTime] as bigint) as data_point, r as internal_resistance, i AS [current], V as voltage
	--, max(cast(EpochTS as bigint)) as data_point, avg(r) as internal_resistance, avg(i) AS [current], avg(V) as voltage
	, max(cast(EpochTS as bigint)) as data_point, max(r) as internal_resistance, max(i) AS [current], max(V) as voltage
	from BeckerBoard.dbo.SPCell
	where cellname = @CellName and [measureEndTime] is not null 
	--and r is not null	
	group by cycle + 1, [time]
	order by max(SPCellID), cycle+1

--select * from @t1
-- first row of output table is automatically direction = start_test
insert into @output( t1id, SPCellID, Test_ID, [time], EpochTS, ci, data_point, Resistance, mc,  lav, rv, lac, direction)
select top 1 
	t1id, SPCellID, Test_ID, [time], EpochTS, ci, data_point, Resistance, [current] mc, voltage lav, voltage rv, 
	[current] lac, 'start_test'
from @t1
where t1id = 1
order by SPCellID

insert into @output(t1id, SPCellID, Test_ID, [time], EpochTS, ci, data_point, Resistance, rv, mc)
select distinct c1.t1id, c1.SPCellID, c1.Test_ID, c1.[time], c1.EpochTS, c1.ci, c1.data_point, c1.Resistance, c1.voltage, c1.[current]
	from @t1 c1
	inner join @t1 c2
	on c1.t1id - 1= c2.t1id
	where --c1.cellname = @CellName and c2.cellname = @cellname and
	c1.Test_ID != 0
	and c1.Resistance != c2.Resistance
	order by c1.SPCellID--, c1.Test_ID, c1.ci, c1.Data_Point

--select * from @t1
--------------------------------------------------
select @lav = lav, @lac = lac from @output where oid = 1

update @output set lav = @lav, lac=@lac, direction = 'no current' where oid = 2

select @count = count(*) from @t1

--select * from @output order by SPCellid

while @count > 0
	BEGIN
	--select top 1 @upperlim = data_point from @output where lav is null
	select top 1 @upperlim = SPCellID from @output where lav is null
	--print @upperlim
	insert into @subset select * from @t1 where SPCellID <= @upperlim -- and [current] !=0
	--select @upperlim upperlim, count(*) from @subset

	delete from @t1 where SPCellID <=@upperlim

	--select * from @subset
	select top 1 @lac = s2.[current], @lav = s2.voltage
	from @subset s1 inner join @subset s2 on s1.t1id-1 = s2.t1id
	where s1.[current] != s2.[current]
	--and s1.[current] = 0 and s2.[current] != 0
	order by s1.t1id desc

	update @output set lac=@lac, lav=@lav where SPCellID = @upperlim

	if not exists (select 1 from @output where lav is null and lac is null )
		begin
		break
		--delete from @t1
		--select @count = 0
		end
	else
		select @count = count(*) from @t1
		delete from @subset
	
	--print 'COUNT IS:  ' + STR(@count)
	END

--select * from @output

if @withzero = 'withzero'
	select
		ci as cycle_index, 
		[time], 
		EpochTS, resistance, rv as rest_voltage, mc as measurement_current, 
		lav, -- as last_active_voltage, 
		round(lav, 1) as last_active_voltage,
		lac as last_active_current, 
		case 
			when oid = 1 then 'start_test'
			when lac > 0 then 'charge with rest'
			when lac < 0 then 'discharge with rest'
			when lac = 0 and oid != 1 then 'no current'
			else direction
		end as direction
	from @output
	where mc != 0
	and round(lav, 1) in (2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.3, 3.4, 3.5, 4.0, 4.1, 4.2, 4.3, 4.4)

	order by cycle_index
else
	select
		ci as cycle_index, 
		[time], 
		EpochTS, resistance, rv as rest_voltage, mc as measurement_current, 
		lav, -- as last_active_voltage, 
		round(lav, 1) as last_active_voltage,
		lac as last_active_current, 
		case 
			when oid = 1 then 'start_test'
			when lac > 0 then 'charge with rest'
			when lac < 0 then 'discharge with rest'
			when lac = 0 and oid != 1 then 'no current'
			else direction
		--else 'start test'
		end as direction
	from @output
	where Resistance != 0
	and mc != 0
	and round(lav, 1) in (2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.3, 3.4, 3.5, 4.0, 4.1, 4.2, 4.3, 4.4)

	--and direction != 'start test'
	order by cycle_index


END
GO


