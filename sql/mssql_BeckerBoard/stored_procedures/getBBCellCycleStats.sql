USE [BeckerBoard]
GO

/****** Object:  StoredProcedure [dbo].[getBBCellCycleStats]    Script Date: 8/4/2020 13:22:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






















CREATE procedure [dbo].[getBBCellCycleStats]
	@cellname varchar(100)

--declare @cellname varchar(100)
--select @cellname = '200225_a'
-- select @cellname = '200217_c'
-- [zDEV_getBBCellCycleStatsUNION] '200206_d'
-- [zDEV_getBBCellCycleStatsUNION] '200217_c'

-- CREATED ON 02/21/2020

as
set nocount on

declare @mass float, @starttime bigint, @endtime bigint, @maxvoltage float, @activecontent float, @minvoltage float, @maxcycle int

select @mass = isnull([Total Cathode Mass (g)], 1.00), 
	@activecontent = isnull([nominal percent active material] , 1.00)
from SP_DataWarehouse.[dbo].[CellLog] where [Cell #] = @cellname

select @maxcycle = [# of cycles completed] from SP_DataWarehouse.dbo.CellLog where [Cell #] = @cellname


select @maxvoltage = max(v), @minvoltage = min(v) from SPCell where cellName = @cellname and v is not null

if @mass = 0.0 set @mass = 1
if @activecontent = 0.0 set @activecontent = 1

select @starttime = min(epochTS), @endtime = max(epochTS) from BeckerBoard.dbo.SPCell where cellname = @cellname

--print cast(@mass as varchar(10)) + '~~' + cast(@starttime as varchar(10)) + '~~' + cast(@endtime as varchar(10)) + '~~' 
--	+ cast(@maxvoltage as varchar(25)) + '~~' + cast(@activecontent as varchar(29))
--select * from (
select 
	Cycle_index,
	max([total charge capacity (mAh)]) [total charge capacity (mAh)],
	max([total discharge capacity (mAh)]) [total discharge capacity (mAh)], 
	max([charge capacity (mAh/g)]) [charge capacity (mAh/g)], 
	max([discharge capacity (mAh/g)]) [discharge capacity (mAh/g)], 
	max([specific charge capacity]) [specific charge capacity], 
	max([specific discharge capacity]) [specific discharge capacity], 
	max([active cathode charge capacity (mAh/g)]) [active cathode charge capacity (mAh/g)],
	max([active cathode discharge capacity (mAh/g)]) [active cathode discharge capacity (mAh/g)], 
	max([specific active charge capacity (mAh/g)]) [specific active charge capacity (mAh/g)], 
	max([specific active cathode discharge capacity (mAh/g)]) [specific active cathode discharge capacity (mAh/g)],
	
	max([charge resistance min]) [charge resistance min], 
	--max([Average ChargeRes]) [Average ChargeRes], 
	max([Maximum Resistance]) [Maximum Resistance],	

	max([discharge resistance min]) [discharge resistance min],	
	max([min discharge voltage]) [min discharge voltage],
	max([minimum voltage per cycle]) [minimum voltage per cycle],	
	max([max charge voltage]) [max charge voltage], 
	max([maximum voltage per cycle]) [maximum voltage per cycle],	
	max(Efficiency) Efficiency,	
	max(InverseEfficiency) InverseEfficiency,
	--max(StartDate) StartDate,	
	--max(EndDate) EndDate, 
	max(OrigCycleValue) OrigCycleValue 
from (
select -- top 10 
	 --ccs.cycle + 1 as Cycle_Index_Plus
	 ccs.cycle + 1 as Cycle_index
	--,  ccs.cycle as Cycle_index
	--, ROW_NUMBER() OVER( ORDER BY ccs.cycle ) AS Cycle_index
	--, MaxChargeCap as [total charge capacity (mAh)]
	, max(chargeCap) as [total charge capacity (mAh)]
	, max(dischargeCap) as [total discharge capacity (mAh)] 

	, max(chargeCap) / @mass [charge capacity (mAh/g)] -- web label
	, max(dischargeCap) / @mass [discharge capacity (mAh/g)] --web label
	, max(chargeCap) / @mass [specific charge capacity]
	, max(dischargeCap) / @mass [specific discharge capacity]

	, ( max(chargecap) / @mass ) / @activecontent as [active cathode charge capacity (mAh/g)] -- web label
	, ( max(dischargecap) / @mass) / @activecontent as [active cathode discharge capacity (mAh/g)]  -- web label
	,  (max(chargecap) / @mass) / @activecontent as [specific active charge capacity (mAh/g)] 
	,  ( max(dischargecap) / @mass) / @activecontent as [specific active cathode discharge capacity (mAh/g)]

	--, ccs.chargeRes ccsChargeRes
	, max(ccs.chargeRes) [charge resistance min]
	, max(ccs.chargeres) [Maximum Resistance]
	--, avg(ccs.chargeRes) as [Average ChargeRes]
	--, ccs.chargeres [maximum resistance per cycle]
	--, ccs.chargeres [maximum resistance per cycle]

	--vt.chargeRes vtChargeRes,
	--, vt.chargeres  [charge resistance min]
	--, vt.chargeres [Maximum Resistance]

	, max(dischargeRes) [discharge resistance min]

	, @minvoltage as [min discharge voltage]
	, @minvoltage as [minimum voltage per cycle]
	, @maxvoltage as [max charge voltage]
	, @maxvoltage as [maximum voltage per cycle]


	--, max(dischargeCap ) / max(isnull(chargeCap , 1)) Efficiency
	, max(dischargecap) / case  when max(chargecap) = 0.0 then 1
							when max(chargecap) is null then 1
							else max(chargecap) end Efficiency
	--, Efficiency
	--, max(chargeCap) / max(isnull(dischargeCap , 1)) InverseEfficiency
	, max(chargecap) / case  when max(dischargecap) = 0.0 then 1
							when max(dischargecap) is null then 1
							else max(dischargecap) end InverseEfficiency
	--, InverseEfficiency

	--, max([time]) StartDate, max([time]) EndDate
	, cycle OrigCycleValue

from BeckerBoard.dbo.CellCycleStats ccs
--from BeckerBoard.dbo.vuCellCycleStats_GroupByCycle ccs
--inner join (
--left outer join (
--	select cellname, cycle, chargeres
--	from cellCycleStats
--	where cellname = @cellname and chargeres is not null
--	--order by cycle
--	) vt
--	on vt.cellname = ccs.cellname
--	and vt.cycle = ccs.cycle
	where ccs.cellname = @cellname
	and ccs.cycle    <= @maxcycle 
	and ccs.cycle is not null
group by cellname , cycle
--order by ccs.cycle
--and dischargeCap is not null
--and chargeCap is not null
--and (chargeCap / @mass) is not null
--and (dischargeCap / @mass) is not null
UNION all
select -- top 10 
	 --ccs.cycle + 1 as Cycle_Index_Plus
	 ccs.cycle + 1 as Cycle_index
	--,  ccs.cycle as Cycle_index
	--ROW_NUMBER() OVER( ORDER BY ccs.cycle ) AS Cycle_index
	--, MaxChargeCap as [total charge capacity (mAh)]
	, max(chargeCap) as [total charge capacity (mAh)]
	, max(dischargeCap) as [total discharge capacity (mAh)] 

	, max(chargeCap) / @mass [charge capacity (mAh/g)] -- web label
	, max(dischargeCap) / @mass [discharge capacity (mAh/g)] --web label
	, max(chargeCap) / @mass [specific charge capacity]
	, max(dischargeCap) / @mass [specific discharge capacity]

	, ( max(chargecap) / @mass ) / @activecontent as [active cathode charge capacity (mAh/g)] -- web label
	, ( max(dischargecap) / @mass) / @activecontent as [active cathode discharge capacity (mAh/g)]  -- web label
	,  (max(chargecap) / @mass) / @activecontent as [specific active charge capacity (mAh/g)] 
	,  ( max(dischargecap) / @mass) / @activecontent as [specific active cathode discharge capacity (mAh/g)]

	, max(ccs.chargeRes) [charge resistance min]
	, max(ccs.chargeres) [Maximum Resistance]
	--, avg(ccs.chargeRes) as [Average ChargeRes]

	, max(dischargeRes) [discharge resistance min]

	, @minvoltage as [min discharge voltage]
	, @minvoltage as [minimum voltage per cycle]
	, @maxvoltage as [max charge voltage]
	, @maxvoltage as [maximum voltage per cycle]

	--, max(dischargeCap ) / max(isnull(chargeCap , 1)) Efficiency
	--, max(chargeCap) / max(isnull(dischargeCap ,1)) InverseEfficiency
	, max(dischargecap) / case  when max(chargecap) = 0.0 then 1
							when max(chargecap) is null then 1
							else max(chargecap) end Efficiency
	--, Efficiency
	--, max(chargeCap) / max(isnull(dischargeCap , 1)) InverseEfficiency
	, max(chargecap) / case  when max(dischargecap) = 0.0 then 1
							when max(dischargecap) is null then 1
							else max(dischargecap) end InverseEfficiency

	--, max([time]) StartDate, max([time]) EndDate
	, cycle OrigCycleValue

from BeckerBoard.[dbo].[z_cellCycleStats] ccs
	where ccs.cellname = @cellname
	and ccs.cycle    <= @maxcycle 
	and ccs.cycle is not null
group by cellname , cycle
) a
where a.Cycle_index <= @maxcycle
group by a.Cycle_index
order by a.Cycle_index
GO


