USE [BeckerBoard]
GO

/****** Object:  StoredProcedure [dbo].[getBBVoltProfile]    Script Date: 8/4/2020 13:26:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









CREATE PROCEDURE [dbo].[getBBVoltProfile]
	@Cellname varchar(100) = '',
	@Cycle_index int = 0
AS
-- exec BeckerBoard.dbo.[getBBVoltProfile] '191211_c'
--exec BeckerBoard.dbo.[getBBVoltProfile] '191213_c', 2
--select top 100 * from BeckerBoard..SPCell where time not like '1%'
/*
2/6/2020  Divide values by 10 to scale with Arbin data per Nathan request

*/

BEGIN
SET NOCOUNT ON;
--declare @CellName varchar(100)
declare @mass float, @ratio float
--select @CellName = '191025_b'
Select @mass = cast(isnull([Total Cathode Mass (g)], 1.00) as float) from SP_DataWarehouse.dbo.CellLog where [Cell #] = @CellName
select @ratio = isnull([ratio active material], 1.00) from SP_DataWarehouse.dbo.CellLog where [Cell #] = @CellName

if @cycle_index = 0
-- By CellName with all Cycles
	BEGIN
	select 
		isnull(ChargeCycle_index, DischargeCycle_index) as Cycle_index,
		isnull(ChargeEpochTS, DischargeEpochTS)  [Test Time], 
		isnull([Specific Charge Cap mAh/g], [Specific Discharge Cap mAh/g]) / 10 AS [Specific Cap mAh/g],
		isnull( [Active Charge Cap mAh/g], [Active Discharge Cap mAh/g]) / 10 AS [Active Cap mAh/g], 
		isnull([Total Charge Cap mAh], [Total Discharge Cap mAh]) / 10 AS [Total Cap mAh],
		isnull([Charge V], [Discharge V]) [Voltage],
		ChargeEpochTS, 
		[Specific Charge Cap mAh/g] / 10 AS [Specific Charge Cap mAh/g], 
		[Total Charge Cap mAh] / 10 AS [Total Charge Cap mAh], 
		[Charge V],
		[Active Charge Cap mAh/g] / 10 AS [Active Charge Cap mAh/g], 
		DischargeEpochTS, 
		[Specific Discharge Cap mAh/g] / 10 AS [Specific Discharge Cap mAh/g], 
		[Total Discharge Cap mAh] / 10 AS [Total Discharge Cap mAh], 
		[Discharge V], 
		[Active Discharge Cap mAh/g] / 10 AS [Active Discharge Cap mAh/g]
		, isnull(a.[State], b.[State]) [State]
	from (

		select --ROW_NUMBER() OVER (ORDER BY SPCellID) pkid,
			--substring(cast( EpochTS as varchar(10)), 0, 11) ChargeEpochTScut
			--, SPCellID
			avg(EpochTS) ChargeEpochTS
			, cycle + 1 as ChargeCycle_index
			, avg(cn.capacity)  /  @mass  [Specific Charge Cap mAh/g]
			, avg(cn.Capacity) as [Total Charge Cap mAh]
			, avg(cn.V) as [Charge V]
			, (avg(cn.Capacity) /  @mass  / @ratio) AS [Active Charge Cap mAh/g]
			, 'charge' as [State]
		from BeckerBoard.dbo.SPCell cn
		inner join SP_DataWarehouse.dbo.CellLog cl
		on cn.cellName = cl.[Cell #]
		where I >= 0 and EpochTS is not null-- and [Time] not like '% 2020 %'
		and cellname = @Cellname
		--and cellname ='191025_b' 
		--and cellname ='191105_f'
		--and cellname ='191211_c'
		--and cellname ='191213_c'
		--group by cycle, substring(cast( EpochTS as varchar(10)), 0, 11) 
		group by cycle, EpochTS
		) a full outer join (

		select --top 10
			--ROW_NUMBER() OVER (ORDER BY SPCellID) pkid
			--substring(cast( EpochTS as varchar(10)), 0, 11) DischargeEpochTScut
			--, SPCellID
			avg(EpochTS) DischargeEpochTS
			, cycle + 1 as DischargeCycle_index
			--, cn.capacity  /  cast(isnull(cl.[Total Cathode Mass (g)], 1.00) as float)	[Specific Discharge Cap mAh/g]
			, avg(cn.capacity) / @mass as [Specific Discharge Cap mAh/g]
			, avg(cn.Capacity) as [Total Discharge Cap mAh]
			, avg(cn.V) as [Discharge V]
			--, (cn.Capacity /  cast(isnull(cl.[Total Cathode Mass (g)], 1.00) as float)  / isnull(cl.[ratio active material], 1.00)) AS [Active Discharge Cap mAh/g]
			, (avg(cn.Capacity) /  @mass  / @ratio) [Active Discharge Cap mAh/g]
			, 'discharge' as [State]
		from BeckerBoard.dbo.SPCell cn
		inner join SP_DataWarehouse.dbo.CellLog cl
		on cn.cellName = cl.[Cell #]
		where I < 0 and EpochTS is not null --and [Time] not like '% 2020 %'
		and cellname = @Cellname
		--and cellname ='191025_b' 
		--and cellname ='191105_f'
		--and cellname ='191211_c'
		--and cellname ='191213_c'
		--group by cycle, substring(cast( EpochTS as varchar(10)), 0, 11) 
		group by cycle, EpochTS
		) b
		--on a.pkid = b.pkid
		--on a.EpochTS = b.EpochTS
		on a.ChargeEpochTS = b.DischargeEpochTS
		--on a.ChargeEpochTScut = b.DischargeEpochTScut  (don't join on EpochCut b/c cartesian product)
		where isnull([Specific Charge Cap mAh/g], [Specific Discharge Cap mAh/g]) != 0
		order by isnull(a.ChargeEpochTS, b.DischargeEpochTS)
		--order by isnull(a.ChargeEpochTS, b.DischargeEpochTS)
		END

--------------------------------------------------------
-- By Cellname and Cycle_index--------------------------
--------------------------------------------------------

ELSE
		BEGIN
		select 
		isnull(ChargeCycle_index, DischargeCycle_index) as Cycle_index,
		isnull(ChargeEpochTS, DischargeEpochTS)  [Test Time], 
		isnull([Specific Charge Cap mAh/g], [Specific Discharge Cap mAh/g]) / 10 AS [Specific Cap mAh/g],
		isnull( [Active Charge Cap mAh/g], [Active Discharge Cap mAh/g]) / 10 AS [Active Cap mAh/g], 
		isnull([Total Charge Cap mAh], [Total Discharge Cap mAh]) / 10 AS [Total Cap mAh],
		isnull([Charge V], [Discharge V]) [Voltage],
		ChargeEpochTS
		, [Specific Charge Cap mAh/g] / 10 AS [Specific Charge Cap mAh/g]
		, [Total Charge Cap mAh] / 10 AS [Total Charge Cap mAh] 
		, [Charge V]
		, [Active Charge Cap mAh/g] / 10 AS [Active Charge Cap mAh/g]
		, DischargeEpochTS
		, [Specific Discharge Cap mAh/g] / 10 AS [Specific Discharge Cap mAh/g]
		, [Total Discharge Cap mAh] / 10 AS [Total Discharge Cap mAh]
		, [Discharge V]
		, [Active Discharge Cap mAh/g] / 10 AS [Active Discharge Cap mAh/g]
		, isnull(a.[State], b.[State]) as [State]
	from (

		select --ROW_NUMBER() OVER (ORDER BY SPCellID) pkid,
			--substring(cast( EpochTS as varchar(10)), 0, 11) ChargeEpochTScut
			--, SPCellID
			avg(EpochTS) ChargeEpochTS
			, cycle + 1 as ChargeCycle_index
			, avg(cn.capacity)  /  @mass  [Specific Charge Cap mAh/g]
			, avg(cn.Capacity) as [Total Charge Cap mAh]
			, avg(cn.V) as [Charge V]
			, (avg(cn.Capacity) /  @mass  / @ratio) AS [Active Charge Cap mAh/g]
			, 'charge' as [State]
		from BeckerBoard.dbo.SPCell cn
		inner join SP_DataWarehouse.dbo.CellLog cl
		on cn.cellName = cl.[Cell #]
		where I >= 0 and EpochTS is not null --and [Time] not like '% 2020 %'
		and cellname = @Cellname
		AND cycle + 1 = @Cycle_index
		--and cellname ='191025_b' 
		--and cellname ='191105_f'
		--and cellname ='191211_c'
		--and cellname ='191213_c'
		--group by cycle, substring(cast( EpochTS as varchar(10)), 0, 11) 
		group by cycle, EpochTS
		) a full outer join (

		select --top 10
			--ROW_NUMBER() OVER (ORDER BY SPCellID) pkid
			--substring(cast( EpochTS as varchar(10)), 0, 11) DischargeEpochTScut
			--, SPCellID
			avg(EpochTS) DischargeEpochTS
			, cycle + 1 as DischargeCycle_index
			--, cn.capacity  /  cast(isnull(cl.[Total Cathode Mass (g)], 1.00) as float)	[Specific Discharge Cap mAh/g]
			, avg(cn.capacity) / @mass as [Specific Discharge Cap mAh/g]
			, avg(cn.Capacity) as [Total Discharge Cap mAh]
			, avg(cn.V) as [Discharge V]
			--, (cn.Capacity /  cast(isnull(cl.[Total Cathode Mass (g)], 1.00) as float)  / isnull(cl.[ratio active material], 1.00)) AS [Active Discharge Cap mAh/g]
			, (avg(cn.Capacity) /  @mass  / @ratio) [Active Discharge Cap mAh/g]
			, 'discharge' as [State]
		from BeckerBoard.dbo.SPCell cn
		inner join SP_DataWarehouse.dbo.CellLog cl
		on cn.cellName = cl.[Cell #]
		where I < 0 and EpochTS is not null-- and [Time] not like '% 2020 %'
		and cellname = @Cellname
		AND cycle + 1 = @Cycle_index
		--and cellname ='191025_b' 
		--and cellname ='191105_f'
		--and cellname ='191211_c'
		--and cellname ='191213_c'
		--group by cycle, substring(cast( EpochTS as varchar(10)), 0, 11) 
		group by cycle, EpochTS
		) b
		--on a.pkid = b.pkid
		--on a.EpochTS = b.EpochTS
		on a.ChargeEpochTS = b.DischargeEpochTS
		where isnull([Specific Charge Cap mAh/g], [Specific Discharge Cap mAh/g]) != 0
		order by isnull(a.ChargeEpochTS, b.DischargeEpochTS)
		END
END

GO


