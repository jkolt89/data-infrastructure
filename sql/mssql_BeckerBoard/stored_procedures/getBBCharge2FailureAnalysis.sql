USE [BeckerBoard]
GO

/****** Object:  StoredProcedure [dbo].[getBBCharge2FailureAnalysis]    Script Date: 8/4/2020 13:23:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








CREATE PROCEDURE [dbo].[getBBCharge2FailureAnalysis]
	@cellname varchar(100) -- = '180327_a'
AS
BEGIN
	SET NOCOUNT ON;
/*
	charge 2 failure analysis calc:
	Cumulative charge capacity (the total amount of charge a cell has passed during its life) vs. time
	Cumulative discharge capacity (the total amount of discharge a cell has passed during its life) vs. time
	Calculated resistance (voltage/current)
	exec [getBBCharge2FailureAnalysis] '200113_b' 
*/
select 
	 cast(isnull(ChargeTime, DischargeTime) as float) / 10000 as [Test Time]
	, isnull(ChargeCycle_index, DischargeCycle_index) Cycle_index
	, isnull([Charge Voltage], [Discharge Voltage]) Voltage
	, isnull( [Charge Capacity], [Discharge Capacity]) Capacity
	, isnull([Charge Calc Res], [Discharge Calc Res]) Calc_Res
	, [Charge Capacity]
	, [Charge Voltage]
	, [Charge Current]
	, [Charge Calc Res]
	, [Cumulative Charge Cap]
	, [Discharge Capacity]
	, [Discharge Voltage]
	, [Discharge Current]
	, [Discharge Calc Res]
	, [Cumulative Discharge Cap]
from (
	select -- top 100 percent
	cellname 
	, cycle + 1 as ChargeCycle_index
	, [Time] as ChargeTime
	, Capacity as [Charge Capacity]
	-- Discharge_Capacity as [Discharge Capacity],
	, V as [Charge Voltage] 
	, I as [Charge Current]
	, (V/I) as [Charge Calc Res]
	, SUM(Capacity) OVER (ORDER BY [Time]) AS [Cumulative Charge Cap]
	, 'charge' [State]
	--, SUM(Discharge_Capacity) OVER (ORDER BY Data_Point) AS [Cumulative Discharge Cap]
from
	BeckerBoard.dbo.SPCell
where
	I >= 0
	and Capacity != 0
	and CellName =  @CellName --'200113_b' 
	
) a full outer join (

	select -- top 100 percent
	cellname 
	, cycle + 1 as DischargeCycle_index
	, [Time] as DischargeTime
	, Capacity as [Discharge Capacity]
	-- Discharge_Capacity as [Discharge Capacity],
	, V as [Discharge Voltage] 
	, I as [Discharge Current]
	, (V/I) as [Discharge Calc Res]
	, SUM(Capacity) OVER (ORDER BY [Time]) AS [Cumulative Discharge Cap]
	, 'discharge' [State]
	--, SUM(Discharge_Capacity) OVER (ORDER BY Data_Point) AS [Cumulative Discharge Cap]
from
	BeckerBoard.dbo.SPCell
where
	I < 0
	and Capacity != 0
	and CellName = @cellname  --'200113_b' 
) b
on a.CellName = b.Cellname
and a.ChargeCycle_Index = b.DischargeCycle_index
and a.[ChargeTime] = b.[DischargeTime]
order by isnull(ChargeTime, DischargeTime)
END
GO


