USE [BeckerBoard]
GO

/****** Object:  StoredProcedure [dbo].[getBBSPCell]    Script Date: 8/4/2020 13:25:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








CREATE procedure [dbo].[getBBSPCell] 
	@cellname  varchar(100)
as
--declare @cellname varchar(100)
--set @cellname = '190419_c'
-- exec [getBBSPCell]  '190419_c'
-- select top 100 * from BeckerBoard.dbo.SPCell
select -- top 10 
	SPCellID, cellName, 
	[time] as measureEndTime, 
	tempCelcius, 
	cycle + 1 as Cycle_index, 
	v as [voltage], 
	I as [current], 
	R as resistance, 
	capacity,
	energy, 
	steptime 
from BeckerBoard.dbo.SPCell
where cellname = @cellname
GO


