USE [BeckerBoard]
GO

/****** Object:  StoredProcedure [dbo].[getBBDqDvProfile]    Script Date: 8/4/2020 13:23:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE procedure [dbo].[getBBDqDvProfile]
	@cellname varchar(100) = 'none',
	@cycle_index int = 0
as
SET NOCOUNT ON
--declare @cellname varchar(100), @cycle_index int
--select @cellname='200116_b', @cycle_index=50
BEGIN
declare @Charge table (chargeID int primary key identity(1,1), SPCellID int, CellName varchar(100), Cycle_Index int, [Time] datetime
			, capacity float, V float, [State] varchar(10) )

declare @Discharge table (dischargeID int primary key identity(1,1), SPCellID int, CellName varchar(100), Cycle_Index int, [Time] datetime
			, capacity float, V float, [State] varchar(10) )


if @cycle_index != 0
	begin
	insert into @Charge ( SPCellID, CellName, Cycle_index, [Time], capacity, V, [State] )
	select SPCellID, CellName, Cycle, [Time], capacity, V, 'charge'
	from SPCell
	where I >= 0
	and cellname = @cellname
	and cycle = @cycle_index
	order by [Time]

	insert into @Discharge ( SPCellID, CellName, Cycle_index, [Time], capacity, V, [State] )
	select SPCellID, CellName, Cycle, [Time], capacity, V, 'discharge'
	from SPCell
	where I < 0
	and cellname = @cellname
	and cycle = @cycle_index
	order by [Time]

	--select * from @discharge

	select 
				--row_number () over ( order by isnull(cSPCellIDCharge, cSPCellIDDischarge)) IncrementalCntr
				isnull(cSPCellIDCharge, cSPCellIDDischarge) SPCellID
			
				--, isnull(ChargeCellName, DischargeCellName) as CellName

				, isnull(ChargeCycle_index, DischargeCycle_index) as Cycle_index
				, isnull(ChargeTime, DischargeTime) [Test Time]

				--, isnull([Delta Charge Capacity]/ [Delta Charge Voltage], [Delta Discharge Capacity]/[Delta Discharge Voltage]) DqDv

				, isnull(cCharge_Voltage, cDischarge_Voltage) [Voltage]
				--, case when isnull([Delta Charge Capacity]/ [Delta Charge Voltage], [Delta Discharge Capacity]/[Delta Discharge Voltage])  <= 0
				--	then isnull(cCharge_Voltage, cDischarge_Voltage) * -1
				--	else isnull(cCharge_Voltage, cDischarge_Voltage) end as BiVoltage

				, isnull(cCharge_Capacity, cdisCharge_Capacity) Capacity
			
				--, case when isnull([Delta Charge Capacity]/ [Delta Charge Voltage], [Delta Discharge Capacity]/[Delta Discharge Voltage])  < 0
				--	then isnull(cCharge_Capacity, cdisCharge_Capacity) * -1
				--	else isnull(cCharge_Capacity, cdisCharge_Capacity) end as BiCapacity

				, isnull( [Delta Charge Capacity], [Delta Discharge Capacity]) DeltaCapacity
				, isnull([Delta Charge Voltage], [Delta disCharge Voltage]) DeltaVoltage

				, [Delta Charge Capacity]/ CASE WHEN [Delta Charge Voltage] = 0 THEN 1 else [Delta Charge Voltage] END [dqdv Charge]
				, [Delta Discharge Capacity]/[Delta Discharge Voltage] [dqdv Discharge]

				, ChargeTime [Charge Time]
				, cCharge_Capacity as [Charge Capacity] 
				, cCharge_Voltage as [Charge V] 

				, [Delta Charge Capacity]
				, [Delta Charge Voltage]
				, cDischarge_Capacity as [Discharge Capacity] 
				, cDischarge_Voltage [Discharge V]

				, [Delta Discharge Capacity]
				, [Delta Discharge Voltage]
				, isnull(a.[State], b.[State]) as [State]
	from (
	select	cn.chargeID
			, cn.SPCellID as cSPCellIDCharge, 
			pr.SPCellID as pSPCellIDCharge,
			cn.CellName ChargeCellName, 
			cn.Cycle_index + 1 ChargeCycle_index, 
			--substring(cast(cn.[Time] as varchar(20)), 0, 20) [ChargeTime],
			cn.[Time] ChargeTime,
			cn.capacity as cCharge_Capacity,
			pr.capacity as pCharge_Capacity,
			cn.V as cCharge_Voltage,
			pr.V as pCharge_Voltage,
			cn.capacity - pr.capacity AS [Delta Charge Capacity],
			cn.V - pr.V as [Delta Charge Voltage]
			, 'charge' [State]
			from @charge cn
			inner join @charge pr
			on pr.chargeID - 1 = cn.chargeID
			where cn.cellname = @CellName and pr.cellname = @CellName 
				and cn.Cycle_index = @Cycle_index and pr.Cycle_index = @Cycle_index

			) a full outer join (

			select 
				cn.dischargeID
				, cn.SPCellID as cSPCellIDDischarge, 
				pr.SPCellID as pSPCellIDDischarge,
				cn.CellName DischargeCellName, 
				cn.Cycle_index + 1 DischargeCycle_index, 
				--substring(cast(cn.[Time] as varchar(20)), 0, 20) [DischargeTime],
				cn.[Time] DischargeTime,
				cn.capacity as cDischarge_Capacity,
				pr.capacity as pDischarge_Capacity,
				cn.V as cDischarge_Voltage,
				pr.V as pDischarge_Voltage,
				cn.capacity - pr.capacity AS [Delta Discharge Capacity],
				cn.V - pr.V as [Delta Discharge Voltage]
				, 'discharge' [State]
			from @discharge cn
			inner join @discharge pr
			on pr.dischargeID - 1 = cn.dischargeID
			where cn.cellname = @CellName and pr.cellname = @CellName
				and cn.Cycle_index = @Cycle_index and pr.Cycle_index = @Cycle_index
			) b
			--on b.cSPCellIDDischarge = a.cSPCellIDcharge
			on b.dischargeID = a.chargeid
			and b.DischargeCycle_index = a.chargeCycle_index
			order by [Test Time]
		--) dqdv
	end

ELSE
--********************************************************************************
--********************************************************************************
--all cycles for cell
--********************************************************************************
--********************************************************************************

	BEGIN
	insert into @Charge ( SPCellID, CellName, Cycle_index, [Time], capacity, V, [State] )
	select SPCellID, CellName, Cycle, [Time], capacity, V, 'charge'
	from SPCell
	where I >= 0
	and cellname = @cellname
	order by [Time]

	insert into @Discharge ( SPCellID, CellName, Cycle_index, [Time], capacity, V, [State] )
	select SPCellID, CellName, Cycle, [Time], capacity, V, 'discharge'
	from SPCell
	where I < 0
	and cellname = @cellname
	order by [Time]

	--select * from @discharge

	select 
				--row_number () over ( order by isnull(cSPCellIDCharge, cSPCellIDDischarge)) IncrementalCntr
				isnull(cSPCellIDCharge, cSPCellIDDischarge) SPCellID
			
				--, isnull(ChargeCellName, DischargeCellName) as CellName

				, isnull(ChargeCycle_index, DischargeCycle_index) as Cycle_index
				, isnull(ChargeTime, DischargeTime) [Test Time]

				--, isnull([Delta Charge Capacity]/ [Delta Charge Voltage], [Delta Discharge Capacity]/[Delta Discharge Voltage]) DqDv

				, isnull(cCharge_Voltage, cDischarge_Voltage) [Voltage]
				--, case when isnull([Delta Charge Capacity]/ [Delta Charge Voltage], [Delta Discharge Capacity]/[Delta Discharge Voltage])  <= 0
				--	then isnull(cCharge_Voltage, cDischarge_Voltage) * -1
				--	else isnull(cCharge_Voltage, cDischarge_Voltage) end as BiVoltage

				, isnull(cCharge_Capacity, cdisCharge_Capacity) Capacity
			
				--, case when isnull([Delta Charge Capacity]/ [Delta Charge Voltage], [Delta Discharge Capacity]/[Delta Discharge Voltage])  < 0
				--	then isnull(cCharge_Capacity, cdisCharge_Capacity) * -1
				--	else isnull(cCharge_Capacity, cdisCharge_Capacity) end as BiCapacity

				, isnull( [Delta Charge Capacity], [Delta Discharge Capacity]) DeltaCapacity
				, isnull([Delta Charge Voltage], [Delta disCharge Voltage]) DeltaVoltage

				, [Delta Charge Capacity]/ CASE WHEN [Delta Charge Voltage] = 0 THEN 1 else [Delta Charge Voltage] END [dqdv Charge]
				, [Delta Discharge Capacity]/CASE WHEN [Delta Discharge Voltage] = 0 THEN 1 else [Delta Discharge Voltage] END [dqdv Discharge]

				, ChargeTime [Charge Time]
				, cCharge_Capacity as [Charge Capacity] 
				, cCharge_Voltage as [Charge V] 

				, [Delta Charge Capacity]
				, [Delta Charge Voltage]
				, cDischarge_Capacity as [Discharge Capacity] 
				, cDischarge_Voltage [Discharge V]

				, [Delta Discharge Capacity]
				, [Delta Discharge Voltage]
				, isnull(a.[State], b.[State]) as [State]
	from (
	select	cn.chargeID
			, cn.SPCellID as cSPCellIDCharge, 
			pr.SPCellID as pSPCellIDCharge,
			cn.CellName ChargeCellName, 
			cn.Cycle_index + 1 ChargeCycle_index, 
			--substring(cast(cn.[Time] as varchar(20)), 0, 20) [ChargeTime],
			cn.[Time] ChargeTime,
			cn.capacity as cCharge_Capacity,
			pr.capacity as pCharge_Capacity,
			cn.V as cCharge_Voltage,
			pr.V as pCharge_Voltage,
			cn.capacity - pr.capacity AS [Delta Charge Capacity],
			cn.V - pr.V as [Delta Charge Voltage]
			, 'charge' [State]
			from @charge cn
			inner join @charge pr
			on pr.chargeID - 1 = cn.chargeID
			where cn.cellname = @CellName and pr.cellname = @CellName 

			) a full outer join (

			select 
				cn.dischargeID
				, cn.SPCellID as cSPCellIDDischarge, 
				pr.SPCellID as pSPCellIDDischarge,
				cn.CellName DischargeCellName, 
				cn.Cycle_index + 1 DischargeCycle_index, 
				--substring(cast(cn.[Time] as varchar(20)), 0, 20) [DischargeTime],
				cn.[Time] DischargeTime,
				cn.capacity as cDischarge_Capacity,
				pr.capacity as pDischarge_Capacity,
				cn.V as cDischarge_Voltage,
				pr.V as pDischarge_Voltage,
				cn.capacity - pr.capacity AS [Delta Discharge Capacity],
				cn.V - pr.V as [Delta Discharge Voltage]
				, 'discharge' [State]
			from @discharge cn
			inner join @discharge pr
			on pr.dischargeID - 1 = cn.dischargeID
			where cn.cellname = @CellName and pr.cellname = @CellName

			) b
			--on b.cSPCellIDDischarge = a.cSPCellIDcharge
			on b.dischargeID = a.chargeid

			order by cycle_index, [Test Time]
		--) dqdv
	end
END
GO


