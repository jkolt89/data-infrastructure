USE [SP_ManuLine]
GO

/****** Object:  StoredProcedure [dbo].[insertBulkMaterialLog]    Script Date: 6/4/2021 16:39:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[insertBulkMaterialLog]
	@IngredientName		varchar(50)	= NULL
	,@IngredientLN		varchar(50) = NULL
	,@IngredientPN		varchar(50) = NULL
	,@Supplier			varchar(100) = NULL
	,@SupplierLN		varchar(50) = NULL
	,@DateReceived		smalldatetime = '1900/01/01'
	,@ContainerNr		int = NULL
	,@TotalWeight		float = NULL
	,@MeasurementType	varchar(10) = NULL
	,@DeviationNr		varchar(50) = NULL
	,@MRBNr				varchar(50) = NULL
	,@Notes				varchar(1000) = NULL
	,@UserOpID			int = NULL
AS
BEGIN

if @DeviationNr in ('N/A', 'na')
	set @DeviationNr = NULL

if @MRBNr in ('N/A', 'na')
	set @MRBNr = NULL

if @Notes in ('N/A', 'na')
	set @Notes = NULL

INSERT INTO [dbo].[BulkMaterialLog]
           (
		   [IngredientName]
           ,[IngredientLN]
           ,[IngredientPN]
           ,[Supplier]
           ,[SupplierLN]
           ,[DateReceived]
           ,[ContainerNr]
           ,[TotalWeight]
           ,[MeasurementType]
           ,[DeviationNr]
           ,[MRBNr]
           ,[Notes]
           ,UserOpID
		   )
     VALUES
			(
			@IngredientName
			,@IngredientLN
			,@IngredientPN
			,@Supplier
			,@SupplierLN
			,@DateReceived
			,@ContainerNr
			,@TotalWeight
			,@MeasurementType
			,@DeviationNr
			,@MRBNr
			,@Notes
			,@UserOpID
			)

END
GO


