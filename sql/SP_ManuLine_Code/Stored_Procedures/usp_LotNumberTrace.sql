USE [SP_ManuLine]
GO

/****** Object:  StoredProcedure [dbo].[usp_LotNumberTrace]    Script Date: 6/4/2021 16:41:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










CREATE PROCEDURE [dbo].[usp_LotNumberTrace]
	@ProductLN varchar(50)
AS
/*
	TRACES PRODUCTLN AND INGREDIENTLN THROUGH PRODUCTION TABLES
	USING WEIGHING.PRODUCTLN WEIGHING.INGREDIENTLN AND GET COMBINEDBATCHES LN. THEN SEARCH TABLES FOR THOSE VALUES
*/

-- exec [usp_LotNumberTrace] '34-2010-308-0'
-- exec [usp_LotNumberTrace] '32-2011-305-0'
-- exec [usp_LotNumberTrace] '34-2011-301-0'
-- exec [usp_LotNumberTrace] '34-2101-303-0'
-- exec [usp_LotNumberTrace] '34-2003-016'
-- exec [usp_LotNumberTrace] '34-2012-321-0'

DECLARE @weighingProductLN varchar(50), @debug bit

select @weighingProductLN = @ProductLN, @debug = 1

BEGIN
	SET NOCOUNT ON;

	declare @IngredientLN varchar(50), @CombinedLN varchar(50)

	declare @IngredientTableVar table (
		IngredientLN varchar(50)
		)

	select * from weighing where ProductLN = @weighingProductLN
	-- select IngredientLN from Weighing where ProductLN = @weighingProductLN

	-- CURSOR FOR COMBINEDBATCH RECORD
	select @CombinedLN = combinedlotnum from CombinedBatchRecord where originallotnum = @weighingProductLN
	select 'ProdConductivity' ProdConductivity, * from ProdConductivity where ProductLN = @CombinedLN
	-- print @CombinedLN
	
	
	--insert into @IngredientTableVar(IngredientLN) values @IngredientLN
	insert into @IngredientTableVar(IngredientLN) select distinct IngredientLN from weighing where ProductLN = @weighingProductLN

	select IngredientLN as WeighingIngredientLN from @IngredientTableVar
	
	-- Use @ProductLN for these tables
	EXEC sp_FindStringInTable @ProductLN , 'dbo', 'CombinedBatchRecord' 
	EXEC sp_FindStringInTable @ProductLN , 'dbo', 'ConsumedBatch' 
	-- EXEC sp_FindStringInTable @ProductLN , 'dbo', 'ProdConductivity'
	EXEC sp_FindStringInTable @ProductLN , 'dbo', 'MillSetup' 
	EXEC sp_FindStringInTable @ProductLN , 'dbo', 'Slurry' 

	declare IngredientLN_cur cursor for
		select distinct IngredientLN from weighing where ProductLN = @weighingProductLN

	open IngredientLN_cur

	fetch next from IngredientLN_cur into @IngredientLN

    WHILE @@FETCH_STATUS = 0  
		BEGIN  
			if @debug = 1
				select @weighingProductLN as ProductLN, @CombinedLN as CombinedLN, @IngredientLN as IngredientLN
			
			--EXEC [dbo].[usp_SearchForLotOrProductNumber] @IngredientLN
			EXEC sp_FindStringInTable @IngredientLN , 'dbo', 'BulkMaterialLog'
			EXEC sp_FindStringInTable @IngredientLN , 'dbo', 'Drying' 
			--EXEC sp_FindStringInTable @input, 'dbo', 'LotNumberList'
			EXEC sp_FindStringInTable @IngredientLN , 'dbo', 'Precursor'
			EXEC sp_FindStringInTable @IngredientLN , 'dbo', 'Solvent'
			EXEC sp_FindStringInTable @IngredientLN , 'dbo', 'SolventDrying'
			EXEC sp_FindStringInTable @IngredientLN , 'dbo', 'Weighing'

			/*
			-- Use @ProductLN for these tables
			EXEC sp_FindStringInTable @ProductLN , 'dbo', 'CombinedBatchRecord' 
			EXEC sp_FindStringInTable @ProductLN , 'dbo', 'ConsumedBatch' 
			EXEC sp_FindStringInTable @ProductLN , 'dbo', 'ProdConductivity'
			EXEC sp_FindStringInTable @ProductLN , 'dbo', 'MillSetup' 
			EXEC sp_FindStringInTable @ProductLN , 'dbo', 'Slurry' 
			*/

			fetch next from IngredientLN_cur into @IngredientLN
		END

	CLOSE IngredientLN_cur;  
	DEALLOCATE IngredientLN_cur;  

END
GO


