USE [SP_ManuLine]
GO

/****** Object:  StoredProcedure [dbo].[getLotNumberWithSplitLetter]    Script Date: 6/4/2021 16:39:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--use SP_ManuLine
--go


CREATE procedure [dbo].[getLotNumberWithSplitLetter]
	@LotNumber varchar(50), 
	@SplitLetter varchar(1), 
	@EnteredBy int
as
-- select * from LotNumberList
--declare @LotNumber varchar(50), @SplitLetter varchar(1), @EnteredBy int
--select @LotNumber = '32-2007-201-0', @SplitLetter = 'A', @EnteredBy = 54
-------------------------------------------------------------------------
BEGIN
	DECLARE @ProcLoc varchar(10), 
		@Yr int, 
		@Mnth int, 
		@GenNr int, 
		@SeqNr int, 
		@LotText varchar(50), 
		@NewLotNr varchar(50), 
		@BaseLotNr varchar(50)

	SELECT @LotText = @LotNumber
	SELECT @BaseLotNr = substring(@LotNumber, 0, 12)

	SELECT @ProcLoc = substring( @LotText, 0, CHARINDEX('-', @LotNumber))

	SELECT @LotText = substring( @LotText, CHARINDEX('-', @LotNumber)+1, len(@LotText))

	SELECT @yr = substring(@LotText, 0, 3)

	SELECT @LotText = substring(@LotText, 3, len(@LotText))

	SELECT @Mnth = substring(@LotText, 0, 3)

	SELECT @LotText = substring(@LotText, 4, len(@LotText))

	SELECT @GenNr = substring(@LotText, 0, 2)

	SELECT @LotText = substring(@LotText, 2, len(@LotText))

	SELECT @SeqNr = substring(@LotText, 0, 3)

	SELECT @LotText = substring(@LotText, 4, len(@LotText))

	SELECT @NewLotNr = reverse(@SplitLetter + substring(reverse(@LotNumber), 2, len(@LotNumber)))

	IF NOT EXISTS ( SELECT 1 FROM dbo.LotNumberList WHERE BaseLotNumber = @BaseLotNr)
		BEGIN
		-- CAN IMPLEMENT RAISEERROR TO CREATE A DB ERROR ALSO
		PRINT ('Base Lot Number does not exist.')
		RETURN
		END
	ELSE
		BEGIN
		--PRINT 'TODO: inserts lot number with new split letter, other data columns'
		INSERT INTO LotNumberList(LotNumber, BaseLotNumber, ProcessLoc, Yr, Mnth, GenerationNr, SequenceNr, SplitLetter, EnteredBy)
		VALUES (@NewLotNr, @BaseLotNr, @ProcLoc, @Yr, @Mnth, @GenNr, @SeqNr, @SplitLetter, @EnteredBy)
		END

	select @NewLotNr

--PRINT ''
--PRINT 'BaseLotNr:  ' + trim(@BaseLotNr)
--PRINT 'ProcLoc:  ' + trim(str(@ProcLoc))
--PRINT 'Yr:  ' + trim(str(@yr))
--PRINT 'Mnth:  ' + trim(str(@Mnth))
--PRINT 'GenNr:  ' + trim(str(@GenNr))
--PRINT 'SeqNr:  ' + trim(str(@SeqNr))
--PRINT 'NewLotNr:  ' + @NewLotNr
--PRINT @LotText
END
GO


