USE [SP_ManuLine]
GO

/****** Object:  StoredProcedure [dbo].[getLotNumber]    Script Date: 6/4/2021 16:38:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
























CREATE PROCEDURE [dbo].[getLotNumber]
	@LotNrGenLocation VARCHAR(2) = NULL
	, @SplitLetter VARCHAR(10) = 0
	, @GenNr INT = 0
	, @EnteredBy VARCHAR(50) = '999'
	, @LotNumber VARCHAR(50) = NULL
	, @PartNumber VARCHAR(50) = NULL
AS
-- exec [getLotNumber] 18, 0
-- exec [getLotNumber] @LotNrGenLocation=12, @SplitLetter=0, @GenNr=0, @EnteredBy=54
-- exec [getLotNumber] @LotNumber = '12-2102-001-0', @PartNumber = 'testtest'
-- exec [getLotNumber] 18, 'a'
-- exec [getLotNumber] 18, 'B'
-- exec [getLotNumber] 18, 0, 1
-- exec [getLotNumber] 18, 'B', 1
-- exec [getLotNumber] 18, 'c', 1, 133
-- delete from [dbo].[LotNumberList] where EnteredBy = 54 -- where baselotnumber is null --where LNID > 1
-- select * from [LotNumberList]
/*
Program to automatically generate and return lot numbers based off of 2 input parameters (ProcessLoc, SplitLetter) when called from python
RULES:
	OLD RULE: when splitletter changes THEN sequence nr continues to increment
	REQUEST from app - when the application needs to pass additional parameter of which it is pulled from 
	when Generation Nr changes THEN sequence starts from 1 again
RULES:
	CHECK if this is a new lot number and if so then split letter must be 0
	ask - add check constraint to SplitLetter in LotNumberList Table to only allow 0, A-G?
	add try...catch rules for errors
	check EnteredBy: if 3 char long then check if 1st char = 0 if yes then remove leading 0 ie 050 becomes 50
*/
BEGIN

	SET NOCOUNT ON;

	declare @MaxSeqNr int, @NewLotNr varchar(50), @ProcLoc varchar(2)

	select @ProcLoc = upper(@LotNrGenLocation)

	-- Check if lot number already exists in LotNumberList table. Return message and end program.
	--if exists (select 1 from LotNumberList where LotNumber = @LotNumber)
	--	begin
	--	select @LotNumber + ' exists.'
	--	return
	--	end

	IF @PartNumber IS NOT NULL AND @LotNumber IS NOT NULL
	BEGIN
		-- PRINT 1
		if exists (  select 1 from SP_ManuLine.[dbo].LotNumberList where LotNumber = @LotNumber )
			UPDATE DBO.LotNumberList set PartNumber = @PartNumber where LotNumber = @LotNumber
		else
			select 'Lot Number does not exist.'
	END
	ELSE
	BEGIN
		------ERROR CHECKING ROUTINE------------------
		IF NOT EXISTS ( SELECT 1 FROM SP_ManuLine.[dbo].[LotNrGenerationLocation] WHERE [lnlgID] = @ProcLoc )
			BEGIN
			SELECT @NewLotNr = 'This Process Location does not exist.'
			SELECT @NewLotNr
			RETURN 
			END

		IF @SplitLetter in (']', '[', ')', '(', '*', '&', '^', '%', '$', '#', '@', '!', '~', ' ' )
			BEGIN
			SELECT @NewLotNr = 'An invalid Split Letter character/number has been entered.'
			SELECT @NewLotNr
			RETURN
			END

		IF LEN(@SplitLetter) != 1
			BEGIN
			SELECT @NewLotNr = 'Split letter can only be 1 character long.'
			SELECT @NewLotNr
			RETURN
			END

		IF NOT EXISTS ( SELECT 1 FROM SP_ManuLine.[dbo].UserOp WHERE [UserOpID] = @EnteredBy )
			BEGIN
			SELECT @NewLotNr = 'This User Operator ID does not exist.'
			SELECT @NewLotNr
			RETURN --@NewLotNr
			END
		------END OF ERROR CHECKING ROUTINE------------------

		SELECT @EnteredBy = CASE
			WHEN LEN(@EnteredBy) = 3 and SUBSTRING(@EnteredBy, 1, 1) = '0'
				THEN SUBSTRING(@EnteredBy, 2, 3)
			ELSE @EnteredBy
		END

		-- MAX SEQUENCE NUMBER
		SELECT @MaxSeqNr = isnull(max(SequenceNr), 0) + 1
			FROM [dbo].[LotNumberList]
			WHERE ProcessLoc = @ProcLoc
			--and SplitLetter = @SplitLetter
			AND Mnth = MONTH(GETDATE())
			AND GenerationNr = @GenNr

		--if len(@GenNr) >= 3
			--select @MaxSeqNr = @GenNr

		INSERT INTO LotNumberList(LotNumber, ProcessLoc, Yr, Mnth, GenerationNr, SequenceNr, SplitLetter, EnteredBy)
		VALUES      ( 'xyzxyzxyz'
					  , @ProcLoc
					  , SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 3, 5)
					  , MONTH(GETDATE())
					  , @GenNr
					  , @MaxSeqNr
					  , UPPER(@SplitLetter)
					  , @EnteredBy ) 

		SELECT TOP 1 @NewLotNr = 
			CAST(ProcessLoc AS VARCHAR(3)) + '-' 
			+ CAST( yr AS VARCHAR(2))
			--+ '-'
			+ CASE WHEN LEN(CAST(mnth AS VARCHAR(2))) = 1
				THEN '0' + CAST( Mnth AS VARCHAR(2))
				WHEN LEN(CAST(mnth AS VARCHAR(2))) = 2
				THEN CAST( Mnth AS VARCHAR(2))
			END
			+ '-'
			+ CASE 
				WHEN LEN(CAST([SequenceNr] AS VARCHAR(10))) = 1
					THEN CAST(@GenNr AS VARCHAR(5))  + '0' + CAST([SequenceNr]   AS VARCHAR(10))
				WHEN LEN([SequenceNr]) = 2
					THEN CAST(@GenNr AS VARCHAR(5)) + CAST([SequenceNr]  AS VARCHAR(10))
				WHEN LEN([SequenceNr]) = 3
					THEN CAST([SequenceNr]  as VARCHAR(10))
				END
			+ '-'
			+ CAST(SplitLetter AS VARCHAR(5))
			FROM [dbo].[LotNumberList]
			WHERE LotNumber = 'xyzxyzxyz'
			ORDER BY LNID DESC

		UPDATE [dbo].[LotNumberList] SET [LotNumber] = @NewLotNr
			WHERE LotNumber = 'xyzxyzxyz'

		UPDATE dbo.LotNumberList SET BaseLotNumber = SUBSTRING(@NewLotNr, 0, 12) WHERE BaseLotNumber IS NULL

		-- This is needed until the LotNumber column is removed from table
		UPDATE dbo.LotNumberList SET GeneralLotNumber = LotNumber WHERE GeneralLotNumber IS NULL

		SELECT @NewLotNr
	END
END
GO


