USE [SP_ManuLine]
GO

/****** Object:  StoredProcedure [dbo].[getCoatingDim]    Script Date: 6/4/2021 16:37:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[getCoatingDim]
	@CoatingDimID int
AS
BEGIN
	SET NOCOUNT ON;

	select * from dbo.CoatingDim where CoatingDimID = @CoatingDimID
END
GO


