USE [SP_ManuLine]
GO

/****** Object:  StoredProcedure [dbo].[insertSlurryQualityMetrics]    Script Date: 6/4/2021 16:41:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[insertSlurryQualityMetrics]
			@ProductLN varchar(50) = null
           ,@ProductPN varchar(50)  = null
           ,@Viscosity float = 0
           ,@ParticleSize  float = 0.0
           ,@PowerLawNr float = 0.0
           ,@PercentSolids  float = 0.0
           ,@Notes varchar(2000) = null
           ,@UserOpID int = null
           --,@EntryDate

as

INSERT INTO [dbo].[SlurryQualityMetric]
           (
		   [ProductLN]
           ,[ProductPN]
           ,[Viscosity]
           ,[ParticleSize]
           ,[PowerLawNr]
           ,[PercentSolids]
           ,[Notes]
           ,[UserOpID]
           --,[EntryDate]
		   )
     VALUES (
			@ProductLN
           ,@ProductPN
           ,@Viscosity
           ,@ParticleSize
           ,@PowerLawNr
           ,@PercentSolids
           ,@Notes
           ,@UserOpID
           --,@EntryDate
		   )
GO


