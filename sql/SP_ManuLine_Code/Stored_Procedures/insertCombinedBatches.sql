USE [SP_ManuLine]
GO

/****** Object:  StoredProcedure [dbo].[insertCombinedBatches]    Script Date: 6/4/2021 16:39:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO














CREATE procedure [dbo].[insertCombinedBatches]
	@OriginalLotNr varchar(30) = NULL -- = 0
	, @OriginalLotNr2 varchar(30) = NULL
	, @OriginalLotNr3 varchar(30) = NULL
	, @OriginalLotNr4 varchar(30) = NULL
	, @OriginalLotNr5 varchar(30) = NULL
	, @OriginalLotNr6 varchar(30) = NULL
	, @OriginalLotNr7 varchar(30) = NULL
	, @OriginalLotNr8 varchar(30) = NULL
	, @OriginalLotNr9 varchar(30) = NULL
	, @OriginalLotNr10 varchar(30) = NULL
	, @CombinedLotNr varchar(30) = 'DEL_ME'
	, @StartWeight float = 0
	, @StartWeight2 float = 0
	, @StartWeight3 float = 0
	, @StartWeight4 float = 0
	, @StartWeight5 float = 0
	, @StartWeight6 float = 0
	, @StartWeight7 float = 0
	, @StartWeight8 float = 0
	, @StartWeight9 float = 0
	, @StartWeight10 float = 0
	, @UserOpID int = 999
as

-- exec insertCombinedBatches '1313', '222', 254, 54
-- exec insertCombinedBatches '12-2007-078-x', 'CB-2007-002-0', 989, 54
-- exec insertCombinedBatches '12-2007-078-Y', 'CB-2007-002-0', 54
-- exec insertCombinedBatches '12-2007-078-Y', 'CB-207-002-0', 54
-- exec insertCombinedBatches @CombinedLotNr='CB-2007-002-0'
-- exec insertCombinedBatches '12-2007-078-A'
-- select * from CombinedBatchrecord
-- delete from CombinedBatchrecord where useropid = 54
-- select CombinedLotNum, sum(StartWeight) CombinedStartWeight from CombinedBatchRecord group by combinedlotnum
BEGIN
SET NOCOUNT ON

	-- select @OriginalLotNr1 = upper(@OriginalLotNr1)
	select @CombinedLotNr = upper(@CombinedLotNr)


	-- CHECKS TO VERIFY THAT THE ORIGINAL LOT NUMBER FORMAT IS CORRECT
	--if (len(@OriginalLotNr) != 13 and (@OriginalLotNr is not null))  
	--	or (len(@OriginalLotNr2) != 13 and (@OriginalLotNr2 is not null))  
	--	or (len(@OriginalLotNr3) != 13 and (@OriginalLotNr3 is not null))  
	--	or (len(@OriginalLotNr4) != 13 and (@OriginalLotNr4 is not null))  
	--	or (len(@OriginalLotNr5) != 13 and (@OriginalLotNr5 is not null))  
	--	or (len(@OriginalLotNr6) != 13 and (@OriginalLotNr6 is not null))  
	--	or (len(@OriginalLotNr7) != 13 and (@OriginalLotNr7 is not null))  
	--	or (len(@OriginalLotNr8) != 13 and (@OriginalLotNr8 is not null))  
	--	or (len(@OriginalLotNr9) != 13 and (@OriginalLotNr9 is not null))  
	--	or (len(@OriginalLotNr10) != 13 and (@OriginalLotNr10 is not null))  
	--	begin
	--		select 'Lot Number is incorrect'
	--		return
	--	end

	-- CHECK TO VALIDATE CORRECTLY FORMATTED COMBINED LOT NUMBER HAS BEEN ENTERED FROM DASH APP
	if len(@CombinedLotNr) != 13
		begin
		select 'Combined Lot Number is not in correct format.'
		return
		end

	if not exists (select 1 from SP_ManuLine.dbo.CombinedBatchRecord WHERE OriginalLotNum = @OriginalLotNr) and @OriginalLotNr is not null
		BEGIN
		insert into SP_ManuLine.dbo.CombinedBatchRecord([OriginalLotNum], [CombinedLotNum], [StartWeight], [UserOpID])
			values ( upper(@OriginalLotNr), @CombinedLotNr, @StartWeight, @UserOpID)
		END
	else
		BEGIN
		PRINT 'Combined Lot Number Batch record already exists.'
		END

		if not exists (select 1 from SP_ManuLine.dbo.CombinedBatchRecord WHERE OriginalLotNum = @OriginalLotNr2) and @OriginalLotNr2 is not null
		BEGIN
			insert into SP_ManuLine.dbo.CombinedBatchRecord([OriginalLotNum], [CombinedLotNum], [StartWeight], [UserOpID])
				values ( upper(@OriginalLotNr2), @CombinedLotNr, @StartWeight2, @UserOpID)
		END

		
		if not exists (select 1 from SP_ManuLine.dbo.CombinedBatchRecord WHERE OriginalLotNum = @OriginalLotNr3) and @OriginalLotNr3 is not null
		BEGIN
			insert into SP_ManuLine.dbo.CombinedBatchRecord([OriginalLotNum], [CombinedLotNum], [StartWeight], [UserOpID])
				values ( upper(@OriginalLotNr3), @CombinedLotNr, @StartWeight3, @UserOpID)
		END

		if not exists (select 1 from SP_ManuLine.dbo.CombinedBatchRecord WHERE OriginalLotNum = @OriginalLotNr4) and @OriginalLotNr4 is not null
		BEGIN
			insert into SP_ManuLine.dbo.CombinedBatchRecord([OriginalLotNum], [CombinedLotNum], [StartWeight], [UserOpID])
				values ( upper(@OriginalLotNr4), @CombinedLotNr, @StartWeight4, @UserOpID)
		END

		if not exists (select 1 from SP_ManuLine.dbo.CombinedBatchRecord WHERE OriginalLotNum = @OriginalLotNr5) and @OriginalLotNr5 is not null
		BEGIN
			insert into SP_ManuLine.dbo.CombinedBatchRecord([OriginalLotNum], [CombinedLotNum], [StartWeight], [UserOpID])
				values ( upper(@OriginalLotNr5), @CombinedLotNr, @StartWeight5, @UserOpID)
		END

		if not exists (select 1 from SP_ManuLine.dbo.CombinedBatchRecord WHERE OriginalLotNum = @OriginalLotNr6) and @OriginalLotNr6 is not null
		BEGIN
			insert into SP_ManuLine.dbo.CombinedBatchRecord([OriginalLotNum], [CombinedLotNum], [StartWeight], [UserOpID])
				values ( upper(@OriginalLotNr6), @CombinedLotNr, @StartWeight6, @UserOpID)
		END

		
		if not exists (select 1 from SP_ManuLine.dbo.CombinedBatchRecord WHERE OriginalLotNum = @OriginalLotNr7) and @OriginalLotNr7 is not null
		BEGIN
			insert into SP_ManuLine.dbo.CombinedBatchRecord([OriginalLotNum], [CombinedLotNum], [StartWeight], [UserOpID])
				values ( upper(@OriginalLotNr7), @CombinedLotNr, @StartWeight7, @UserOpID)
		END

		
		if not exists (select 1 from SP_ManuLine.dbo.CombinedBatchRecord WHERE OriginalLotNum = @OriginalLotNr8) and @OriginalLotNr8 is not null
		BEGIN
			insert into SP_ManuLine.dbo.CombinedBatchRecord([OriginalLotNum], [CombinedLotNum], [StartWeight], [UserOpID])
				values ( upper(@OriginalLotNr8), @CombinedLotNr, @StartWeight8, @UserOpID)
		END

		
		if not exists (select 1 from SP_ManuLine.dbo.CombinedBatchRecord WHERE OriginalLotNum = @OriginalLotNr9) and @OriginalLotNr9 is not null
		BEGIN
			insert into SP_ManuLine.dbo.CombinedBatchRecord([OriginalLotNum], [CombinedLotNum], [StartWeight], [UserOpID])
				values ( upper(@OriginalLotNr9), @CombinedLotNr, @StartWeight9, @UserOpID)
		END

		
		if not exists (select 1 from SP_ManuLine.dbo.CombinedBatchRecord WHERE OriginalLotNum = @OriginalLotNr10) and @OriginalLotNr10 is not null
		BEGIN
			insert into SP_ManuLine.dbo.CombinedBatchRecord([OriginalLotNum], [CombinedLotNum], [StartWeight], [UserOpID])
				values ( upper(@OriginalLotNr10), @CombinedLotNr, @StartWeight10, @UserOpID)
		END

	SELECT distinct
		[OriginalLotNum]
	  FROM [dbo].[CombinedBatchRecord]
	  where CombinedLotNum = @CombinedLotNr
	  and OriginalLotNum != '0'

END
GO


