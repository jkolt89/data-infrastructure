USE [SP_ManuLine]
GO

/****** Object:  StoredProcedure [dbo].[insertConsumedBatch]    Script Date: 6/4/2021 16:39:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO














CREATE PROCEDURE [dbo].[insertConsumedBatch]
	@ProductLN varchar(50),
	@startdate varchar(50) = null,
	@finishdate varchar(50) = null,
	@Quantity_g varchar(30) = null,
	@MRB varchar(10) = null,
	@Conductivity_mScm varchar(30) = null,
	@Yield_g varchar(15) = null,
	@PassFail varchar(20) = null,
	@Slurry varchar(200) = null,
	@PSd90 decimal(18, 2) = null,
	@PSd50 decimal(18, 2) = null,
	@PSd10 decimal(18, 2) = null,
	@SSAm2g decimal(18, 2) = null,
	@RGBRed int = null,
	@RGBGreen int = null,
	@RGBBlue int = null,
	@ElectronicConductivity_mScm varchar(10) = null,
	@GuseGen varchar(10) = null,
	@UserOpID int
AS
--exec insertConsumedBatch @ProductLN='1111', @startdate='20200101', @finishdate='20200102', @Quantity_g=11, @MRB=2, @Conductivity_mScm=12, @Yield_g=232, @ElectronicConductivity_mScm=11, @GuseGen=1, @UserOpID=99, @PassFail='Pass'
-- exec dbo.insertConsumedBatch @UserOpID = '99', @ProductLN = 'CG-2103-112-0',@startdate = '2021-04-22 00:00:00', @finishdate = '2021-05-09 00:00:00', @Quantity_g = '2000', @MRB = 'None', @Conductivity_mScm = '2.34', @Yield_g = '1811', @PassFail = 'Pass', @ElectronicConductivity_mScm = '2.5', @GuseGen = '1'
-- exec insertConsumedBatch @lotnr='1111', @startdate='20200110'
-- select * from [dbo].[ConsumedBatch] --where lotnr = '1111'
-- select * into ConsumedBatch_BAK from ConsumedBatch
-- delete from [dbo].[ConsumedBatch] where lotnr = '1111'

/*
if lotnr doesn't exist in table INSERT data.
if lotnr exists in table UPDATE data
*/

DECLARE 
	@lotnr varchar(50), 
	@debug int

SELECT 
	@lotnr = @ProductLN, 
	@debug = 0

if @debug = 1 print 'start'

BEGIN

	SET NOCOUNT ON;
	
	if @PassFail = 'pass'
		select @PassFail = 1
	else select @PassFail = @PassFail

	if @PassFail = 'fail'
		select @PassFail = 0
	else select @PassFail = @PassFail

	if @MRB = 'None'
		select @MRB = NULL

	if @Yield_g = 'None'
		select @Yield_g = NULL
	
	if @debug = 1
		begin
		print('MRB: ' + @MRB)
		print('PASSFAIL:' + @PassFail)
		end

	if @ElectronicConductivity_mScm = 'None'
		select @ElectronicConductivity_mScm = NULL

	if @GuseGen = 'None'
		select @GuseGen = NULL

	if @Conductivity_mScm = 'None'
		select @Conductivity_mScm = NULL

	if @Quantity_g = 'None'
		select @Quantity_g = NULL

	if @startdate is not null
		select @startdate = cast(@startdate as smalldatetime)

	if @finishdate is not null
	select @finishdate = cast(@finishdate as smalldatetime)

	if not exists (select 1 from dbo.ConsumedBatch where ProductLN = @ProductLN)
		BEGIN
		if @debug = 1 print 'INSERT'
		INSERT INTO [dbo].[ConsumedBatch]
			   ([LotNr]
			   ,ProductLN
			   ,[StartDate]
			   ,[FinishDate]
			   ,[Quantity_g]
			   ,[MRB]
			   ,[Conductivity_mScm]
			   ,[Yield_g]
			   ,[PassFail]
			   ,[Slurry]
			   ,[PSd90]
			   ,[PSd50]
			   ,[PSd10]
			   ,[SSAm2g]
			   ,[RGBRed]
			   ,[RGBGreen]
			   ,[RGBBlue]
			   ,[ElectronicConductivity_SCm] 
			   ,[GuseGen]
			   ,UserOpID
			   )
		 VALUES
			   (
			   @lotnr
			   ,@ProductLN
			   , @StartDate
			   , @FinishDate
			   , @Quantity_g
			   , @MRB
			   , @Conductivity_mScm
			   , @Yield_g
			   , @PassFail
			   ,@Slurry
			   ,@PSd90
				,@PSd50 
				,@PSd10 
				,@SSAm2g 
				,@RGBRed 
				,@RGBGreen
				,@RGBBlue 
			   , @ElectronicConductivity_mScm
			   , @GuseGen
			   , @UserOpID
			   )
		END
	ELSE 
		BEGIN
		if @debug = 1 PRINT 'UPDATE'

		if @startdate is not null AND @startdate != ''
			update dbo.ConsumedBatch set StartDate = @startdate
			where LotNr = @lotnr

		if @finishdate is not null AND @finishdate != ''
			update dbo.ConsumedBatch set FinishDate = @finishdate
			where LotNr = @lotnr

		if @Quantity_g is not null AND @Quantity_g != ''
			update dbo.ConsumedBatch set Quantity_g = @Quantity_g
			where LotNr = @lotnr

		if @MRB is not null AND @MRB != ''
			update dbo.ConsumedBatch set MRB = @MRB
			where LotNr = @lotnr

		if @Conductivity_mScm is not null AND @Conductivity_mScm != ''
			update dbo.ConsumedBatch set Conductivity_mScm = @Conductivity_mScm
			where LotNr = @lotnr

		if @Yield_g is not null AND @Yield_g != ''
			update dbo.ConsumedBatch set Yield_g = @Yield_g
			where LotNr = @lotnr

		--if @PassFail is not null AND @PassFail != ''
			--update dbo.ConsumedBatch set PassFail = @PassFail
		--if @PassFail = 'Fail' and @PassFail is not null
		if @PassFail = 'Fail' or @PassFail = 0
			update dbo.ConsumedBatch set PassFail = 0
				where LotNr = @lotnr
		
		if @PassFail = 'Pass'  or @PassFail = 1
			update dbo.ConsumedBatch set PassFail = 1
				where LotNr = @lotnr
		
		if @Slurry is not null AND @Slurry  != ''
			update dbo.ConsumedBatch set Slurry = @Slurry
			where LotNr = @lotnr

		if @PSd90  is not null AND @PSd90  != ''
			update dbo.ConsumedBatch set PSd90 = @PSd90
			where LotNr = @lotnr

		if @PSd10  is not null AND @PSd10  != ''
			update dbo.ConsumedBatch set PSd10 = @PSd10
			where LotNr = @lotnr

		if @PSd50  is not null AND @PSd50  != ''
			update dbo.ConsumedBatch set PSd50  = @PSd50 
			where LotNr = @lotnr

		if @SSAm2g is not null AND @SSAm2g != ''
			update dbo.ConsumedBatch set SSAm2g = @SSAm2g
			where LotNr = @lotnr

		if @RGBRed is not null AND @RGBRed != ''
			update dbo.ConsumedBatch set RGBRed = @RGBRed
			where LotNr = @lotnr

		if @RGBGreen is not null AND @RGBGreen != ''
			update dbo.ConsumedBatch set RGBGreen = @RGBGreen
			where LotNr = @lotnr

		if @RGBBlue is not null AND @RGBBlue != ''
			update dbo.ConsumedBatch set RGBBlue = @RGBBlue
			where LotNr = @lotnr

		if @ElectronicConductivity_mScm is not null AND @ElectronicConductivity_mScm != ''
			update dbo.ConsumedBatch set ElectronicConductivity_SCm = @ElectronicConductivity_mScm
			where LotNr = @lotnr
		
		if @GuseGen is not null AND @GuseGen != ''
			update dbo.ConsumedBatch set GuseGen = @GuseGen
			where LotNr = @lotnr
		
		if @UserOpID is not null AND @UserOpID != ''
			update dbo.ConsumedBatch set UserOpID = @UserOpID
			where LotNr = @lotnr

		END

END
GO


