USE [SP_ManuLine]
GO

/****** Object:  StoredProcedure [dbo].[getCoatingFact]    Script Date: 6/4/2021 16:38:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[getCoatingFact]
	@CoatingDimID int
AS
-- getCoatingFact 1
BEGIN
	SET NOCOUNT ON;

	SELECT X_Points, Y_Points from dbo.CoatingFact where CoatingDimID = @CoatingDimID
END
GO


