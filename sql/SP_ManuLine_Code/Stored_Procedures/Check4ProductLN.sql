USE [SP_ManuLine]
GO

/****** Object:  StoredProcedure [dbo].[Check4ProductLN]    Script Date: 6/4/2021 16:36:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[Check4ProductLN]
	@ProductLN varchar(50)
as

-- exec Check4ProductLN '66-2104-005-0'
-- exec Check4ProductLN '66-9804-005-0'
-- select * from LotNumberList

if exists ( select 1 from SP_ManuLine.dbo.LotNumberList where GeneralLotNumber = @ProductLN )
	begin
		select 'True'
	end
else
	begin
		select 'False'
	end
GO


