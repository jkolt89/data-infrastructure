USE [SP_ManuLine]
GO

/****** Object:  StoredProcedure [dbo].[usp_WeighingMultiInput]    Script Date: 6/4/2021 16:42:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO













CREATE PROCEDURE [dbo].[usp_WeighingMultiInput]
	@ProductLN varchar(50),
	@WeighEquipment varchar(50) = 'Dash App',
	@IngredientLN1 varchar(50) = NULL,
	@IngredientPN1 varchar(50) = NULL,
	@IngredientMass1 float = NULL,
	@IngredientActual1 float = NULL,
	@UserOpID int = NULL,

	@IngredientLN2 varchar(50) = NULL,
	@IngredientPN2 varchar(50) = NULL,
	@IngredientMass2 float = NULL,
	@IngredientActual2 float = NULL,
	--@UserOpID2 int = NULL,

	@IngredientLN3 varchar(50) = NULL,
	@IngredientPN3 varchar(50) = NULL,
	@IngredientMass3 float = NULL,
	@IngredientActual3 float = NULL,
	--@UserOpID3 int = NULL,

	@IngredientLN4 varchar(50) = NULL,
	@IngredientPN4 varchar(50) = NULL,
	@IngredientMass4 float = NULL,
	@IngredientActual4 float = NULL,
	--@UserOpID4 int = NULL,

	@IngredientLN5 varchar(50) = NULL,
	@IngredientPN5 varchar(50) = NULL,
	@IngredientMass5 float = NULL,
	@IngredientActual5 float = NULL,
	--@UserOpID5 int = NULL,

	@IngredientLN6 varchar(50) = NULL,
	@IngredientPN6 varchar(50) = NULL,
	@IngredientMass6 float = NULL,
	@IngredientActual6 float = NULL,
	--' @UserOpID6 int = NULL

	@IngredientLN7 varchar(50) = NULL,
	@IngredientPN7 varchar(50) = NULL,
	@IngredientMass7 float = NULL,
	@IngredientActual7 float = NULL,
	--' @UserOpID6 int = NULL

	@IngredientLN8 varchar(50) = NULL,
	@IngredientPN8 varchar(50) = NULL,
	@IngredientMass8 float = NULL,
	@IngredientActual8 float = NULL,

	@IngredientLN9 varchar(50) = NULL,
	@IngredientPN9 varchar(50) = NULL,
	@IngredientMass9 float = NULL,
	@IngredientActual9 float = NULL,

	@IngredientLN10 varchar(50) = NULL,
	@IngredientPN10 varchar(50) = NULL,
	@IngredientMass10 float = NULL,
	@IngredientActual10 float = NULL


AS
/*
exec dbo.usp_WeighingMultiInput @UserOpID = '50', @IngredientLN1 = '1.1', @IngredientPN1 = '1.2', @IngredientActual1 = '2.1', @IngredientLN2 = '2.2', 
@IngredientPN2 = '3.1', @IngredientActual2 = '3.2', @IngredientLN3 = '4.1', @IngredientPN3 = '4.2', @IngredientActual3 = '5.1', @IngredientLN4 = '5.2', 
@IngredientPN4 = '6.1', @IngredientActual4 = '6.2', @IngredientLN5 = '7.1', @IngredientPN5 = '7.2', @IngredientActual5 = '1.3', @IngredientLN6 = '2.3', 
@IngredientPN6 = '3.3', @IngredientActual6 = '4.3', @IngredientLN7 = '5.3', @IngredientPN7 = '6.3', @IngredientActual7 = '7.3', @ProductLN = '1'

select * from combinedbatchrecord
*/

BEGIN

	SET NOCOUNT ON;

	if not exists ( select 1 from [SP_ManuLine].[dbo].LotNumberList where [LotNumber] = @ProductLN )
		begin
		select 'Product Lot Number does not exist.'
		return
		end

	If @IngredientLN1 is not null
		BEGIN
		--if not exists ( select 1 from [SP_ManuLine].[dbo].Weighing where IngredientLN =  @IngredientLN1 )
		--	BEGIN
		--	SELECT 'Ingredient Lot Number does not exist.'
		--	RETURN
		--	END

		insert into weighing(ProductLN, WeighEquipment, IngredientLN, IngredientPN, IngredientMass, IngredientActual, UserOpID)
		values ( @ProductLN, @WeighEquipment, @IngredientLN1, @IngredientPN1, @IngredientMass1, @IngredientActual1, @UserOpID )
		END

	If @IngredientLN2 is not null
		BEGIN
		--if not exists ( select 1 from [SP_ManuLine].[dbo].Weighing where IngredientLN =  @IngredientLN2 )
		--	BEGIN
		--	SELECT 'Ingredient Lot Number does not exist.'
		--	RETURN
		--	END

		insert into weighing(ProductLN, WeighEquipment, IngredientLN, IngredientPN, IngredientMass, IngredientActual, UserOpID)
		values ( @ProductLN, @WeighEquipment, @IngredientLN2, @IngredientPN2, @IngredientMass2, @IngredientActual2, @UserOpID )
		END

	If @IngredientLN3 is not null
		BEGIN

		--if not exists ( select 1 from [SP_ManuLine].[dbo].Weighing where IngredientLN =  @IngredientLN3 )
		--	BEGIN
		--	SELECT 'Ingredient Lot Number does not exist.'
		--	RETURN
		--	END

		insert into weighing(ProductLN, WeighEquipment, IngredientLN, IngredientPN, IngredientMass, IngredientActual, UserOpID)
		values ( @ProductLN, @WeighEquipment, @IngredientLN3, @IngredientPN3, @IngredientMass3, @IngredientActual3, @UserOpID )
		END

	If @IngredientLN4 is not null
		BEGIN

		--if not exists ( select 1 from [SP_ManuLine].[dbo].Weighing where IngredientLN =  @IngredientLN4 )
		--	BEGIN
		--	SELECT 'Ingredient Lot Number does not exist.'
		--	RETURN
		--	END

		insert into weighing(ProductLN, WeighEquipment, IngredientLN, IngredientPN, IngredientMass, IngredientActual, UserOpID)
		values ( @ProductLN, @WeighEquipment, @IngredientLN4, @IngredientPN4, @IngredientMass4, @IngredientActual4, @UserOpID )
		END

	If @IngredientLN5 is not null
		BEGIN

		--if not exists ( select 1 from [SP_ManuLine].[dbo].Weighing where IngredientLN =  @IngredientLN5 )
		--	BEGIN
		--	SELECT 'Ingredient Lot Number does not exist.'
		--	RETURN
		--	END

		insert into weighing(ProductLN, WeighEquipment, IngredientLN, IngredientPN, IngredientMass, IngredientActual, UserOpID)
		values ( @ProductLN, @WeighEquipment, @IngredientLN5, @IngredientPN5, @IngredientMass5, @IngredientActual5, @UserOpID )
		END

	If @IngredientLN6 is not null
		BEGIN

		--if not exists ( select 1 from [SP_ManuLine].[dbo].Weighing where IngredientLN =  @IngredientLN6 )
		--	BEGIN
		--	SELECT 'Ingredient Lot Number does not exist.'
		--	RETURN
		--	END

		insert into weighing(ProductLN, WeighEquipment, IngredientLN, IngredientPN, IngredientMass, IngredientActual, UserOpID)
		values ( @ProductLN, @WeighEquipment, @IngredientLN6, @IngredientPN6, @IngredientMass6, @IngredientActual6, @UserOpID )
		END

	If @IngredientLN7 is not null
		BEGIN

		--if not exists ( select 1 from [SP_ManuLine].[dbo].Weighing where IngredientLN =  @IngredientLN7 )
		--	BEGIN
		--	SELECT 'Ingredient Lot Number does not exist.'
		--	RETURN
		--	END


		insert into weighing(ProductLN, WeighEquipment, IngredientLN, IngredientPN, IngredientMass, IngredientActual, UserOpID)
		values ( @ProductLN, @WeighEquipment, @IngredientLN7, @IngredientPN7, @IngredientMass7, @IngredientActual7, @UserOpID )
		END

	If @IngredientLN8 is not null
		BEGIN

		--if not exists ( select 1 from [SP_ManuLine].[dbo].Weighing where IngredientLN =  @IngredientLN8 )
		--	BEGIN
		--	SELECT 'Ingredient Lot Number does not exist.'
		--	RETURN
		--	END

		insert into weighing(ProductLN, WeighEquipment, IngredientLN, IngredientPN, IngredientMass, IngredientActual, UserOpID)
		values ( @ProductLN, @WeighEquipment, @IngredientLN8, @IngredientPN8, @IngredientMass8, @IngredientActual8, @UserOpID )
		END

	If @IngredientLN9 is not null
		BEGIN

		--if not exists ( select 1 from [SP_ManuLine].[dbo].Weighing where IngredientLN =  @IngredientLN9 )
		--	BEGIN
		--	SELECT 'Ingredient Lot Number does not exist.'
		--	RETURN
		--	END

		insert into weighing(ProductLN, WeighEquipment, IngredientLN, IngredientPN, IngredientMass, IngredientActual, UserOpID)
		values ( @ProductLN, @WeighEquipment, @IngredientLN9, @IngredientPN9, @IngredientMass9, @IngredientActual9, @UserOpID )
		END

	If @IngredientLN10 is not null
		BEGIN

		--if not exists ( select 1 from [SP_ManuLine].[dbo].Weighing where IngredientLN =  @IngredientLN10 )
		--	BEGIN
		--	SELECT 'Ingredient Lot Number does not exist.'
		--	RETURN
		--	END

		insert into weighing(ProductLN, WeighEquipment, IngredientLN, IngredientPN, IngredientMass, IngredientActual, UserOpID)
		values ( @ProductLN, @WeighEquipment, @IngredientLN10, @IngredientPN10, @IngredientMass10, @IngredientActual10, @UserOpID )
		END
END
GO


