USE [SP_ManuLine]
GO

/****** Object:  StoredProcedure [dbo].[insertProdConductivity]    Script Date: 6/4/2021 16:40:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO














CREATE PROCEDURE [dbo].[insertProdConductivity]
				@UserOpID varchar(10) = NULL
			   , @ProductLN varchar(50) = NULL
			   , @ProductPN varchar(50) = NULL
			   , @TestCondition varchar(50) = NULL
			   , @TestTemperature float = NULL
			   , @MaterialMass_g float = NULL
			   , @PlungerLengthNoPellet_mm float = NULL
			   , @PelletPlungerThickness_mm float = NULL
			   , @PelletThickness_cm float = NULL
			   , @MaterialDensity_gcc float = NULL
			   , @XAxisNyquist_ohms float = NULL
			   , @IonicConductivity_Scm varchar(10) = NULL
			   , @ElectronicConductivity_Scm varchar(10) = NULL
			   , @IonicIntercept varchar(10) = NULL
			   , @ElectronicIntercept varchar(10) = NULL
			   , @Notes varchar(4000) = NULL
			   , @UpdateTime smalldatetime = NULL
AS
BEGIN
/*
TO DO:
1) remove LotNr from insert when LotNr column is removed

example of insert and update
exec dbo.insertProdConductivity @UserOpID = '54', @ProductLN = 'CG-2103-112-0', @TestTemperature = '25', 
@MaterialMass_g = '0.318', @PlungerLengthNoPellet_mm = '40.02', @PelletPlungerThickness_mm = '41.02', 
@MaterialDensity_gcc = '1.5816035829', @PelletThickness_cm = '0.1', @IonicConductivity_Scm = '3.94729854971', 
@ElectronicConductivity_Scm = 'None',@IonicIntercept = '12.6', @ElectronicIntercept = 'None', @Notes = 'test data'

exec dbo.insertProdConductivity @UserOpID = '54', @ProductLN = 'CG-2103-112-0', @TestTemperature = '25', 
@MaterialMass_g = '0.318', @PlungerLengthNoPellet_mm = '40.02', @PelletPlungerThickness_mm = '41.02', 
@MaterialDensity_gcc = '1.5816035829', @PelletThickness_cm = '0.1', @IonicConductivity_Scm = 'None',
@ElectronicConductivity_Scm = '0.157148151565',@IonicIntercept = '', @ElectronicIntercept = '318518666.0', 
@Notes = 'test data'

---------------------------------------------------------------------
exec dbo.insertProdConductivity @UserOpID = '50', @ProductLN = '38-2103-315-0',@TestCondition = 'Ionic', @TestTemperature = '24.5', @MaterialMass_g = '0.3', 
@PlungerLengthNoPellet_mm = '4', @PelletPlungerThickness_mm = '5.6', @MaterialDensity_gcc = '0.197888441882', @XAxisNyquist_ohms = '20.53', @PelletThickness_cm = '6.5', 
@IonicConductivity_Scm = '14.982387192', @ElectronicConductivity_Scm = 'None',@IonicIntercept = '25.03', @ElectronicIntercept = 'None'

*/

	SET NOCOUNT ON;

-- check to make sure LN exists in master LotNumberList table
	if not exists ( select 1 from [dbo].[LotNumberList] where LotNumber = @ProductLN )
	begin
		select 'Product Lot Number does not exist.'
		--RAISERROR (15600,-1,-1, 'insertProdConductivity: LotNumber Does Not Exist.'); 
		return
	end
---------------------------------------------------------------------------------------------------
-- change any dash input variable that contains the value None or is an empty string to NULL
	if @ElectronicConductivity_Scm = 'None'
	begin
		set @ElectronicConductivity_Scm = NULL
	end

	if @IonicConductivity_Scm = 'None'
	begin
		set @IonicConductivity_Scm = NULL
	end
	
	if @ElectronicIntercept = 'None' or @ElectronicIntercept = ''
	begin
		set @ElectronicIntercept = NULL
	end

	if @IonicIntercept = 'None' or @IonicIntercept = ''
	begin
		set @IonicIntercept = NULL
	end	
------------------------------------------------------------------------------------

	INSERT INTO [dbo].[ProdConductivity]
			   (
			   -- [MaterialID]
			   [Operator]
			   --,[EntryDate]
			   ,[LotNr]
			   ,[ProductLN]
			   ,[ProductPN]
			   ,[TestCondition]
			   ,[TestTemperature]
			   ,[MaterialMass_g]
			   ,[PlungerLengthNoPellet_mm]
			   ,[PelletPlungerThickness_mm]
			   ,[PelletThickness_cm]
			   ,[MaterialDensity_gcc]
			   ,[XAxisNyquist_ohms]
			   ,[IonicConductivity_Scm]
			   ,[ElectronicConductivity_Scm]
			   ,IonicIntercept
			   ,ElectronicIntercept
			   ,[Notes]
			   ,[UpdateTime]
			   )
		 VALUES
			   (
			   --<MaterialID, varchar(50),>
			   @UserOpID
			   -- ,<EntryDate, smalldatetime,>
			   , @ProductLN
			   , @ProductLN
			   , @ProductPN
			   , @TestCondition
			   , @TestTemperature
			   , @MaterialMass_g
			   , @PlungerLengthNoPellet_mm
			   , @PelletPlungerThickness_mm
			   , @PelletThickness_cm
			   , @MaterialDensity_gcc
			   , @XAxisNyquist_ohms
			   , @IonicConductivity_Scm
			   , @ElectronicConductivity_Scm
			   , @IonicIntercept
			   , @ElectronicIntercept
			   , @Notes
			   , @UpdateTime
			   )
END
GO


