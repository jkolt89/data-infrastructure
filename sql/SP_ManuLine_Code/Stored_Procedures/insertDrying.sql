USE [SP_ManuLine]
GO

/****** Object:  StoredProcedure [dbo].[insertDrying]    Script Date: 6/4/2021 16:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[insertDrying]
           @IngredientLN varchar(50) = NULL
           ,@ProductPN varchar(50) = NULL
           ,@Item varchar(50) = NULL
           ,@Supplier varchar(50) = NULL
           ,@SupplierLotNr varchar(50) = NULL
           ,@KSolidMoisture_ppm varchar(50) = NULL
           ,@Notes varchar(4000) = NULL
           -- ,@EntryDate smalldatetime
           ,@UpdateTime smalldatetime = NULL
		   ,@UserOpID int = NULL

as
INSERT INTO [dbo].[Drying]
           ([SPLotNr]
           ,[IngredientLN]
           ,[PartNr]
           ,[ProductPN]
           ,[Item]
           ,[Supplier]
           ,[SupplierLotNr]
           ,[KSolidMoisture_ppm]
           ,[Notes]
           --,[EntryDate]
           ,[UpdateTime]
		   ,UserOpID)
     VALUES
           ( @IngredientLN
           ,@IngredientLN
           ,@ProductPN
           ,@ProductPN
           ,@Item
           ,@Supplier
           ,@SupplierLotNr
           ,@KSolidMoisture_ppm
           ,@Notes
           -- ,@EntryDate
           ,@UpdateTime
		   ,@UserOpID
		   )
GO


