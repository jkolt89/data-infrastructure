USE [SP_ManuLine]
GO

/****** Object:  StoredProcedure [dbo].[usp_LotNumberTraceSingleOrCombinedBatch]    Script Date: 6/4/2021 16:42:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_LotNumberTraceSingleOrCombinedBatch]
	@Input varchar(50)
	
AS
/*
	This program takes a combined batch LN or singular LN and traces back to the ingredients (precursors) used in each lot
	If it's a combined batch (1st letter is C) it checks every LN in the combined batch record table and traces back to the precursors used, etc
	if it's a singular batch (ie LN starts with a number) it returns data values for that single batch

*/
-- [usp_LotNumberTraceSingleOrCombinedBatch] 'CG-2101-303-0'
-- [usp_LotNumberTraceSingleOrCombinedBatch] '34-2101-303-0'
BEGIN
	
	SET NOCOUNT ON;

    declare @lotnr varchar(50), @count int

	select @count = 1

	if SUBSTRING(@Input, 0, 2) = 'C'
		BEGIN
		declare cur_combinedbatch cursor for
		select OriginalLotNum
		from CombinedBatchRecord
		where CombinedLotNum = @Input
	
		open cur_combinedbatch

		FETCH NEXT FROM cur_combinedbatch INTO @LotNr

		WHILE @@FETCH_STATUS = 0
			BEGIN
			select @lotNr as CurrentLotNr, cast(@count as varchar(10)) LotNrCount
			EXEC [dbo].[usp_LotNumberTrace] @LotNr

			FETCH NEXT FROM cur_combinedbatch INTO @LotNr

			select @count = @count + 1
			select 'NEW PRODUCTLN STARTS HERE'
			END

		close cur_combinedbatch
		deallocate cur_combinedbatch
		END
	else
		BEGIN
		print 'not combined batch'
		exec [dbo].[usp_LotNumberTrace] @Input
		END
END
GO


