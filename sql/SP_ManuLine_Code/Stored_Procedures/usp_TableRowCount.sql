USE [SP_ManuLine]
GO

/****** Object:  StoredProcedure [dbo].[usp_TableRowCount]    Script Date: 6/4/2021 16:42:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_TableRowCount]
AS
BEGIN

/*
counts the number of rows in all tables in database
-- usp_TableRowCount
*/

DECLARE @Table_Name varchar(50), @sql varchar(2000)

DECLARE TableCursor CURSOR FOR
	select TABLE_NAME
	from INFORMATION_SCHEMA.TABLES 
	where TABLE_Type != 'VIEW'
	and TABLE_NAME not like 'zDELME%'
	and TABLE_NAME not like 'sys%'
	order by TABLE_NAME



open TableCursor

fetch next from TableCursor into @Table_name

while @@FETCH_STATUS = 0
	BEGIN
	print @Table_Name
	FETCH NEXT FROM TableCursor into @Table_name
	END

close TableCursor
deallocate TableCursor
END
GO


