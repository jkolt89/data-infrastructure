USE [SP_ManuLine]
GO

/****** Object:  StoredProcedure [dbo].[getWeighingData]    Script Date: 6/4/2021 16:39:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[getWeighingData] 
	@lotnr varchar(100)
AS
BEGIN
-- program created on 10/25/2020
--exec [getWeighingData] '32-2010-311-0'
	SET NOCOUNT ON;

	select * from Weighing where ProductLN = @lotnr
END
GO


