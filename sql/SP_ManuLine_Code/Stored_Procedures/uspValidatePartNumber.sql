USE [SP_ManuLine]
GO

/****** Object:  StoredProcedure [dbo].[uspValidatePartNumber]    Script Date: 6/4/2021 16:42:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[uspValidatePartNumber]
	@partnumber varchar(20)
as
SET NOCOUNT ON
--exec uspValidatePartNumber 'CE132-0001'
--exec uspValidatePartNumber 'ZZ132-0001'
--exec uspValidatePartNumber 'CE932-0001'
--exec uspValidatePartNumber 'CE192-0001'
--exec uspValidatePartNumber 'CE132-0099'

--declare @partnumber varchar(20)
--select @partnumber = 'CE132-0001'
--select @partnumber = 'ZZ132-0001'
--select @partnumber = 'CE932-0001'
--select @partnumber = 'CE192-0001'
--select @partnumber = 'CE132-0099'

--DECLARE @debug int
--select @debug = 0 -- 1 for debugging 0 for no debugging

declare @MaterialShortName char(2), @GenID char(1), @MaterialFormID varchar(10), @ProcessLocID int, @MDID int

select    @MaterialShortName = substring(@partnumber, 1,2), @GenID = substring(@partnumber, 3,1)
		, @MaterialFormID = substring(@partnumber, 4, 1), @ProcessLocID = substring(@partnumber, 5, 1)
		, @MDID = substring(@partnumber, 7, 4)

if len(@partnumber) != 10
	BEGIN
	SELECT 'Part number must be 10 characters long.' as Error
	RETURN
	END

if not exists ( select 1 from [dbo].[Material] where MaterialShortName = @MaterialShortName )
	BEGIN
	--THROW 51000, 'Material does not exist.', 1
	SELECT 'Material does not exist.' AS Error
	RETURN
	END
--else
--	print '@MaterialShortName: ' + @MaterialShortName	

IF NOT EXISTS ( SELECT 1 FROM [dbo].Generation WHERE GenID = @GenID )
	BEGIN
	SELECT 'Generation does not exist.' AS Error
	RETURN
	END
--else
--	print '@GenID: ' + cast(@GenID as varchar(10))

if NOT exists ( select 1 from [dbo].MaterialForm where MaterialFormID = @MaterialFormID )
	BEGIN
	SELECT 'MaterialForm does not exist.' AS Error
	RETURN
	END
--else
--	print '@MaterialFormID: ' + cast(@MaterialFormID as varchar(10))

if NOT exists ( select 1 from [dbo].ProcessLocation where ProcessLocID = @ProcessLocID )
	BEGIN
	SELECT 'Process Location does not exist.' AS Error
	RETURN
	END
--else
--	print '@ProcessLocID: ' + cast(@ProcessLocID as varchar(10))
	
if NOT exists ( select 1 from [dbo].MaterialDescription where MDID = @MDID )
	BEGIN
	SELECT 'Material Description does not exist.' AS Error
	RETURN
	END
--else
--	print '@MDID: ' + cast(@MDID as varchar(10))
	
SELECT @partnumber as PartNumber

GO


