select cellname, max(data_point) maxDP 
from dev.dbo.ChannelStatistic 
group by cellname
order by cellname, max(data_point)