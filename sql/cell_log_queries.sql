USE [SP_RND]
GO

select * from cell_log_STAGE 
where temperature != ''

select * from cell_log_STAGE
-- truncate table cell_log
select *  from cell_log


--insert into cell_log([CellName], [PurposeDescription], [Cathode], [temperature], [MaxVoltage], [die], [CurrentCollector], [ActiveCathode], [ActiveCathodeDateCoated], [electrolyte], [TotalCathodeMass_g], [DieDiameter_cm)], [CathodeParticleCoating], [WeightCoatingPercent], [NominalPercentActiveMaterial], [RatioActiveMaterial], [ActiveMaterialTheoreticalCapacity_mAhg], [CellTheoreticalCapacity_mAhg], [Loading_mg_cm2)], [Capacity_mAh], [Anode], [AnodeMass_g], [Separator], [SeparatorMass_g], [MaximumStackPressure_ton], [CathodePressure_ton], [CageTorque_lbs], [Operator], [Location], [NrCyclesCompleted], [FirstCycleCEPercent], [FirstCycleCharge], [FirstCycleDischarge], [InitialResistance_ohm], [MaxVoltageWithOutlier], [MaxVoltageNoOutlier], [MaxDischargeCapacity], [ExcelTime], [PythonTime], [LastDateTime], [Channel], [ArbinNotes], [StoppageReason], [StoppageCycleNr], [Notes])
SELECT [Cell #]
      ,replace([Purpose Description], '~', ',') [Purpose Description]
      ,replace([Cathode], '~', ',') [Cathode]
      ,[temperature]
      ,[max voltage]
      ,[die]
      ,[Current Collector]
      ,[active cathode]
      ,[active cathode date coated]
      ,[electrolyte]
      ,[Total Cathode Mass (g)]
      ,[Die Diameter (1 3 or 1 6 cm)]
      ,[cathode particle coating]
      ,[wt% coating]
      ,[nominal percent active material]
      ,[ratio active material]
      ,[active material theoretical capacity (mAh g)]
      ,[cell theoretical capacity (mAh g)]
      ,[Loading (mg cm^2)]
      ,[Capacity (mAh)]
      ,[Anode]
      ,[Anode Mass (g)]
      ,replace([Separator], '~', ',') [Separator]
      ,[Separator Mass (g)]
      ,[Maximum Stack Pressure (tons)]
      ,[Cathode Pressure (tons)]
      ,[Cage torque (in-lbs)]
      ,[Operator]
      ,[Location]
      ,[# of cycles completed]
      ,[1st cycle CE (%)]
      ,[1st cycle charge]
      ,[1st cycle discharge]
      ,[Initial Resistance (ohms)]
      ,[max voltage w  outlier]
      ,[max voltage w o outlier]
      ,[maximum discharge capacity]
      ,[Excel Time]
      ,[Python Time]
      ,[Last DateTime]
      ,[Channel]
      ,[Arbin Notes]
      ,[Reason for stoppage]
      ,[Cycle # of stoppage]
      ,replace([Notes], '~', ',') [Notes]
  FROM [SP_RND].[dbo].[cell_log_STAGE]

select * from cell_log 
--where [PurposeDescription]= ''

update cell_log set ActiveCathode = null where ActiveCathode = ''
update cell_log set ActiveCathodeDateCoated = null where ActiveCathodeDateCoated = ''
update cell_log set ActiveMaterialTheoreticalCapacity_mAhg = null where ActiveMaterialTheoreticalCapacity_mAhg = ''
update cell_log set Anode = null where Anode = ''
update cell_log set AnodeMass_g = null where AnodeMass_g = ''
update cell_log set ArbinNotes = null where ArbinNotes = ''
update cell_log set CageTorque_lbs = null where CageTorque_lbs = ''
update cell_log set Capacity_mAh = null where Capacity_mAh = ''
update cell_log set Cathode = null where Cathode = ''
update cell_log set CathodeParticleCoating = null where CathodeParticleCoating = ''
update cell_log set CathodePressure_ton = null where CathodePressure_ton = ''
update cell_log set CellName = null where CellName = ''
update cell_log set CellTheoreticalCapacity_mAhg = null where CellTheoreticalCapacity_mAhg = ''
update cell_log set Channel = null where Channel = ''
update cell_log set CurrentCollector = null where CurrentCollector = ''
update cell_log set die = null where die = ''
update cell_log set DieDiameter_cm = null where DieDiameter_cm = ''
update cell_log set electrolyte = null where electrolyte = ''
update cell_log set ExcelTime = null where ExcelTime = ''
update cell_log set FirstCycleCEPercent = null where FirstCycleCEPercent = ''
update cell_log set FirstCycleCharge = null where FirstCycleCharge = ''
update cell_log set FirstCycleDischarge = null where FirstCycleDischarge = ''
update cell_log set InitialResistance_ohm = null where InitialResistance_ohm = ''
update cell_log set LastDateTime = null where LastDateTime = ''
update cell_log set Loading_mg_cm2 = null where Loading_mg_cm2 = ''
update cell_log set Location = null where Location = ''
update cell_log set MaxDischargeCapacity = null where MaxDischargeCapacity = ''
update cell_log set MaximumStackPressure_ton = null where MaximumStackPressure_ton = ''
update cell_log set MaxVoltage = null where MaxVoltage = ''
update cell_log set MaxVoltageNoOutlier = null where MaxVoltageNoOutlier = ''
update cell_log set MaxVoltageWithOutlier = null where MaxVoltageWithOutlier = ''
update cell_log set NominalPercentActiveMaterial = null where NominalPercentActiveMaterial = ''
update cell_log set Notes = null where Notes = ''
update cell_log set NrCyclesCompleted = null where NrCyclesCompleted = ''
update cell_log set Operator = null where Operator = ''
update cell_log set PurposeDescription = null where PurposeDescription = ''
update cell_log set PythonTime = null where PythonTime = ''
update cell_log set RatioActiveMaterial = null where RatioActiveMaterial = ''
update cell_log set Separator = null where Separator = ''
update cell_log set SeparatorMass_g = null where SeparatorMass_g = ''
update cell_log set StoppageCycleNr = null where StoppageCycleNr = ''
update cell_log set StoppageReason = null where StoppageReason = ''
update cell_log set temperature = null where temperature = ''
update cell_log set TotalCathodeMass_g = null where TotalCathodeMass_g = ''
update cell_log set WeightCoatingPercent = null where WeightCoatingPercent = ''
