CREATE TABLE channel (
    channelid      INTEGER NOT NULL,
    chname         VARCHAR(100),
    instrumentid   INTEGER NOT NULL
)

go

ALTER TABLE Channel ADD constraint channel_pk PRIMARY KEY CLUSTERED (ChannelID)
     WITH (
     ALLOW_PAGE_LOCKS = ON , 
     ALLOW_ROW_LOCKS = ON ) 
go

CREATE TABLE employee (
    employeeid    INTEGER NOT NULL,
    opfirstname   VARCHAR(100),
    oplastname    VARCHAR(100)
)

go

ALTER TABLE Employee ADD constraint operator_pk PRIMARY KEY CLUSTERED (EmployeeID)
     WITH (
     ALLOW_PAGE_LOCKS = ON , 
     ALLOW_ROW_LOCKS = ON ) 
go

CREATE TABLE equipment (
    equipmentid   INTEGER NOT NULL,
    ename         VARCHAR(100),
    ipaddress     VARCHAR(25),
    locationid    INTEGER NOT NULL
)

go

ALTER TABLE Equipment ADD constraint equipment_pk PRIMARY KEY CLUSTERED (EquipmentID)
     WITH (
     ALLOW_PAGE_LOCKS = ON , 
     ALLOW_ROW_LOCKS = ON ) 
go

CREATE TABLE instrument (
    instrumentid   INTEGER NOT NULL,
    iname          VARCHAR(100),
    idesc          VARCHAR(500),
    equipmentid    INTEGER NOT NULL
)

go

ALTER TABLE Instrument ADD constraint instrument_pk PRIMARY KEY CLUSTERED (InstrumentID)
     WITH (
     ALLOW_PAGE_LOCKS = ON , 
     ALLOW_ROW_LOCKS = ON ) 
go

CREATE TABLE location (
    locationid   INTEGER NOT NULL,
    locname      VARCHAR(100),
    locdesc      VARCHAR(200)
)

go

ALTER TABLE Location ADD constraint location_pk PRIMARY KEY CLUSTERED (LocationID)
     WITH (
     ALLOW_PAGE_LOCKS = ON , 
     ALLOW_ROW_LOCKS = ON ) 
go

CREATE TABLE measurement (
    measurementid       INTEGER NOT NULL,
    mkey                VARCHAR(50),
    mvalue              VARCHAR(100),
    channelid           INTEGER NOT NULL,
    measurementtypeid   INTEGER NOT NULL,
    widget_widgetid     INTEGER NOT NULL
)

go

ALTER TABLE Measurement ADD constraint measurement_pk PRIMARY KEY CLUSTERED (MeasurementID)
     WITH (
     ALLOW_PAGE_LOCKS = ON , 
     ALLOW_ROW_LOCKS = ON ) 
go

CREATE TABLE measurementtype (
    measurementtypeid   INTEGER NOT NULL,
    mtypename           VARCHAR(100),
    mtypeunit           VARCHAR(100)
)

go

ALTER TABLE MeasurementType ADD constraint measurementtype_pk PRIMARY KEY CLUSTERED (MeasurementTypeID)
     WITH (
     ALLOW_PAGE_LOCKS = ON , 
     ALLOW_ROW_LOCKS = ON ) 
go

CREATE TABLE widget (
    widgetid     INTEGER NOT NULL,
    widgetname   VARCHAR(200),
    barcode      VARCHAR(100),
    runstart     VARCHAR(100),
    employeeid   INTEGER NOT NULL
)

go

ALTER TABLE Widget ADD constraint widget_pk PRIMARY KEY CLUSTERED (WidgetID)
     WITH (
     ALLOW_PAGE_LOCKS = ON , 
     ALLOW_ROW_LOCKS = ON ) 
go

ALTER TABLE Channel
    ADD CONSTRAINT channel_instrument_fk FOREIGN KEY ( instrumentid )
        REFERENCES instrument ( instrumentid )
ON DELETE NO ACTION 
    ON UPDATE no action 
go

ALTER TABLE Equipment
    ADD CONSTRAINT equipment_location_fk FOREIGN KEY ( locationid )
        REFERENCES location ( locationid )
ON DELETE NO ACTION 
    ON UPDATE no action 
go

ALTER TABLE Instrument
    ADD CONSTRAINT instrument_equipment_fk FOREIGN KEY ( equipmentid )
        REFERENCES equipment ( equipmentid )
ON DELETE NO ACTION 
    ON UPDATE no action 
go

ALTER TABLE Measurement
    ADD CONSTRAINT measurement_channel_fk FOREIGN KEY ( channelid )
        REFERENCES channel ( channelid )
ON DELETE NO ACTION 
    ON UPDATE no action 
go

ALTER TABLE Measurement
    ADD CONSTRAINT measurement_measurementtype_fk FOREIGN KEY ( measurementtypeid )
        REFERENCES measurementtype ( measurementtypeid )
ON DELETE NO ACTION 
    ON UPDATE no action 
go

ALTER TABLE Measurement
    ADD CONSTRAINT measurement_widget_fk FOREIGN KEY ( widget_widgetid )
        REFERENCES widget ( widgetid )
ON DELETE NO ACTION 
    ON UPDATE no action 
go

ALTER TABLE Widget
    ADD CONSTRAINT widget_operator_fk FOREIGN KEY ( employeeid )
        REFERENCES employee ( employeeid )
ON DELETE NO ACTION 
    ON UPDATE no action 
go



-- Oracle SQL Developer Data Modeler Summary Report: 
-- 
-- CREATE TABLE                             8
-- CREATE INDEX                             0
-- ALTER TABLE                             15
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE DATABASE                          0
-- CREATE DEFAULT                           0
-- CREATE INDEX ON VIEW                     0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE ROLE                              0
-- CREATE RULE                              0
-- CREATE SCHEMA                            0
-- CREATE SEQUENCE                          0
-- CREATE PARTITION FUNCTION                0
-- CREATE PARTITION SCHEME                  0
-- 
-- DROP DATABASE                            0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
