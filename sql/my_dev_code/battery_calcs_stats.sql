use Dev
go

create view zdev_myBatteryCalcs as
--select step_index, step_time, step_time/60
--from ChannelNormal
--where cellname = 'A1910170004_Cycling'
--order by Data_Point


--select cycle_index, step_index, max(step_time), max(step_time)/60, avg(charge_capacity), max(charge_capacity)
--from ChannelNormal
--where cellname = 'A1910170004_Cycling'
--group by cycle_index, step_index
--order by cycle_index, Step_Index


--select a.Step_index, a.step_time, a.step_time/60, Charge_Capacity, maxcc-Charge_Capacity,
--	[current]
--from ChannelNormal a
--inner join (
--select cellname, cycle_index, step_index, max(charge_capacity) maxcc
--from ChannelNormal
--where cellname = 'A1910170004_Cycling'
--group by cellname, cycle_index, step_index
--) b on a.cellname=b.cellname and a.cycle_index = b.cycle_index and  a.step_index=b.step_index
--order by a.cellname, a.cycle_index, a.step_index

select top 100 percent
	cellname
	, cycle_index, data_point, step_index

	, voltage volt
	, [current] * Internal_Resistance myvolt

	, [current] I_Amps
	, voltage/internal_resistance myI_Amps

	, internal_resistance R_Ohms
	, voltage/[current] myR_Ohms

	, power([current], 2) * internal_resistance Power_I2R_Watts
	, [current] * voltage Power_IxV_Watts

	--, voltage * [current] [Power_Watts]
	--, (voltage * [current] ) * (step_time/60) Joules
	--, (voltage * [current] ) * (step_time*60) Joules
	, (voltage * [current] ) * (step_time) Joules

	, step_time
	, step_time/60/60 step_hours
	, charge_capacity ccap_Ah
	
	, discharge_capacity dcap_Ah
	, discharge_capacity * -1 discharge_capacity_Ah_neg
	, Test_Time TestTimeSeconds
	, Test_Time/60/60 test_hours
	--, Test_Time/60/60/60/24 test_days
	, Test_Time / 86400 test_days
from Channelnormal
where [current] != 0
	and internal_resistance != 0
	and cellname = 'A1910170005_Cycling'
	and cycle_index between 90 and 100

--group by cellname
order by cellname, data_point

