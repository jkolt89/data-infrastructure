-- CLINT'S FORMULATION PAGE QUERY ################################################################
select 
	p.PartNumberShortName, 
	p.Density, 
	round( w.IngredientActual / p.Density, 2) Vol_ml, 
	round((round( w.IngredientActual / p.Density, 2) / SumVol_Ml ) * 100, 2) as VolPercent,
	round((w.IngredientActual / a.SumIngredientActual) * 100, 2) ActualMassPercent,
	w.IngredientActual, 
	'  ' Spacer, *
from Weighing w
left outer join PartNumberList p 
	on p.partnumbername = w.IngredientName
inner join ( SELECT ProductLN, sum(ingredientactual) SumIngredientActual from weighing where ProductLN = '32-2012-301-0' group by ProductLN ) 
	a on a.ProductLN = w.ProductLN
inner join
	( 	select w.productLN, sum(round( w.IngredientActual / p.Density, 2)) as SumVol_Ml
	from Weighing w
	left outer join PartNumberList p 
		on p.partnumbername = w.IngredientName
	where w.ProductLN = '32-2012-301-0'
	group by w.ProductLN ) b
on b.ProductLN = w.ProductLN
where w.ProductLN = '32-2012-301-0'
--order by substring(w.ProductLN, 1, 2) desc
--################################################################################################


select 'BatchSynthesis', count(*) from BatchSynthesis union all
select 'Calendar', count(*) from Calendar union all
select 'CellComponent', count(*) from CellComponent union all
select 'Channel', count(*) from Channel union all
select 'CombinedBatchRecord', count(*) from CombinedBatchRecord union all
select 'ConsumedBatch', count(*) from ConsumedBatch union all
select 'EquipCryst', count(*) from EquipCryst union all
select 'Equipment', count(*) from Equipment union all
select 'EquipmentLocation', count(*) from EquipmentLocation union all
select 'Generation', count(*) from Generation union all
select 'LotNrGenerationLocation', count(*) from LotNrGenerationLocation union all
select 'LotNrGenerationLocationArchive', count(*) from LotNrGenerationLocationArchive union all
select 'LotNumberList', count(*) from LotNumberList union all
select 'LotNumLookup', count(*) from LotNumLookup union all
select 'Material', count(*) from Material union all
select 'MaterialDescription', count(*) from MaterialDescription union all
select 'MaterialForm', count(*) from MaterialForm union all
select 'MaterialRecipe', count(*) from MaterialRecipe union all
select 'Measurement', count(*) from Measurement union all
select 'MeasurementType', count(*) from MeasurementType union all
select 'MillSetup', count(*) from MillSetup union all
select 'Parameter', count(*) from Parameter union all
select 'Precursor', count(*) from Precursor union all
select 'PrecursorType', count(*) from PrecursorType union all
select 'ProcessLocation', count(*) from ProcessLocation union all
select 'ProdConductivity', count(*) from ProdConductivity union all
select 'QA_Bulk_Material_Log', count(*) from QA_Bulk_Material_Log union all
select 'QA_NMC', count(*) from QA_NMC union all
select 'QA_Synthesis', count(*) from QA_Synthesis union all
select 'Sensor', count(*) from Sensor union all
select 'Slurry', count(*) from Slurry union all
select 'Solvent', count(*) from Solvent union all
select 'SolventDrying', count(*) from SolventDrying union all
select 'SPLocation', count(*) from SPLocation union all
select 'Supplier', count(*) from Supplier union all
select 'UserOp', count(*) from UserOp union all
select 'Weighing', count(*) from Weighing union all
select 'WeightLot', count(*) from WeightLot union all
select 'Widget', count(*) from Widget


-- Below uses LN and PN from solventdrying table but there are no matches
select * from Weighing where ingredientln = '10-2010-121-0'
select * from Weighing where ingredientpn = 'EP111-0000'
select * from Weighing where ingredientpn = 'BE132-0000'
select * from Weighing where ingredientln like '10-2011-110-A'

--introduces redundant data
select * from Weighing w 
inner join Precursor p on w.IngredientLN=p.ingredientLN  and w.IngredientPN = p.ingredientpn and p.ingredientname = w.IngredientName
where p.UpdateTime is null
order by productpn, ProductLN

--nothing matches between weighing table and SolventDrying table
select * from Weighing w 
inner join SolventDrying p on w.IngredientLN=p.ingredientLN  and w.IngredientPN = p.ingredientpn and p.ingredientname = w.IngredientName
where p.UpdateTime is null
order by productpn, ProductLN


--------- END ISSUES QUESTIONS----------------------------

select * from SolventDrying


select * from ProdConductivity where UpdateTime is null
select * from millsetup where UpdateTime is null
select * from ConsumedBatch
select * from Precursor Where updatetime is null
select * from Weighing where ingredientln like '35-2007-801-0'
select * from Weighing where ingredientln like '35-2003-001'
select * from Solvent  where productln like '34-2011-320-0'
select * from SolventDrying where updatetime is null
select * from LotNumberList where lotnumber like '35-2007-801-0'

--update ProdConductivity set ProductLN = Lotnr
select ingredientLN, count(ingredientLN) from MillSetup group by ingredientLN
select ingredientLN, count(ingredientLN) from ConsumedBatch group by ingredientLN having count(ingredientln) > 1
select * from MillSetup m inner join ConsumedBatch c on m.ingredientln = c.IngredientLN
select distinct ingredientln, ingredientpn, ingredientname from Precursor where year(entrydate) = '2020' and ingredientpn is not null --order by entrydate desc
--update Precursor set IngredientName = Item
select distinct ingredientln+ ingredientpn+ ingredientname from Precursor where year(entrydate) = '2020' and ingredientpn is not null --order by entrydate desc
select ProductLN, count(ProductLN) from weighing group by ProductLN having count(ProductLN) >1

select * from Weighing where ProductLN = '35-2003-001'
select * from solventdrying

select * from Weighing w 
left outer join ConsumedBatch c on w.IngredientpN = c.IngredientpN 
where w.ProductLN = '35-2003-001'

select * from Weighing w 
inner join Precursor p on w.IngredientLN=p.ingredientLN  and w.IngredientPN = p.ingredientpn and p.ingredientname = w.IngredientName
--where w.ProductLN = '35-2003-001'
order by 1 desc

select ProductLN, ProductPN, IngredientName, IngredientMass
from weighing w
where w.ProductLN = '35-2003-001'

select distinct ingredientln, ingredientpn, ingredientname from SP_ManuLine..Weighing where year(entrydate) = '2020' and ingredientpn is not null --order by entrydate desc

--introduces redundant data
select * from Weighing w 
inner join Precursor p on w.IngredientLN=p.ingredientLN  and w.IngredientPN = p.ingredientpn and p.ingredientname = w.IngredientName
where p.UpdateTime is null
order by productpn, ProductLN

select * from precursor where updatetime is null

--nothing matches between weighing table and SolventDrying table
select * from Weighing w 
inner join SolventDrying p on w.IngredientLN=p.ingredientLN  and w.IngredientPN = p.ingredientpn and p.ingredientname = w.IngredientName
where p.UpdateTime is null
order by productpn, ProductLN

select * from lotnumberlist l join weighing w on w.ingredientln = l.IngredientLN
where l.IngredientLN = '10-2007-010-0'

select * from LotNumberList where ingredientln = '35-2007-802-0'

 select EntryDate, ProductPN, IngredientName, IngredientMass, IngredientActual, QuantityMassSum, QuantityActualSum 
 from Weighing w inner join 
 (select IngredientLN, sum(IngredientActual) QuantityActualSum, sum(ingredientmass) QuantityMassSum from Weighing where IngredientLN = '35-2007-802-0' group by IngredientLN) s 
 on w.IngredienctLN = s.ingredientln where w.productln ='35-2007-802-0'

select ProductLN, sum(IngredientActual) QuantityActualSum, sum(ingredientmass) QuantityMassSum from Weighing where ProductLN = '35-2007-802-0' group by ProductLN

select distinct ingredientln, ingredientpn from SolventDrying order by 1 desc, 2 desc
select distinct ingredientln, ingredientpn  from weighing order by 1 desc, 2 desc

om SolventDrying where UpdateTime is null
select * from SolventDrying where ingredientln = '10-2011-103-A'

select * from weighing where IngredientLN = '10-2011-105-0'
select * from Weighing
select * from solventdrying

select * from Weighing w 
inner join SolventDrying sd on w.IngredientLN = sd.IngredientLN --and w.IngredientPN = sd.IngredientPN

select * from Weighing w inner join Solvent sd on w.IngredientLN = sd.IngredientLN and w.IngredientPN = sd.IngredientPN
select * from Weighing w inner join ConsumedBatch cb on w.IngredientLN = cb.IngredientLN
select * from solvent s inner join millsetup m on s.productPN = m.productPN
select * from solvent s inner join ProdConductivity p on P.ProductLN = s.ProductLN where p.UpdateTime is null
select * from MillSetup



declare @ProductLN varchar(50), @ProductPN varchar(50)

select @ProductLN = '35-2003-001', @ProductPN = '1032-BE1'

select * from weighing where ProductLN = @ProductLN  --relates
select * from dev.dbo.MillSetup where IngredientLN = @ProductLN
select * from SP_ManuLine.dbo.MillSetup where IngredientLN = @ProductLN
select * from ProdConductivity where ProductLN = @ProductLN
select * from Solvent where ProductLN = @ProductLN

select * from Weighing where ProductPN = @ProductPN     --relates
select * from MillSetup where ProductPN = @ProductPN
select * from Solvent where ProductPN = @ProductPN

-- select * from INFORMATION_SCHEMA.COLUMNS where COLUMN_NAME = 'ProductLN'
-- select * from INFORMATION_SCHEMA.COLUMNS where COLUMN_NAME = 'ProductPN'

--program to try to match up LN and PN

-- select * from INFORMATION_SCHEMA.columns where COLUMN_NAME like '%LN' or COLUMN_NAME like '%PN' order by TABLE_NAME
-- select distinct TABLE_NAME from INFORMATION_SCHEMA.columns where COLUMN_NAME like '%LN' or COLUMN_NAME like '%PN' order by TABLE_NAME
/*
select * from ConsumedBatch			-- LotNr to IngredientLN
select * from LotNumberList			-- LotNumber to IngredientLN
select * from MillSetup				-- ProductLN to IngredientLN
select * from Precursor				-- SPLotNr to IngredientLN, PartNr to IngredientPN, Item to IngredientName
select * from ProdConductivity		-- LotNr to ProductLN
select * from Slurry				
select * from Solvent				-- SolventLN to IngredientLN, SolventPN to IngredientPN
select * from SolventDrying			-- SPLotNr to IngredientLN, PartNr to IngredientPN, Item to IngredientName
select * from Weighing

select distinct 'select * from ' + TABLE_NAME from INFORMATION_SCHEMA.columns where COLUMN_NAME like '%LN' or COLUMN_NAME like '%PN' -- order by TABLE_NAME
update weighing set ingredientLN = null where IngredientLN = ''
*/
-- select * from precursor
select * from Weighing 
select * from MillSetup
select w.*, '' Spacer, p.* from Weighing w inner join precursor p 
on w.ingredientLN = p.IngredientLN  -- and w.IngredientPN = p.IngredientPN
where w.IngredientLN is not null
-- and w.productpn = '18-2011-107-0'
and w.productpn = '32-1991-001'
and w.IngredientLN = '10-2009-105-0'
order by w.ProductLN, w.productpn

select * from Weighing --w inner join millsetup m on w.ProductLN=m._ProductLN
select * from Weighing_OLD
select * from LotNumberList


select distinct 'exec sp_FindStringInTable ''32-2009-301-0'' , ''dbo'', ''' + TABLE_NAME +'''' from INFORMATION_SCHEMA.columns where COLUMN_NAME like '%LN' or COLUMN_NAME like '%PN'

exec sp_FindStringInTable 'BE132-0000' , 'dbo', 'ConsumedBatch' --ProductPN
exec sp_FindStringInTable 'BE132-0000' , 'dbo', 'LotNumberList'
exec sp_FindStringInTable 'BE132-0000' , 'dbo', 'MillSetup' --ProductPN
exec sp_FindStringInTable 'BE132-0000' , 'dbo', 'Precursor'
exec sp_FindStringInTable 'BE132-0000' , 'dbo', 'ProdConductivity'
exec sp_FindStringInTable 'BE132-0000' , 'dbo', 'SolventDrying'
exec sp_FindStringInTable 'BE132-0000' , 'dbo', 'Weighing' --ProductPN

exec sp_FindStringInTable '32-2009-301-0' , 'dbo', 'ConsumedBatch'  -- ProductLN
exec sp_FindStringInTable '32-2009-301-0' , 'dbo', 'LotNumberList' -- ProductLN
exec sp_FindStringInTable '32-2009-301-0' , 'dbo', 'MillSetup'  -- ProductLN
exec sp_FindStringInTable '32-2009-301-0' , 'dbo', 'Precursor'  
exec sp_FindStringInTable '32-2009-301-0' , 'dbo', 'ProdConductivity' -- ProductLN
exec sp_FindStringInTable '32-2009-301-0' , 'dbo', 'SolventDrying'
exec sp_FindStringInTable '32-2009-301-0' , 'dbo', 'Weighing'   -- ProductLN
exec usp_FindStringInTable '32-2009-301-0', 'dbo', 'Weighing'

exec sp_FindStringInTable '10-2009-110-0' , 'dbo', 'ConsumedBatch'  
exec sp_FindStringInTable '10-2009-110-0' , 'dbo', 'LotNumberList' -- IngredientLN
exec sp_FindStringInTable '10-2009-110-0' , 'dbo', 'MillSetup'  
exec sp_FindStringInTable '10-2009-110-0' , 'dbo', 'Precursor'  
exec sp_FindStringInTable '10-2009-110-0' , 'dbo', 'ProdConductivity' 
exec sp_FindStringInTable '10-2009-110-0' , 'dbo', 'SolventDrying'
exec sp_FindStringInTable '10-2009-110-0' , 'dbo', 'Weighing'  -- IngredientLN

-- INGREDIENT PART NUMBER
exec sp_FindStringInTable 'PP131-0000' , 'dbo', 'ConsumedBatch'  
exec sp_FindStringInTable 'PP131-0000' , 'dbo', 'LotNumberList'
exec sp_FindStringInTable 'PP131-0000' , 'dbo', 'MillSetup'  
exec sp_FindStringInTable 'PP131-0000' , 'dbo', 'Precursor'  
exec sp_FindStringInTable 'PP131-0000' , 'dbo', 'ProdConductivity' 
exec sp_FindStringInTable 'PP131-0000' , 'dbo', 'SolventDrying'
exec sp_FindStringInTable 'PP131-0000' , 'dbo', 'Weighing'  -- IngredientPN

/*
select * 
from weighing w inner join SolventDrying sd
on w.ingredientLN = sd.ingredientLN
order by 1

select * into Weighing from SP_ManuLine.dbo.Weighing
select * into SolventDrying from SP_ManuLine.dbo.SolventDrying
drop table solventdrying
select * into MillSetup from SP_ManuLine.dbo.MillSetup
select * into Precursor from SP_ManuLine.dbo.Precursor
select * into ProdConductivity from SP_ManuLine.dbo.ProdConductivity
select * into ConsumedBatch from SP_ManuLine.dbo.ConsumedBatch

select * into Weighing from admin.dbo.Weighing
select * into SolventDrying from admin.dbo.SolventDrying
select * into MillSetup from admin.dbo.MillSetup
select * into Precursor from admin.dbo.Precursor
select * into ProdConductivity from admin.dbo.ProdConductivity
select * into ConsumedBatch from admin.dbo.ConsumedBatch
*/

select * from weighing
select * from solventdrying
select * from millsetup
--update millsetup set millpn = _ProductpN
-- drop table millsetup
select * from precursor
select * from prodconductivity
select * from ConsumedBatch

--------------------------------------------------------------------------------
--12 8 2020

select * from MillSetup m inner join Weighing w on m.ProductLN = w.ProductLN where m.ProductLN != 'new entry'

-- update SolventDrying set IngredientLN = SPLotNR
select * from LotNumberList -- where LotNumber='35-2003-001'
select * from solventdrying where UpdateTime is null
select * from precursor where UpdateTime is null
select * from weighing
select * from millsetup
select * from prodconductivity
select * from ConsumedBatch

---- update ConsumedBatch set IngredientLN = LotNr
--update LotNumberList set GeneralLotNUmber = LotNumber
--update millsetup set IngredientLN = ProductLN
--update Precursor set IngredientLN = _SPLotNR
--update Precursor set IngredientPN = Partnr
--update ProdConductivity set ProductLN = lotnr
--update solvent set IngredientLN = SolventLN
--update solvent set IngredientPN = SolventPN
--update SolventDrying set IngredientlN = SPLotNr
--update solventdrying set IngredientPN = Partnr
--update solventdrying set IngredientName = Item

select * from lotnumberlist l inner join solventdrying s on l.lotnumber = s.IngredientLN -- relates
select * from lotnumberlist l inner join Precursor p on l.lotnumber = p.IngredientLN -- relates
select * from Weighing w inner join solventdrying s on w.IngredientLN = s.IngredientLN -- relates
select * from Weighing w inner join Precursor p on w.IngredientLN = p.IngredientLN -- relates
select m.msid, * from weighing w inner join millsetup m on w.productln = m.productln where m.productln != 'new entry' order by weighingid, m.msid --relates
select * from [dbo].[CombinedBatchRecord]

select * from INFORMATION_SCHEMA.COLUMNS where COLUMN_NAME = 'ProductLN' and TABLE_NAME not like 'vu%' order by TABLE_NAME
select * from INFORMATION_SCHEMA.COLUMNS where COLUMN_NAME like '%LN' and TABLE_NAME not like 'vu%' order by TABLE_NAME
select * from INFORMATION_SCHEMA.COLUMNS where COLUMN_NAME = 'ProductPN' and TABLE_NAME not like 'vu%' order by TABLE_NAME
select * from INFORMATION_SCHEMA.COLUMNS where COLUMN_NAME like '%PN' and TABLE_NAME not like 'vu%' order by TABLE_NAME

select * from weighing where ProductLN = '34-2010-308-0'
select * from combinedbatchrecord where OriginalLotNum = '34-2010-308-0'

select distinct cbr.* from weighing w
inner join CombinedBatchRecord cbr on w.ProductLN = cbr.OriginalLotNum
where ProductLN = '34-2010-308-0'

exec [usp_SearchForLotOrProductNumber] 'CG-2010-304-0' 
exec [usp_SearchForLotOrProductNumber] '10-2011-104-C'

select distinct _item from SolventDrying order by 1 
select * from SolventDrying 
select distinct IngredientName, count(IngredientName) from weighing group by IngredientName order by IngredientName
select * from SP_ManuLine.[dbo].[LotNrGenerationLocation]
[usp_SearchForLotOrProductNumber] 'ibib'

select * from weighing where ProductLN = '32-2011-305-0'
select distinct ingredientpn from weighing order by 1
select * from weighing where ProductLN = '34-2010-308-0'
select * from weighing where ProductLN like '34%'
select * from weighing where ProductLN like '32%'

select OriginalLotNum, count(OriginalLotNum) from [dbo].[CombinedBatchRecord] group by OriginalLotNum having count(OriginalLotNum)>1
select CombinedLotNum, count(CombinedLotNum) from [dbo].[CombinedBatchRecord] group by CombinedLotNum having count(CombinedLotNum)>1
select count(originallotnum), CombinedLotNum, count(CombinedLotNum) from [dbo].[CombinedBatchRecord] group by CombinedLotNum having count(CombinedLotNum)>1


select CombinedLotNum from [dbo].[CombinedBatchRecord] group by CombinedLotNum having count(CombinedLotNum)>1

select * from weighing w inner join CombinedBatchRecord cb on w.ProductLN = cb.OriginalLotNum 
-- where w.ProductLN = '34-2012-305-0'
order by w.ProductLN, cb.CombinedLotNum

select distinct w.ProductLN, cb.CombinedLotNum, w.IngredientLN, w.IngredientName 
from weighing w inner join CombinedBatchRecord cb on w.ProductLN = cb.OriginalLotNum 
where w.ProductLN =  '32-2011-305-0'  --'34-2012-305-0'

SELECT * from CombinedBatchRecord where CombinedLotNum =  'CG-2012-304-0'
select * from weighing where ProductLN like '32%'
select * from weighing where ProductLN = '35-2010-301-0'
select * from LotNumberList where LotNumber like '10%'

select substring(lotnumber, 1, 2), count(substring(lotnumber, 1, 2)), max(entrydate) from LotNumberList group by substring(lotnumber, 1, 2)
select substring(ProductPN, 1, 2), count(substring(ProductPN, 1, 2)), max(entrydate) from sp_manuline..weighing group by substring(ProductPN, 1, 2)

select * from SP_ManuLine.dbo.Weighing w
inner join SP_ManuLine.dbo.PartNumberList p on p.partnumber = w.IngredientpN
order by substring(w.ProductLN, 1, 2) desc

select *
from Weighing w
left outer join PartNumberList p on p.partnumber = w.IngredientPN
where w.ProductLN = '32-2012-301-0'
order by substring(w.ProductLN, 1, 2) desc

-- update sp_manuline.dbo.partnumberlist set partnumbername = trim(partnumbername)
-- update dbo.weighing set ingredientname = ltrim(rtrim(ingredientname))

select len(ltrim(rtrim(partnumbername))), * from sp_manuline.dbo.partnumberlist where partnumbername like 'Lithium Chloride%'
select len(ltrim(rtrim(partnumbername))), * from dbo.partnumberlist where partnumbername like 'Lithium Chloride%'

select w.IngredientName, len(w.IngredientName), p.partnumbername, len(p.partnumbername), *
from Weighing w
left outer join PartNumberList p on p.partnumbername = w.IngredientName
where w.ProductLN = '32-2012-301-0'
order by substring(w.ProductLN, 1, 2) desc

SELECT sum(ingredientactual) from weighing where ProductLN = '32-2012-301-0'

--- EXCEL FORMULATION TAB QUERIES
select p.PartNumberShortName, p.Density, 
	round( w.IngredientActual / p.Density, 2) Vol_ml, 
	round((round( w.IngredientActual / p.Density, 2) / VolPercentDivisor ) * 100, 2) as VolPercent,
	w.IngredientActual, 
	'  ' Spacer, *
from Weighing w
left outer join PartNumberList p 
	on p.partnumbername = w.IngredientName
inner join ( SELECT ProductLN, sum(ingredientactual) VolPercentDivisor from weighing where ProductLN = '32-2012-301-0' group by ProductLN ) 
	a on a.ProductLN = w.ProductLN
where w.ProductLN = '32-2012-301-0'
order by substring(w.ProductLN, 1, 2) desc

select * from SolventDrying where UpdateTime is null and notes like '%dried%' order by IngredientLN


-- CLINT'S FORMULATION PAGE QUERY ################################################################
select 
	p.PartNumberShortName, 
	p.Density, 
	round( w.IngredientActual / p.Density, 2) Vol_ml, 
	round((round( w.IngredientActual / p.Density, 2) / SumVol_Ml ) * 100, 2) as VolPercent,
	round((w.IngredientActual / a.SumIngredientActual) * 100, 2) ActualMassPercent,
	w.IngredientActual, 
	'  ' Spacer, *
from Weighing w
left outer join PartNumberList p 
	on p.partnumbername = w.IngredientName
inner join ( SELECT ProductLN, sum(ingredientactual) SumIngredientActual from weighing where ProductLN = '32-2012-301-0' group by ProductLN ) 
	a on a.ProductLN = w.ProductLN
inner join
	( 	select w.productLN, sum(round( w.IngredientActual / p.Density, 2)) as SumVol_Ml
	from Weighing w
	left outer join PartNumberList p 
		on p.partnumbername = w.IngredientName
	where w.ProductLN = '32-2012-301-0'
	group by w.ProductLN ) b
on b.ProductLN = w.ProductLN
where w.ProductLN = '32-2012-301-0'
order by substring(w.ProductLN, 1, 2) desc
--################################################################################################


select 
	isnull(PartNumberShortName, 'Totals') PartNumberShortName,
	sum(Density) Density,
	sum(Vol_ml) Vol_ml,
	sum(VolPercent) VolPercent,
	sum(ActualMassPercent) ActualMassPercent,
	sum(IngredientActual) IngredientActual
from (
select 
	p.PartNumberShortName, 
	p.Density, 
	round( w.IngredientActual / p.Density, 2) Vol_ml, 
	round((round( w.IngredientActual / p.Density, 2) / SumVol_Ml ) * 100, 2) as VolPercent,
	round((w.IngredientActual / a.SumIngredientActual) * 100, 2) ActualMassPercent,
	w.IngredientActual
	--, '  ' Spacer, *
from Weighing w
left outer join PartNumberList p 
	on p.partnumbername = w.IngredientName
inner join ( SELECT ProductLN, sum(ingredientactual) SumIngredientActual from weighing where ProductLN = '32-2012-301-0' group by ProductLN ) 
	a on a.ProductLN = w.ProductLN
inner join
	( 	select w.productLN, sum(round( w.IngredientActual / p.Density, 2)) as SumVol_Ml
	from Weighing w
	left outer join PartNumberList p 
		on p.partnumbername = w.IngredientName
	where w.ProductLN = '32-2012-301-0'
	group by w.ProductLN ) b
on b.ProductLN = w.ProductLN
where w.ProductLN = '32-2012-301-0'
) tbl
group by rollup (PartNumberShortName)
order by isnull(PartNumberShortName, 'Totals') 

DECLARE @output int;  
EXECUTE zDELME_countrows  '34-2011-313-0', @output = @output OUTPUT;
PRINT 'Count: ' +   convert(varchar(10),@output);  
------------------------------------------------------------------------------------------------

select * from weighing w inner join solventdrying sd on w.IngredientLN = sd.IngredientLN
where year(w.entrydate) = '2020' and month(w.entrydate) >= '8'
--and w.productln = '32-2012-305-0'
order by w.entrydate desc


select s.*, '~~~~' Spacer, cb.*, '~~~~' Spacer, pc.*
from solvent s
inner join ConsumedBatch cb on cb.ProductLN = s.Productln
inner join ProdConductivity pc on pc.ProductLN  = s.ProductLN
where s.ProductLN = '17-2010-112-0'
and s.updatetime is null
and pc.TestCondition = 'Ionic' 

select solventname, density from solvent s
inner join PartNumberList p on s.IngredientPN = p.partnumber
where ProductLN = '17-2012-101-0'

select IngredientActual * 2 as IngActDoubled, * from fn_formulation('99-2101-001-1')

select * from [dbo].[CombinedBatchRecord] where OriginalLotNum = '32-2012-306-0'
select * from weighing where ProductLN = '32-2012-306-0'
select * from [dbo].[ConsumedBatch] cb inner join CombinedBatchRecord cbr on cb.LotNr = cbr.OriginalLotNum
select * from [dbo].weighing w inner join CombinedBatchRecord cbr on w.ProductLN = cbr.OriginalLotNum
where w.ProductLN = '34-2012-324-0'


select *
from weighing w
inner join consumedbatch cb on w.Productln = cb.productLN
inner join prodconductivity pc on pc.productLN = w.productLN
where w.productln = '17-2010-108-0'
and pc.TestCondition = 'Electronic' -- 'Ionic'


select * from dev..ClintData c
left outer join weighing w on c.productln = w.productln
left outer join prodconductivity pc on c.productln = pc.productln
--left outer join SolventDrying sd on sd.IngredientLN = w.IngredientLN
left outer join precursor p on p.IngredientLN = w.IngredientLN
where pc.updatetime is null
and p.UpdateTime is null
and pc.TestCondition = 'electronic'
order by c.productln

select * from dev..ClintData c
left outer join weighing w on c.productln = w.productln
order by c.ProductLN

select * from dev..ClintData c
left outer join prodconductivity pc on c.productln = pc.productln
where pc.TestCondition = 'electronic' --'ionic'
and pc.UpdateTime is null
order by c.ProductLN

select * from SolventDrying
select * from precursor
select distinct substring(trim(productln), 0,3) from ProdConductivity
select * from solvent
select * from weighing


---------------------------------------------------------------------------------------------------------------------
usp_searchalltables '10-2101-104-0'
usp_searchalltables '10-2011-104-F'
usp_searchalltables 'CG-2012-308-0'
usp_searchalltables '10-2011-104A'
usp_searchalltables 'CG-2012-307-0'
select * from ProdConductivity
select * from SolventDrying
select * from Solvent
SELECT * from SolventDrying where IngredientLN = '10-2011-104-F'

select pc.* from Solvent s inner join SolventDrying sd on s.IngredientLN = sd.IngredientLN
inner join ProdConductivity pc on pc.ProductLN = s.ProductLN
where sd.IngredientLN = '10-2011-104-F'

select pc.* from Solvent s inner join SolventDrying sd on s.IngredientLN = sd.IngredientLN
inner join ProdConductivity pc on pc.ProductLN = s.ProductLN
inner join weighing w on w.productln = s.productln
where sd.IngredientLN = '10-2011-104-F'

select distinct w.productLN, w.IngredientName, w.IngredientActual, sd.KSolidMoisture_ppm, pc.ElectronicConductivity_Scm
from Solvent s inner join SolventDrying sd on s.IngredientLN = sd.IngredientLN
inner join ProdConductivity pc on pc.ProductLN = s.ProductLN
inner join weighing w on w.productln = s.productln
where sd.IngredientLN = '10-2011-104-F'


select distinct w.productLN, w.IngredientName, w.IngredientActual, sd.KSolidMoisture_ppm, pc.ElectronicConductivity_Scm
select *
from dev..ClintData cd left outer join Solvent s on cd.ProductLN = s.ProductLN
left outer join SolventDrying sd on s.IngredientLN = sd.IngredientLN
left outer join ProdConductivity pc on pc.ProductLN = s.ProductLN
left outer join weighing w on w.productln = s.productln

select *
from dev..ClintData cd
left outer join weighing w on w.ProductLN = cd.ProductLN


select 'clint', cd.*, 'Weighing' Tbl1, w.*, 'ProdCond' tbl2, pc.*, 'SolDry' tbl3, sd.*
select distinct cd.productLN, w.IngredientName, w.IngredientActual, sd.KSolidMoisture_ppm, pc.ElectronicConductivity_Scm
select *
from dev..ClintData cd
left outer join weighing w on cd.ProductLN = w.ProductLN
left outer join ProdConductivity pc on w.ProductLN = pc.ProductLN
left outer join SolventDrying sd on sd.IngredientLN = w.IngredientLN

select * 
from dev..ClintData cd
left outer join weighing w on cd.ProductLN = w.ProductLN
left outer join ProdConductivity pc on w.ProductLN = pc.ProductLN
left outer join SolventDrying sd on sd.IngredientLN = w.IngredientLN
where pc.UpdateTime is null
and pc.TestCondition = 'ionic'

select originallotNum, count(originallotNum) from CombinedBatchRecord group by originallotNum order by 2 desc

select * 
from dev..ClintData cd
left outer join weighing w on cd.ProductLN = w.ProductLN

select distinct w.ProductLN, cbr.OriginalLotNum, cbr.CombinedLotNum from weighing w 
left outer join [dbo].[CombinedBatchRecord] cbr on w.ProductLN = cbr.OriginalLotNum

select w.ProductLN, cbr.OriginalLotNum, cbr.CombinedLotNum 
from ( select distinct productLN from weighing where ProductLN is not null ) w
left outer join CombinedBatchRecord cbr on w.ProductLN = cbr.OriginalLotNum
where cbr.OriginalLotNum is not null
order by cbr.OriginalLotNum, cbr.CombinedLotNum

-- multiple ingredientLN for different productLN
select productLN, ingredientLN, count(ingredientLN) from weighing group by IngredientLN, productln order by 4 desc