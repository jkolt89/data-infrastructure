USE [master]
GO
/****** Object:  Database [SolidPowerDev]    Script Date: 9/5/2019 19:13:52 ******/
CREATE DATABASE [SolidPowerDev]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SolidPowerDev', FILENAME = N'/var/opt/mssql/data/SolidPowerDev.mdf' , SIZE = 54836096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 260096KB )
 LOG ON 
( NAME = N'SolidPowerDev_log', FILENAME = N'/var/opt/mssql/data/SolidPowerDev_log.ldf' , SIZE = 2236416KB , MAXSIZE = 2048GB , FILEGROWTH = 131072KB )
GO
ALTER DATABASE [SolidPowerDev] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SolidPowerDev].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SolidPowerDev] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SolidPowerDev] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SolidPowerDev] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SolidPowerDev] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SolidPowerDev] SET ARITHABORT OFF 
GO
ALTER DATABASE [SolidPowerDev] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SolidPowerDev] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SolidPowerDev] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SolidPowerDev] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SolidPowerDev] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SolidPowerDev] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SolidPowerDev] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SolidPowerDev] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SolidPowerDev] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SolidPowerDev] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SolidPowerDev] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SolidPowerDev] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SolidPowerDev] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SolidPowerDev] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SolidPowerDev] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SolidPowerDev] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SolidPowerDev] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SolidPowerDev] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SolidPowerDev] SET  MULTI_USER 
GO
ALTER DATABASE [SolidPowerDev] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SolidPowerDev] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SolidPowerDev] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SolidPowerDev] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [SolidPowerDev] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'SolidPowerDev', N'ON'
GO
ALTER DATABASE [SolidPowerDev] SET QUERY_STORE = OFF
GO
USE [SolidPowerDev]
GO
/****** Object:  User [spuser]    Script Date: 9/5/2019 19:13:53 ******/
CREATE USER [spuser] FOR LOGIN [spuser] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [solidpower]    Script Date: 9/5/2019 19:13:53 ******/
CREATE USER [solidpower] FOR LOGIN [solidpower] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [spuser]
GO
ALTER ROLE [db_datareader] ADD MEMBER [solidpower]
GO
/****** Object:  Table [dbo].[ChannelNormal]    Script Date: 9/5/2019 19:13:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChannelNormal](
	[CNTID] [int] IDENTITY(1,1) NOT NULL,
	[CellName] [varchar](250) NOT NULL,
	[Test_ID] [int] NOT NULL,
	[Data_Point] [int] NOT NULL,
	[Test_Time] [float] NULL,
	[Step_Time] [float] NULL,
	[Date_Time] [float] NULL,
	[Step_Index] [smallint] NULL,
	[Cycle_Index] [int] NULL,
	[Is_FC_Data] [tinyint] NULL,
	[Current] [real] NULL,
	[Voltage] [real] NULL,
	[Charge_Capacity] [float] NULL,
	[Discharge_Capacity] [float] NULL,
	[Charge_Energy] [float] NULL,
	[Discharge_Energy] [float] NULL,
	[dVdT] [real] NULL,
	[Internal_Resistance] [real] NULL,
	[AC_Impedance] [real] NULL,
	[ACI_Phase_Angle] [real] NULL,
	[EntryDate] [datetime] NULL,
	[Cell_ID] [int] NULL,
	[ArbinDate] [date] NULL,
	[ArbinTime] [time](2) NULL,
	[CTD_ID] [varchar](80) NULL,
 CONSTRAINT [PK_ChannelNormalAll] PRIMARY KEY CLUSTERED 
(
	[CNTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cell]    Script Date: 9/5/2019 19:13:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cell](
	[CellID] [int] IDENTITY(1,1) NOT NULL,
	[CellName] [varchar](250) NOT NULL,
	[EntryDate] [datetime] NOT NULL,
	[Test_ID] [int] NULL,
	[Test_Name] [nvarchar](250) NULL,
	[Channel_Index] [smallint] NOT NULL,
	[Start_DateTime] [float] NULL,
	[DAQ_Index] [smallint] NULL,
	[Channel_Type] [smallint] NULL,
	[Creator] [nvarchar](250) NULL,
	[Comments] [nvarchar](250) NULL,
	[Schedule_File_Name] [nvarchar](250) NULL,
	[Channel_Number] [smallint] NULL,
	[Software_Version] [nvarchar](250) NULL,
	[Serial_Number] [nvarchar](250) NULL,
	[Schedule_Version] [nvarchar](250) NULL,
	[MASS] [real] NULL,
	[Specific_Capacity] [real] NULL,
	[Capacity] [real] NULL,
	[BuiltInVoltCount] [int] NULL,
	[Item_ID] [nvarchar](250) NULL,
 CONSTRAINT [PK_Cell] PRIMARY KEY CLUSTERED 
(
	[CellID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vuBatteryStatistics]    Script Date: 9/5/2019 19:13:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE view [dbo].[vuBatteryStatistics] as
-- select * from vuBatteryStatistics


SELECT
   cn.CellName,
   cn.Data_Point,
   --MAX(cn.ArbinDate) AS ArbinDate,
   --CAST(CAST(cn.ArbinTime AS VARCHAR(5)) AS TIME(0)) ArbinTimeMinute,
   --MAX(YEAR(cn.ArbinDate)) YearNr,
   --MAX(MONTH(cn.ArbinDate)) AS MonthNr,
   --MAX(DAY(cn.ArbinDate)) AS DayNr,
   --CAST(ArbinTime AS VARCHAR(2)) ArbinHour,
   --SUBSTRING(CAST( ArbinTime AS VARCHAR(5)), 4, 5) AS ArbinMin,

   cn.Cycle_Index,
   c.Mass,
   -- SPECIFIC CHARGE AND DISCHARGE CAPACITY
   MAX(cn.charge_capacity) / c.mass AS SpecificMaxChargeCapacity,
   MAX(cn.Discharge_capacity) / c.mass AS SpecificMaxDischargeCapacity,
   max(cn.Charge_Energy)/c.mass as SpecificMaxEnergyCapacity,
   max(cn.Discharge_Energy)/c.mass as SpecificMinEnergyCapacity,
   -- CHARGE CAPACITY
   AVG(cn.charge_capacity) AvgChargeCapacity,
   MAX(cn.charge_capacity) MaxChargeCapacity,
   -- DISCHARGE CAPACITY
   MAX(cn.discharge_capacity) MaxDischargeCapacity,
   --EFFICIENCY
   MAX(cn.Discharge_Capacity) / MAX(cn.Charge_Capacity) AS Efficiency,
   MAX(cn.Charge_Capacity) / MAX(cn.Discharge_Capacity) AS InverseEfficiency,
   -- VOLTAGE
   MAX(cn.Voltage) AS MaxChargeVoltage,
   MIN(cn.Voltage) AS MinChargeVoltage,
   AVG(Voltage) AvgChargeVoltage,
   --STDEV(Voltage) StdevVoltage, 	-- var(Voltage) VarVoltage
   -- CHARGE / DISCHARGE ENERGY
   max(charge_energy) as MaxChargeEnergy,
   max(discharge_energy) as MaxDischargeEnergy
   --, c.mass(
FROM
   ChannelNormal cn 
   INNER JOIN
      Cell c 
      ON c.CellName = cn.CellName 
WHERE
   c.mass > 0 
   AND cn.Charge_Capacity > 0 
   AND cn.Discharge_Capacity > 0
   --and cn.cellName ='190517_a'
GROUP BY
   cn.CellName,
   cn.Cycle_Index,
   cn.Data_Point,
   c.mass
   --, CAST(CAST(cn.ArbinTime AS VARCHAR(5)) AS TIME(0)),
   --CAST(ArbinTime AS VARCHAR(2)),
   --SUBSTRING(CAST( ArbinTime AS VARCHAR(5)), 4, 5) 
--ORDER BY
--   cn.CellName,
--   cn.Cycle_Index


-- ) a


GO
/****** Object:  View [dbo].[vuCycleStatistic]    Script Date: 9/5/2019 19:13:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create View [dbo].[vuCycleStatistic]
as
select CellName, Cycle_Index, Data_Point, 'Cycle_Index' as Measurement, Cycle_Index as Value
	from vuBatteryStatistics
	
	union all
	select CellName, Cycle_Index, Data_Point, 'Mass' as Measurement, Mass as Value
	from vuBatteryStatistics
	
	union all
	select CellName, Cycle_Index, Data_Point, 'SpecificMaxChargeCapacity' as Measurement, SpecificMaxChargeCapacity as Value
	from vuBatteryStatistics
	
	union all
	select CellName, Cycle_Index, Data_Point, 'SpecificMaxDischargeCapacity' as Measurement, SpecificMaxDischargeCapacity as Value
	from vuBatteryStatistics
	
	union all
	select CellName, Cycle_Index, Data_Point, 'SpecificMaxEnergyCapacity' as Measurement, SpecificMaxEnergyCapacity as Value
	from vuBatteryStatistics
	
	union all
	select CellName, Cycle_Index, Data_Point, 'SpecificMinEnergyCapacity' as Measurement, SpecificMinEnergyCapacity as Value
	from vuBatteryStatistics
	
	union all
	select CellName, Cycle_Index, Data_Point, 'AvgChargeCapacity' as Measurement, AvgChargeCapacity as Value
	from vuBatteryStatistics
	
	union all
	select CellName, Cycle_Index, Data_Point, 'MaxChargeCapacity' as Measurement, MaxChargeCapacity as Value
	from vuBatteryStatistics
	
	union all
	select CellName, Cycle_Index, Data_Point, 'MaxDischargeCapacity' as Measurement, MaxDischargeCapacity as Value
	from vuBatteryStatistics
	
	union all
	select CellName, Cycle_Index, Data_Point, 'Efficiency' as Measurement, Efficiency as Value
	from vuBatteryStatistics
	
	union all
	select CellName, Cycle_Index, Data_Point, 'InverseEfficiency' as Measurement, InverseEfficiency as Value
	from vuBatteryStatistics
	
	union all
	select CellName, Cycle_Index, Data_Point, 'MaxChargeVoltage' as Measurement, MaxChargeVoltage as Value
	from vuBatteryStatistics
	
	union all
	select CellName, Cycle_Index, Data_Point, 'MinChargeVoltage' as Measurement, MinChargeVoltage as Value
	from vuBatteryStatistics
	
	union all
	select CellName, Cycle_Index, Data_Point, 'AvgChargeVoltage' as Measurement, AvgChargeVoltage as Value
	from vuBatteryStatistics
	
	union all
	select CellName, Cycle_Index, Data_Point, 'MaxChargeEnergy' as Measurement, MaxChargeEnergy as Value
	from vuBatteryStatistics
	
	union all
	select CellName, Cycle_Index, Data_Point, 'MaxDischargeEnergy' as Measurement, MaxDischargeEnergy as Value
	from vuBatteryStatistics
	
GO
/****** Object:  View [dbo].[vuDqDv]    Script Date: 9/5/2019 19:13:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE view [dbo].[vuDqDv] as
with dqdv_cte(cCNTID, pCNTID, Cycle_Index, DeltaChargeCapacity, DeltaDischargeCapacity, DeltaVoltage, cCellName, cTest_ID, cData_Point, cVoltage, cCharge_Capacity,
				pCellName, pTest_ID, pData_Point, pVoltage, pCharge_Capacity)
as (
select 
	cn.CNTID cCNTID, pr.CNTID pCNTID, cn.Cycle_Index, 
	cn.Charge_Capacity - pr.Charge_Capacity DeltaChargeCapacity, 
	cn.Discharge_Capacity - pr.Charge_Capacity DeltaDischargeCapacity,
	cn.Voltage - pr.Voltage DeltaVoltage,
	cn.CellName cCellName, cn.Test_ID cTest_ID, cn.Data_Point cData_Point, cn.Voltage cVoltage, cn.Charge_Capacity cCharge_Capacity,
	pr.CellName pCellName, pr.Test_ID pTest_ID, pr.Data_Point pData_Point, pr.Voltage pVoltage, pr.Charge_Capacity pCharge_Capacity
from [ChannelNormal] cn
inner join [dbo].[ChannelNormal] pr
	on pr.CellName = cn.CellName
	and pr.Test_ID = cn.Test_ID
	and pr.Data_Point = cn.Data_Point-1
where pr.Charge_Capacity != 0
	and cn.Voltage - pr.Voltage  != 0
	-- FOR TESTING PURPOSES ONLY. REMOVE NEXT LINE FOR PROD
	--and cn.CellName = '180208_b'
	--and cn.Cycle_Index = @cycle_index
)
select -- top 100 percent 
	cCellName CellName, cTest_ID Test_ID, Cycle_Index, cData_Point Data_Point, DeltaChargeCapacity/DeltaVoltage dqdvCharge,
	DeltaDischargeCapacity/DeltaVoltage dqdvDischarge,
	DeltaChargeCapacity, DeltaDischargeCapacity, DeltaVoltage
from dqdv_cte
--order by ccellname, ctest_ID, cdata_point;
GO
/****** Object:  View [dbo].[vuDqDvColumnStore]    Script Date: 9/5/2019 19:13:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[vuDqDvColumnStore]
as
-- select * from vuDqDvColumnStore where cellname IN ('2019_0016c_PC_H') 
select CellName, Test_ID, Cycle_Index, Data_Point, 'Data_Point' as Measurement, Data_Point as Value
from vuDqDv
union all
select CellName, Test_ID, Cycle_Index, Data_Point, 'dqdvCharge' as Measurement, dqdvCharge as Value
from vuDqDv
union all
select CellName, Test_ID, Cycle_Index, Data_Point, 'dqdvDischarge' as Measurement, dqdvDischarge as Value
from vuDqDv
union all
select CellName, Test_ID, Cycle_Index, Data_Point, 'DeltaChargeCapacity' as Measurement, DeltaChargeCapacity as Value
from vuDqDv
union all
select CellName, Test_ID, Cycle_Index, Data_Point, 'DeltaDischargeCapacity' as Measurement, DeltaDischargeCapacity as Value
from vuDqDv
union all
select CellName, Test_ID, Cycle_Index, Data_Point, 'DeltaVoltage' as Measurement, DeltaVoltage as Value
from vuDqDv

GO
/****** Object:  View [dbo].[vuChannelNormalPivot]    Script Date: 9/5/2019 19:13:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- select * from vuChannelNormalPivot
CREATE view [dbo].[vuChannelNormalPivot] as
select top 100 percent * from (
select CellName, Cycle_Index, Test_Time, 'Voltage' as Measurement, Voltage as [Value] from ChannelNormal -- where CellName in (select distinct top 10 CellName from Cell order by CellName) 
union all
select CellName, Cycle_Index, Test_Time, 'Current' as Measurement, [Current] from ChannelNormal -- where CellName in (select distinct top 10 CellName from Cell order by CellName) 
union all
select CellName, Cycle_Index, Test_Time, 'Internal_Resistance' as Measurement, Internal_Resistance from ChannelNormal -- where CellName in (select distinct top 10 CellName from Cell order by CellName) 
union all
select CellName, Cycle_Index, Test_Time, 'dVdT' as Measurement, dVdT from ChannelNormal -- where CellName in (select distinct top 10 CellName from Cell order by CellName) 
union all
select CellName, Cycle_Index, Test_Time, 'Charge_Capacity' as Measurement, Charge_Capacity from ChannelNormal -- where CellName in (select distinct top 10 CellName from Cell order by CellName) 
union all
select CellName, Cycle_Index, Test_Time, 'Discharge_Capacity' as Measurement, Discharge_Capacity from ChannelNormal -- where CellName in (select distinct top 10 CellName from Cell order by CellName) 
union all
select CellName, Cycle_Index, Test_Time, 'Charge_Energy' as Measurement, Charge_Energy from ChannelNormal -- where CellName in (select distinct top 10 CellName from Cell order by CellName) 
union all
select CellName, Cycle_Index, Test_Time, 'Discharge_Energy' as Measurement, Discharge_Energy from ChannelNormal -- where CellName in (select distinct top 10 CellName from Cell order by CellName) 
) a
-- where CellName in (select distinct top 10 CellName from Cell order by CellName) 
where CellName in (select top 10 CellName from ChannelNormal where cycle_index = 5 group by CellName )-- having count(cycle_index)<6 
and Cycle_Index <= 10
order by CellName, Cycle_Index, Test_Time
GO
/****** Object:  Table [dbo].[CellLog]    Script Date: 9/5/2019 19:13:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CellLog](
	[SCL_ID] [int] NOT NULL,
	[Cell #] [nvarchar](255) NULL,
	[# of cycles completed] [float] NULL,
	[1st cycle CE (%)] [float] NULL,
	[1st cycle charge] [float] NULL,
	[1st cycle discharge] [float] NULL,
	[Anode] [nvarchar](255) NULL,
	[Anode Mass (g)] [nvarchar](255) NULL,
	[Arbin Notes] [nvarchar](255) NULL,
	[Cage torque (in-lbs)] [nvarchar](255) NULL,
	[Capacity (mAh)] [nvarchar](255) NULL,
	[Cathode] [nvarchar](255) NULL,
	[Cathode Pressure (tons)] [nvarchar](255) NULL,
	[Channel] [nvarchar](255) NULL,
	[Current Collector] [nvarchar](255) NULL,
	[Cycle # of stoppage] [nvarchar](255) NULL,
	[Die Diameter (1#3 or 1#6 cm)] [nvarchar](255) NULL,
	[Excel Time] [float] NULL,
	[Initial Resistance (ohms)] [float] NULL,
	[Last DateTime] [nvarchar](255) NULL,
	[Loading (mg/cm^2)] [nvarchar](255) NULL,
	[Location] [nvarchar](255) NULL,
	[Maximum Stack Pressure (tons)] [nvarchar](255) NULL,
	[Notes] [nvarchar](255) NULL,
	[Operator] [nvarchar](255) NULL,
	[Purpose/Description] [nvarchar](255) NULL,
	[Python Time] [nvarchar](255) NULL,
	[Reason for stoppage] [nvarchar](255) NULL,
	[Separator] [nvarchar](255) NULL,
	[Separator Mass (g)] [nvarchar](255) NULL,
	[Total Cathode Mass (g)] [nvarchar](255) NULL,
	[active cathode] [nvarchar](255) NULL,
	[active cathode date coated] [nvarchar](255) NULL,
	[active material theoretical capacity (mAh/g)] [nvarchar](255) NULL,
	[cathode particle coating] [nvarchar](255) NULL,
	[cell theoretical capacity (mAh/g)] [nvarchar](255) NULL,
	[die] [nvarchar](255) NULL,
	[electrolyte] [nvarchar](255) NULL,
	[max voltage] [nvarchar](255) NULL,
	[max voltage w/ outlier] [float] NULL,
	[max voltage w/o outlier] [float] NULL,
	[maximum discharge capacity] [nvarchar](255) NULL,
	[nominal percent active material] [nvarchar](255) NULL,
	[ratio active material] [nvarchar](255) NULL,
	[temperature] [nvarchar](255) NULL,
	[wt% coating] [nvarchar](255) NULL,
 CONSTRAINT [PK__CellLog__2F58417B6943A24A] PRIMARY KEY CLUSTERED 
(
	[SCL_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CellMasterList]    Script Date: 9/5/2019 19:13:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CellMasterList](
	[CML_ID] [int] IDENTITY(1,1) NOT NULL,
	[CellName] [varchar](250) NOT NULL,
	[Test_ID] [int] NOT NULL,
	[Data_Point] [int] NOT NULL,
	[EntryDate] [datetime] NOT NULL,
 CONSTRAINT [PK_CellMasterList] PRIMARY KEY CLUSTERED 
(
	[CML_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChannelNormal2019]    Script Date: 9/5/2019 19:13:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChannelNormal2019](
	[CNTID] [int] IDENTITY(1,1) NOT NULL,
	[CellName] [varchar](250) NOT NULL,
	[Test_ID] [int] NOT NULL,
	[Data_Point] [int] NOT NULL,
	[Test_Time] [float] NULL,
	[Step_Time] [float] NULL,
	[Date_Time] [float] NULL,
	[Step_Index] [smallint] NULL,
	[Cycle_Index] [int] NULL,
	[Is_FC_Data] [tinyint] NULL,
	[Current] [real] NULL,
	[Voltage] [real] NULL,
	[Charge_Capacity] [float] NULL,
	[Discharge_Capacity] [float] NULL,
	[Charge_Energy] [float] NULL,
	[Discharge_Energy] [float] NULL,
	[dVdT] [real] NULL,
	[Internal_Resistance] [real] NULL,
	[AC_Impedance] [real] NULL,
	[ACI_Phase_Angle] [real] NULL,
	[EntryDate] [datetime] NULL,
	[Cell_ID] [int] NULL,
	[ArbinDate] [date] NULL,
	[ArbinTime] [time](2) NULL,
	[CTD_ID] [varchar](80) NULL,
 CONSTRAINT [PK_ChannelNormal2018] PRIMARY KEY CLUSTERED 
(
	[CNTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChannelNormalTable]    Script Date: 9/5/2019 19:13:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChannelNormalTable](
	[CNTID] [int] IDENTITY(1,1) NOT NULL,
	[CellName] [varchar](250) NULL,
	[Test_ID] [int] NULL,
	[Data_Point] [int] NULL,
	[Test_Time] [float] NULL,
	[Step_Time] [float] NULL,
	[Date_Time] [float] NULL,
	[Step_Index] [smallint] NULL,
	[Cycle_Index] [int] NULL,
	[Is_FC_Data] [tinyint] NULL,
	[Current] [real] NULL,
	[Voltage] [real] NULL,
	[Charge_Capacity] [float] NULL,
	[Discharge_Capacity] [float] NULL,
	[Charge_Energy] [float] NULL,
	[Discharge_Energy] [float] NULL,
	[dVdT] [real] NULL,
	[Internal_Resistance] [real] NULL,
	[AC_Impedance] [real] NULL,
	[ACI_Phase_Angle] [real] NULL,
	[PulseStageIndex] [int] NULL,
	[PulseStageTime] [real] NULL,
	[ACR] [real] NULL,
	[TC_Counter1] [int] NULL,
	[TC_Counter2] [int] NULL,
	[TC_Counter3] [int] NULL,
	[TC_Counter4] [int] NULL,
	[EntryDate] [datetime] NULL,
 CONSTRAINT [PK__ChannelN__NormalTbl] PRIMARY KEY CLUSTERED 
(
	[CNTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChannelStatistic]    Script Date: 9/5/2019 19:13:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChannelStatistic](
	[CSTID] [int] IDENTITY(1,1) NOT NULL,
	[CellName] [varchar](250) NULL,
	[Test_ID] [int] NULL,
	[Data_Point] [int] NULL,
	[Vmax_On_Cycle] [real] NULL,
	[Charge_Time] [float] NULL,
	[Discharge_Time] [float] NULL,
	[EntryDate] [datetime] NULL,
 CONSTRAINT [PK_ChannelStatiicTable] PRIMARY KEY CLUSTERED 
(
	[CSTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChannelStatisticTable]    Script Date: 9/5/2019 19:13:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChannelStatisticTable](
	[CSTID] [int] IDENTITY(1,1) NOT NULL,
	[CellName] [varchar](250) NULL,
	[Test_ID] [int] NULL,
	[Data_Point] [int] NULL,
	[Vmax_On_Cycle] [real] NULL,
	[Charge_Time] [float] NULL,
	[Discharge_Time] [float] NULL,
	[EntryDate] [datetime] NULL,
 CONSTRAINT [PK_ChannelStatisticTable] PRIMARY KEY CLUSTERED 
(
	[CSTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CycleBasedStatistic]    Script Date: 9/5/2019 19:13:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CycleBasedStatistic](
	[CBS_ID] [int] IDENTITY(1,1) NOT NULL,
	[CellName] [varchar](250) NOT NULL,
	[Cycle_Index] [int] NULL,
	[Mass] [real] NULL,
	[SpecificMaxChargeCapacity] [float] NULL,
	[SpecificMaxDischargeCapacity] [float] NULL,
	[SpecificMaxEnergyCapacity] [float] NULL,
	[SpecificMinEnergyCapacity] [float] NULL,
	[AvgChargeCapacity] [float] NULL,
	[MaxChargeCapacity] [float] NULL,
	[MaxDischargeCapacity] [float] NULL,
	[Efficiency] [float] NULL,
	[InverseEfficiency] [float] NULL,
	[MaxChargeVoltage] [real] NULL,
	[MinChargeVoltage] [real] NULL,
	[AvgChargeVoltage] [float] NULL,
	[MaxChargeEnergy] [float] NULL,
	[MaxDischargeEnergy] [float] NULL,
	[ChargeRate] [float] NULL,
	[DischargeRate] [float] NULL,
	[EntryDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CBS_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CycleBasedStatisticStage]    Script Date: 9/5/2019 19:13:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CycleBasedStatisticStage](
	[CBS_ID] [int] IDENTITY(1,1) NOT NULL,
	[CellName] [varchar](250) NOT NULL,
	[Cycle_Index] [int] NULL,
	[Mass] [real] NULL,
	[SpecificMaxChargeCapacity] [float] NULL,
	[SpecificMaxDischargeCapacity] [float] NULL,
	[SpecificMaxEnergyCapacity] [float] NULL,
	[SpecificMinEnergyCapacity] [float] NULL,
	[AvgChargeCapacity] [float] NULL,
	[MaxChargeCapacity] [float] NULL,
	[MaxDischargeCapacity] [float] NULL,
	[Efficiency] [float] NULL,
	[InverseEfficiency] [float] NULL,
	[MaxChargeVoltage] [real] NULL,
	[MinChargeVoltage] [real] NULL,
	[AvgChargeVoltage] [float] NULL,
	[MaxChargeEnergy] [float] NULL,
	[MaxDischargeEnergy] [float] NULL,
	[ChargeRate] [float] NULL,
	[DischargeRate] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[CBS_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Global_Table]    Script Date: 9/5/2019 19:13:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Global_Table](
	[CellName] [varchar](250) NULL,
	[Test_ID] [int] NULL,
	[Test_Name] [nvarchar](250) NULL,
	[Channel_Index] [smallint] NULL,
	[Start_DateTime] [float] NULL,
	[DAQ_Index] [smallint] NULL,
	[Channel_Type] [smallint] NULL,
	[Creator] [nvarchar](250) NULL,
	[Comments] [nvarchar](250) NULL,
	[Schedule_File_Name] [nvarchar](250) NULL,
	[Channel_Number] [smallint] NULL,
	[Software_Version] [nvarchar](250) NULL,
	[Serial_Number] [nvarchar](250) NULL,
	[Schedule_Version] [nvarchar](250) NULL,
	[MASS] [real] NULL,
	[Specific_Capacity] [real] NULL,
	[Capacity] [real] NULL,
	[Item_ID] [nvarchar](250) NULL,
	[EntryDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_CellName_Test_ID_Data_Point]    Script Date: 9/5/2019 19:13:54 ******/
CREATE NONCLUSTERED INDEX [IX_CellName_Test_ID_Data_Point] ON [dbo].[CellMasterList]
(
	[CellName] ASC,
	[Test_ID] ASC,
	[Data_Point] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_CellNameChargeCapDischargCap]    Script Date: 9/5/2019 19:13:54 ******/
CREATE NONCLUSTERED INDEX [IX_CellNameChargeCapDischargCap] ON [dbo].[ChannelNormal]
(
	[CellName] ASC,
	[Charge_Capacity] ASC,
	[Discharge_Capacity] ASC
)
INCLUDE([Cycle_Index],[Voltage],[Charge_Energy],[Discharge_Energy],[ArbinDate],[ArbinTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_cellNameTestIDDataPoint]    Script Date: 9/5/2019 19:13:54 ******/
CREATE NONCLUSTERED INDEX [IX_cellNameTestIDDataPoint] ON [dbo].[ChannelNormal]
(
	[CellName] ASC,
	[Test_ID] ASC,
	[Data_Point] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_ChannelNormalAll]    Script Date: 9/5/2019 19:13:54 ******/
CREATE NONCLUSTERED INDEX [IX_ChannelNormalAll] ON [dbo].[ChannelNormal]
(
	[CellName] ASC,
	[Test_ID] ASC,
	[Data_Point] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Cell] ADD  CONSTRAINT [DF_Cell_EntryDate]  DEFAULT (getdate()) FOR [EntryDate]
GO
ALTER TABLE [dbo].[CellMasterList] ADD  CONSTRAINT [DF_CellMasterList_EntryDate]  DEFAULT (getdate()) FOR [EntryDate]
GO
ALTER TABLE [dbo].[ChannelNormal2019] ADD  CONSTRAINT [DF_ChannelNormalTable1_EntryDate2018]  DEFAULT (getdate()) FOR [EntryDate]
GO
ALTER TABLE [dbo].[ChannelNormalTable] ADD  CONSTRAINT [DF_ChannelNormalTable1_EntryDate]  DEFAULT (getdate()) FOR [EntryDate]
GO
ALTER TABLE [dbo].[ChannelStatistic] ADD  CONSTRAINT [DF_ChannelStisticTable_EntryDate]  DEFAULT (getdate()) FOR [EntryDate]
GO
ALTER TABLE [dbo].[ChannelStatisticTable] ADD  CONSTRAINT [DF_ChannelStatisticTable1_EntryDate]  DEFAULT (getdate()) FOR [EntryDate]
GO
ALTER TABLE [dbo].[CycleBasedStatistic] ADD  CONSTRAINT [DF_CycleBasedStatistic_EntryDate]  DEFAULT (getdate()) FOR [EntryDate]
GO
ALTER TABLE [dbo].[Global_Table] ADD  CONSTRAINT [DF_Global_Table_EntryDate]  DEFAULT (getdate()) FOR [EntryDate]
GO
/****** Object:  StoredProcedure [dbo].[getCellLog]    Script Date: 9/5/2019 19:13:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec getCellLog 'hasp_cp_190819_b'
CREATE PROCEDURE [dbo].[getCellLog]
	--@cellname varchar(250) = 'hasp_cp_190819_b'
	@cellname varchar(250) = 'ALL'
AS
BEGIN
	if @cellname != 'ALL'
		SELECT 
			[SCL_ID], [Cell #] as CellName, [# of cycles completed], [1st cycle CE (%)], [1st cycle charge], [1st cycle discharge], 
			[Anode], [Anode Mass (g)], [Arbin Notes], [Cage torque (in-lbs)], [Capacity (mAh)], [Cathode], [Cathode Pressure (tons)], [Channel], 
			[Current Collector], [Cycle # of stoppage], [Die Diameter (1#3 or 1#6 cm)], [Excel Time], [Initial Resistance (ohms)], [Last DateTime], 
			[Loading (mg/cm^2)], [Location], [Maximum Stack Pressure (tons)], [Notes], [Operator], [Purpose/Description], [Python Time], 
			[Reason for stoppage], [Separator], [Separator Mass (g)], [Total Cathode Mass (g)], [active cathode], 
			[active cathode date coated], [active material theoretical capacity (mAh/g)], [cathode particle coating], 
			[cell theoretical capacity (mAh/g)], [die], [electrolyte], [max voltage], [max voltage w/ outlier], [max voltage w/o outlier], 
			[maximum discharge capacity], [nominal percent active material], [ratio active material], [temperature], [wt% coating]
		from celllog 
		where [Cell #] = @cellname
	else
		select
		[SCL_ID], [Cell #] as CellName, [# of cycles completed], [1st cycle CE (%)], [1st cycle charge], [1st cycle discharge], 
		[Anode], [Anode Mass (g)], [Arbin Notes], [Cage torque (in-lbs)], [Capacity (mAh)], [Cathode], [Cathode Pressure (tons)], [Channel], 
		[Current Collector], [Cycle # of stoppage], [Die Diameter (1#3 or 1#6 cm)], [Excel Time], [Initial Resistance (ohms)], [Last DateTime], 
		[Loading (mg/cm^2)], [Location], [Maximum Stack Pressure (tons)], [Notes], [Operator], [Purpose/Description], [Python Time], 
		[Reason for stoppage], [Separator], [Separator Mass (g)], [Total Cathode Mass (g)], [active cathode], 
		[active cathode date coated], [active material theoretical capacity (mAh/g)], [cathode particle coating], 
		[cell theoretical capacity (mAh/g)], [die], [electrolyte], [max voltage], [max voltage w/ outlier], [max voltage w/o outlier], 
		[maximum discharge capacity], [nominal percent active material], [ratio active material], [temperature], [wt% coating]
		from celllog
END
GO
/****** Object:  StoredProcedure [dbo].[getSelectFromCell]    Script Date: 9/5/2019 19:13:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[getSelectFromCell]
	@CellName varchar(100) = 'All'
AS
BEGIN

	SET NOCOUNT ON;
	if @Cellname != 'All'
		select * from Cell where CellName = @CellName
	else
		select * from Cell
    
END
GO
/****** Object:  StoredProcedure [dbo].[sp_getvoltprofile]    Script Date: 9/5/2019 19:13:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_getvoltprofile]
	@CellName varchar(100) = 'All', 
	@CycleIndex int = 0		-- Passing 0 will return all Cell Cycles
	-- , @orderby = 'Test_Time'
AS
-- ===========================================================================================
-- TEST EXAMPLES:
-- exec sp_getvoltprofile
-- exec sp_getvoltprofile '190228_a'
-- exec sp_getvoltprofile '190228_a', 2
-- exec sp_getvoltprofile 'All', 2
-- ===========================================================================================
-- EXAMPLE OF EXPORTING DATA TO A .csv FILE USING BCP:
-- declare @sql varchar(8000)
-- select @sql = 'bcp "select * from EmailVarification..tblTransaction" queryout c:\bcp\Tom.xls -c -t, -T -S' + @@servername
-- exec master..xp_cmdshell @sql
-- https://www.red-gate.com/simple-talk/sql/database-administration/working-with-the-bcp-command-line-utility/
-- https://www.red-gate.com/simple-talk/sql/database-administration/creating-csv-files-using-bcp-and-stored-procedures/
-- https://www.codeproject.com/Questions/546777/HowplusToplusExportplusDataplusToplustheplus-csvpl
-- ===========================================================================================
BEGIN
	SET NOCOUNT ON;

	-- load All Cells, All Cycles
	if @CellName = 'All' and @CycleIndex = 0
		BEGIN
		SELECT	
			CellName, Data_Point, Test_Time, Step_Time, Date_Time, Step_Index, Cycle_Index, [Current], Voltage, Charge_Capacity, Discharge_Capacity, 
			Charge_Energy, Discharge_Energy, Internal_Resistance
		FROM 
			ChannelNormal
		END
	
	-- load specific Cell, All Cycles
	if @CellName != 'All' and @CycleIndex = 0
		BEGIN
		SELECT	
			CellName, Data_Point, Test_Time, Step_Time, Date_Time, Step_Index, Cycle_Index, [Current], Voltage, Charge_Capacity, Discharge_Capacity, 
			Charge_Energy, Discharge_Energy, Internal_Resistance
		FROM 
			ChannelNormal
		WHERE 
			CellName = @CellName
		ORDER BY 
			data_point
		END

	-- load specific Cell with specific cycle
	IF @CellName != 'All' and @CycleIndex > 0
		BEGIN
		SELECT	
			CellName, Data_Point, Test_Time, Step_Time, Date_Time, Step_Index, Cycle_Index, [Current], Voltage, Charge_Capacity, Discharge_Capacity, 
			Charge_Energy, Discharge_Energy, Internal_Resistance
		FROM 
			ChannelNormal
		WHERE 
			CellName = @CellName
		AND
			Cycle_Index = @CycleIndex
		ORDER BY 
			data_point
		END

		--load all cells for a specific cycle
	IF @CellName = 'All' and @CycleIndex > 0
		BEGIN
		SELECT	
			CellName, Data_Point, Test_Time, Step_Time, Date_Time, Step_Index, Cycle_Index, [Current], Voltage, Charge_Capacity, Discharge_Capacity, 
			Charge_Energy, Discharge_Energy, Internal_Resistance
		FROM 
			ChannelNormal
		WHERE 
			Cycle_Index = @CycleIndex
		ORDER BY 
			CellName, data_point
		END
END
GO
/****** Object:  StoredProcedure [dbo].[spCycleStatistics]    Script Date: 9/5/2019 19:13:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCycleStatistics]
	@cellname varchar(500) = 'ALL'
AS
-- spCycleStatistics '2015_0018_PC_A'
BEGIN
	SET NOCOUNT ON;

    select CellName, Cycle_Index, Data_Point, 'Cycle_Index' as Measurement, Cycle_Index as Value
	from vuBatteryStatistics
	where cellname = @cellname
	union all
	select CellName, Cycle_Index, Data_Point, 'Mass' as Measurement, Mass as Value
	from vuBatteryStatistics
	where cellname = @cellname
	union all
	select CellName, Cycle_Index, Data_Point, 'SpecificMaxChargeCapacity' as Measurement, SpecificMaxChargeCapacity as Value
	from vuBatteryStatistics
	where cellname = @cellname
	union all
	select CellName, Cycle_Index, Data_Point, 'SpecificMaxDischargeCapacity' as Measurement, SpecificMaxDischargeCapacity as Value
	from vuBatteryStatistics
	where cellname = @cellname
	union all
	select CellName, Cycle_Index, Data_Point, 'SpecificMaxEnergyCapacity' as Measurement, SpecificMaxEnergyCapacity as Value
	from vuBatteryStatistics
	where cellname = @cellname
	union all
	select CellName, Cycle_Index, Data_Point, 'SpecificMinEnergyCapacity' as Measurement, SpecificMinEnergyCapacity as Value
	from vuBatteryStatistics
	where cellname = @cellname
	union all
	select CellName, Cycle_Index, Data_Point, 'AvgChargeCapacity' as Measurement, AvgChargeCapacity as Value
	from vuBatteryStatistics
	where cellname = @cellname
	union all
	select CellName, Cycle_Index, Data_Point, 'MaxChargeCapacity' as Measurement, MaxChargeCapacity as Value
	from vuBatteryStatistics
	where cellname = @cellname
	union all
	select CellName, Cycle_Index, Data_Point, 'MaxDischargeCapacity' as Measurement, MaxDischargeCapacity as Value
	from vuBatteryStatistics
	where cellname = @cellname
	union all
	select CellName, Cycle_Index, Data_Point, 'Efficiency' as Measurement, Efficiency as Value
	from vuBatteryStatistics
	where cellname = @cellname
	union all
	select CellName, Cycle_Index, Data_Point, 'InverseEfficiency' as Measurement, InverseEfficiency as Value
	from vuBatteryStatistics
	where cellname = @cellname
	union all
	select CellName, Cycle_Index, Data_Point, 'MaxChargeVoltage' as Measurement, MaxChargeVoltage as Value
	from vuBatteryStatistics
	where cellname = @cellname
	union all
	select CellName, Cycle_Index, Data_Point, 'MinChargeVoltage' as Measurement, MinChargeVoltage as Value
	from vuBatteryStatistics
	where cellname = @cellname
	union all
	select CellName, Cycle_Index, Data_Point, 'AvgChargeVoltage' as Measurement, AvgChargeVoltage as Value
	from vuBatteryStatistics
	where cellname = @cellname
	union all
	select CellName, Cycle_Index, Data_Point, 'MaxChargeEnergy' as Measurement, MaxChargeEnergy as Value
	from vuBatteryStatistics
	where cellname = @cellname
	union all
	select CellName, Cycle_Index, Data_Point, 'MaxDischargeEnergy' as Measurement, MaxDischargeEnergy as Value
	from vuBatteryStatistics
	where cellname = @cellname
END
GO
/****** Object:  StoredProcedure [dbo].[spgetDqDv]    Script Date: 9/5/2019 19:13:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spgetDqDv]
	@CellName varchar(250) = '190819_Albemarle-S2_C2F-6_Ian',
	@Cycle_index int = 1
AS
-- EXAMPLES
-- exec spgetDqDv '180403_Ilya_Indigo'
-- exec spgetDqDv '171111_graphite_a', 2
BEGIN
	with dqdv_cte(cCNTID, pCNTID, Cycle_Index, DeltaChargeCapacity, DeltaDischargeCapacity, DeltaVoltage, cCellName, cTest_ID, 
					cData_Point, cVoltage, cCharge_Capacity, pCellName, pTest_ID, pData_Point, pVoltage, pCharge_Capacity)
	as (
	select 
		cn.CNTID cCNTID, pr.CNTID pCNTID, cn.Cycle_Index, cn.Charge_Capacity - pr.Charge_Capacity DeltaChargeCapacity, 
		cn.Discharge_Capacity - pr.Charge_Capacity DeltaDischargeCapacity,
		cn.Voltage - pr.Voltage DeltaVoltage,
		cn.CellName cCellName, cn.Test_ID cTest_ID, cn.Data_Point cData_Point, cn.Voltage cVoltage, cn.Charge_Capacity cCharge_Capacity,
		pr.CellName pCellName, pr.Test_ID pTest_ID, pr.Data_Point pData_Point, pr.Voltage pVoltage, pr.Charge_Capacity pCharge_Capacity
	from ChannelNormal cn
	inner join ChannelNormal pr
	on pr.cntid = cn.cntid-1
	where pr.Charge_Capacity != 0
		and cn.Voltage - pr.Voltage  != 0
		-- FOR TESTING PURPOSES ONLY. REMOVE NEXT LINE FOR PROD
		and cn.CellName = @cellname
		and cn.Cycle_Index = @cycle_index
	)
	select cCNTID, pCNTID, cCellName CellName, Cycle_Index, cTest_ID Test_ID, cData_Point Data_Point, DeltaChargeCapacity/DeltaVoltage dqdvCharge,
		DeltaDischargeCapacity/DeltaVoltage dqdvDischarge,
		DeltaChargeCapacity, DeltaDischargeCapacity, DeltaVoltage
	from dqdv_cte
	order by cCNTID
END
GO
/****** Object:  StoredProcedure [dbo].[spSelectChannelNormal]    Script Date: 9/5/2019 19:13:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spSelectChannelNormal]
	@cellname varchar(500) = 'ALL',
	@random varchar(3) = 'No'
AS
BEGIN
	
	/*
	GET CELL DATA FOR ALL DATA POINTS FROM CHANNEL NORMAL TABLE
	GETS ALL DATA POINTS FOR A PARTICULAR CELL
	exec spGetCellFromChannelNormal 'HASP_CP_190820_a'
	GETS ALL FOR ALL CELLS - DO NOT EXECUTE THIS UNLESS YOU DON'T MIND WAITING 30 SECONDS BECAUSE THE TABLE IS 150 MILLION ROWS LONG
	exec spGetCellFromChannelNormal 
	GETS ALL DATA FOR A RANDOM CELL
	exec spGetCellFromChannelNormal '', 'Y'
[CNTID], [Test_Time], [Step_Time], [Date_Time], [Step_Index], [Cycle_Index], [Is_FC_Data], [Current], [Voltage], [Charge_Capacity], [Discharge_Capacity], [Charge_Energy], [Discharge_Energy], [dVdT], [Internal_Resistance], [AC_Impedance], [ACI_Phase_Angle], [EntryDate]

	*/

	SET NOCOUNT ON;

	if @Random like 'Y%'
		select top 1 @cellname = cellname from Cell order by newid()
		
	-- print @cellname

	if @CellName != ''
		select 
			[CNTID], [CellName], [Test_ID], [Data_Point], [Test_Time], [Step_Time], [Date_Time], [Step_Index], [Cycle_Index], 
			[Is_FC_Data], [Current], [Voltage], [Charge_Capacity], [Discharge_Capacity], [Charge_Energy], [Discharge_Energy], 
			[dVdT], [Internal_Resistance], [AC_Impedance], [ACI_Phase_Angle], [EntryDate], [Cell_ID], [ArbinDate], [ArbinTime], [CTD_ID]
			from ChannelNormal
			where CellName = @cellname
	else
		print ('Enter a Cell Name Number')
END
GO
USE [master]
GO
ALTER DATABASE [SolidPowerDev] SET  READ_WRITE 
GO
