--use master
--go
--create procedure sp_getColumnDiskSpaceUsed
--as
create table #space ([table] nvarchar(255), [column] nvarchar(255) not null, [bytes] bigint null);

declare @sql varchar(max) = ''
declare @tablepattern as varchar(255) = '%'
declare @exclusionpattern as varchar(255) = ''

select @sql = @sql + 'insert into #space select ''' + t.name + ''', ''' + c.name + ''', sum(datalength([' + c.name + '])) as bytes from [' + t.name + '];' 
from sys.columns c
inner join sys.tables t on c.object_id = t.object_id
where t.name like @tablepattern and t.name not like @exclusionpattern
and t.name not in ( 'dbo_LotNrGenerationLocation_CT', 'dbo_BulkMaterialLog_CT','dbo_MaterialForm_CT','change_tables', 'ddl_history'
	, 'lsn_time_mapping', 'captured_columns', 'index_columns', 'dbo_UserOp_CT','dbo_Material_CT','dbo_MaterialDescription_CT')
and t.name = 'weighing' --'consumedbatch'
exec (@sql)

select [table], format(sum([bytes]), '#,#') as [size]
from #space
group by [table]
order by sum(bytes) desc;

select [table], [column], format([bytes], '#,#') as [size]
from [#space]
order by [bytes] desc;

drop table #space