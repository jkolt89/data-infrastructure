# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.graph_objs as go
import pymssql
from dash.dependencies import Input, Output

server = r'192.168.14.119'
user = r'spoperator'
    
password = '50lidp0w3r'
db = 'SolidPowerDev'
#cell = "'190604_a', '190819_a', '2018_0016_PC_H1'"
#cell = "'2015_0018_PC_A'"
cell = "'NPL0002001280010_FORM_70C'"

# run the following cmdln: gunicorn CycleBasedStatistics:app.server -b :8060

spgetCellLog = "exec getCellLog"
#query = "spCycleStatistics " + cell
#query = "select * from channelstatistic where cellname IN (" + cell + " )"
query = "exec getCellStatisticData_ARBIN "  + cell
print(query)
celllog_query = "exec dbo.getSelectFromCell"

conn = pymssql.connect(host=server, user=user, password=password, database=db)

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
df = pd.read_sql(query, conn)
celllog_df = pd.read_sql(celllog_query, conn)
# df = df.sort_values("Data_Point")
available_indicators = df['Measurement'].unique()
cell_names_df = celllog_df.sort_values(by=["Start_DateTime"])
cell_names_df = celllog_df["CellName"].unique()

app.layout = html.Div([
    html.Div([
        html.H4('Cycle Based Statistics'),
        html.Div([
            # dropdown for cell names
            dcc.Dropdown(
                id='cell_dropdown',
                options=[{'label': i, 'value': i} for i in cell_names_df],
                value='CellName'
            ),
            dcc.Dropdown(
                id='xaxis-column',
                options=[{'label': i, 'value': i} for i in available_indicators],
                value='Cycle_Index'
            ),
            dcc.RadioItems(
                id='xaxis-type',
                options=[{'label': i, 'value': i} for i in ['Linear', 'Log']],
                value='Linear',
                labelStyle={'display': 'inline-block'}
            )
        ],
        style={'width': '48%', 'display': 'inline-block'}),

        html.Div([
            dcc.Dropdown(
                id='yaxis-column',
                options=[{'label': i, 'value': i} for i in available_indicators],
                value='SpecificMaxChargeCapacity'
            ),
            dcc.RadioItems(
                id='yaxis-type',
                options=[{'label': i, 'value': i} for i in ['Linear', 'Log']],
                value='Linear',
                labelStyle={'display': 'inline-block'}
            )
        ],style={'width': '48%', 'float': 'right', 'display': 'inline-block'})
    ]),

    dcc.Graph(id='indicator-graphic'),

    dcc.Slider(
        id='cycle--slider',
        min=df['Cycle_Index'].min(),
        max=df['Cycle_Index'].max(),
        value=df['Cycle_Index'].max(),
        marks={str(Cycle_Index): str(Cycle_Index) for Cycle_Index in df['Cycle_Index'].unique()},
        step=None
    )
])

## CALLBACK ###########################################

@app.callback(
    Output('indicator-graphic', 'figure'),
    [Input('xaxis-column', 'value'),
     Input('yaxis-column', 'value'),
     Input('xaxis-type', 'value'),
     Input('yaxis-type', 'value'),
     Input('cycle--slider', 'value'),
     Input('cell_dropdown', 'value')])

def update_graph(xaxis_column_name, yaxis_column_name, xaxis_type, yaxis_type, cycle_value, cell_value):
    df = pd.read_sql("select * from vuCycleStatistic where cellname IN ('" + cell_value + "' )", conn)
    #dff = dff[dff["Cycle_Index"] == cycle_value]
    dff = df[df['Cycle_Index'] == cycle_value]

    return {
        'data': [go.Scatter(
            x=dff['Data_Point'],
            #x=dff[dff['Measurement'] == xaxis_column_name]['Value'],
            y=dff[dff['Measurement'] == yaxis_column_name]['Value'],
            #text=dff[dff['Measurement'] == yaxis_column_name]['Cycle_Index'],
            mode='markers',
            marker={
                'size': 15,
                'opacity': 0.5,
                'line': {'width': 0.5, 'color': 'white'}
            }
        )],
        'layout': go.Layout(
            xaxis={
                'title': xaxis_column_name,
                'type': 'linear' if xaxis_type == 'Linear' else 'log'
            },
            yaxis={
                'title': yaxis_column_name,
                'type': 'linear' if yaxis_type == 'Linear' else 'log'
            },
            margin={'l': 40, 'b': 40, 't': 10, 'r': 0},
            hovermode='closest'
        )
    }


if __name__ == '__main__':
    app.run_server(debug=True)