# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
import numpy as np
import pymssql

host = '192.168.14.119'
user = 'spuser'
password = 'LyftBatt5280'
db = 'SolidPowerDev'

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
#cell = '190707_RER-109_NMC-Si_CP_1'
#cell = 'HASP_CP_190611_a'
cell = '2019_0016c_PC_D1'
#cell = 'HASP_CP_190531_d'
#cell = 'HASP_CP_190611_b'

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
conn = pymssql.connect(user=user, password=password, database=db, server=host)

QUERY = ("""SELECT Cycle_Index, count(Cycle_Index) grpCycle_Index, avg(Voltage) Voltage, avg(Test_Time) Test_Time, avg(Step_Time) Step_Time, avg([DateTime]) Date_Time, 
                avg([Current]) [Current], avg(Charge_Capacity) Charge_Capacity, avg(Discharge_Capacity) Discharge_Capacity, 
                avg(Charge_Energy) Charge_Energy, avg(Discharge_Energy) Discharge_Energy, sum(dVdt) dVdT, avg(Internal_Resistance) Internal_Resistance
                FROM ChannelNormalTable
                WHERE CellName = '%s'
                GROUP BY Cycle_Index""" % cell)
                
df = pd.read_sql(QUERY, conn)

trace1 = go.Bar(x=df['Cycle_Index'], y=df['Charge_Capacity'], name='Charge Cap')
trace2 = go.Bar(x=df['Cycle_Index'], y=df['Discharge_Capacity']*100, name='Discharge Cap')
trace3 = go.Bar(x=df['Cycle_Index'], y=df['Charge_Energy'], name='Charge Energy')
trace4 = go.Bar(x=df['Cycle_Index'], y=df['Discharge_Energy'], name='Discharge Energy')

app = dash.Dash()


app.layout = html.Div(children=[
    html.H1(children=cell),
    html.Div(children='''Cycle and Capacity Report'''),
    dcc.Graph(
        id='example-graph',
        figure={
            'data': [trace1, trace2, trace3, trace4],
            'layout':{
                'title':'Capacity and Energy Averages by Cycles',
                'xaxis':{
                    'title':'Cylce Index'
                },
                'yaxis':{
                     'title':'Capacity and Energy'
                }
            }
            #'layout':
            #go.Layout(title='Test Time Report', barmode='stack')
        })
])
    
if __name__ == '__main__':
    app.run_server(debug=True)