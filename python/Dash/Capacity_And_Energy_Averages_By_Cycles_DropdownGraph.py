# -*- coding: utf-8 -*-

######################################################
## By: johnk
## Created on 07/22/2019
## based on info from webpage: https://pbpython.com/plotly-dash-intro.html
## TO DO:
## 1) dropdown menu needs to come from DROPDOWN_QUERY dataframe instead of the where clause in QUERY df
## 
######################################################

import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
import numpy as np
import pymssql

host = '192.168.14.119'
user = 'spuser'
password = 'LyftBatt5280'
db = 'SolidPowerDev'

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
#cell = '190707_RER-109_NMC-Si_CP_1'
#cell = 'HASP_CP_190611_a'
cell = '2019_0016c_PC_D1'
#cell = 'HASP_CP_190531_d'
#cell = 'HASP_CP_190611_b'

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
conn = pymssql.connect(user=user, password=password, database=db, server=host)

QUERY = ("""select CellName, Cycle_Index, count(Cycle_Index) NrOfCycles, round(avg(Voltage),2) Voltage, round(avg(Test_Time), 0) Test_Time, 
                   round(avg(Step_Time),0) Step_Time, 
                   round(avg([DateTime]),0) Date_Time, round(avg([Current]),4) [Current], round(avg(Charge_Capacity),4) Charge_Capacity, 
                   round(avg(Discharge_Capacity),4) Discharge_Capacity, round(avg(Charge_Energy),4) Charge_Energy, 
                   round(avg(Discharge_Energy),4) Discharge_Energy, round(avg(dVdt),4) dVdT, round(avg(Internal_Resistance),4) Internal_Resistance
                from ChannelNormalTable
                where CellName in ('190707_RER-109_NMC-Si_CP_1', 'HASP_CP_190611_a', '2019_0016c_PC_D1')
                group by CellName, Cycle_Index
                order by CellName, Cycle_Index""")

DROPDOWN_QUERY = ("SELECT DISTINCT CellName FROM ChannelNormalTable")
                
df = pd.read_sql(QUERY, conn)
dd_df = pd.read_sql(DROPDOWN_QUERY, conn)

cell_options = df["CellName"].unique();

app = dash.Dash()

app.layout = html.Div([
    html.H2("Dynamic Cell Report"),
    html.Div(
        [
            dcc.Dropdown(
                id="Cell",
                options=[{
                    'label': i,
                    'value': i
                } for i in cell_options],
                value='All Cells'),
        ],
        style={'width': '25%',
               'display': 'inline-block'}),
    dcc.Graph(id='funnel-graph'),
])


@app.callback(
    dash.dependencies.Output('funnel-graph', 'figure'),
    [dash.dependencies.Input('Cell', 'value')])

def update_graph(CellName):
    if CellName == "All Cells":
        df_plot = df.copy()
    else:
        df_plot = df[df['CellName'] == CellName]

    pv = pd.pivot_table(
        df_plot,
        index=['CellName'],
        columns=["Cycle_Index"],
        values=['NrOfCycles'],
        aggfunc=sum,
        fill_value=0)

    #trace1 = go.Bar(x=pv.index, y=pv[('Quantity', 'declined')], name='Declined')
    #trace2 = go.Bar(x=pv.index, y=pv[('Quantity', 'pending')], name='Pending')
    #trace3 = go.Bar(x=pv.index, y=pv[('Quantity', 'presented')], name='Presented')
    #trace4 = go.Bar(x=pv.index, y=pv[('Quantity', 'won')], name='Won')
    trace1 = go.Bar(x=df['Cycle_Index'], y=df['Charge_Capacity'], name='Charge Cap')
    trace2 = go.Bar(x=df['Cycle_Index'], y=df['Discharge_Capacity']*100, name='Discharge Cap')
    trace3 = go.Bar(x=df['Cycle_Index'], y=df['Charge_Energy'], name='Charge Energy')
    trace4 = go.Bar(x=df['Cycle_Index'], y=df['Discharge_Energy'], name='Discharge Energy')
    
    return {
        'data': [trace1, trace2, trace3, trace4],
        'layout':
        go.Layout(
            title='Cell Report For {}'.format(CellName),
            barmode='stack')
    }


if __name__ == '__main__':
    app.run_server(debug=True)