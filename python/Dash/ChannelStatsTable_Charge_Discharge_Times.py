# -*- coding: utf-8 -*-
"""
Created on Wed Jul 24 09:51:59 2019

@author: johnk
"""

import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
import numpy as np
import pymssql

host = '192.168.14.119'
user = 'spuser'
password = 'LyftBatt5280'
db = 'SolidPowerDev'

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
conn = pymssql.connect(user=user, password=password, database=db, server=host)

QUERY = """select * from ChannelStatisticTable 
            where CellName = '190707_RER-109_NMC-Si_CP_1' 
            order by CellName, Test_ID, Data_Point"""

df = pd.read_sql(QUERY, conn)

#print(df)

trace1 = go.Bar(x=df['Cycle_Index'], y=df['Test_Time'], name='Test_Time')
trace2 = go.Bar(x=df['Cycle_Index'], y=df['Step_Time'], name='Step_Time')