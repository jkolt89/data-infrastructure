# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
import numpy as np
import pymssql

host = '192.168.14.119'
user = 'spuser'
password = 'LyftBatt5280'
db = 'SolidPowerDev'

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
cell = '190707_RER-109_NMC-Si_CP_1'
#cell = 'HASP_CP_190611_a'
#cell = '2019_0016c_PC_D1'

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
conn = pymssql.connect(user=user, password=password, database=db, server=host)

QUERY = ("""SELECT Cycle_Index, count(Cycle_Index) grpCycle_Index, avg(Voltage) Voltage, avg(Test_Time) Test_Time, avg(Step_Time) Step_Time, avg([DateTime]) Date_Time, 
                avg([Current]) [Current], avg(Charge_Capacity) Charge_Capacity, avg(Discharge_Capacity) Discharge_Capacity, 
                avg(Charge_Energy) Charge_Energy, avg(Discharge_Energy) Discharge_Energy, sum(dVdt) dVdT, avg(Internal_Resistance) Internal_Resistance
                FROM ChannelNormalTable
                WHERE CellName = '%s'
                GROUP BY Cycle_Index""" % cell)

DROPDOWN_QUERY = ("SELECT DISTINCT CellName FROM ChannelNormalTable")
                
#df = pd.read_sql(QUERY, conn)
#dd_df = pd.read_sql(DROPDOWN_QUERY, conn)
df = pd.read_sql(DROPDOWN_QUERY, conn)

def generate_table(dataframe, max_rows=10):
    return html.Table(
        # Header
        [html.Tr([html.Th(col) for col in dataframe.columns])] +

        # Body
        [html.Tr([
            html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))]
    )

app = dash.Dash()

app.layout = html.Div(children=[
    html.H4(children='Cell Stats'),
    dcc.Dropdown(id='dropdown', options=[
        {'label': i, 'value': i} for i in df.CellName.unique()
    ], multi=True, placeholder='Filter by Cell...'),
    html.Div(id='table-container')
])

@app.callback(
    dash.dependencies.Output('table-container', 'children'),
    [dash.dependencies.Input('dropdown', 'value')])
def display_table(dropdown_value):
    if dropdown_value is None:
        return generate_table(df)

    dff = df[df.CellName.str.contains('|'.join(dropdown_value))]
    return generate_table(dff)

app.css.append_css({"external_url": "https://codepen.io/chriddyp/pen/bWLwgP.css"})

if __name__ == '__main__':
    app.run_server(debug=True)