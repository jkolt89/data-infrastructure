# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.graph_objs as go
import pymssql
from dash.dependencies import Input, Output
import dash_table

LOCAL=False
    
password = '50lidp0w3r'
db = 'SolidPowerDev'
#cell = "'190604_a', '190819_a', '2018_0016_PC_H1'"
cell = "'2019_0016c_PC_J'"
server = r'192.168.14.119'
user = r'spoperator'
query = "select * from vuDqDvColumnStore where cellname IN (" + cell + " )"

spgetCellLog = "exec getCellLog"
#query = "spCycleStatistics " + cell

celllog_query = "exec dbo.getSelectFromCell"

conn = pymssql.connect(host=server, user=user, password=password, database=db)

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

#app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app = dash.Dash(__name__)

df = pd.read_sql(query, conn)
# selects from the Cell Table
celllog_df = pd.read_sql(celllog_query, conn)
# selects from CellLog Table
#celllog_df = pd.read_sql(spgetCellLog, conn)

# df = df.sort_values("Data_Point")
available_indicators = df['Measurement'].unique()
#cell_names_df = celllog_df.sort_values(by=["Excel Date"])
cell_names_df = celllog_df["CellName"].unique()

#GLOBAL LAYOUT
#layout = dict(
#    dcc.Slider(
#        id='cycle--slider',
#        min=df['Cycle_Index'].min(),
#        max=df['Cycle_Index'].max(),
#        value=df['Cycle_Index'].max(),
#        marks={str(Cycle_Index): str(Cycle_Index) for Cycle_Index in df['Cycle_Index'].unique()},
#        step=None
#    )
#)

app.layout = html.Div([
    html.Div([
        html.H4('DqDv Statistics'),
        html.Div([           
            # dropdown for cell names
            dcc.Dropdown(
                id='cell_dropdown',
                options=[{'label': i, 'value': i} for i in cell_names_df],
                value='CellName'
            ),]),
        html.Div([
            dcc.Dropdown(
                id='xaxis-column',
                options=[{'label': i, 'value': i} for i in available_indicators],
                value='Data_Point'
            ),
            dcc.RadioItems(
                id='xaxis-type',
                options=[{'label': i, 'value': i} for i in ['Linear', 'Log']],
                value='Linear',
                labelStyle={'display': 'inline-block'}
            )
        ],
        style={'width': '48%', 'display': 'inline-block'}),

        html.Div([
            dcc.Dropdown(
                id='yaxis-column',
                options=[{'label': i, 'value': i} for i in available_indicators],
                value='dqdvCharge'
            ),
            dcc.RadioItems(
                id='yaxis-type',
                options=[{'label': i, 'value': i} for i in ['Linear', 'Log']],
                value='Linear',
                labelStyle={'display': 'inline-block'}
            )
        ],
        style={'width': '48%', 'float': 'right', 'display': 'inline-block'})
    ]),

    dcc.Graph(id='indicator-graphic'),

    dcc.Slider(
        id='cycle--slider',
        min=df['Cycle_Index'].min(),
        max=df['Cycle_Index'].max(),
        value=df['Cycle_Index'].min(),
        marks={str(Cycle_Index): str(Cycle_Index) for Cycle_Index in df['Cycle_Index'].unique()},
        step=None
    ),
    
    dash_table.DataTable(
        id="cell_table",
        columns=[{"name": i, "id": i} for i in celllog_df.columns],
        data=celllog_df.to_dict('records'),
        
        style_data_conditional=[
            {
                'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(204, 204, 204)'
            }
        ],
                
        style_header={
            'backgroundColor': 'grey',
            'color': 'white',
            'fontWeight': 'bold',
            'border': '2px solid black'
        },
                
        style_cell_conditional=[
            {
                'if': {'column_id': 'CellName'},
                'textAlign': 'left',
                'fontWeight':'bold'
            },
            {
                'if': {'column_id': 'Comments'},
                'textAlign': 'left'
            }
        ],

        style_cell={
                'padding': '1px',
                'color':'black',
                'border': '1px solid black'
        },
            
        style_table={ #'overflowY': 'auto', 'overflowX': 'auto', 'Width': '50px', # width of col
            'maxHeight': '300px',          # sets size of scrollable window
            'overflowY': 'auto',
            'border': 'thin lightgrey solid'
        },
        #fixed_rows={ 'headers': True, 'data': 0 },          # locks the header row (1st row)
        #fixed_columns={ 'headers': True, 'data': 2 },      # locks the left side by column number (in this case the CellName column)
        editable=False,
        filter_action="native",
        sort_action="native",
        sort_mode="multi",
        row_selectable="multi",
        row_deletable=True,
        selected_rows=[],
        page_action="native",
        page_current= 0,
        page_size= 250
        #sort_action='custom',
        )
])

@app.callback(
    Output('indicator-graphic', 'figure'),
    [Input('xaxis-column', 'value'),
     Input('yaxis-column', 'value'),
     Input('xaxis-type', 'value'),
     Input('yaxis-type', 'value'),
     Input('cycle--slider', 'value'),
     Input('cell_dropdown', 'value')])

def update_graph(xaxis_column_name, yaxis_column_name, xaxis_type, yaxis_type, cycle_value, cell_value):
    
    conn = pymssql.connect(host=server, user=user, password=password, database=db)
    # this calls select each time cycle is changed but we need to call it only when cell name is changed
    df = pd.read_sql("select * from vuDqDvColumnStore where cellname IN ('" + cell_value + "' )", conn)
    #dff = dff[dff["Cycle_Index"] == cycle_value]
    dff = df[df['Cycle_Index'] == cycle_value]
    
    meanvalue = df[df['Measurement'] == yaxis_column_name]['Value']
    meanvalue = meanvalue.mean()

#    if meanvalue > dff[dff['Measurement'] == yaxis_column_name]['Value']:
#        mycolor = 'red'
#    else:
#        mycolor = 'black'
    
    return {
        'data': [go.Scatter(
            #x=dff['Data_Point'],
            x=dff[dff['Measurement'] == xaxis_column_name]['Value'],
            y=dff[dff['Measurement'] == yaxis_column_name]['Value'],
            #text=dff[dff['Measurement'] == yaxis_column_name]['Cycle_Index'],
            mode='markers',
            marker={
                'size': 15,
                'opacity': 1,
                'line': {'width': 1, 'color': 'red'}
            }
        )],
        'layout': go.Layout(
            xaxis={
                'title': "Cylce Index: " + str(cycle_value),
                'type': 'linear' if xaxis_type == 'Linear' else 'log'
            },
            yaxis={
                'title': yaxis_column_name,
                'type': 'linear' if yaxis_type == 'Linear' else 'log'
            },
            margin={'l': 40, 'b': 40, 't': 10, 'r': 0},
            hovermode='closest'
        ),
    }


if __name__ == '__main__':
    app.run_server(debug=True)