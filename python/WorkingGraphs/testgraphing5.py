# -*- coding: utf-8 -*-

import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import numpy as np
import pyodbc
import pymssql
from dash.dependencies import Input, Output
import plotly.graph_objs as go


server = r'192.168.14.119'
user = 'spuser'
password = 'LyftBatt5280'
db = 'SolidPowerDev'
#cell = "'HASP_CP_190712_a'"
#cell = "'181029_a'"
#cell = "'190424_b'"
cell = "'2019_0016c_PC_H', 'HASP_CP_190611_a'"
#sproc = "exec dbo.sp_getvoltprofile " + cell
#query = "select CellName, Test_Time, Cycle_Index, Voltage, [Current], Internal_Resistance from dbo.ChannelNormal where CellName IN (" + cell + ") order by CellName, Cycle_Index, Test_Time"
#query = "select CellName, Test_Time as Cycle_Index, Voltage, [Current], Internal_Resistance from dbo.ChannelNormal where CellName = " + cell + " order by Test_Time"
#query = "select CellName, Cycle_Index, round(avg([Current]), 4)*10000 AvgCurrent, avg(Voltage) as AvgVoltage, avg(Internal_Resistance) as AvgIR from ChannelNormal where CellName = " + cell + " group by CellName, Cycle_Index order by Cycle_Index"
#query = "select * from vuChannelNormalPivot where CellName IN (" + cell + ")'
#query = "select * from vuChannelNormalPivot where CellName IN ('181029_a', '2019_0016c_PC_H', 'HASP_CP_190611_a')"
#query = "select * from vuChannelNormalPivot ORDER BY Test_Time"
query = "select * from vuBatteryStatisticsColStore where Cycle_Index <= 100 order by 1, 2"
#query = "select * from vuBatteryStatisticsColStore where Cycle_Index <= 20 order by 1, 2"
#query = "select * from vuBatteryStatistics where Cycle_Index <= 5 order by 1, 2"

#conn = pyodbc.connect(server=server, user=user, password=password, database=db)
conn = pymssql.connect(host=server, user=user, password=password, database=db)
#conn = pyodbc.connect('DRIVER={SQL Server};SERVER=' + server + ';DATABASE=' + db + ';Trusted_Connection=yes')

#df = pd.read_sql(sproc, conn)
df = pd.read_sql(query, conn)

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

available_indicators = df['Measurement'].unique()
available_cells = df['CellName'].unique()

app.layout = html.Div([
    html.Div([
        html.Div([
                dcc.Dropdown(
                        id = 'dropdown-menu',
                        options=[{'label': i, 'value': i} for i in available_cells],
                        #multi=True,
                        value=available_cells[0]
                        )
                ],
                style={'width': '100%', 'display': 'inline-block'}),

        html.Div([
            #Creates a drop down using the values in the Measurement Column
            dcc.Dropdown(
                id='xaxis-column',
                options=[{'label': i, 'value': i} for i in available_indicators],
                value='Cycle_Index'
            ),
            dcc.RadioItems(
                id='xaxis-type',
                options=[{'label': i, 'value': i} for i in ['Linear', 'Log']],
                value='Linear',
                labelStyle={'display': 'inline-block'}
            )
        ],
        style={'width': '48%', 'display': 'inline-block'}),

        html.Div([
            #Creates a drop down using the values in the Measurement Column
            dcc.Dropdown(
                id='yaxis-column',
                options=[{'label': i, 'value': i} for i in available_indicators],
                value='AvgChargeCapacity'
            ),
            dcc.RadioItems(
                id='yaxis-type',
                options=[{'label': i, 'value': i} for i in ['Linear', 'Log']],
                value='Linear',
                labelStyle={'display': 'inline-block'}
            )
        ],style={'width': '48%', 'float': 'right', 'display': 'inline-block'})
    ]),

    dcc.Graph(id='indicator-graphic') #,

    #dcc.Slider(
        #id='cycle--slider',
        #min=df['Cycle_Index'].min(),
        #max=df['Cycle_Index'].max(),
        #value=df['Cycle_Index'].max(),
        #marks={str(Cycle_Index): str(Cycle_Index) for Cycle_Index in df['Cycle_Index'].unique()},
        #step=None
    #)
])

@app.callback(
    Output('indicator-graphic', 'figure'),
    [Input('xaxis-column', 'value'),
     Input('yaxis-column', 'value'),
     Input('xaxis-type', 'value'),
     Input('yaxis-type', 'value'),
     Input('dropdown-menu', 'value')
     #, Input('cycle--slider', 'value')
     ])

def update_graph(xaxis_column_name, yaxis_column_name, xaxis_type, yaxis_type, dropdown_value):#, cycle_value):
    #dff = df[df['Cycle_Index'] == cycle_value]
    dff = df[df['CellName'] == dropdown_value]
    
    return {
        'data': [go.Scatter(
            x=dff[dff['Measurement'] == xaxis_column_name]['Value'],
            y=dff[dff['Measurement'] == yaxis_column_name]['Value'],
            text=dff[dff['Measurement'] == yaxis_column_name]['CellName'],
            mode='markers',
            marker={
                'size': 15,
                'opacity': 0.5,
                'line': {'width': 0.5, 'color': 'white'}
            }
        )],
        'layout': go.Layout(
            xaxis={
                'title': xaxis_column_name,
                'type': 'linear' if xaxis_type == 'Linear' else 'log'
            },
            yaxis={
                'title': yaxis_column_name,
                'type': 'linear' if yaxis_type == 'Linear' else 'log'
            },
            margin={'l': 40, 'b': 40, 't': 10, 'r': 0},
            hovermode='closest'
        )
    }


if __name__ == '__main__':
    app.run_server(debug=True)