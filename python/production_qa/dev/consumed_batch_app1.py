# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 14:30:27 2020

@author: johnk
"""


import dash
import dash_html_components as html
import dash_core_components as dcc
import flask
import sys
#sys.path.append('/home/solidpower/pipython/Utilities')
#sys.path.append('/home/solidpower/pipython/NodeRed')
import sqlalchemy
from sqlalchemy import create_engine
from dash.exceptions import PreventUpdate

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
server = flask.Flask(__name__) #define flask app.server
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

#run the following cmdln PROD: gunicorn basic:app.server -b :8000
#FROM DIRECTORY: /home/solidpower/pipython/Active_Dev/Dash/dev
#   OR
#                /home/solidpower/pipython/Active_Dev/Dash/DashApp
#DEV as spvirt (LBatt5280) run the following cmdln in the directory of file: sudo gunicorn consumed_batch_app1:app.server -b :8060


app.layout = html.Div([
    dcc.Tabs([

        ##################     Tab 1
        dcc.Tab(label='Consumed Batches', children=[
            html.Div([
                html.H2('Consumed Batches Entry Form'),
                html.H6('Enter consumed batch data into this application form.'),
                html.Br(),
                html.H6('Enter the following consumed batches information:'),
                html.Br(),
                html.H6('Lot Number:'),
                html.Div(dcc.Input(id='input-on-lotnumber', type='text', placeholder="Lot_Number", required=True, debounce=True)),
                html.H6('Start Date:'),
                html.Div(dcc.Input(id='input-on-startdate', type='text', placeholder="Start_Date", required=True, debounce=True)),
                html.H6('Finish date:'),
                html.Div(dcc.Input(id='input-on-finishdate', type='text', placeholder='Finish_Date', required=True, debounce=True)),
                html.H6('Quantity_g:'),
                html.Div(dcc.Input(id='input-on-Quantity_g', type='text', placeholder='Quantity_g', required=True, debounce=True)),
                html.H6('MRB:'),
                html.Div(dcc.Input(id='input-on-MRB', type='text', placeholder='Quantity_g', required=True, debounce=True)),
                html.H6('Conductivity_mScm:'),
                html.Div(dcc.Input(id='input-on-Conductivity_mScm', type='text', placeholder='Conductivity_mScm', required=True, debounce=True)),
                html.H6('Yield_g:'),
                html.Div(dcc.Input(id='input-on-Yield_g', type='text', placeholder='Yield_g', required=True, debounce=True)),
                html.Br(),
                html.Br(),
                html.Button('Submit', id='submit-val', n_clicks=0),
                #html.Br(),
                html.H6(id='container-button-basic', children='Please only click submit once'),
                html.Br()
            ])
        ]),
    ])
])
#-------------------callbacks---------------------------------------------
"""
@app.callback(
    dash.dependencies.Output('container-button-basic', 'children'),
    [dash.dependencies.Input('submit-val', 'n_clicks')],
    [dash.dependencies.State('input-on-lotnumber', 'value'), 
     dash.dependencies.State('input-on-startdate', 'value'),
     dash.dependencies.State('input-on-finishdate', 'value')

def update_output(n_clicks, splitNum, userNum, Generation):
    if LNGL is not None and splitNum is not None and userNum is not None:
        # print("hello")
        print(LNGL, splitNum, userNum, Generation)
        if (len(splitNum) != 1) or (splitNum == ''):
            print("bad split number, try again")
            return u'Invalid split number. Should be 1 digit. Probably 0'
        if len(userNum) != 2 or userNum == '':
            print("Invalid user number. Should be 2 Digits")
            return u'Invalid user number. Should be 2 digits'
        if len(LNGL) != 2 or LNGL == '':  # checking correct input -- this shoud be it's own funciton someday
            print("bad length")
            return u'Invalid lot number getneration location. Should be 2 digits. See Hannah or Matt for assitance finding the correct number'
        if len(Generation) != 1 or Generation == '':
            print("bad Generation")
            return u'Invalid material generation. Must be a single character.'
        engine = create_engine("mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
        print('tab1 - hello from the first callback - lot number generator')
        # pd.options.mode.chained_assignment = None
        # querystring =  "exec getLotNumber {},{},{}".format(LNGL,splitNum,userNum)
        # newLotNum = pd.read_sql_query(querystring, con=engine)
        connection = engine.raw_connection()
        # print('this is the try clause')
        try:
            cursor = connection.cursor()
            querystring = 'exec getLotNumber @LotNrGenLocation = \'{}\', @SplitLetter = \'{}\', @EnteredBy = \'{}\', @GenNr = \'{}\''.format(
                LNGL, splitNum, userNum, Generation)
            cursor.execute(querystring)
            print('querystring is: ')
            print(querystring)
            newLotNum, = list(cursor.fetchall())   #THIS COMMA MATTERS A LOT
            print('results are: ')
            print(newLotNum)
            cursor.close()
            connection.commit()
        except Exception as e:
            print('there was an exception that occurred. This will need to be looked at more')
            print(e)
            connection.close()
            return '---error-tab1--- {}'.format(e)
        finally:
            print('closing connection to DB')
            connection.close()

        engine.dispose()
        # print(type(newLotNum))
        # newLotNum1 = newLotNum.pop(0)
        # print(type(newLotNum))
        # print(str(newLotNum))

        # this is an interesting thing:
        # The fetchall required a comma after the asignee variable name beacuse it was making a list that I couldnt format
        # solving that raised the issue of now it returned a class object. fortunately a string conversion worked, then a substring to finalize the conversion
        newLotNum = str(newLotNum)
        newLotNum = newLotNum[3:16]  
        return u'Your new lot number is: {}'.format(newLotNum)
    else:
        raise PreventUpdate
"""

if __name__ == '__main__':
    app.run_server(debug=True, host = '0.0.0.0', port=8050)