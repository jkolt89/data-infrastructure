# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 14:59:30 2020

@author: johnk
"""

import dash
import dash_html_components as html
import dash_core_components as dcc
#import dash_table_experiments as dt
import dash.dependencies
from dash.dependencies import Input, Output, State
import plotly
import flask
import sys
import sqlalchemy
import pymssql
import pyodbc
import dash_table
from sqlalchemy import create_engine
from dash.exceptions import PreventUpdate
import pandas as pd

#run the following cmdln PROD: gunicorn basic:app.server -b :8000
#FROM DIRECTORY: /home/solidpower/pipython/Active_Dev/Dash/dev
#   OR
#                /home/solidpower/pipython/Active_Dev/Dash/DashApp
#DEV as spvirt (LBatt5280) run the following cmdln in the directory of file: gunicorn proto_dash_table:app.server -b :8061
DEBUG = 1

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

server = flask.Flask(__name__) #define flask app.server
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

#username = 't35t4ppu53r'
#pw = 't35t4ppu53r'
username = 'shackpie'
pw = 'LyftBatt5280'
if DEBUG != 1:
    db = 'SP_ManuLine'
else:
    db = 'dev'
port = 1433
server = '192.168.14.119'

app = dash.Dash(__name__)

#cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+db+';UID='+username+';PWD='+ pw)
cnxn = pymssql.connect(server, username, pw, db)

cursor = cnxn.cursor()

#cnxn.autocommit = True

#query = "select * from dbo.consumedbatch"
#query = "select lotNr, CBID as id from consumedbatch"
query = "select [CBID], [LotNr], [StartDate], [FinishDate], [Quantity_g], [MRB], [Conductivity_mScm], [Yield_g], [PassFail], [ElectronicConductivity_SCm], [GuseGen] \
    from dbo.ConsumedBatch"

df = pd.read_sql(query, cnxn)

#print(df.head(20))

cnxn.close()

app.layout = dash_table.DataTable(
    id='table',
    columns=[{"name": i, "id": i} for i in df.columns],
    #data=df #df.read_sql(query, conn),
    data=df.to_dict('records'),
)

#-------------------callbacks---------------------------------------------

#@app.callback(
#        cnxn.close()
#        )

#---------------end callbacks---------------------------------------------


if __name__ == '__main__':
    app.run_server(debug=True, host = '0.0.0.0', port=8061)
    