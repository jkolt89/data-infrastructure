# -*- coding: utf-8 -*-
"""
Created on Fri Aug  7 12:32:46 2020

@author: johnk
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 14:30:27 2020

@author: johnk
"""


import dash
import dash_html_components as html
import dash_core_components as dcc
import flask
import sys
import sqlalchemy
import plotly
from sqlalchemy import create_engine
from dash.exceptions import PreventUpdate
import pandas as pd
import dash_table
import dash.dependencies
from dash.dependencies import Input, Output, State
import pymssql
import pyodbc
from sqlalchemy import create_engine


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

server = flask.Flask(__name__) #define flask app.server
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

#run the following cmdln PROD: gunicorn basic:app.server -b :8000
#FROM DIRECTORY: /home/solidpower/pipython/Active_Dev/Dash/dev
#   OR
#                /home/solidpower/pipython/Active_Dev/Dash/DashApp
#DEV as spvirt (LBatt5280) run the following cmdln in the directory of file: gunicorn DEV_consumed_batch_app6_data_table:app.server -b :8061

DEBUG = 1   # 1 = for debugging using the dev sql database, 0 uses production sql database


app.layout = html.Div([
    dcc.Tabs([

        ##################     Tab 1
        dcc.Tab(label='Completed Batches', children=[
            html.Div([
                html.H2('Completed Batches Entry Form'),
                html.H6('Enter completed batch data into this application form.'),
                html.Br(),
                html.H6('Enter the following completed batches information:'),
                #html.Br(),
                html.H6('Lot Number:'),
                html.Div(dcc.Input(id='input-on-lotnumber', type='text', placeholder="Lot_Number", required=True, debounce=True)),
                html.H6('Start Date:'),
                html.Div(dcc.Input(id='input-on-startdate', type='text', placeholder="Start_Date", required=True, debounce=True)),
                html.H6('Finish date:'),
                html.Div(dcc.Input(id='input-on-finishdate', type='text', placeholder='Finish_Date', required=True, debounce=True)),
                html.H6('Quantity_g:'),
                html.Div(dcc.Input(id='input-on-Quantity_g', type='text', placeholder='Quantity_g', required=True, debounce=True)),
                html.H6('MRB:'),
                html.Div(dcc.Input(id='input-on-MRB', type='text', placeholder='MRB', required=True, debounce=True)),
                html.H6('Conductivity_mScm:'),
                html.Div(dcc.Input(id='input-on-Conductivity_mScm', type='text', placeholder='Conductivity_mScm', required=True, debounce=True)),
                html.H6('Yield_g:'),
                html.Div(dcc.Input(id='input-on-Yield_g', type='text', placeholder='Yield_g', required=True, debounce=True)),
                html.Br(),
                html.H6("Pass/Fail"),
                dcc.RadioItems(
                        id="input-passfail",
                        options=[
                        {"label": "Pass", "value": 1 },
                        {"label": "Fail", "value": 0}
                        ],
                        value=0),
                html.H6('ElectronicConductivity_mScm:'),
                html.Div(dcc.Input(id='input-on-ElectronicConductivity_mScm', type='text', placeholder='ElectronicConductivity_mScm', required=True, debounce=True)),
                html.H6('GuseGen:'),
                html.Div(dcc.Input(id='input-on-GuseGen', type='text', placeholder='GuseGen', required=True, debounce=True)),
                html.Br(),
                
                html.Button('Submit', id='submit-val', n_clicks=0),
                #html.Br(),
                html.H6(id='container-button-basic', children='Please only click submit once')
                ])
        ]),
    ])
])
#-------------------callbacks---------------------------------------------

@app.callback(
    dash.dependencies.Output('container-button-basic', 'children'),
    [dash.dependencies.Input('submit-val', 'n_clicks')],
    [dash.dependencies.State('input-on-lotnumber', 'value'), 
     dash.dependencies.State('input-on-startdate', 'value'),
     dash.dependencies.State('input-on-finishdate', 'value'),
     dash.dependencies.State('input-on-Quantity_g', 'value'),
     dash.dependencies.State('input-on-MRB', 'value'),
     dash.dependencies.State('input-on-Conductivity_mScm', 'value'),
     dash.dependencies.State('input-on-Yield_g', 'value'),
     dash.dependencies.State('input-passfail', 'value'),
     dash.dependencies.State('input-on-ElectronicConductivity_mScm', 'value'),
     dash.dependencies.State('input-on-GuseGen', 'value')])

def update_output(n_clicks, LotNr, startdate, finishdate, quantity_g, MRB, Conductivity_mScm, Yield_g, passfail, ElectronicConductivity_mScm, GuseGen):
    #print("hello")
    #print(LotNr, startdate, finishdate)
    
    #################################################################################################################
    #if DEBUG == 1 then use development sql database else use production sql database
    if DEBUG ==1:
        engine = create_engine("mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/dev?driver=FreeTDS")
    else:
        engine = create_engine("mssql+pyodbc://shackpie:LyftBatt5280@192.168.14.119:1433/SP_ManuLine?driver=FreeTDS")
    #################################################################################################################
    
    
    #pd.options.mode.chained_assignment = None
    #querystring =  "exec insertConsumedBatch {},{},{}".format(LotNr,startdate,finishdate)
    #print(querystring)
    
    connection = engine.raw_connection()
    
    try:
        cursor = connection.cursor()
        querystring = 'exec dbo.insertConsumedBatch @LotNr = \'{}\', @startdate = \'{}\', @finishdate = \'{}\', @quantity_g = \'{}\', @MRB = \'{}\', @Conductivity_mScm = \'{}\', @Yield_g = \'{}\', @PassFail= \'{}\', @ElectronicConductivity_mScm = \'{}\', @GuseGen = \'{}\''.format(
            LotNr, startdate, finishdate, quantity_g, MRB, Conductivity_mScm, Yield_g, passfail, ElectronicConductivity_mScm, GuseGen)
        cursor.execute(querystring)
        print('querystring is: ')
        print(querystring)
        cursor.close()
        connection.commit()
    except Exception as e:
        print('There was an exception that occurred. This will need to be looked at more')
        print(e)
        connection.close()
        return '---ERROR--- {}'.format(e)
    finally:
        print('Closing connection to DB')
        connection.close()
        engine.dispose()
    
"""
        
        # print(type(newLotNum))
        # newLotNum1 = newLotNum.pop(0)
        # print(type(newLotNum))
        # print(str(newLotNum))

        # this is an interesting thing:
        # The fetchall required a comma after the asignee variable name beacuse it was making a list that I couldnt format
        # solving that raised the issue of now it returned a class object. fortunately a string conversion worked, then a substring to finalize the conversion
        newLotNum = str(newLotNum)
        newLotNum = newLotNum[3:16]  
        return u'Your new lot number is: {}'.format(newLotNum)
    else:
        raise PreventUpdate
"""

if __name__ == '__main__':
    app.run_server(debug=True, host = '0.0.0.0', port=8060)