<?php 
  ob_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>SolidPower Cell Viewer</title>

	<!-- jQuery -->
	<script src="js/jquery-latest.min.js"></script>

	<!-- Demo stuff -->
	<link class="ui-theme" rel="stylesheet" href="css/jquery-ui.min.css">
	<script src="js/jquery-3.5.1.js"></script>
	<link rel="stylesheet" href="css/jq.css">
	<link href="css/prettify.css" rel="stylesheet">
	<script src="js/prettify.js"></script>
	<script src="js/docs.js"></script>

	<!-- Tablesorter: required -->
	<link rel="stylesheet" href="css/theme.blue.css">
	<script src="js/jquery.tablesorter.js"></script>
	<script src="js/widgets/widget-storage.js"></script>
	<script src="js/widgets/widget-filter.js"></script>

	<script id="js">$(function() {

	var $table = $('table').tablesorter({
		theme: 'blue',
		widgets: ["zebra", "filter"],
		widgetOptions : {
			// filter_anyMatch replaced! Instead use the filter_external option
			// Set to use a jQuery selector (or jQuery object) pointing to the
			// external filter (column specific or any match)
			filter_external : '.search',
			// add a default type search to the first name column
			filter_defaultFilter: { 1 : '~{query}' },
			// include column filters
			filter_columnFilters: true,
			filter_placeholder: { search : 'Search...' },
			filter_saveFilters : true,
			filter_reset: '.reset'
		}
	});

	// make demo search buttons work
	$('button[data-column]').on('click', function() {
		var $this = $(this),
			totalColumns = $table[0].config.columns,
			col = $this.data('column'), // zero-based index or "all"
			filter = [];

		// text to add to filter
		filter[ col === 'all' ? totalColumns : col ] = $this.text();
		$table.trigger('search', [ filter ]);
		return false;
	});

});</script>

<?php

require_once('dbcon/connect_SP_RND.php'); 
//echo "<p>Connection Made.</p>";

?>

</head>
<body>

<?php

$tsql = "SELECT * FROM sys.Tables";
//$tsql = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='sys.Tables'";

/* Execute the query. */  

$stmt = sqlsrv_query( $dbCon, $tsql);  

if ( $stmt )  
{  
     echo "Statement executed.<br>\n";  
}   
else   
{  
     echo "Error in statement execution.\n";  
     die( print_r( sqlsrv_errors(), true));  
}  
echo "<table>";
echo "<thead><tr><th>TABLE_NAME</th></tr></thead>";
while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_BOTH))  
{  
	echo "<tr class=\"tabData\"><td class=\"view\">" . $row[0] . "</td></tr>";  
}  

echo "</table>";

sqlsrv_free_stmt( $stmt);

?>



<div class="viewIt">You can update date in this table by clicking on a row. A table will replace this text.
</div>
	<script>
	var valIndex="";
		$("tr.tabData").click(function(){
			valIndex= $(this).find( "td.view" ).text();
			$("tr.tabData").css("font-weight","normal");
			$(this).css("font-weight","bold");
			$( "div.viewIt" ).html( "Showing details: " + valIndex );
			
	 if( valIndex.length > 0 ){
        $.ajax({
            type:'GET',
            url: 'viewRND.php',
	    data: {'view': valIndex},
            dataType: 'html',
            success: function(result){
                $('div.viewIt').html(result);
            } // End of success function of ajax form
        }); // End of ajax call    
        }
        else{
        	$( "div.viewIt" ).text("Select information." );
        }

	$( "div.viewIt" ).show();
    });
	</script>

</div>
</div>
</body>
</html>