<?php

require_once('dbcon/connect.php'); 
//echo "<p>Connection Made.</p>";


$pcid = $_GET["pcid"];
//echo $pcid . "<br/>";


$tsql = "SELECT * FROM ProdConductivity where PCID=$pcid";
$stmt = sqlsrv_query( $dbCon, $tsql);  
//echo $stmt;
if ( $stmt )  
{  
    // echo "Statement executed.<br>\n";  
}   
else   
{  
     echo "Error in statement execution.\n";  
     die( print_r( sqlsrv_errors(), true));  
}  
//echo "query complete.";
$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_BOTH);
//echo $row['MaterialID'];

?>
<table id="page"><tr><td>
<form id="updater" method="post" action="index.php?f=production"> 
<table align="center" border="0">
<tbody>
<tr>
<td align="right">ID:</td>
<td align="left"><input name="pcid" size="6" value="<?php echo $pcid ?>" readonly="readonly" type="text" /></td>
<td align="right">Material ID:</td>
<td align="left"><input name="MaterialID" size="25" value="<?php echo $row['MaterialID']; ?>" type="text" /></td>
</tr>
<tr>
<td align="right">Lot #:</td>
<td align="left"><input name="lotNum" size="25" value="<?php echo $row['LotNr']; ?>" type="text" /></td>
<td align="right">Operator</td>
<td align="left"><input name="operator" size="5"  value="<?php echo $row['Operator']; ?>" type="text" /></td>
<td align="right">Temperature:</td>
<td align="left"><input name="temperature" size="5" value="<?php echo $row['TestTemperature']; ?>" type="text" />&deg;C</td>
</tr>
<tr>
<td align="right">Material Mass:</td>
<td align="left"><input name="mass" size="5" value="<?php echo $row['MaterialMass_g']; ?>" type="text" /> g</td>
<td align="right">Plunger Tare:</td>
<td align="left"><input name="lengthTare" size="5" value="<?php echo $row['PlungerLengthNoPellet_mm']; ?>" type="text" /> mm</td>
<td align="right">Full Length:</td>
<td align="left"><input name="lengthTotal" size="5" value="<?php echo $row['PelletPlungerThickness_mm']; ?>" type="text" /> mm</td>
</tr>
<tr>
<td align="right">Ionic Intercept:</td>
<td id="ionic" align="left"><input name="ionicIntercept" size="5" value="<?php echo ($row['TestCondition'] == 'Ionic'? $row['XAxisNyquist_ohms']:""); ?>" onchange="updateCond()" type="text" /> Ohms</td>
<td align="right">Electronic Intercept:</td>
<td id="electronic" align="left"><input name="electronicIntercept" size="5" value="<?php echo (trim($row['TestCondition']) == 'Electronic'? $row['XAxisNyquist_ohms']:""); ?>" onchange="updateCond()" type="text" /> Ohms</td>
</tr>
<tr>
<td colspan="4" align="center"><input value="Calculate" onclick="updateCond()" type="button" /></td>
</tr>
<tr>
<td align="right">Ionic Conductivity:</td>
<td id="ionic" align="left"><input name="ionic" size="6" readonly="readonly" type="text" /> mS/cm</td>
<td align="right">Electronic Conductivity:</td>
<td id="electronic" align="left"><input name="electronic" size="6" readonly="readonly" type="text" /> nS/cm</td>
<td align="right">Material Density:</td>
<td align="left"><input name="density" size="6" readonly="readonly" type="text" /> g/cc</td>
</tr>
<tr>
<td align="right">Mark for Deletion:</td>
<td><input name="deleteCheck" id="deleteCheck" type="checkbox"/></td>
<td align="right">Notes:</td>
<td colspan="5" align="left"><input name="notes" value="<?php echo $row['Notes']; ?>" onchange="updateCond()" size="100%" type="text" /></td>
</tr>
<tr>
<td colspan="6" align="left"><p id="datadump"></p>
<input name="dd" size="100%" type="hidden" />
</td>
</tr>
<tr>
<td colspan="1" align="right"><input value="Mark for Deletion" onclick="markConDelete()" type="submit" /></td>
<td colspan="6" align="right"><input value="Update - Be Cautious" onclick="updateCon()" type="submit" /></td>
</tr>
</tbody>
</table>
</form>
</td></tr></table>