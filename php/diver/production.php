
	<script id="js">$(function() {

	var $table = $('table').tablesorter({
		theme: 'blue',
		// default sortInitialOrder setting
		sortList: [[2,1]],
		widgets: ["zebra", "filter"],
		widgetOptions : {
			// filter_anyMatch replaced! Instead use the filter_external option
			// Set to use a jQuery selector (or jQuery object) pointing to the
			// external filter (column specific or any match)
			filter_external : '.search',
			// add a default type search to the first name column
			filter_defaultFilter: { 1 : '~{query}' },
			// include column filters
			filter_columnFilters: true,
			filter_placeholder: { search : 'Search...' },
			filter_saveFilters : true,
			filter_reset: '.reset'
		}
	});

	// make demo search buttons work
	$('button[data-column]').on('click', function() {
		var $this = $(this),
			totalColumns = $table[0].config.columns,
			col = $this.data('column'), // zero-based index or "all"
			filter = [];

		// text to add to filter
		filter[ col === 'all' ? totalColumns : col ] = $this.text();
		$table.trigger('search', [ filter ]);
		return false;
	});

});</script>


<body>
<div id="main">

<div class="editEntry">You can update date in this table by clicking on a row. A table will replace this text.
</div>
	<h1>Electrolyte Conductivity Data</h1>

	<div id="demo"><input class="search" type="search" data-column="all"> (Match any column)<br>
<input class="search" type="search" data-column="3"> (Lot#; fuzzy search... try "CG")<br>

<!-- targeted by the "filter_reset" option -->
<button type="button" class="reset">Reset Search</button>

<!-- db stuff -->
<?php
require_once('dbcon/connect.php'); 
//echo "<p>Connection Made.</p>";

if ($_REQUEST["dd"] != null){
	$query = $_REQUEST["dd"];
	$_POST['dd'] = NULL;
};
echo $query . "<br/>";

$stmt = sqlsrv_query($dbCon, $query);

if($stmt === false) {
    die(print_r(sqlsrv_errors(), true));
}
sqlsrv_free_stmt($stmt);

$tsql = "SELECT * FROM vuProdConductivity";  

/* Execute the query. */  

$stmt = sqlsrv_query( $dbCon, $tsql);  

if ( $stmt )  
{  
     //echo "Statement executed.<br>\n";  
}   
else   
{  
     echo "Error in statement execution.\n";  
     die( print_r( sqlsrv_errors(), true));  
}  

?>


<!-- end db stuff -->


<table class="tablesorter">
	<thead>
		<tr>
			<th>MaterialID</th>
			<th>Operator</th>
			<th>EntryDate</th>
			<th data-placeholder="Fuzzy search">Lot #</th>
			<th>Type</th>
			<th>Temperature</th>
			<th>Mass</th>
			<th>Tare</th>
			<th>Total Length</th>
			<th>Thickness (mm)</th>
			<th>Density (g/cc)</th>
			<th>Intercept</th>
			<th>Conductivity (mS/cm)</th>
			<th>Conductivity (nS/cm)</th>
			<th>Notes</th>
		</tr>
	</thead>
	<tbody>
 
 <?php
/* Iterate through the result set printing a row of data upon each iteration.*/  

while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_BOTH))  
{  
echo "<tr class=\"condata\">";
     echo "<td>" . "<p hidden class=\"pcid\">" . $row['PCID'] . "</p>" . $row['MaterialID'] . "</td>";
     echo "<td>" . $row['Operator'] . "</td>";
     echo "<td>" . $row['EntryDate'] . "</td>";
     echo "<td>" . $row['LotNr'] . "</td>";
     echo "<td>" . $row['TestCondition'] . "</td>";
     echo "<td>" . $row['TestTemperature'] . "</td>";
     echo "<td>" . $row['MaterialMass_g'] . "</td>";
     echo "<td>" . $row['PlungerLengthNoPellet_mm'] . "</td>";
     echo "<td>" . $row['PelletPlungerThickness_mm'] . "</td>";
     echo "<td>" . ($row['PelletPlungerThickness_mm'] - $row['PlungerLengthNoPellet_mm']) . "</td>";
     echo "<td>" . round(1000*$row['MaterialDensity_gcc'])/1000.0 . "</td>";
    // echo "<td>" . round(1000*$row['MaterialMass_g']/((($row['PelletPlungerThickness_mm'] - $row['PlungerLengthNoPellet_mm'])/10)*0.8*0.8*3.14159))/1000.0 . "</td>";
     echo "<td>" . $row['XAxisNyquist_ohms'] . "</td>";
     echo "<td>" . $row['IonicConductivity_Scm']*1000 . "</td>";
     echo "<td>" . $row['ElectronicConductivity_Scm']*1000000000 . "</td>";
     echo "<td>" . $row['Notes'] . "</td></tr>";  
}  

/* Free statement and connection resources. */  
sqlsrv_free_stmt( $stmt);  
sqlsrv_close( $conn);  
?>

	</tbody>
</table>

	<script>
	var valIndex="";
		$("tr.condata").click(function(){
			valIndex= $(this).find( "p.pcid" ).text();
			$("tr.condata").css("font-weight","normal");
			$(this).css("font-weight","bold");
			$( "div.editEntry" ).html( "Showing details: " + valIndex );
			
	 if( valIndex.length > 0 ){
        $.ajax({
            type:'GET',
            url: 'editCon.php',
	    data: {'pcid': valIndex},
            dataType: 'html',
            success: function(result){
                $('div.editEntry').html(result);
            } // End of success function of ajax form
        }); // End of ajax call    
        }
        else{
        	$( "div.editEntry" ).text("Select information." );
        }

	$( "div.editEntry" ).show();
    });
	</script>
</div>
</div>