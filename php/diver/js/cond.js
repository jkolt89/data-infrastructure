function calcCond() {
var input = document.getElementById("builder");

var materialMass = input.mass.value;
var thickness = (input.lengthTotal.value - input.lengthTare.value)/10.0;
var ionicIntercept = input.ionicIntercept.value;
input.density.value = Math.round(1000*materialMass/(thickness*0.8*0.8*3.14159))/1000.0;
input.ionic.value = Math.round(1000*(thickness/(ionicIntercept*0.8*0.8*3.14159))*1000.0)/1000.0;
input.electronic.value = Math.round(1000*(thickness/(input.electronicIntercept.value*0.8*0.8*3.14159))*1e9)/1000.0;

var query = "INSERT INTO ProdConductivity (MaterialID, Operator, LotNr, TestCondition, TestTemperature, MaterialMass_g, "
			+ "PlungerLengthNoPellet_mm, PelletPlungerThickness_mm, XAxisNyquist_ohms, Notes )"
			+ "VALUES( "
			+ "'" + input.MaterialID.value + "', "
			+ "'" + input.operator.value + "', "
			+ "'" + input.lotNum.value + "', "
			+ (input.ionicIntercept.value > 0? "'Ionic'": (input.electronicIntercept.value > 0? "'Electronic'": null)) + ", "
			+ input.temperature.value + ", "
			+ materialMass + ", "
			+ input.lengthTare.value + ", "
			+ input.lengthTotal.value + ", "	
			+ (input.ionicIntercept.value > 0? input.ionicIntercept.value: (input.electronicIntercept.value > 0? input.electronicIntercept.value: null)) + ", "
			+ (input.notes.value == ''? null: "'" + input.notes.value + "'")
			+ ")";
			
document.getElementById("datadump").innerHTML = query;
input.dd.value = query;
			
return false;
}


// Submit form with name function.
function submit() {
	calcCond();
	var x = document.getElementsByName('builder');
	x[0].submit(); //form submission
}

function updateCond() {
var input = document.getElementById("updater");

var materialMass = input.mass.value;
var thickness = (input.lengthTotal.value - input.lengthTare.value)/10.0;
var ionicIntercept = input.ionicIntercept.value;
input.density.value = Math.round(1000*materialMass/(thickness*0.8*0.8*3.14159))/1000.0;
input.ionic.value = Math.round(1000*(thickness/(ionicIntercept*0.8*0.8*3.14159))*1000.0)/1000.0;
input.electronic.value = Math.round(1000*(thickness/(input.electronicIntercept.value*0.8*0.8*3.14159))*1e9)/1000.0;

var query = "UPDATE ProdConductivity SET UpdateTime=GetDate() WHERE pcid=" + input.pcid.value + ";"
			+ "INSERT INTO ProdConductivity (MaterialID, Operator, LotNr, TestCondition, TestTemperature, MaterialMass_g, "
			+ "PlungerLengthNoPellet_mm, PelletPlungerThickness_mm, XAxisNyquist_ohms, Notes)"
			+ "VALUES( "
			+ "'" + input.MaterialID.value + "', "
			+ "'" + input.operator.value + "', "
			+ "'" + input.lotNum.value + "', "
			+ (input.ionicIntercept.value > 0? "'Ionic'": (input.electronicIntercept.value > 0? "'Electronic'": null)) + ", "
			+ input.temperature.value + ", "
			+ materialMass + ", "
			+ input.lengthTare.value + ", "
			+ input.lengthTotal.value + ", "	
			+ (input.ionicIntercept.value > 0? input.ionicIntercept.value: (input.electronicIntercept.value > 0? input.electronicIntercept.value: null)) + ", "
			+ "'" + input.notes.value + "'"
			+ ")";
			
document.getElementById("datadump").innerHTML = query;
input.dd.value = query;
			
return false;
}

// update table .
function updateCon() {
	updateCond();
	var x = document.getElementsByName('updater');
	
	x[0].submit(); //form submission
}

function createCondEntry() {
	
	var input = document.getElementById("creater");
	var query = "INSERT INTO ProdConductivity (LotNr) VALUES( 'new entry' )";
				
	input.dd.value = query;

	document.getElementById("creater").submit();
	return false;
}

function markConDelete() {
	var input = document.getElementById("updater");
	var query = "trying to mark for deletion";

	var d = document.getElementById("deleteCheck");
	if (d.checked == true){
		var query = "UPDATE ProdConductivity SET UpdateTime=GetDate() WHERE pcid=" + input.pcid.value;
				
		document.getElementById("datadump").innerHTML = query;
	}

	input.dd.value = query;
				
	return false;
}