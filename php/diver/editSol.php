<?php

require_once('dbcon/connect.php'); 
//echo "<p>Connection Made.</p>";


$sdid = $_GET["SDID"];
//echo $sdid . "<br/>";


$tsql = "SELECT * FROM SolventDrying where SDID=$sdid";
$stmt = sqlsrv_query( $dbCon, $tsql);  
//echo $stmt;
if ( $stmt )  
{  
    // echo "Statement executed.<br>\n";  
}   
else   
{  
     echo "Error in statement execution.\n";  
     die( print_r( sqlsrv_errors(), true));  
}  
//echo "query complete.";
$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_BOTH);
//echo $row['MaterialID'];

?>
<table id="page"><tr><td>
<form id="updater" method="post" action="index.php?f=plotSolvents"> 
<table align="center" border="0">
<tbody>
<tr>
<td align="right">ID:</td>
<td align="left"><input name="SDID" size="6" value="<?php echo $row['SDID']; ?>" readonly="readonly" type="text" /></td>
<td align="right">Entry Date:</td>
<td align="left"><input name="EntryDate" size="25" value="<?php echo $row['EntryDate']; ?>" readonly="readonly" type="text" /></td>
</tr>
<tr>
<td align="right">Solvent</td>
<td align="left"><select name="Solvent" id="Solvent">
                    <option value="<?php echo $row['Item']; ?>"><?php echo $row['Item']; ?></option>
                    <option value="Ethyl Proprionate">Ethyl Proprionate</option>
                    <option value="Isobutyl Isobutyrate (IBIB)">Isobutyl Isobutyrate (IBIB)</option>
                    <option value="Xylenes">Xylenes</option>
                </select>
</td>
</tr>
<tr>
<td align="right">Supplier:</td>
<td align="left"><select name="Supplier" id="Supplier">
                    <option value="<?php echo $row['Supplier']; ?>"><?php echo $row['Supplier']; ?></option>
                    <option value="Alfa Aesar">Alfa Aesar</option>
                    <option value="Midland Scientific">Midland Scientific</option>
                    <option value="Rocky Mountain">Rocky Mountain</option>
                    <option value="Sigma Aldrich">Sigma Aldrich</option>
                    <option value="Vigon">Vigon</option>
                </select>
</td>
<td align="right">Supplier Lot#:</td>
<td align="left"><input name="SupplierLotNr" size="15" value="<?php echo $row['SupplierLotNr']; ?>" type="text" /></td>
</tr>
<tr>
<td align="right">SP Lot#:</td>
<td align="left"><input name="SPLotNr" size="15" value="<?php echo $row['SPLotNr']; ?>" type="text" /></td>
<td align="right">SP Part#:</td>
<td align="left"><input name="PartNr" size="15" value="<?php echo $row['PartNr']; ?>" type="text" /></td>
</tr>
<tr>
<td align="right">KF Moisture:</td>
<td align="left"><input name="KSolidMoisture_ppm" size="15" value="<?php echo $row['KSolidMoisture_ppm']; ?>" type="text" /> ppm</td>
</tr>
<tr>
<td align="right">Notes:</td>
<td colspan="9" align="left"><input name="notes" value="<?php echo $row['Notes']; ?>" onchange="updateSolv()" size="100%" type="text" /></td>
</tr>
<tr>
<td align="right">Mark for Deletion:</td>
<td><input name="deleteCheck" id="deleteCheck" type="checkbox"/></td>
</tr>
<tr>
<td colspan="9" align="left"><p id="datadump"></p>
<input name="dd" size="100%" type="hidden" />
</td>
</tr>
<tr>
<td colspan="1" align="right"><input value="Mark for Deletion" onclick="markSolDelete()" type="submit" /></td>
<td colspan="6" align="right"><input value="Update - Solvent" onclick="updateSol()" type="submit" /></td>
</tr>
</tbody>
</table>
</form>
</td></tr></table>