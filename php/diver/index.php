<?php 
  ob_start();
?>
<!DOCTYPE html>
<html>
<head>
<?php
    include('header.php');
?>
</head>
<body>
<div class="flex-container">      
<header>
<img src="images/SolidPowerLogo.png" height="30px"> Data Diver
</header>

<div class="table">
<nav class="nav">
<?php
	include('menu.php');
?>
</nav>
</div>


<article>
<?php
	include('controller.php');
?>
</article>

<footer>&copy;2020 - SolidPower</footer>
</div>

<script type="text/javascript" src="./js/base.js"></script>
<script type="text/javascript" src="./js/solvents.js"></script>
<script type="text/javascript" src="./js/precursors.js"></script>
</body>
</html>