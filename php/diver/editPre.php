<?php

require_once('dbcon/connect.php'); 
//echo "<p>Connection Made.</p>";


$sdid = $_GET["PID"];
//echo $sdid . "<br/>";


$tsql = "SELECT * FROM Precursor where PID=$sdid";
$stmt = sqlsrv_query( $dbCon, $tsql);  
//echo $stmt;
if ( $stmt )  
{  
    // echo "Statement executed.<br>\n";  
}   
else   
{  
     echo "Error in statement execution.\n";  
     die( print_r( sqlsrv_errors(), true));  
}  
//echo "query complete.";
$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_BOTH);
//echo $row['MaterialID'];

?>
<table id="page"><tr><td>
<form id="updater" method="post" action="index.php?f=plotPre"> 
<table align="center" border="0">
<tbody>
<tr>
<td align="right">ID:</td>
<td align="left"><input name="PID" size="6" value="<?php echo $row['PID']; ?>" readonly="readonly" type="text" /></td>
<td align="right">Entry Date:</td>
<td align="left"><input name="EntryDate" size="25" value="<?php echo $row['EntryDate']; ?>" readonly="readonly" type="text" /></td>
</tr>
<tr>
<td align="right">Precursor</td>
<td align="left"><select name="Precursor" id="Precursor">
                    <option value="<?php echo $row['Item']; ?>"><?php echo $row['Item']; ?></option>
                    <option value="lithium bromide">lithium bromide</option>
                    <option value="lithium chloride">lithium chloride</option>
                    <option value="lithium iodide">lithium iodide</option>
                </select>
</td>
</tr>
<tr>
<td align="right">Supplier:</td>
<td align="left"><select name="Supplier" id="Supplier">
                    <option value="<?php echo $row['Supplier']; ?>"><?php echo $row['Supplier']; ?></option>
                    <option value="Acros">Acros</option>
                    <option value="Ajay SQM">Ajay SQM</option>
                    <option value="Alfa Aesar">Alfa Aesar</option>
                    <option value="Leverton">Leverton</option>
                    <option value="Sigma Aldrich">Sigma Aldrich</option>
                    <option value="GFS">GFS</option>
                    <option value="LiI Guagdong">LiI Guagdong</option>
                </select>
</td>
<td align="right">Supplier Lot#:</td>
<td align="left"><input name="SupplierLotNr" size="15" value="<?php echo $row['SupplierLotNr']; ?>" type="text" /></td>
</tr>
<tr>
<td align="right">SP Lot#:</td>
<td align="left"><input name="SPLotNr" size="15" value="<?php echo $row['SPLotNr']; ?>" type="text" /></td>
<td align="right">SP Part#:</td>
<td align="left"><input name="PartNr" size="15" value="<?php echo $row['PartNr']; ?>" type="text" /></td>
</tr>
<tr>
<td align="right">KF Moisture:</td>
<td align="left"><input name="KSolidMoisture_ppm" size="15" value="<?php echo $row['KSolidMoisture_ppm']; ?>" type="text" /> ppm</td>
</tr>
<tr>
<td align="right">Notes:</td>
<td colspan="9" align="left"><input name="notes" value="<?php echo $row['Notes']; ?>" onchange="updatePre()" size="100%" type="text" /></td>
</tr>
<tr>
<td align="right">Mark for Deletion:</td>
<td><input name="deleteCheck" id="deleteCheck" type="checkbox"/></td>
</tr>
<tr>
<td colspan="9" align="left"><p id="datadump"></p>
<input name="dd" size="100%" type="hidden" />
</td>
</tr>
<tr>
<td colspan="1" align="right"><input value="Mark for Deletion" onclick="markPreDelete()" type="submit" /></td>
<td colspan="6" align="right"><input value="Update - Precursor" onclick="updatePrecursor()" type="submit" /></td>
</tr>
</tbody>
</table>
</form>
</td></tr></table>