<?php

echo '<html>';
$Instance = "192.168.14.119";
$ConnectionInfo = array( "Database"=>"dev", "UID"=>"superuser", "PWD"=>"90rl()RL");

$conn = sqlsrv_connect( $Instance, $ConnectionInfo);
if( $conn ) {
        echo "Connection established to $Instance.";
}else{
 echo "Connection could not be established.";
 print_r($ConnectionInfo);
     die( print_r( sqlsrv_errors(), true));
}

$SQL_String = "SELECT 
  MAX(sys.sysDataBases.Name) as DatabaseName,
  Coalesce(max(CONVERT(VARCHAR(28),msdb.dbo.BackUpset.BackUp_Finish_Date,120))
               ,'No backup taken') as LastBackupCompleted,
  Coalesce(max(convert(varchar(50),CEILING(msdb.dbo.BackUpset.BackUp_Size/1000/1000)))
               ,'No backup taken') as MBBackedUp
FROM     
  sys.sysDataBases
  LEFT OUTER JOIN msdb.dbo.BackUpset
    ON msdb.dbo.BackUpset.DataBase_Name = sys.sysDataBases.Name
GROUP BY sys.sysDataBases.Name
ORDER BY 1";

$result = sqlsrv_query( $conn , $SQL_String) or die ( "sqlsrv_query failed"  );

echo '<table align="center">';
echo '<th>Database</th><th>Last Backup Date (120 format)</th><th>Size MB</th>';
$output = '';
while ( $row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC))
        {
  $output .= '<tr>'. 
   '<td>' . $row['DatabaseName'] . '</td>' .
   '<td><center>' . $row['LastBackupCompleted'] . '</center></td>' .
   '<td>' . $row['MBBackedUp']  . '</td>' .
   '</tr>';
 }
echo $output;
echo '</table>';
echo '</br>';
echo '</html>';

?>

