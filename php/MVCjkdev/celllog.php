
	<script id="js">$(function() {

	var $table = $('table').tablesorter({
		theme: 'blue',
		//theme: 'blackice',
		//widgets: ["zebra", "filter", "editable"],
		widgets: ["zebra", "filter"],
		widgetOptions : {
			// filter_anyMatch replaced! Instead use the filter_external option
			// Set to use a jQuery selector (or jQuery object) pointing to the
			// external filter (column specific or any match)
			filter_external : '.search',
			// add a default type search to the first name column
			filter_defaultFilter: { 1 : '~{query}' },
			// include column filters
			filter_columnFilters: true,
			filter_placeholder: { search : 'Search...' },
			filter_saveFilters : true,
			filter_reset: '.reset',
			//editable_autoAccept    : true
		}
	});

	// make demo search buttons work
	$('button[data-column]').on('click', function() {
		var $this = $(this),
			totalColumns = $table[0].config.columns,
			col = $this.data('column'), // zero-based index or "all"
			filter = [];

		// text to add to filter
		filter[ col === 'all' ? totalColumns : col ] = $this.text();
		$table.trigger('search', [ filter ]);
		return false;
	});

});</script>

<!-- db stuff -->
<?php
require_once('dbcon/connect_SP_RND.php'); 
//echo "<p>Connection Made.</p>";

//////////////////////////////////////////////////////////////////////////////////

if ($_REQUEST["dd"] != null){
	$query = $_REQUEST["dd"];
	$_POST['dd'] = NULL;
};
echo $query . "<br/>";

$stmt = sqlsrv_query($dbCon, $query);

if($stmt === false) {
    die(print_r(sqlsrv_errors(), true));
}
sqlsrv_free_stmt($stmt);
///////////////////////////////////////////////////////////////////////////////////

$tsql = "SELECT * FROM cell_log";
//$tsql = "SELECT * FROM dbo.cell_log";  
//$tsql = "SELECT * FROM sys.Tables";

//$tsql = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='cell_log'";
/* Execute the query. */  

$stmt = sqlsrv_query( $dbCon, $tsql);  

if ( $stmt )  
{  
     //echo "Statement executed.<br>\n";  
}   
else   
{  
     echo "Error in statement execution.\n";  
     die( print_r( sqlsrv_errors(), true));  
}  

?>


<!-- end db stuff -->

<body>
<div id="banner">
	<h1>table<em>sorter</em></h1>
	<h2>SP Cell Log Search and Filter</h2>
	<h3>Flexible client-side table sorting</h3>
</div>
<div id="main">
	<h1>Demo</h1>

	<div id="demo"><input class="search" type="search" data-column="all"> (Match any column)<br>
<input class="search" type="search" data-column="1"> (First Name; fuzzy search... try "be")<br>

<!-- targeted by the "filter_reset" option -->
<button type="button" class="reset">Reset Search</button>

<table class="tablesorter">
	<thead>
		<tr>
			<th>CLID</th>
			<th>CellName</th>
			<th>PurposeDescription</th>
			<th>Cathode</th>
			<th>temperature</th>
			<th>MaxVoltage</th>
			<th>die</th>
			<th>CurrentCollector</th>
			<th>ActiveCathode</th>
			<th>ActiveCathodeDateCoated</th>
			<th>electrolyte</th>
			<th>TotalCathodeMass_g</th>
			<th>DieDiameter_cm</th>
			<th>CathodeParticleCoating</th>
			<th>WeightCoatingPercent</th>
			<th>NominalPercentActiveMaterial</th>
			<th>RatioActiveMaterial</th>
			<th>ActiveMaterialTheoreticalCapacity_mAhg</th>
			<th>CellTheoreticalCapacity_mAhg</th>
			<th>Loading_mg_cm2</th>
			<th>Capacity_mAh</th>
			<th>Anode</th>
			<th>AnodeMass_g</th>
			<th>Separator</th>
			<th>SeparatorMass_g</th>
			<th>MaximumStackPressure_ton</th>
			<th>CathodePressure_ton</th>
			<th>CageTorque_lbs</th>
			<th>Operator</th>
			<th>Location</th>
			<th>NrCyclesCompleted</th>
			<th>FirstCycleCEPercent</th>
			<th>FirstCycleCharge</th>
			<th>FirstCycleDischarge</th>
			<th>InitialResistance_ohm</th>
			<th>MaxVoltageWithOutlier</th>
			<th>MaxVoltageNoOutlier</th>
			<th>MaxDischargeCapacity</th>
			<th>ExcelTime</th>
			<th>PythonTime</th>
			<th>LastDateTime</th>
			<th>Channel</th>
			<th>ArbinNotes</th>
			<th>StoppageReason</th>
			<th>StoppageCycleNr</th>
			<th>Notes</th>
		</tr>
	</thead>
	<tbody>
	
 <?php
/* Iterate through the result set printing a row of data upon each iteration.*/  

while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_BOTH))  
{  
echo "<tr>";
	echo "<td>" . $row['CLID'] . "</td>";
	echo "<td>" . $row['CellName'] . "</td>";
	echo "<td>" . $row['PurposeDescription'] . "</td>";
	echo "<td>" . $row['Cathode'] . "</td>";
	echo "<td>" . $row['temperature'] . "</td>";
	echo "<td>" . $row['MaxVoltage'] . "</td>";
	echo "<td>" . $row['die'] . "</td>";
	echo "<td>" . $row['CurrentCollector'] . "</td>";
	echo "<td>" . $row['ActiveCathode'] . "</td>";
	echo "<td>" . $row['ActiveCathodeDateCoated'] . "</td>";
	echo "<td>" . $row['electrolyte'] . "</td>";
	echo "<td>" . $row['TotalCathodeMass_g'] . "</td>";
	echo "<td>" . $row['DieDiameter_cm'] . "</td>";
	echo "<td>" . $row['CathodeParticleCoating'] . "</td>";
	echo "<td>" . $row['WeightCoatingPercent'] . "</td>";
	echo "<td>" . $row['NominalPercentActiveMaterial'] . "</td>";
	echo "<td>" . $row['RatioActiveMaterial'] . "</td>";
	echo "<td>" . $row['ActiveMaterialTheoreticalCapacity_mAhg'] . "</td>";
	echo "<td>" . $row['CellTheoreticalCapacity_mAhg'] . "</td>";
	echo "<td>" . $row['Loading_mg_cm2'] . "</td>";
	echo "<td>" . $row['Capacity_mAh'] . "</td>";
	echo "<td>" . $row['Anode'] . "</td>";
	echo "<td>" . $row['AnodeMass_g'] . "</td>";
	echo "<td>" . $row['Separator'] . "</td>";
	echo "<td>" . $row['SeparatorMass_g'] . "</td>";
	echo "<td>" . $row['MaximumStackPressure_ton'] . "</td>";
	echo "<td>" . $row['CathodePressure_ton'] . "</td>";
	echo "<td>" . $row['CageTorque_lbs'] . "</td>";
	echo "<td>" . $row['Operator'] . "</td>";
	echo "<td>" . $row['Location'] . "</td>";
	echo "<td>" . $row['NrCyclesCompleted'] . "</td>";
	echo "<td>" . $row['FirstCycleCEPercent'] . "</td>";
	echo "<td>" . $row['FirstCycleCharge'] . "</td>";
	echo "<td>" . $row['FirstCycleDischarge'] . "</td>";
	echo "<td>" . $row['InitialResistance_ohm'] . "</td>";
	echo "<td>" . $row['MaxVoltageWithOutlier'] . "</td>";
	echo "<td>" . $row['MaxVoltageNoOutlier'] . "</td>";
	echo "<td>" . $row['MaxDischargeCapacity'] . "</td>";
	echo "<td>" . $row['ExcelTime'] . "</td>";
	echo "<td>" . $row['PythonTime'] . "</td>";
	echo "<td>" . $row['LastDateTime'] . "</td>";
	echo "<td>" . $row['Channel'] . "</td>";
	echo "<td>" . $row['ArbinNotes'] . "</td>";
	echo "<td>" . $row['StoppageReason'] . "</td>";
	echo "<td>" . $row['StoppageCycleNr'] . "</td>";
	echo "<td>" . $row['Notes'] . "</td>";
}  

/* Free statement and connection resources. */  
sqlsrv_free_stmt( $stmt);  
sqlsrv_close( $conn);  
?>
	</tbody>
</table></div>
</div>