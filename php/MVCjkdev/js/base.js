$( "div#button" ).click(function() {
  if( $( "div#loginDIV" ).is( ":hidden" )){
    $( "div#loginDIV" ).show();
    $( "div#button" ).text( "Dismiss" );
    } 
  else {
    $( "div#loginDIV" ).hide();
    $( "div#button" ).text( "Welcome " + $( "#username" ).val() );
    }
});

$( ".link" ).click(function() {
  var text = $( this ).text();
  $( "p#status" ).text( "action:> " + text );
  if( text === "create account" ){
  $( "p#status" ).append( " action:> Please create an account." );
  }
  else if( text === "email hint" ){
  $( "p#status" ).append( " action:> an email (possibly) has been sent to your account email." );
  }
  else{
  $( "p#status" ).text( "action:> " + text );
  }
  
  $( "p#status" ).toggle( "fast" );
});

/*$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
        $("#athleteTable").tablesorter(); 
        $("#athletes").tablesorter(); 
        $( "div#filter" ).hide();        
        $( "div#athlete" ).hide();
    } 
); */
  
  //This is the datepicker for the from and to dates 
  $( function() {
	
    var dateFormat = "yy-mm-dd",
      from = $( "#from" )
        .datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          changeYear: true,
          dateFormat: 'yy-mm-dd',
          numberOfMonths: 1
        })
        .on( "change", function() {
          from.datepicker( "option", "maxDate", getDate( this ) );
        }),
      to = $( "#to" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
          changeYear: true,
          dateFormat: 'yy-mm-dd',
        numberOfMonths: 1
      })
      .on( "change", function() {
        to.datepicker( "option", {"minDate": getDate( this )} );
      });
  });
  

    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      } 
      return date;
    };
 
	function pickDate(event) {
		var dateFormat = "yy-mm-dd";
		var selDate = $('.pickDate')
		.datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd',
			numberOfMonths: 1
			})
		.on( "change", function() {
			selDate.datepicker( "option", getDate( this ) );
		});
	};