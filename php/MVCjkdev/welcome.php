
<p>cu&bull;ri&bull;ous<br/>
'kyo&bull;ore&bull;s<br/>
adjective<br/>
<ul><li>eager to know or learn something. "I began to be curious about the whereabouts of the bride and groom"</li>
    <li>strange; unusual. "a curious sensation overwhelmed her"</li>
    </ul>
<p>synonyms:	strange, odd, peculiar, funny, unusual, bizarre, weird, eccentric, queer, unexpected, unfamiliar, extraordinary, abnormal, out of the ordinary, anomalous, surprising, incongruous, unconventional, offbeat, unorthodox
"her curious behavior"</p>
</p>