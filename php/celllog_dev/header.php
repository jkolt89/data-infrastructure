<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">  
  
<title>SP Data Diver</title>

<head>
	<meta charset="utf-8">
	<link href="css/base.css" rel="stylesheet" type="text/css" />
	<script src="js/cond.js"></script>
	<title>jQuery tablesorter 2.0 - Chart Widget</title>

	<!-- jQuery -->
	<script src="./js/jquery-latest.min.js"></script>
	<script src="./js/jquery-ui.min.js"></script>

	<!-- Demo stuff -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
	<link class="ui-theme" rel="stylesheet" href="./css/jquery-ui.min.css">
	<link rel="stylesheet" href="./css/jq.css">
	<link rel="stylesheet" href="./css/prettify.css">
	<script src="./js/prettify.js"></script>
	<script src="./js/docs.js"></script>
	<!--<link rel="stylesheet" href="css/bootstrap-v3.min.css">-->
	<script src="./js/bootstrap.min.js"></script>

	<!-- Google charts -->
	<script src="https://www.google.com/jsapi"></script>

	<!-- Tablesorter: required -->
	<link rel="stylesheet" href="./css/theme.blue.css">
	<script src="./js/jquery.tablesorter.js"></script>
	<script src="./js/jquery.tablesorter.widgets.js"></script>

	<script src="./js/widgets/widget-cssStickyHeaders.min.js"></script>
	<script src="./js/widgets/widget-columnSelector.min.js"></script>
	<script src="./js/widgets/widget-pager.min.js"></script>
	<script src="./js/widgets/widget-chart.min.js"></script>

	<style>
	h1 { font-size: 28px; }
	/* override jQuery UI overriding Bootstrap */
	.accordion .ui-widget-content a {
		color: #337ab7;
	}
	/* .spacer in docs css is set to height:800px for stickyHeader demo */
	#pager .spacer { height: auto; }
	.wrapper { padding: 5px; }
	</style>

	<style id="css">#pager div {
	display: inline-block;
}

#pager-container .spacer {
	padding: 0 5px;
}

#pager-container,
#chart-container {
	text-align: center;
}

#chart-container > div {
	display: inline-block;
}

#chartbar {
	text-align: center;
}

#chartbar i.clickable {
	padding: 0 5px;
}

i.clickable {
	cursor: pointer;
	color: #999;
}

i.clickable.disabled,
i.clickable.disabled:hover {
	cursor: not-allowed;
	color: #ccc;
}

i.active.clickable,
i.clickable:hover {
	color: #000;
}

/*** custom css only popup ***/
.columnSelector, .hidden, #chart-container {
	display: none;
}
#colSelect:checked + label {
	background: #5797d7;
	border-color: #555;
}
#colSelect:checked ~ #columnSelector {
	display: block;
}
#chartSelect:checked + label {
	background: #5797d7;
	border-color: #555;
}
.columnSelector {
	width: 120px;
	position: absolute;
	margin-top: 5px;
	padding: 10px;
	background: #fff;
	border: #ccc 1px solid;
	border-radius: 5px;
}
.columnSelector label {
	display: block;
	text-align: left;
}
.columnSelector label:nth-child(1) {
	border-bottom: #99bfe6 solid 1px;
	margin-bottom: 5px;
}
.columnSelector input {
	margin-right: 5px;
}
.columnSelector .disabled {
	color: #ddd;
}</style>
</head>