<ul class="nav">
<li class="nav"><a href="?f=home">Home</a></li>
<li class="nav"><a href="?f=build">Build</a></li>
<li class="nav"><a href="?f=plotting">Plotting</a></li>
<li class="nav"><a href="?f=production">Production</a></li>
<li class="nav"><a href="?f=consumed">Consumed</a></li>
<li class="nav"><a href="?f=weighing">Weighing</a></li>
<li class="nav"><a href="?f=weighing_error_log">Weighing_Error_Log</a></li>
<li class="nav"><a href="?f=celllog">CellLog</a></li>
<li class="nav"><a href="?f=celllog_build">CellLog_Build</a></li>
<li class="nav"><a href="?f=getChannelNormalData">Channel Normal</a></li>
<li class="nav"><a href="?f=precurweight">Precursor Weight</a></li>
<li class="nav"><a href="?f=devpage">DevPage</a></li>
<li class="nav"><a href="?f=devpage2">DevPage2</a></li>
<li class="nav"><a href="?f=help">Help</a></li>
<li class="nav"><a href="?f=contact">Contact</a></li>
</ul>