<script src="js/celllog_insert.js"></script>
<?php

require_once('dbcon/connect_SP_RND.php'); 
//echo "<p>Connection Made.</p>";

if ($_REQUEST["dd"] != null){
	$query = $_REQUEST["dd"];
};
echo $query . "<br/>";

$stmt = sqlsrv_query($dbCon, $query);

if($stmt === false) {
    die(print_r(sqlsrv_errors(), true));
}
sqlsrv_free_stmt($stmt);


//$tsql = "SELECT MAX(clid) FROM ProdConductivity";
$tsql = "SELECT MAX(CLID) FROM cell_log";
$stmt = sqlsrv_query( $dbCon, $tsql);  
$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_BOTH);
echo "<p>Last Index: " . $row[0] . "</p>";

sqlsrv_free_stmt($stmt);

$clid = 0;

if($_REQUEST["clid"] != null){
	$clid = $_REQUEST["clid"];
	//echo $clid . "<br/>";
}


$tsql = "SELECT * FROM cell_log where clid=$clid";
$stmt = sqlsrv_query( $dbCon, $tsql);  
//echo $stmt;
if ( $stmt )  
{  
    // echo "Statement executed.<br>\n";  
}   
else   
{  
     echo "Error in statement execution.\n";  
     die( print_r( sqlsrv_errors(), true));  
}  
//echo "query complete.";
$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_BOTH);
//echo $row['MaterialID'];

?>

<form id="template" method="post" action="index.php?f=build">
<input name="clid" size="6" value="<?php echo $clid ?>" type="text" />
</form>

<table id="page"><tr><td>
<form id="builder" method="post" action="index.php?f=production"> 
<table align="center" border="0">
<tbody>
<tr>
<td align="right">Cell Name:</td>
<td align="left"><input name="cellname" size="25" value="<?php echo $row['cellname']; ?>" type="text" /></td>
</tr>
<tr>
<td align="right">Purpose/Description:</td>
<td align="left"><input name="purposedescription" size="25" value="<?php echo $row['purposedescription']; ?>" type="text" /></td>
<td align="right">Cathode:</td>
<td align="left"><input name="cathode" size="5"  value="<?php echo $row['cathode']; ?>" type="text" /></td>
<td align="right">Temperature:</td>
<td align="left"><input name="temperature" size="5" value="<?php echo $row['temperature']; ?>" type="text" />&deg;C</td>
</tr>
<tr>
<td align="right">Max. Voltage:</td>
<td align="left"><input name="maxvoltage" size="5" value="<?php echo $row['maxvoltage']; ?>" type="text" /> v</td>
<td align="right">Die:</td>
<td align="left"><input name="die" size="5" value="<?php echo $row['die']; ?>" type="text" /></td>
<td align="right">Current Collector:</td>
<td align="left"><input name="currentcollector" size="5" value="<?php echo $row['currentcollector']; ?>" type="text" /></td>
</tr>
<tr>
<tr>
<td align="right">Active Cathode:</td>
<td align="left"><input name="activecathode" size="25" value="<?php echo $row['activecathode']; ?>" type="text" /></td>
<td align="right">Active Cathode Date Coated:</td>
<td align="left"><input name="activecathodedatecoated" size="25" value="<?php echo $row['activecathodedatecoated']; ?>" type="text" /></td>
</tr>

<tr>
<td align="right">Electrolyte:</td>
<td align="left"><input name="electrolyte" size="25" value="<?php echo $row['electrolyte']; ?>" type="text" /></td>
</tr>

<tr>
<td align="right">Total Cathode Mass:</td>
<td align="left"><input name="totalcathodemass" size="25" value="<?php echo $row['totalcathodemass']; ?>" type="text" /> g</td>
<td align="right">Die Diameter:</td>
<td align="left"><input name="diediameter" size="25" value="<?php echo $row['diediameter']; ?>" type="text" /> cm</td>
</tr>

<tr>
<td align="right">Cathode Particle Coating:</td>
<td align="left"><input name="cathodeparticlecoating" size="25" value="<?php echo $row['cathodeparticlecoating']; ?>" type="text" /></td>
<td align="right">Weight Coating Percent:</td>
<td align="left"><input name="weightcoatingpercent" size="25" value="<?php echo $row['weightcoatingpercent']; ?>" type="text" /></td>
</tr>

<tr>
<td align="right">Nominal Active Material Percent:</td>
<td align="left"><input name="nominalactivematerialpercent" size="25" value="<?php echo $row['nominalactivematerialpercent']; ?>" type="text" /></td>
<td align="right">Ratio Active Percent:</td>
<td align="left"><input name="ratioactivepercent" size="25" value="<?php echo $row['ratioactivepercent']; ?>" type="text" /></td>
</tr>

<tr>
<td colspan="6" align="left"><p id="datadump"></p>
<input name="dd" type="hidden" />
</td>
</tr>
<tr>
<td colspan="6" align="right"><input value="Submit" onclick="submit()" type="submit" /></td>
</tr>
</tbody>
</table>
</form>
</td></tr></table>