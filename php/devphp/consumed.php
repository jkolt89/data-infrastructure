
	<script id="js">$(function() {

	var $table = $('table').tablesorter({
		theme: 'blue',
		widgets: ["zebra", "filter"],
		widgetOptions : {
			// filter_anyMatch replaced! Instead use the filter_external option
			// Set to use a jQuery selector (or jQuery object) pointing to the
			// external filter (column specific or any match)
			filter_external : '.search',
			// add a default type search to the first name column
			filter_defaultFilter: { 1 : '~{query}' },
			// include column filters
			filter_columnFilters: true,
			filter_placeholder: { search : 'Search...' },
			filter_saveFilters : true,
			filter_reset: '.reset'
		}
	});

	// make demo search buttons work
	$('button[data-column]').on('click', function() {
		var $this = $(this),
			totalColumns = $table[0].config.columns,
			col = $this.data('column'), // zero-based index or "all"
			filter = [];

		// text to add to filter
		filter[ col === 'all' ? totalColumns : col ] = $this.text();
		$table.trigger('search', [ filter ]);
		return false;
	});

});</script>

<!-- db stuff -->
<?php
require_once('dbcon/connect.php'); 
//echo "<p>Connection Made.</p>";

$tsql = "SELECT * FROM vuConsumedBatch";  
//$tsql = "SELECT * FROM sys.Tables";

//$tsql = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='ProdConductivity'";
/* Execute the query. */  

$stmt = sqlsrv_query( $dbCon, $tsql);  

if ( $stmt )  
{  
     //echo "Statement executed.<br>\n";  
}   
else   
{  
     echo "Error in statement execution.\n";  
     die( print_r( sqlsrv_errors(), true));  
}  

?>


<!-- end db stuff -->

<body>
<div id="banner">
	<h1>table<em>sorter</em></h1>
	<h2>SP Search and Filter</h2>
	<h3>Flexible client-side table sorting</h3>
</div>
<div id="main">
	<h1>Demo</h1>

	<div id="demo"><input class="search" type="search" data-column="all"> (Match any column)<br>
<input class="search" type="search" data-column="1"> (First Name; fuzzy search... try "be")<br>

<!-- targeted by the "filter_reset" option -->
<button type="button" class="reset">Reset Search</button>

<table class="tablesorter">
	<thead>
		<tr>
			<th>Lot #</th>
			<th>Pass/Fail</th>
			<th>StartDate</th>
			<th>FinishDate</th>
			<th>Yield (g)</th>
			<th>Yield (%)</th>
			<th>Guse Gen</th>
			<th>Conductivity (mS/cm)</th>
		</tr>
	</thead>
	<tbody>
	
 <?php
/* Iterate through the result set printing a row of data upon each iteration.*/  

while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_BOTH))  
{  
echo "<tr>";
     echo "<td>" . $row['LotNr'] . "</td>";
     echo "<td>" . $row['PassFail'] . "</td>";
     echo "<td>" . $row['StartDate'] . "</td>";
     echo "<td>" . $row['FinishDate'] . "</td>";
     echo "<td>" . $row['Yield_g'] . "</td>";
     echo "<td>" . $row['YieldQuantityRatio'] . "</td>";
     echo "<td>" . $row['GuseGen'] . "</td>";
     echo "<td>" . $row['Conductivity_mScm'] . "</td>";
}  

/* Free statement and connection resources. */  
sqlsrv_free_stmt( $stmt);  
sqlsrv_close( $conn);  
?>
	</tbody>
</table></div>
</div>