function build_celllog() {

var input = document.getElementById("build_log");
// var query = "exec dbo.Cell_Log_Insert '" + input.cellname.value + "'";

///////////////////////////////////////////////////////////////////////////////////////////
//var query = "exec dbo.Cell_Log_Insert 'deleteme'";
//var query = "insert into dbo.cell_log (CellName) values ('deleteme')";
var query = "exec dbo.Cell_Log_Insert @cellname = '" + input.cellname.value +"', @purposedescription='" + input.purposedescription.value 
	+ "', @Cathode='" + input.cathode.value + "', @temperature='" + input.temperature.value +"', @MaxVoltage='" + input.maxvoltage.value 
	+ "', @die='" + input.die.value + "', @CurrentCollector='" + input.currentcollector.value +"', @ActiveCathode='" + input.activecathode.value 
	+ "', @ActiveCathodeDateCoated='" + input.activecathodedatecoated.value + "', @electrolyte='" + input.electrolyte.value 
	+ "', @TotalCathodeMass_g='" + input.totalcathodemass.value 
	+ "', @DieDiameter_cm='" + input.diediameter.value + "', @CathodeParticleCoating='" + input.cathodeparticlecoating.value 
	+ "', @WeightCoatingPercent='" + input.weightcoatingpercent.value + "', @NominalPercentActiveMaterial='" + input.nominalactivematerialpercent.value 
	+ "', @RatioActiveMaterial='" + input.ratioactivepercent.value + "', @ActiveMaterialTheoreticalCapacity_mAhg='" + input.ActiveMaterialTheoreticalCapacity_mAhg.value
	+ "', @CellTheoreticalCapacity_mAhg='" + input.CellTheoreticalCapacity_mAhg.value + "', @Loading_mg_cm2='" + input.Loading_mg_cm2.value
	+ "', @Capacity_mAh='" + input.Capacity_mAh.value + "', @Anode='" + input.Anode.value
	+ "', @AnodeMass_g='" + input.AnodeMass_g.value + "', @Separator='" + input.Separator.value
	+ "', @SeparatorMass_g='" + input.SeparatorMass_g.value + "', @MaximumStackPressure_ton='" + input.MaximumStackPressure_ton.value
	+ "', @CathodePressure_ton='" + input.CathodePressure_ton.value + "', @CageTorque_lbs='" + input.CageTorque_lbs.value
	+ "', @Operator='" + input.Operator.value + "', @Location='" + input.Location.value
	+ "', @NrCyclesCompleted='" + input.NrCyclesCompleted.value + "', @FirstCycleCEPercent='" + input.FirstCycleCEPercent.value
	+ "', @FirstCycleCharge='" + input.FirstCycleCharge.value + "', @FirstCycleDischarge='" + input.FirstCycleDischarge.value
	+ "', @InitialResistance_ohm='" + input.InitialResistance_ohm.value + "', @MaxVoltageWithOutlier='" + input.MaxVoltageWithOutlier.value
	+ "', @MaxVoltageNoOutlier='" + input.MaxVoltageNoOutlier.value + "', @MaxDischargeCapacity='" + input.MaxDischargeCapacity.value
	+ "', @ExcelTime='" + input.ExcelTime.value + "', @PythonTime='" + input.PythonTime.value
	+ "', @LastDateTime='" + input.LastDateTime.value + "', @Channel='" + input.Channel.value
	+ "', @ArbinNotes='" + input.ArbinNotes.value + "', @StoppageReason='" + input.StoppageReason.value
	+ "', @StoppageCycleNr='" + input.StoppageCycleNr.value + "', @Notes='" + input.Notes.value
	+ "'";

///////////////////////////////////////////////////////////////////////////////////////////



document.getElementById("datadump").innerHTML = query;
input.dd.value = query;

return false;
}


// Submit form with name function.
function submit_celllog() {
	build_celllog();
	var x = document.getElementsByName('build_log');
	x[0].submit(); //form submission
}

function updateCond() {
var input = document.getElementById("updater");

var materialMass = input.mass.value;
var thickness = (input.lengthTotal.value - input.lengthTare.value)/10.0;
var ionicIntercept = input.ionicIntercept.value;
input.density.value = Math.round(1000*materialMass/(thickness*0.8*0.8*3.14159))/1000.0;
input.ionic.value = Math.round(1000*(thickness/(ionicIntercept*0.8*0.8*3.14159))*1000.0)/1000.0;
input.electronic.value = Math.round(1000*(thickness/(input.electronicIntercept.value*0.8*0.8*3.14159))*1e9)/1000.0;

var query = "UPDATE ProdConductivity SET UpdateTime=GetDate() WHERE pcid=" + input.pcid.value + ";"
			+ "INSERT INTO ProdConductivity (MaterialID, Operator, LotNr, TestCondition, TestTemperature, MaterialMass_g, "
			+ "PlungerLengthNoPellet_mm, PelletPlungerThickness_mm, XAxisNyquist_ohms, Notes)"
			+ "VALUES( "
			+ "'" + input.MaterialID.value + "', "
			+ "'" + input.operator.value + "', "
			+ "'" + input.lotNum.value + "', "
			+ (input.ionicIntercept.value > 0? "'Ionic'": (input.electronicIntercept.value > 0? "'Electronic'": null)) + ", "
			+ input.temperature.value + ", "
			+ materialMass + ", "
			+ input.lengthTare.value + ", "
			+ input.lengthTotal.value + ", "	
			+ (input.ionicIntercept.value > 0? input.ionicIntercept.value: (input.electronicIntercept.value > 0? input.electronicIntercept.value: null)) + ", "
			+ "'" + input.notes.value + "'"
			+ ")";
			
document.getElementById("datadump").innerHTML = query;
input.dd.value = query;
			
return false;
}
// update table .
function update() {
	updateCond();
	var x = document.getElementsByName('updater');
	
	x[0].submit(); //form submission
}