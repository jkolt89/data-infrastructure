<h1>Google Charts Demo</h1>
	<div id="demo"><div id="chart-container">
		<div id="chart"></div>
		<p></p>
		<div id="chartbar">
			<i class="fa fa-cube active clickable tooltip" title="3D Pie Chart" data-toggle="tooltip" data-placement="top"></i>
			<i class="fa fa-pie-chart clickable" title="Pie Chart" data-toggle="tooltip" data-placement="top"></i>
			<i class="fa fa-line-chart clickable" title="Line Graph" data-toggle="tooltip" data-placement="top"></i>
			<i class="fa fa-area-chart clickable" title="Area Graph" data-toggle="tooltip" data-placement="top"></i>
			<i class="fa fa-bar-chart clickable" title="Vertical Bar Chart" data-toggle="tooltip" data-placement="top"></i>
			<i class="fa fa-tasks fa-rotate-90 clickable" title="Stacking Vertical Bar Chart" data-toggle="tooltip" data-placement="top"></i>
			<i class="fa fa-align-left clickable" title="Horizontal Bar Chart" data-toggle="tooltip" data-placement="top"></i>
			<i class="fa fa-tasks fa-rotate-180 clickable" title="Stacking Horizontal Bar Chart" data-toggle="tooltip" data-placement="top"></i>
		</div>
		<p></p>
	</div>

<!-- ColumnSelector -->
<label for="colSelect" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Toggle visible columns">
	<input id="colSelect" type="checkbox" class="hidden">
	<i class="fa fa-table"></i> Column
	<div id="columnSelector" class="columnSelector"></div>
</label>
<!-- Chart -->
<label for="chartSelect" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Toggle chart visibility">
	<input id="chartSelect" type="checkbox" class="hidden">
	<i class="fa fa-pie-chart"></i> Chart
</label>
<!-- row selector -->
<span class="wrapper" data-toggle="tooltip" data-placement="top" title="Chart filtered, visible or all rows">
<div class="btn-group chart-filter-all" data-toggle="buttons">
	<label class="btn btn-default btn-sm active">
		<input type="radio" name="getrows" data-type="filter" checked="checked"> Filtered
	</label>
	<label class="btn btn-default btn-sm">
		<input type="radio" name="getrows" data-type="visible"> Visible
	</label>
	<label class="btn btn-default btn-sm">
		<input type="radio" name="getrows" data-type="all"> All
	</label>
</div>
</span>

<!-- db stuff -->
<?php
require_once('dbcon/connect.php'); 
//echo "<p>Connection Made.</p>";

if ($_REQUEST["dd"] != null){
	$query = $_REQUEST["dd"];
	$_POST['dd'] = NULL;
}

echo "|" . $query . "|<br/>";

$stmt = sqlsrv_query($dbCon, $query);

if($stmt === false) {
    die(print_r(sqlsrv_errors(), true));
}
sqlsrv_free_stmt($stmt);

$tsql = "SELECT * FROM SolventDrying WHERE updateTime IS NULL";  
/* Execute the query. */  

$stmt = sqlsrv_query( $dbCon, $tsql);  

if ( $stmt )  
{  
     //echo "Statement executed.<br>\n";  
}   
else   
{  
     echo "Error in statement execution.\n";  
     die( print_r( sqlsrv_errors(), true));  
}  

?>

<form id="creater" method="post" action="index.php?f=plotSolvents"> 
	<input name="dd" size="100%" type="hidden" />
	<input value="Create New Entry" onclick="createEntry()" type="submit" />
</form>

<!-- end db stuff -->
<table id="table" class="tablesorter">
	<thead>
		<tr>
			<th>EntryDate</th>
			<th>Solvent</th>
			<th>Supplier</th>
			<th>Supplier Lot#</th>
			<th data-placeholder="Fuzzy search">SP Lot #</th>
			<th>Part#</th>
			<th>Moisture Content (ppm)</th>
			<th>Notes</th>
		</tr>
	</thead>
	<tbody>
 
 <?php
/* Iterate through the result set printing a row of data upon each iteration.*/  

while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_BOTH))  
{  
echo "<tr class=\"soldata\">";
	echo "<td>" . "<p hidden class=\"pcid\">" . $row['SDID'] . "</p>" . $row['EntryDate'] . "</td>";
     echo "<td>" . $row['Item'] . "</td>";
     echo "<td>" . $row['Supplier'] . "</td>";
     echo "<td>" . $row['SupplierLotNr'] . "</td>";
     echo "<td>" . $row['SPLotNr'] . "</td>";
     echo "<td>" . $row['PartNr'] . "</td>";
     echo "<td>" . $row['KSolidMoisture_ppm'] . "</td>";
     echo "<td>" . $row['Notes'] . "</td></tr>";  
}  

/* Free statement and connection resources. */  
sqlsrv_free_stmt( $stmt);  
sqlsrv_close( $conn);  
?>

<!-- pager -->
<div id="pager-container">
	<div id="pager">
		<form>
			<select class="pagesize" title="Select rows per page" data-toggle="tooltip" data-placement="left">
				<option value="5">5</option>
				<option value="10">10</option>
				<option value="25">25</option>
				<option value="50">50</option>
				<option value="100">100</option>
				<option value="1000">1000</option>
			</select>
			<div class="spacer"><div class="separator"></div></div>
			<i class="fa fa-step-backward first clickable" title="First page"></i>
			<i class="fa fa-backward prev clickable" title="Previous page"></i>
			<div class="spacer"><div class="separator"></div></div>
			<span class="pagedisplay"></span>
			<div class="spacer"><div class="separator"></div></div>
			<i class="fa fa-forward next clickable" title="Next page"></i>
			<i class="fa fa-step-forward last clickable" title="Last page"></i>
			<div class="spacer"><div class="separator"></div></div>
			<select class="gotoPage" title="Select page" data-toggle="tooltip" data-placement="right"></select>
		</form>
	</div>
</div>
</div>

</div>

<script id="js">google.load("visualization", "1.1", {
	packages: ["bar", "corechart", "line"]
});

$(function() {
	/* Initial settings */
	var $table = $('#table'),
		$chart = $('#chart'),
		$bar = $('#chartbar'),
		$rowType = $('[name=getrows]'),
		$icons = $('#chart-container i'),
		initType = 'pie', // graph types ('pie', 'pie3D', 'line', 'area', 'vbar', 'vstack', 'hbar' or 'hstack')
		chartTitle = 'Solvent Properties',
		axisTitle = 'Year',
		width = 900,
		height = 500,
		// extra data processing
		processor = function(data) {
			// console.log(data);
			return data;
		},

	// don't change anything below, unless you want to remove some types; modify styles and/or font-awesome icons
	types = {
		pie3D  : { in3D: true,  maxCol: 2, stack: false, type: 'pie',  titleStyle: { color: '#333' }, icon: 'fa-cube' },
		pie    : { in3D: false, maxCol: 2, stack: false, type: 'pie',  titleStyle: { color: '#333' }, icon: 'fa-pie-chart' },
		line   : { in3D: false, maxCol: 99,stack: false, type: 'line', titleStyle: { color: '#333' }, icon: 'fa-line-chart' },
		area   : { in3D: false, maxCol: 5, stack: false, type: 'area', titleStyle: { color: '#333' }, icon: 'fa-area-chart' },
		vbar   : { in3D: false, maxCol: 5, stack: false, type: 'vbar', titleStyle: { color: '#333' }, icon: 'fa-bar-chart' },
		vstack : { in3D: false, maxCol: 5, stack: true,  type: 'vbar', titleStyle: { color: '#333' }, icon: 'fa-tasks fa-rotate-90' },
		hbar   : { in3D: false, maxCol: 5, stack: false, type: 'hbar', titleStyle: { color: '#333' }, icon: 'fa-align-left' },
		hstack : { in3D: false, maxCol: 5, stack: true,  type: 'hbar', titleStyle: { color: '#333' }, icon: 'fa-tasks fa-rotate-180' }
	},
	/* internal variables */
	settings = {
		table : $table,
		chart : $chart[0],
		chartTitle : chartTitle,
		axisTitle : axisTitle,
		type : initType,
		processor : processor
	},
	drawChart = function() {
		if (!$table[0].config) { return; }
		var options, chart, numofcols, data,
			s = settings,
			t = types[s.type],
			obj = s.chart,
			rawdata = $table[0].config.chart.data;
		if ( $.isFunction( s.processor ) ) {
			rawdata = s.processor( rawdata );
		}
		if ( rawdata.length < 2 ) {
			return;
		}
		data = google.visualization.arrayToDataTable( rawdata );

		numofcols = rawdata[1].length;
		if (numofcols > t.maxCol) {
			// default to line chart if too many columns selected
			t = types['line'];
		}

		options = {
			title: s.chartTitle,
			chart: {
				title: s.chartTitle
			},
			hAxis: {
				title: s.axisTitle,
				titleTextStyle: t.titleStyle
			},
			vAxis: {},
			is3D: t.in3D,
			isStacked: t.stack,
			width: width,
			height: height
		};

		if (t.type == 'vbar' && !t.stack) {
			chart = new google.charts.Bar(obj);
		} else if (t.type == 'vbar') {
			chart = new google.visualization.ColumnChart(obj);
		} else if (t.type == 'hbar') {
			options.hAxis = {};
			options.vAxis = {
				title: s.axisTitle,
				titleTextStyle: t.titleStyle,
				minValue: 0
			};
			chart = new google.visualization.BarChart(obj);
		} else if (t.type == 'area') {
			chart = new google.visualization.AreaChart(obj);
		} else if (t.type == 'line') {
			chart = new google.charts.Line(obj);
		} else {
			chart = new google.visualization.PieChart(obj);
		}
		chart.draw(data, options);
	};

	$('#chartSelect').change(function() {
		$('#chart-container').slideToggle( $(this).is(':checked') );
		drawChart();
	});

	$icons.click(function(e) {
		if ( $(e.target).hasClass('disabled') ) {
			return true;
		}
		$icons.removeClass('active');
		var $t = $(this).addClass('active');
		$.each(types, function(i, v) {
			if ($t.hasClass(v.icon)) {
				settings.type = i;
			}
		});
		drawChart();
	});

	$rowType.on('change', function() {
		$table[0].config.widgetOptions.chart_incRows = $rowType.filter(':checked').attr('data-type');
		// update data, then draw new chart
		$table.trigger('chartData');
		drawChart();
	});

	$table.on('columnUpdate pagerComplete', function(e) {
		var table = this,
			c = table.config,
			t = types['pie'],
			max = t && t.maxCol || 2;
		setTimeout(function() {
			if (table.hasInitialized) {
				$table.trigger('chartData');
				drawChart();
				// update chart icons
				if (typeof c.chart !== 'undefined') {
					var cols =  c.chart.data[0].length;
					if (cols > max) {
						$bar.find('.fa-cube, .fa-pie-chart').addClass('disabled');
						if ($bar.find('.fa-cube, .fa-pie-chart').hasClass('active')) {
							$bar.find('.fa-cube, .fa-pie-chart').removeClass('active');
							$bar.find('.fa-line-chart').addClass('active');
						}
					} else {
						$bar.find('.fa-cube, .fa-pie-chart').removeClass('disabled');
						if (settings.type == 'pie') {
							$bar.find('.active').removeClass('active');
							$bar.find( settings.in3D ? '.fa-cube' : '.fa-pie-chart' ).addClass('active');
						}
					}
				}
			}
		}, 10);
	});

	$table
		.tablesorter({
			theme: 'blue',
			// default sortInitialOrder setting
			sortList: [[0,1]],
			widgets: ['pager', 'zebra', 'filter', 'cssStickyHeaders', 'columnSelector', 'chart'],
			widgetOptions: {
				columnSelector_container: '#columnSelector',
				cssStickyHeaders_filteredToTop: false,
				pager_selectors: { container: '#pager' },
				pager_output: 'Showing {startRow} to {endRow} of {filteredRows} results',
				pager_size: 100,
				chart_incRows: 'f',
				chart_useSelector: true
			}
		});

});</script>

<script>
$(function() {
	$('[data-toggle="tooltip"]').tooltip();
});
</script>

<div class="editEntry">You can update date in this table by clicking on a row. A table will replace this text.
</div>
	<script>
	var valIndex="";
		$("tr.soldata").click(function(){
			valIndex= $(this).find( "p.pcid" ).text();
			$("tr.soldata").css("font-weight","normal");
			$(this).css("font-weight","bold");
			$( "div.editEntry" ).html( "Showing details: " + valIndex );
			
	 if( valIndex.length > 0 ){
        $.ajax({
            type:'GET',
            url: 'editSol.php',
	    data: {'SDID': valIndex},
            dataType: 'html',
            success: function(result){
                $('div.editEntry').html(result);
            } // End of success function of ajax form
        }); // End of ajax call    
        }
        else{
        	$( "div.editEntry" ).text("Select information." );
        }

	$( "div.editEntry" ).show();
    });
	</script>