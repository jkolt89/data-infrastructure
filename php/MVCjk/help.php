<p>This will eventually have instructions to aid the data diving.</p>
<ul>
	<li>Build</li>
		<ul>
			<li>Build your data</li>
			<li>Use the first text box to build a template</li>
			<li>The first number shown is the last data set added</li>
			<li>You can find the ID on the Production tab click the one you want, then scroll to the bottom.</li>
			<li>sub-menu</li>
				<ul>
					<li>Click and view</li>
					<li>Filter as desired</li>
				</ul>
		</ul>
	<li>Plotting</li>
		<ul>
			<li>Filter data as desired</li>
			<li>Select the chart icon</li>
			<li>Limit the columns</li>
		</ul>
	<li>Production</li>
		<ul>
			<li>Click and view</li>
			<li>Filter as desired</li>
		</ul>
	<li>Weighing</li>
		<ul>
			<li>Click and view</li>
			<li>Filter as desired</li>
		</ul>
	<li>Tools</li>
		<ul>
			<li><a href="viewer.php" target="_blank">Viewer</a></li>
			<li>Filter as desired</li>
		</ul>
</ul>