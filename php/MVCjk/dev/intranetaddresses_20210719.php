<div class="editEntry">You can update date in this table by clicking on a row. A table will replace this text.
</div>
	<h1>Intranet IP Addresses</h1>

	<div id="demo"><div id="chart-container">
		<div id="chart"></div>
		<p></p>
		<div id="chartbar">
			<i class="fa fa-cube active clickable toolti" title="3D Pie Chart" data-toggle="tooltip" data-placement="top"></i>
			<i class="fa fa-pie-chart clickable" title="Pie Chart" data-toggle="tooltip" data-placement="top"></i>
			<i class="fa fa-line-chart clickable" title="Line Graph" data-toggle="tooltip" data-placement="top"></i>
			<i class="fa fa-area-chart clickable" title="Area Graph" data-toggle="tooltip" data-placement="top"></i>
			<i class="fa fa-bar-chart clickable" title="Vertical Bar Chart" data-toggle="tooltip" data-placement="top"></i>
			<i class="fa fa-tasks fa-rotate-90 clickable" title="Stacking Vertical Bar Chart" data-toggle="tooltip" data-placement="top"></i>
			<i class="fa fa-align-left clickable" title="Horizontal Bar Chart" data-toggle="tooltip" data-placement="top"></i>
			<i class="fa fa-tasks fa-rotate-180 clickable" title="Stacking Horizontal Bar Chart" data-toggle="tooltip" data-placement="top"></i>
		</div>
		<p></p>
	</div>

<!-- ColumnSelector -->
<label for="colSelect" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Toggle visible columns">
	<input id="colSelect" type="checkbox" class="hidden">
	<i class="fa fa-table"></i> Column
	<div id="columnSelector" class="columnSelector"></div>
</label>
<!-- Chart -->
<label for="chartSelect" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Toggle chart visibility">
	<input id="chartSelect" type="checkbox" class="hidden">
	<i class="fa fa-pie-chart"></i> Chart
</label>
<!-- row selector -->
<span class="wrapper" data-toggle="tooltip" data-placement="top" title="Chart filtered, visible or all rows">
<div class="btn-group chart-filter-all" data-toggle="buttons">
	<label class="btn btn-default btn-sm active">
		<input type="radio" name="getrows" data-type="filter" checked="checked"> Filtered
	</label>
	<label class="btn btn-default btn-sm">
		<input type="radio" name="getrows" data-type="visible"> Visible
	</label>
	<label class="btn btn-default btn-sm">
		<input type="radio" name="getrows" data-type="all"> All
	</label>
</div>
</span>


<!-- db stuff -->
<?php
require_once('dbcon/connect_SP_ManuLine.php'); 
//echo "<p>Connection Made.</p>";

if ($_REQUEST["dd"] != null){
	$query = $_REQUEST["dd"];
	$_POST['dd'] = NULL;
}

echo "|" . $query . "|<br/>";

$stmt = sqlsrv_query($dbCon, $query);

if($stmt === false) {
    die(print_r(sqlsrv_errors(), true));
}
sqlsrv_free_stmt($stmt);

#$tsql = "select iaid, IPaddress, Initiator, ControllerDesc, comments, UpdateDate from dbo.IntranetAddress";
$tsql = "select iaid, IPaddress, Initiator, ControllerDesc, comments, UpdateDate from dbo.IntranetAddress order by cast(replace(cast(substring(ipaddress, 9,2) as varchar(2)) + cast(substring(ipaddress, 12,3) as varchar(3)), '.', '') as int)"
/* Execute the query. */  

$stmt = sqlsrv_query( $dbCon, $tsql);  

if ( $stmt )  
{  
     //echo "Statement executed.<br>\n";  
}   
else   
{  
     echo "Error in statement execution.\n";  
     die( print_r( sqlsrv_errors(), true));  
}  

?>

<form id="creater" method="post" action="index.php?f=intranetaddresses"> 
	<input name="dd" size="100%" type="hidden" />
	<input value="Create New Entry" onclick="createCondEntry()" type="submit" />
</form>

<!-- end db stuff -->

<table id="table" class="tablesorter">
	<thead>
		<tr>
			<th>iaid</th>
			<th data-placeholder="Fuzzy search">IPaddress</th>
			<th>Initiator</th>
			<th>ControllerDesc</th>
			<th>comments</th>
			<th>UpdateDate</th>

		</tr>
	</thead>
	<tbody>
 
 <?php




while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_BOTH))  
{  
echo "<tr>";
	echo "<td>" . $row['iaid'] . "</td>";
	echo "<td>" . $row['IPaddress'] . "</td>";
	echo "<td>" . $row['Initiator'] . "</td>";
	echo "<td>" . $row['ControllerDesc'] . "</td>";
	echo "<td>" . $row['comments'] . "</td>";
	echo "<td>" . $row['UpdateDate'] . "</td>";
	
}  

/* Free statement and connection resources. */  
sqlsrv_free_stmt( $stmt);  
sqlsrv_close( $conn);  
?>
	</tbody>
</table></div>
</div>