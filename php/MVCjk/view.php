
	<!-- jQuery -->
	<script src="js/jquery-latest.min.js"></script>

	<!-- Demo stuff -->
	<link class="ui-theme" rel="stylesheet" href="css/jquery-ui.min.css">
	<script src="js/jquery-3.5.1.js"></script>
	<link rel="stylesheet" href="css/jq.css">
	<link href="css/prettify.css" rel="stylesheet">
	<script src="js/prettify.js"></script>
	<script src="js/docs.js"></script>

	<!-- Tablesorter: required -->
	<link rel="stylesheet" href="css/theme.blue.css">
	<script src="js/jquery.tablesorter.js"></script>
	<script src="js/widgets/widget-storage.js"></script>
	<script src="js/widgets/widget-filter.js"></script>

	<script id="js">$(function() {

	var $table = $('table').tablesorter({
		theme: 'blue',
		widgets: ["zebra", "filter"],
		widgetOptions : {
			// filter_anyMatch replaced! Instead use the filter_external option
			// Set to use a jQuery selector (or jQuery object) pointing to the
			// external filter (column specific or any match)
			filter_external : '.search',
			// add a default type search to the first name column
			filter_defaultFilter: { 1 : '~{query}' },
			// include column filters
			filter_columnFilters: true,
			filter_placeholder: { search : 'Search...' },
			filter_saveFilters : true,
			filter_reset: '.reset'
		}
	});

	// make demo search buttons work
	$('button[data-column]').on('click', function() {
		var $this = $(this),
			totalColumns = $table[0].config.columns,
			col = $this.data('column'), // zero-based index or "all"
			filter = [];

		// text to add to filter
		filter[ col === 'all' ? totalColumns : col ] = $this.text();
		$table.trigger('search', [ filter ]);
		return false;
	});

});</script>


<?php
require_once('dbcon/connect.php'); 
//echo "<p>Connection Made.</p>";

$viewMe = $_GET["view"];
echo $viewMe . "<br/>";

$tsql = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='" . $viewMe . "'";

echo $tsql . "<br>\n";

$stmt = sqlsrv_query( $dbCon, $tsql);  

if ( $stmt )  
{  
     echo "Statement executed.<br>\n";  
}   
else   
{  
     echo "Error in statement execution.\n";  
     die( print_r( sqlsrv_errors(), true));  
}  
?>

<div id="banner">
	<h1>table<em>sorter</em></h1>
	<h2>SP Search and Filter</h2>
	<h3>Flexible client-side table sorting</h3>
</div>
<div id="main">
	<h1>Demo</h1>

	<div id="demo"><input class="search" type="search" data-column="all"> (Match any column)<br>
<input class="search" type="search" data-column="1"> (First Name; fuzzy search... try "be")<br>

<!-- targeted by the "filter_reset" option -->
<button type="button" class="reset">Reset Search</button>

<table class="tablesorter"  style="width:240px;">
	<thead>
		<tr>
			<th>DB</th>
			<th data-placeholder="Fuzzy search">type</th>
			<th>Table</th>
			<th>Name</th>
			<th>Field</th>
		</tr>
	</thead>
	<tbody>
 
 <?php
/* Iterate through the result set printing a row of data upon each iteration.*/  

$names = array();

while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC))  {  
	echo "<tr>";
		for ($x = 0; $x < 5; $x++) {
			echo "<td>" . $row[$x] . "</td>"; 
			if($x == 3){
				array_push($names, $row[$x]);
			}
		}
		 echo "</tr>";  
}  


//print_r($names) . "<br>";
?>

	</tbody>
</table></div>

</div>

<table class="tablesorter"  style="width:100%;">
	<thead>
		<tr>
<?php 

foreach( $names as $item ){	
			echo "<th>" . $item . "</th>"; 
}

	echo "</tr><br/><thead>";
	
$tsql = "SELECT * FROM " . $viewMe . "";

$stmt = sqlsrv_query( $dbCon, $tsql);  

if ( $stmt )  
{  
     echo "Statement executed.<br>\n";  
}   
else   
{  
     echo "Error in statement execution.\n";  
     die( print_r( sqlsrv_errors(), true));  
}  
	echo "<tbody>";
while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC))  {  
	echo "<tr>";
		foreach ($row as $value) {
			echo "<td>" . $value . "</td>"; 
		}
		 echo "</tr>";  
}  

/* Free statement and connection resources. */  
sqlsrv_free_stmt( $stmt);  
sqlsrv_close( $conn);  
?>
	</tbody>
</table>