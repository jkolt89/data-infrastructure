<script src="js/celllog_cond.js"></script>
<?php

require_once('dbcon/connect_SP_RND.php'); 
//echo "<p>Connection Made.</p>";

if ($_REQUEST["dd"] != null){
	$query = $_REQUEST["dd"];
};
echo $query . "<br/>";

$stmt = sqlsrv_query($dbCon, $query);

if($stmt === false) {
    die(print_r(sqlsrv_errors(), true));
}
sqlsrv_free_stmt($stmt);


//$tsql = "SELECT MAX(clid) FROM ProdConductivity";
$tsql = "SELECT MAX(CLID) FROM cell_log";
$stmt = sqlsrv_query( $dbCon, $tsql);  
$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_BOTH);
echo "<p>Last Index: " . $row[0] . "</p>";

sqlsrv_free_stmt($stmt);

$clid = 0;

if($_REQUEST["clid"] != null){
	$clid = $_REQUEST["clid"];
	//echo $clid . "<br/>";
}


$tsql = "SELECT * FROM cell_log where clid=$clid";
$stmt = sqlsrv_query( $dbCon, $tsql);  
//echo $stmt;
if ( $stmt )  
{  
    // echo "Statement executed.<br>\n";  
}   
else   
{  
     echo "Error in statement execution.\n";  
     die( print_r( sqlsrv_errors(), true));  
}  
//echo "query complete.";
$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_BOTH);
//echo $row['MaterialID'];

?>


<form id="template" method="post" action="index.php?f=celllog_build">
<input name="clid" size="6" value="<?php echo $clid ?>" type="text" />
</form>

<table id="page"><tr><td>
<form id="build_log" method="post" action="index.php?f=celllog"> 
<table align="center" border="0">
<tbody>
<tr>
<td align="right"><mark><b>Cell Name:</b></mark></td>
<td align="left"><input name="cellname" size="25" value="<?php echo $row['cellname']; ?>" type="text"  onchange="build_celllog()" /></td>
</tr>
<tr>
<td align="right">Purpose/Description:</td>
<td align="left"><input name="purposedescription" size="25" value="<?php echo $row['purposedescription']; ?>" type="text" onchange="build_celllog()" /></td>
<td align="right">Cathode:</td>
<td align="left"><input name="cathode" size="5"  value="<?php echo $row['cathode']; ?>" type="text" onchange="build_celllog()" /></td>
<td align="right">Temperature:</td>
<td align="left"><input name="temperature" size="5" value="<?php echo $row['temperature']; ?>" type="text" onchange="build_celllog()" />&deg;C</td>
</tr>
<tr>
<td align="right">Max. Voltage:</td>
<td align="left"><input name="maxvoltage" size="5" value="<?php echo $row['maxvoltage']; ?>" type="text" onchange="build_celllog()" /> v</td>
<td align="right">Die:</td>
<td align="left"><input name="die" size="5" value="<?php echo $row['die']; ?>" type="text" onchange="build_celllog()" /></td>
<td align="right">Current Collector:</td>
<td align="left"><input name="currentcollector" size="5" value="<?php echo $row['currentcollector']; ?>" type="text" onchange="build_celllog()" /></td>
</tr>
<tr>
<tr>
<td align="right">Active Cathode:</td>
<td align="left"><input name="activecathode" size="25" value="<?php echo $row['activecathode']; ?>" type="text" onchange="build_celllog()" /></td>
<td align="right">Active Cathode Date Coated:</td>
<td align="left"><input name="activecathodedatecoated" size="25" value="<?php echo $row['activecathodedatecoated']; ?>" type="text" onchange="build_celllog()" /></td>
</tr>

<tr>
<td align="right">Electrolyte:</td>
<td align="left"><input name="electrolyte" size="25" value="<?php echo $row['electrolyte']; ?>" type="text" onchange="build_celllog()" /></td>
</tr>

<tr>
<td align="right"><mark><b>Total Cathode Mass:</b></mark></td>
<td align="left"><input name="totalcathodemass" size="25" value="<?php echo $row['totalcathodemass']; ?>" type="text" onchange="build_celllog()" /> g</td>
<td align="right"><mark><b>Die Diameter:</b></mark></td>
<td align="left"><input name="diediameter" size="25" value="<?php echo $row['diediameter']; ?>" type="text" onchange="build_celllog()" /> cm</td>
</tr>

<tr>
<td align="right">Cathode Particle Coating:</td>
<td align="left"><input name="cathodeparticlecoating" size="25" value="<?php echo $row['cathodeparticlecoating']; ?>" type="text" onchange="build_celllog()" /></td>
<td align="right"><mark><b>Weight Coating Percent:</b><mark></td>
<td align="left"><input name="weightcoatingpercent" size="25" value="<?php echo $row['weightcoatingpercent']; ?>" type="text" onchange="build_celllog()" /></td>
</tr>

<tr>
<td align="right"><mark><b>Nominal Active Material Percent:</b><mark></td>
<td align="left"><input name="nominalactivematerialpercent" size="25" value="<?php echo $row['nominalactivematerialpercent']; ?>" type="text" onchange="build_celllog()" /></td>
<td align="right">Ratio Active Percent:</td>
<td align="left"><input name="ratioactivepercent" size="25" value="<?php echo $row['ratioactivepercent']; ?>" type="text" onchange="build_celllog()" /></td>
</tr>

<tr>
<td align="right">Active Material Theoretical Capacity (mAh/g):</td>
<td align="left"><input name="ActiveMaterialTheoreticalCapacity_mAhg" size="25" value="<?php echo $row['ActiveMaterialTheoreticalCapacity_mAhg']; ?>" type="text" onchange="build_celllog()" /></td>
<td align="right">Cell Theoretical Capacity (mAh/g):</td>
<td align="left"><input name="CellTheoreticalCapacity_mAhg" size="25" value="<?php echo $row['CellTheoreticalCapacity_mAhg']; ?>" type="text" onchange="build_celllog()" /></td>
</tr>

<tr>
<td align="right">Loading (mg/cm^2):</td>
<td align="left"><input name="Loading_mg_cm2" size="25" value="<?php echo $row['Loading_mg_cm2']; ?>" type="text" onchange="build_celllog()" /></td>
<td align="right">Capacity (mAh):</td>
<td align="left"><input name="Capacity_mAh" size="25" value="<?php echo $row['Capacity_mAh']; ?>" type="text" onchange="build_celllog()" /></td>
</tr>

<tr>
<td align="right">Anode:</td>
<td align="left"><input name="Anode" size="25" value="<?php echo $row['Anode']; ?>" type="text" onchange="build_celllog()" /></td>
<td align="right">Anode Mass (g):</td>
<td align="left"><input name="AnodeMass_g" size="25" value="<?php echo $row['AnodeMass_g']; ?>" type="text" onchange="build_celllog()" /></td>
</tr>

<tr>
<td align="right">Separator:</td>
<td align="left"><input name="Separator" size="25" value="<?php echo $row['Separator']; ?>" type="text" onchange="build_celllog()" /></td>
<td align="right">Separator Mass (g):</td>
<td align="left"><input name="SeparatorMass_g" size="25" value="<?php echo $row['SeparatorMass_g']; ?>" type="text" onchange="build_celllog()" /></td>
</tr>

<tr>
<td align="right">Maximum Stack Pressure (tons):</td>
<td align="left"><input name="MaximumStackPressure_ton" size="25" value="<?php echo $row['MaximumStackPressure_ton']; ?>" type="text" onchange="build_celllog()" /></td>
<td align="right">Cathode Pressure (tons):</td>
<td align="left"><input name="CathodePressure_ton" size="25" value="<?php echo $row['CathodePressure_ton']; ?>" type="text" onchange="build_celllog()" /></td>
</tr>

<tr>
<td align="right">Cage torque (in-lbs):</td>
<td align="left"><input name="CageTorque_lbs" size="25" value="<?php echo $row['CageTorque_lbs']; ?>" type="text" onchange="build_celllog()" /></td>
<td align="right">Operator:</td>
<td align="left"><input name="Operator" size="25" value="<?php echo $row['Operator']; ?>" type="text" onchange="build_celllog()" /></td>
</tr>

<tr>
<td align="right">Location:</td>
<td align="left"><input name="Location" size="25" value="<?php echo $row['Location']; ?>" type="text" onchange="build_celllog()" /></td>
<td align="right"># Of Cycles Completed:</td>
<td align="left"><input name="NrCyclesCompleted" size="25" value="<?php echo $row['NrCyclesCompleted']; ?>" type="text" onchange="build_celllog()" /></td>
</tr>

<tr>
<td align="right">1st Cycle CE (%):</td>
<td align="left"><input name="FirstCycleCEPercent" size="25" value="<?php echo $row['FirstCycleCEPercent']; ?>" type="text" onchange="build_celllog()" /></td>
<td align="right">1st Cycle Charge:</td>
<td align="left"><input name="FirstCycleCharge" size="25" value="<?php echo $row['FirstCycleCharge']; ?>" type="text" onchange="build_celllog()" /></td>
</tr>

<tr>
<td align="right">1st Cycle Discharge:</td>
<td align="left"><input name="FirstCycleDischarge" size="25" value="<?php echo $row['FirstCycleDischarge']; ?>" type="text" onchange="build_celllog()" /></td>
<td align="right">Initial Resistance (ohms):</td>
<td align="left"><input name="InitialResistance_ohm" size="25" value="<?php echo $row['InitialResistance_ohm']; ?>" type="text" onchange="build_celllog()" /></td>
</tr>

<tr>
<td align="right">Max Voltage w/ Outlier:</td>
<td align="left"><input name="MaxVoltageWithOutlier" size="25" value="<?php echo $row['MaxVoltageWithOutlier']; ?>" type="text" onchange="build_celllog()" /></td>
<td align="right">Max Voltage w/o Outlier:</td>
<td align="left"><input name="MaxVoltageNoOutlier" size="25" value="<?php echo $row['MaxVoltageNoOutlier']; ?>" type="text" onchange="build_celllog()" /></td>
</tr>

<tr>
<td align="right">Maximum Discharge Capacity:</td>
<td align="left"><input name="MaxDischargeCapacity" size="25" value="<?php echo $row['MaxDischargeCapacity']; ?>" type="text" onchange="build_celllog()" /></td>
<td align="right">Excel Time:</td>
<td align="left"><input name="ExcelTime" size="25" value="<?php echo $row['ExcelTime']; ?>" type="text" onchange="build_celllog()" /></td>
</tr>

<tr>
<td align="right">Python Time:</td>
<td align="left"><input name="PythonTime" size="25" value="<?php echo $row['PythonTime']; ?>" type="text" onchange="build_celllog()" /></td>
<td align="right">Last DateTime:</td>
<td align="left"><input name="LastDateTime" size="25" value="<?php echo $row['LastDateTime']; ?>" type="text" onchange="build_celllog()" /></td>
</tr>

<tr>
<td align="right">Channel:</td>
<td align="left"><input name="Channel" size="25" value="<?php echo $row['Channel']; ?>" type="text" onchange="build_celllog()" /></td>
<td align="right">Arbin Notes:</td>
<td align="left"><input name="ArbinNotes" size="25" value="<?php echo $row['ArbinNotes']; ?>" type="text" onchange="build_celllog()" /></td>
</tr>

<tr>
<td align="right">Reason for Stoppage:</td>
<td align="left"><input name="StoppageReason" size="25" value="<?php echo $row['StoppageReason']; ?>" type="text" onchange="build_celllog()" /></td>
<td align="right">Cycle # of Stoppage:</td>
<td align="left"><input name="StoppageCycleNr" size="25" value="<?php echo $row['StoppageCycleNr']; ?>" type="text" onchange="build_celllog()" /></td>
</tr>

<tr>
<td align="right">Notes:</td>
<td align="left"><input name="Notes" size="50" value="<?php echo $row['Notes']; ?>" type="text" onchange="build_celllog()" /></td>
</tr>

<tr>
<td colspan="6" align="left"><p id="datadump"></p>
<input name="dd" type="hidden" />
</td>
</tr>
<tr>
<td colspan="6" align="right"><input value="Submit" onclick="submit_celllog()" type="submit" /></td>
</tr>
</tbody>
</table>
</form>
</td></tr></table>