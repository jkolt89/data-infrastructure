function calcCond() {

var input = document.getElementById("builder");
// var query = "exec dbo.Cell_Log_Insert '" + input.cellname.value + "'";

///////////////////////////////////////////////////////////////////////////////////////////
//var query = "exec dbo.Cell_Log_Insert 'deleteme'";
var query = "insert into dbo.cell_log (CellName) values ('deleteme')";
///////////////////////////////////////////////////////////////////////////////////////////

/*
var query = "INSERT INTO [dbo].[cell_log]([CellName],[PurposeDescription],[Cathode],[temperature],[MaxVoltage],[die],[CurrentCollector],[ActiveCathode]"
		   + ",[ActiveCathodeDateCoated],[electrolyte],[TotalCathodeMass_g],[DieDiameter_cm],[CathodeParticleCoating],[WeightCoatingPercent],[NominalPercentActiveMaterial],[RatioActiveMaterial]"
		   +",[ActiveMaterialTheoreticalCapacity_mAhg],[CellTheoreticalCapacity_mAhg],[Loading_mg_cm2],[Capacity_mAh],[Anode],[AnodeMass_g]"
           +",[Separator],[SeparatorMass_g],[MaximumStackPressure_ton],[CathodePressure_ton],[CageTorque_lbs],[Operator],[Location],[NrCyclesCompleted],[FirstCycleCEPercent]"
           +",[FirstCycleCharge],[FirstCycleDischarge],[InitialResistance_ohm],[MaxVoltageWithOutlier],[MaxVoltageNoOutlier],[MaxDischargeCapacity],[ExcelTime]"
           +",[PythonTime],[LastDateTime],[Channel],[ArbinNotes],[StoppageReason],[StoppageCycleNr],[Notes])"
     	+"VALUES( "
*/

/*
var query = "INSERT INTO ProdConductivity (MaterialID, Operator, LotNr, TestCondition, TestTemperature, MaterialMass_g, "
			+ "PlungerLengthNoPellet_mm, PelletPlungerThickness_mm, XAxisNyquist_ohms, Notes )"
			+ "VALUES( "
			+ "'" + input.MaterialID.value + "', "
			+ "'" + input.operator.value + "', "
			+ "'" + input.lotNum.value + "', "
			+ (input.ionicIntercept.value > 0? "'Ionic'": (input.electronicIntercept.value > 0? "'Electronic'": null)) + ", "
			+ input.temperature.value + ", "
			+ materialMass + ", "
			+ input.lengthTare.value + ", "
			+ input.lengthTotal.value + ", "	
			+ (input.ionicIntercept.value > 0? input.ionicIntercept.value: (input.electronicIntercept.value > 0? input.electronicIntercept.value: null)) + ", "
			+ (input.notes.value == ''? null: "'" + input.notes.value + "'")
			+ ")";
*/

document.getElementById("datadump").innerHTML = query;
input.dd.value = query;

return false;
}


// Submit form with name function.
function submit() {
	calcCond();
	var x = document.getElementsByName('builder');
	x[0].submit(); //form submission
}

function updateCond() {
var input = document.getElementById("updater");

var materialMass = input.mass.value;
var thickness = (input.lengthTotal.value - input.lengthTare.value)/10.0;
var ionicIntercept = input.ionicIntercept.value;
input.density.value = Math.round(1000*materialMass/(thickness*0.8*0.8*3.14159))/1000.0;
input.ionic.value = Math.round(1000*(thickness/(ionicIntercept*0.8*0.8*3.14159))*1000.0)/1000.0;
input.electronic.value = Math.round(1000*(thickness/(input.electronicIntercept.value*0.8*0.8*3.14159))*1e9)/1000.0;

var query = "UPDATE ProdConductivity SET UpdateTime=GetDate() WHERE pcid=" + input.pcid.value + ";"
			+ "INSERT INTO ProdConductivity (MaterialID, Operator, LotNr, TestCondition, TestTemperature, MaterialMass_g, "
			+ "PlungerLengthNoPellet_mm, PelletPlungerThickness_mm, XAxisNyquist_ohms, Notes)"
			+ "VALUES( "
			+ "'" + input.MaterialID.value + "', "
			+ "'" + input.operator.value + "', "
			+ "'" + input.lotNum.value + "', "
			+ (input.ionicIntercept.value > 0? "'Ionic'": (input.electronicIntercept.value > 0? "'Electronic'": null)) + ", "
			+ input.temperature.value + ", "
			+ materialMass + ", "
			+ input.lengthTare.value + ", "
			+ input.lengthTotal.value + ", "	
			+ (input.ionicIntercept.value > 0? input.ionicIntercept.value: (input.electronicIntercept.value > 0? input.electronicIntercept.value: null)) + ", "
			+ "'" + input.notes.value + "'"
			+ ")";
			
document.getElementById("datadump").innerHTML = query;
input.dd.value = query;
			
return false;
}
// update table .
function update() {
	updateCond();
	var x = document.getElementsByName('updater');
	
	x[0].submit(); //form submission
}