<script src="js/cond.js"></script>
<?php

$query = "|" . $_REQUEST["datadump"] . "|" . $_REQUEST["lotNum"] . "|";
if ($_REQUEST["datadump"] != null){
	$query = $_REQUEST["datadump"];
};
echo $query;
?>

<table id="page"><tr><td>
<form id="calcGR" method="post" action="index.php?f=dataEntry"> 
<table align="center" border="0">
<tbody>
<tr>
<td align="right">Lot #:</td>
<td colspan="4" align="left"><input name="lotNum" size="25" value="BD-2108-405-0" type="text" /></td>
<td align="right">Operator</td>
<td align="left"><input name="operator" size="5"  value="crb" type="text" /></td>
<td align="right">Temperature:</td>
<td align="left"><input name="temperature" size="5" value="25" onchange="calcCond()" type="text" />&deg;C</td>
</tr>
<tr>
<td align="right">Material Mass:</td>
<td align="left"><input name="mass" size="5" value="0.3" onchange="calcCond()" type="text" /> g</td>
<td align="right">Plunger Tare:</td>
<td align="left"><input name="lengthTare" size="5" value="40.0" onchange="calcCond()" type="text" /> mm</td>
<td align="right">Full Length:</td>
<td align="left"><input name="lengthTotal" size="5" value="41.0" onchange="calcCond()" type="text" /> mm</td>
</tr>
<tr>
<td align="right">Ionic Intercept:</td>
<td id="ionic" align="left"><input name="ionicIntercept" size="5" onchange="calcCond()" type="text" /> Ohms</td>
<td align="right">Electronic Intercept:</td>
<td id="electronic" align="left"><input name="electronicIntercept" size="5" onchange="calcCond()" type="text" /> Ohms</td>
</tr>
<tr>
<td colspan="4" align="center"><input value="Calculate" onclick="calcCond()" type="button" /></td>
</tr>
<tr>
<td align="right">Ionic Conductivity:</td>
<td id="ionic" align="left"><input name="ionic" size="6" readonly="readonly" type="text" /> mS/cm</td>
<td align="right">Electronic Conductivity:</td>
<td id="electronic" align="left"><input name="electronic" size="6" readonly="readonly" type="text" /> nS/cm</td>
<td align="right">Material Density:</td>
<td align="left"><input name="density" size="6" readonly="readonly" type="text" /> g/cc</td>
</tr>
<tr>
<td align="right">Notes:</td>
<td colspan="6" align="left"><input name="notes" value="This is where we add notes" size="250" type="text" /></td>
</tr>
<tr>
<td align="right">Data to input:</td>
<td colspan="6" align="left"><input name="datadump" size="250" readonly="readonly" type="text" /></td>
</tr>
<tr>
<td colspan="6" align="right"><input value="Submit" onclick="calcCond()" type="button" /></td>
</tr>
</tbody>
</table>
</form>
</td></tr></table>